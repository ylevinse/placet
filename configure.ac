#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_INIT([placet], [1.0.4 (SWIG)], [andrea.latina@cern.ch])

EXTERNAL_CFLAGS="$CFLAGS"
EXTERNAL_CXXFLAGS="$CXXFLAGS"

DEFAULT_CFLAGS="-O2"
DEFAULT_CXXFLAGS="-std=c++11"

AC_CANONICAL_SYSTEM
AM_INIT_AUTOMAKE
AC_CONFIG_SRCDIR([src/placet.cc])
AM_CONFIG_HEADER([include/config.h])

if test "$prefix" = "NONE" ; then
  prefix="/usr/local"
fi

make_all=placet
make_interfaces=

USE_HTGEN=false
USE_OCTAVE=false
USE_PYTHON=false
USE_MPI=false


AC_ARG_ENABLE([htgen], [AC_HELP_STRING([--enable-htgen],[enable the use of HTGEN (default is no)])], [USE_HTGEN=true], [USE_HTGEN=false])
AC_ARG_ENABLE([octave], [AC_HELP_STRING([--enable-octave],[enable the creation of the Octave interface (default is no)])], [USE_OCTAVE=true], [USE_OCTAVE=false])
AC_ARG_ENABLE([python], [AC_HELP_STRING([--enable-python],[enable the creation of the Python interface (default is no)])], [USE_PYTHON=true], [USE_PYTHON=false])
AC_ARG_ENABLE([mpi], [AC_HELP_STRING([--enable-mpi],[enable the creation of the MPI module (default is no)])], [USE_MPI=true], [USE_MPI=false])

AC_ARG_WITH([gsldir], [AC_HELP_STRING([--with-gsldir=DIR],[GSL root directory (such that '$DIR/bin/gsl-config' exists)])], [GSL_CONFIG="$with_gsldir/bin/gsl-config"])
AC_ARG_WITH([gslconfig], [AC_HELP_STRING([--with-gslconfig=EXEC],[use '$EXEC' as 'gsl-config' executable])], [GSL_CONFIG="$with_gslconfig"])
AC_ARG_WITH([htgendir], [AC_HELP_STRING([--with-htgendir=DIR],[HTGEN root directory])], [HTGEN_DIR="$with_htgendir"])
# AC_ARG_WITH([clhepdir], [AC_HELP_STRING([--with-clhepdir=DIR],[CLHEP root directory])], [CLHEP_BASE_DIR="$with_clhepdir"])
AC_ARG_WITH([octdir], [AC_HELP_STRING([--with-octdir=DIR],[Octave root directory (such that '$DIR/bin/octave-config' exists)])], [OCTAVE_CONFIG="$with_octdir/bin/octave-config"; MKOCTFILE="$with_octdir/bin/mkoctfile"])
AC_ARG_WITH([octave], [AC_HELP_STRING([--with-octave=EXEC],[use '$EXEC' as 'octave' executable])], [OCTAVE_BIN="$with_octave"])
AC_ARG_WITH([octconfig], [AC_HELP_STRING([--with-octconfig=EXEC],[use '$EXEC' as 'octave-config' executable])], [OCTAVE_CONFIG="$with_octconfig"])
AC_ARG_WITH([mkoctfile], [AC_HELP_STRING([--with-mkoctfile=EXEC],[use '$EXEC' as 'mkoctfile' executable])], [MKOCTFILE="$with_mkoctfile"])
AC_ARG_WITH([pydir], [AC_HELP_STRING([--with-pydir=DIR],[Python root directory (such that '$DIR/bin/python' exists)])], [PYTHON_PREFIX="$with_pydir"; USE_PYTHON=true])
AC_ARG_WITH([python], [AC_HELP_STRING([--with-python=EXEC],[use '$EXEC' as 'python' executable])], [PYTHON_BIN="$with_python"; USE_PYTHON=true])

AC_ARG_WITH([tcldir], [AC_HELP_STRING([--with-tcldir=DIR],[Tcl include directory])], [tcl_includedir="$with_tcldir"])
AC_ARG_WITH([tkdir], [AC_HELP_STRING([--with-tkdir=DIR],[Tk include directory])], [tk_includedir="$with_tkdir"])

#
# Checks for programs.
#

AC_PROG_CC
AC_PROG_CXX
AC_PROG_MAKE_SET

CFLAGS="$DEFAULT_CFLAGS"
CXXFLAGS="$DEFAULT_CXXFLAGS"

#
# Check for extra options..
#
m4_include([m4/ax_mpi.m4])
AC_LANG_PUSH([C++])
AX_MPI([HAVE_MPI=1])
AC_LANG_POP

AC_OPENMP
CFLAGS="$CFLAGS $OPENMP_CFLAGS"
LDFLAGS="$LDFLAGS $OPENMP_CFLAGS"
CXXFLAGS="$CXXFLAGS $OPENMP_CXXFLAGS"

#
# setting up hardware dependent options
#

AC_MSG_CHECKING([Checking C Compiler Vendor])
# Check if Intel compiler:
AC_COMPILE_IFELSE( [AC_LANG_SOURCE([
    #ifndef __INTEL_COMPILER
    error if not ICC
    #endif
  ])],
  [
   C_VENDOR=Intel
  ]
)
# Check if GNU compiler:
AC_COMPILE_IFELSE( [AC_LANG_SOURCE([
    #if (!defined __GNUC__ || __GNUC__ < 4 || __INTEL_COMPILER)
    error if not GCC4 or higher
    #endif
  ])],
  [
   C_VENDOR=GNU
  ]
)
AC_MSG_RESULT([$C_VENDOR])


CPPFLAGS="$CPPFLAGS -DUSE_NON_CONST"
CFLAGS="$CFLAGS -pipe -finline"
if test "$C_VENDOR" == "GNU"; then
    CFLAGS="$CFLAGS -ffast-math"
fi
CFLAGS="$CFLAGS -Wall -Wno-sign-compare -Wundef -Wno-uninitialized -Wno-strict-aliasing -Wno-parentheses -Wno-write-strings -Wno-format -Wno-unknown-pragmas"
LDFLAGS="$LDFLAGS -ldl"

case "$target" in
i[[3456]]86-*-linux-*)
  CFLAGS="$CFLAGS -fPIC"
  CPPFLAGS="$CPPFLAGS -malign-double"
;;
x86_64-*-linux-*)
  CFLAGS="$CFLAGS -fPIC"
;;
*-apple-darwin*)
  CFLAGS="$CFLAGS"
  ;;
esac



# Checks for libraries.
# FIXME: Replace `main' with a function in `-lfftw':
#AC_CHECK_LIB([fftw], [fftw_malloc])
# FIXME: Replace `main' with a function in `-lm':
AC_CHECK_LIB([m], [sqrt])
#AC_CHECK_LIB([ncurses], [initscr])
#AC_CHECK_LIB([readline], [readline])

AC_CHECK_LIB([m], [sincos], [sincos_exists=1], [sincos_exists=0] )
AC_CHECK_FUNC([strnstr], [strnstr_exists=1], [strnstr_exists=0] )

# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([arpa/inet.h fcntl.h netdb.h netinet/in.h stdlib.h string.h strings.h sys/socket.h sys/time.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_HEADER_STDBOOL
AC_C_CONST
AC_C_INLINE
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_HEADER_TIME

# Checks for library functions.
AC_FUNC_ALLOCA
AC_FUNC_ERROR_AT_LINE
AC_REPLACE_FNMATCH
AC_FUNC_FORK
AC_FUNC_MALLOC
AC_FUNC_REALLOC
AC_FUNC_SELECT_ARGTYPES
AC_FUNC_STRTOD
AC_FUNC_VPRINTF
AC_CHECK_FUNCS([atexit getcwd dup2 floor gethostbyaddr gethostbyname inet_ntoa memset pow rint select socket sqrt strtol])

#
# searches for Tcl/Tk
#

AC_SEARCH_LIBS(Tcl_Init, [tcl tcl8.6 tcl8.5 tcl8.4 tcl8.3],[],AC_MSG_ERROR([unable to find tcl on your system]))
AC_SEARCH_LIBS(Tk_Init, [tk tk8.6 tk8.5 tk8.4 tk8.3])

TCL_CFLAGS=""
for dir in $tcl_includedir /usr/include /usr/local/include /opt/include /opt/local/include ; do
  TCL_CFLAGS=`find "$dir" -name tcl.h | grep -v tcl-private | head -n 1`
  if test -n "$TCL_CFLAGS" ; then
    TCL_CFLAGS="-I`dirname "$TCL_CFLAGS"`"
    break
  fi
done

if test -z "$TCL_CFLAGS"; then
  AC_MSG_ERROR(['tcl.h' not found. Tcl/Tk is required to compile and run placet.])
fi

CFLAGS="$CFLAGS $TCL_CFLAGS"

TK_CFLAGS=""
for dir in $tk_includedir /usr/include /usr/local/include /opt/include /opt/local/include ; do
  TK_CFLAGS=`find "$dir" -name tk.h | grep -v tk-private | head -n 1`
  if test -n "$TK_CFLAGS" ; then
    TK_CFLAGS="-I`dirname "$TK_CFLAGS"`"
    break
  fi
done

if test -z "$TK_CFLAGS"; then
  AC_MSG_ERROR(['tk.h' not found. Tcl/Tk is required to compile and run placet.])
fi

if test "$TCL_CFLAGS" != "$TK_CFLAGS"; then
  CFLAGS="$CFLAGS $TK_CFLAGS"
fi


#
# checks for GSL
#

test -z $GSL_CONFIG && AC_CHECK_PROG(GSL_CONFIG, gsl-config, gsl-config)

if test -z $GSL_CONFIG; then
  AC_MSG_ERROR(['gsl-config' not found. GNU Scientific Library (GSL) is required to compile and run placet. Check out http://www.gnu.org/software/gsl
  If you know that GSL is properly installed, you can set the environment variable 'GSL_CONFIG' to point to 'gsl-config'. ])
fi


#
# checks for Octave
#

OCTAVE_INTERFACE=0
if test "$USE_OCTAVE" = "true" ; then

  test -z $OCTAVE_CONFIG && AC_CHECK_PROGS(OCTAVE_CONFIG, octave-config)

  if test -z $OCTAVE_CONFIG ; then
    AC_MSG_ERROR([Octave not found. Octave is required to compile and run placet. Check out http://www.octave.org
    If you know that Octave is installed, you can set the environment variable 'OCTAVE_CONFIG' to point to 'octave-config'.])
  fi

  OCTAVE_BINDIR=`$OCTAVE_CONFIG --print BINDIR 2>&1`

  test -z $MKOCTFILE && AC_CHECK_PROGS(MKOCTFILE, mkoctfile)

  if test -z $MKOCTFILE ; then
    AC_MSG_ERROR([mkoctfile not found. Octave is required to compile and run placet. Check out http://www.octave.org
    If you know that Octave is correctly installed, you can set the environment variable 'MKOCTFILE' to 'mkoctfile'])
  fi

  OCTAVE_CFLAGS="`$MKOCTFILE --print CFLAGS` `$MKOCTFILE --print INCFLAGS`"
  OCTAVE_LDFLAGS="`$MKOCTFILE --print LDFLAGS` `$MKOCTFILE --print LIBS`"
  OCTAVE_INCLUDEDIR="`$OCTAVE_CONFIG --print OCTINCLUDEDIR`"

  test -z $OCTAVE_BIN && AC_CHECK_PROGS(OCTAVE_BIN, octave-cli)

  make_interfaces="$make_interfaces octave-interface"

  OCTAVE_INTERFACE=1

fi

#
# checks for Python
#

PYTHON_INTERFACE=0
if test "$USE_PYTHON" = "true" ; then

  if test -n "$PYTHON_BIN" -a ! -x "$PYTHON_BIN" ; then
    _python_names="$PYTHON_BIN"
  else
    _python_names="python3.8 python3.7 python3.6 python3 python"
  fi

  for _name in $_python_names
  do
      _python_config_names="$_python_config_names ${_name}-config"
  done

  if test -d "$PYTHON_PREFIX" ; then
    PYTHON_LIBDIR="-L$PYTHON_PREFIX/lib/" 
    PYTHON_BINPATH="$PYTHON_PREFIX/bin/"

    test ! -x "$PYTHON_BIN" && AC_PATH_PROGS(PYTHON_BIN, $_python_names,, path = $PYTHON_BINPATH)
    test -x "${PYTHON_BIN}-config" && PYTHON_CONFIG=${PYTHON_BIN}-config
    test -z "$PYTHON_CONFIG" && AC_PATH_PROGS(PYTHON_CONFIG, $_python_config_names,, path = $PYTHON_BINPATH)
  else
    test ! -x "$PYTHON_BIN" && AC_PATH_PROGS(PYTHON_BIN, $_python_names)
    test -x "${PYTHON_BIN}-config" && PYTHON_CONFIG=${PYTHON_BIN}-config
    test -z "$PYTHON_CONFIG" && AC_PATH_PROGS(PYTHON_CONFIG, ${_python_config_names})
  fi

  if test -z $PYTHON_CONFIG ; then
    AC_MSG_ERROR([Python not found. Python is required to compile the Placet's Python interface. Check out http://www.python.org
    If you know that Python is installed, use the input argument --with-python=EXEC.])
  fi
  
  make_interfaces="$make_interfaces python-interface"

  PYTHON_INTERFACE=1

fi

#
# checks for HTGEN and HTGEN
#

 if test "$USE_HTGEN" = "true" ; then

# CLHEP is no longer necessary
#
#  if test -z $CLHEP_BASE_DIR ; then
#    AC_MSG_ERROR([\`CLHEP_BASE_DIR' not defined. Set up properly \`CLHEP_BASE_DIR' to compile and run placet-htgen. Check out http://cern.ch/clhep])
#  fi
#
  if test -z $HTGEN_DIR ; then
    AC_MSG_ERROR(['HTGEN_DIR' not defined. Set up properly 'HTGEN_DIR' to compile and run placet-htgen. Check out http://hbu.home.cern.ch/hbu/HTGEN.html])
  fi

  make_all="$make_all placet-htgen"

fi


#
# setting up compiler options for GSL and Octave
#

GSL_CFLAGS="`$GSL_CONFIG --cflags`"
GSL_LDFLAGS="`$GSL_CONFIG --libs`"


#
# setting up options for using HTGEN
#

if test "$USE_HTGEN" = "true" ; then

# CLHEP is no longer necessary
#
#  HTGEN_CFLAGS="-I$CLHEP_BASE_DIR/include -I$HTGEN_DIR/include -DHTGEN"
#  HTGEN_LDFLAGS="-L$HTGEN_DIR/lib -L$CLHEP_BASE_DIR/lib -lCLHEP -lhtplacet -lhtgen"
 if test -d $HTGEN_DIR/include/htgen ; then
  HTGEN_CFLAGS="-I$HTGEN_DIR/include/htgen -DHTGEN"
 else
  HTGEN_CFLAGS="-I$HTGEN_DIR/include -DHTGEN"
 fi
  HTGEN_LDFLAGS="-L$HTGEN_DIR/lib -lhtplacet -Wl,-rpath,$HTGEN_DIR/lib"

fi

#
# last operations before substitutions
#

if test -n "$EXTERNAL_CFLAGS" ; then
  CFLAGS="$CFLAGS $EXTERNAL_CFLAGS"
fi

if test -n "$EXTERNAL_CXXFLAGS" ; then
  CXXFLAGS="$CXXFLAGS $EXTERNAL_CXXFLAGS"
fi

# we want the "most advanced"
# placet which is built,
# should be the last of make_all...
for trgt in $make_all
do
    PLACET_BIN=$prefix/bin/$trgt
done

make_all="$make_all utils"

target="$make_all"

test -z "$make_interfaces" || target="$target interfaces"
test -z "$make_interfaces" || placet_deps="interfaces"

#
# Check for MPI options..
#
if test "$USE_MPI" = "true" ; then
  if test "$HAVE_MPI" != "1" ; then
    AC_MSG_ERROR([The option '--enable-mpi' requires you have MPI installed.])
  fi
  placet_deps="$placet_deps mpi"
  MPI_MODULE=1
else
  MPI_MODULE=0
fi

AC_SUBST(target)
AC_SUBST(make_all)
AC_SUBST(make_interfaces)
AC_SUBST(placet_deps)

AC_SUBST(MPICXX)
AC_SUBST(MPI_MODULE)

AC_SUBST(GSL_CFLAGS)
AC_SUBST(GSL_LDFLAGS)

AC_SUBST(HTGEN_CFLAGS)
AC_SUBST(HTGEN_LDFLAGS)

AC_SUBST(MKOCTFILE)
AC_SUBST(OCTAVE_BIN)
AC_SUBST(OCTAVE_CFLAGS)
AC_SUBST(OCTAVE_LDFLAGS)
AC_SUBST(OCTAVE_INCLUDEDIR)
AC_SUBST(OCTAVE_INTERFACE)

AC_SUBST(PYTHON_BIN)
AC_SUBST(PYTHON_CONFIG)
AC_SUBST(PYTHON_INTERFACE)
AC_SUBST(PYTHON_LIBDIR)

AC_SUBST(PLACET_BIN)
AC_SUBST(PWD)

# The test system needs to find ctest:
AC_PATH_PROGS(CTEST_PROGRAM, ctest28 ctest,ctest)
AC_SUBST(CTEST_PROGRAM)

# the test system to know which modules are active:
AC_SUBST(USE_MPI)
AC_SUBST(USE_HTGEN)
AC_SUBST(USE_OCTAVE)
AC_SUBST(USE_PYTHON)

AC_SUBST(sincos_exists)
AC_SUBST(strnstr_exists)

AC_CONFIG_FILES([Makefile src/Makefile.common include/defaults.h testing/CTestTestfile.cmake])
AC_CONFIG_FILES([doc/placet-doc], [chmod +x doc/placet-doc])
AC_CONFIG_FILES([doc/placet-pydoc], [chmod +x doc/placet-pydoc])

AC_OUTPUT
