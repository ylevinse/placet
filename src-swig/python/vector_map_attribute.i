#if defined(SWIGPYTHON)
%typemap(in, numinputs=0) std::vector<std::map<std::string,Attribute> > & (std::vector<std::map<std::string,Attribute> > temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<std::map<std::string,Attribute> > & {
  if (PyObject *res = PyList_New($1->size())) {
    for (size_t i=0; i<$1->size(); i++) {
      if (PyObject *dict = PyDict_New()) {
	const std::map<std::string,Attribute> &attributes = (*$1)[i];
	for (std::map<std::string,Attribute>::const_iterator itr=attributes.begin(); itr!=attributes.end(); ++itr) {
	  const std::string &key = (*itr).first;
	  const Attribute &attribute = (*itr).second;
	  PyObject *_key = PyString_FromString(key.c_str());
	  PyObject *_val;
	  switch(attribute.type()) {
	  case OPT_INT: _val = PyLong_FromLong(int(attribute)); break;
	  case OPT_UINT: _val = PyLong_FromUnsignedLong(size_t(attribute)); break;
	  case OPT_BOOL: _val = PyInt_FromLong(bool(attribute) ? 1: 0); break;
	  case OPT_DOUBLE: _val = PyFloat_FromDouble(double(attribute)); break;
	  case OPT_STRING: _val = PyString_FromString(std::string(attribute).c_str()); break;
	  case OPT_COMPLEX: _val = PyComplex_FromDoubles(attribute.get_complex().real(), attribute.get_complex().imag()); break;
	  }
	  PyDict_SetItem(dict, _key, _val);
	}
	PyList_SetItem(res, i, dict);
      }
    }
    $result = SWIG_Python_AppendOutput($result, res);
  }
}

#endif
