/* matrixnd.i */

%{
#include "matrixnd.hh"
%}

#if defined(SWIGPYTHON)

// NumPy Matrix -> C++ MatrixNd
%typemap(in) const MatrixNd & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 2);
    npy_intp strides[2] = { 0, 0 };
    int rows, cols;
    if (PyArray_NDIM(array) == 1) {
      rows = 1;
      cols = PyArray_DIMS(array)[0];
      strides[0] = 0;
      strides[1] = PyArray_STRIDES(array)[0];
    } else {
      rows = PyArray_DIMS(array)[0];
      cols = PyArray_DIMS(array)[1];
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
    }
    char *data = PyArray_BYTES(array);
    $1 = new MatrixNd(rows, cols);
    for (size_t i=0; i<rows; i++)
      for (size_t j=0; j<cols; j++)
        (*$1)[i][j] = *(double *)(data + i*strides[0] + j*strides[1]);
  } else {
    $1 = new MatrixNd(1, 1);
    (*$1)[0][0] = PyFloat_AsDouble($input);
  }
}

%typemap(freearg) const MatrixNd & {
  if ($1) delete $1;
}

%typemap(typecheck) const MatrixNd & {
  $1 = is_array($input) || PyFloat_Check($input) ? 1 : 0;
}

%typemap(argout,noblock=1) const MatrixNd & {
}

// C++ MatrixNd -> NumPy Matrix (as a return value)
%typemap(out) MatrixNd {
  npy_intp dimensions[2] = { $1.rows(), $1.columns() };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (size_t i=0; i<dimensions[0]; i++)
    for (size_t j=0; j<dimensions[1]; j++)
      *(double*)(data + i*strides[0] + j*strides[1]) = $1[i][j];
  $result = PyArray_Return(res);
}

// C++ MatrixNd -> NumPy Matrix (as an output argument)
%typemap(in, numinputs=0) MatrixNd & (MatrixNd temp ) {
  $1 = &temp;
}

%typemap(argout) MatrixNd & {
  npy_intp dimensions[2] = { $1->rows(), $1->columns() };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (size_t i=0; i<dimensions[0]; i++)
    for (size_t j=0; j<dimensions[1]; j++)
      *(double*)(data + i*strides[0] + j*strides[1]) = (*$1)[i][j];
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res));
}

%typemap(freearg,noblock=1) MatrixNd & {
}

#endif
