/* attribute.i */

%{
#include <vector>
#include <map>
#include "attribute.hh"
%}

#if defined(SWIGOCTAVE)

// Octave array or cell -> C++ Attribute
%typemap(in) const std::vector<Attribute> & {
  if ($input.is_string()) {
    if (($1 = new std::vector<Attribute>(1))) {
      std::vector<Attribute> &attributes = *$1;
      attributes[0] = Attribute($input.string_value());
    }
  } else if (($1 = new std::vector<Attribute>($input.length()))) {
    std::vector<Attribute> &attributes = *$1;
    if ($input.is_integer_type()) {
      const Array<int> &array = $input.int_vector_value();
      for (int i=0; i<array.length(); i++)
        attributes[i] = Attribute(int(array(i)));
    } else if ($input.is_bool_type()) {
      const boolMatrix &array = $input.bool_matrix_value();
      for (int i=0; i<array.length(); i++)
        attributes[i] = Attribute(array(i));
    } else if ($input.is_real_type()) {
      const Matrix &array = $input.matrix_value();
      for (int i=0; i<array.length(); i++)
        attributes[i] = Attribute(array(i));
    } else if ($input.is_complex_type()) {
      const ComplexMatrix &array = $input.complex_matrix_value();
      for (int i=0; i<array.length(); i++)
        attributes[i] = Attribute(array(i));
    } else if ($input.is_cell()) {
      const string_vector &array = $input.all_strings();
      for (int i=0; i<array.length(); i++)
        attributes[i] = Attribute(array(i));
    } else
      error("Unknown type in attributes list");
  }
}

%typemap(freearg) const std::vector<Attribute> & {
   if ($1) delete $1;
}

%typemap(typecheck) const std::vector<Attribute> & {
  octave_value obj = $input;
  $1 = obj.is_string() ||
    obj.is_integer_type() ||
    obj.is_bool_type() ||
    obj.is_real_type() ||
    obj.is_complex_type() ||
    obj.is_cell() ? 1 : 0;
}

%typemap(argout,noblock=1) const std::vector<Attribute> & {
}

// C++ Attribute -> Octave array (as an output argument)
%typemap(in, numinputs=0) std::vector<Attribute> & (std::vector<Attribute> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<Attribute> & {
  std::vector<Attribute> &attributes = *$1;
  if (attributes.size()>0) {
    dim_vector dv;
    dv.resize(2);
    dv.elem(0)=attributes.size();
    dv.elem(1)=1;
    option_type type = attributes[0].type();
    for (size_t i=1; i<attributes.size(); i++) {
      if (attributes[i].type()!=type) {
	if (attributes[i].type()==OPT_STRING || type==OPT_STRING) {
	  std::cerr << "error: you cannot mix numerical with string-based attributes";
	  exit(1);
	} else if (attributes[i].type()==OPT_COMPLEX || type==OPT_COMPLEX) {
	  type = OPT_COMPLEX;
	} else if (attributes[i].type()==OPT_DOUBLE || type==OPT_DOUBLE) {
	  type = OPT_DOUBLE;
	} else if (attributes[i].type()==OPT_INT || type==OPT_INT) {
	  type = OPT_INT;
	}
      }
    }
    switch(type) {
    case OPT_INT: { int32NDArray ret(dv); for (size_t i=0; i<attributes.size(); i++) ret(i) = int(attributes[i]); $result->append(ret); } break;
    case OPT_UINT: { uint32NDArray ret(dv); for (size_t i=0; i<attributes.size(); i++) ret(i) = size_t(attributes[i]); $result->append(ret); } break;
    case OPT_BOOL: { boolMatrix ret(dv); for (size_t i=0; i<attributes.size(); i++) ret(i) = bool(attributes[i]); $result->append(ret); } break;
    case OPT_DOUBLE: { Matrix ret(dv); for (size_t i=0; i<attributes.size(); i++) ret(i) = double(attributes[i]); $result->append(ret); } break;
    case OPT_STRING: { string_vector ret(attributes.size()); for (size_t i=0; i<attributes.size(); i++) ret(i) = std::string(attributes[i]); $result->append(ret); } break;
    case OPT_COMPLEX: {
      ComplexMatrix ret(dv);
      for (size_t i=0; i<attributes.size(); i++) {
	if (attributes[i].type() == OPT_COMPLEX)
	  ret(i) = attributes[i].get_complex();
	else if (attributes[i].type() == OPT_DOUBLE)
	  ret(i) = std::complex<double>(double(attributes[i]));
	else {
	  std::cerr << "error: promotion to complex only allowed for real scalars\n";
	  exit(1);
	}
      }
      $result->append(ret);
    } break;
    }
  } else  {
    $result->append(octave_value());
  }
}

%typemap(freearg,noblock=1) std::vector<Attribute> & {
}

#endif
