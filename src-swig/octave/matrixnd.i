/* matrixnd.i */

%{
#include "matrixnd.hh"
%}

#if defined(SWIGOCTAVE)

// Octave Matrix -> C++ MatrixNd
%typemap(in) const MatrixNd & {
  const Matrix &matrix = $input.matrix_value();
  $1 = new MatrixNd(matrix.rows(), matrix.columns());
  for (int i=0; i<matrix.rows(); i++)
    for (int j=0; j<matrix.columns(); j++)
      (*$1)[i][j] = matrix(i,j);
}

%typemap(freearg) const MatrixNd & {
  if ($1) delete $1;
}

%typemap(typecheck) const MatrixNd & {
  octave_value obj = $input;
  $1 = obj.is_real_scalar() || obj.is_real_matrix() ? 1 : 0;
}

%typemap(argout,noblock=1) const MatrixNd & {
}

// C++ MatrixNd -> Octave Matrix (as a return value)
%typemap(out) MatrixNd {
  Matrix ret($1.rows(), $1.columns());
  for (size_t i=0; i<$1.rows(); i++)
    for (size_t j=0; j<$1.columns(); j++)
      ret(i,j) = $1[i][j];
  $result = ret;
}

// C++ MatrixNd -> Octave Matrix (as an output argument)
%typemap(in, numinputs=0) MatrixNd & (MatrixNd temp ) {
  $1 = &temp;
}

%typemap(argout) MatrixNd & {
  MatrixNd &ref = *$1;
  Matrix ret(ref.rows(), ref.columns());
  for (size_t i=0; i<ref.rows(); i++)
    for (size_t j=0; j<ref.columns(); j++)
      ret(i,j) = ref[i][j];
  $result->append(ret);
 }

%typemap(freearg,noblock=1) MatrixNd & {
}

#endif
