#ifndef STOPWATCH_CLASS
#define STOPWATCH_CLASS

#include <sys/times.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>

#include <iostream>
#include <sstream>
#include <iomanip>

class Stopwatch {

  unsigned long elapsed;
  unsigned long elapsed_cpu;
  unsigned long elapsed_system;
	
  clock_t initial;
  clock_t final;
	
  tms initial_tms;
  tms final_tms;
	
  long clocks_per_sec;
	
public:
	
  Stopwatch() { clocks_per_sec = sysconf(_SC_CLK_TCK); }

  inline void reset()
  {
    elapsed = 0;
    elapsed_cpu = 0;
    elapsed_system = 0;
  } 

  inline void start()
  { 
    elapsed = 0;
    elapsed_cpu = 0;
    elapsed_system = 0;

    initial = times(&initial_tms); 
  }

  inline void resume()
  { 
    initial = times(&initial_tms); 
  }
	
  inline void stop()
  { 
    final = times(&final_tms); 
	
    elapsed += final - initial;
    elapsed_cpu += final_tms.tms_utime - initial_tms.tms_utime;
    elapsed_system += final_tms.tms_stime - initial_tms.tms_stime;
  }

  double cpu_seconds() const	{ return double(elapsed_cpu) / clocks_per_sec; }
  double real_seconds() const	{ return double(elapsed) / clocks_per_sec; }
  double system_seconds() const	{ return double(elapsed_system) / clocks_per_sec; }
	
  friend std::ostream &operator<<(std::ostream &stream, const Stopwatch &timer )
  {
    std::ostringstream buffer;
    buffer << std::setprecision(5);
    buffer << std::showpoint;
    buffer << "real\t" << timer.real_seconds() << "s\ncpu\t" << timer.cpu_seconds() << "s\nsystem\t" << timer.system_seconds() << "s";
    return stream << buffer.str();
  }

};

#endif /* stopwatch_hh */
