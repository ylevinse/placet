#ifndef sort_index_hh
#define sort_index_hh

#include <vector>
#include <algorithm>

namespace {
  template <class T>
  std::vector<int> sort_index(const T *data, const std::vector<int> &index_ )
  {
    bool need_sorting = false;
    for (int i=1; i<index_.size(); i++) {
      if (data[index_[i]]<data[index_[i-1]]) {
	need_sorting = true;
	break;
      }
    }
    std::vector<int> index = index_;
    if (need_sorting) {
      std::sort(index.begin(), index.end(), [&data] (const int &a, const int &b ) { return data[a] < data[b]; });
    }
    return index;
  }
  template <class T>
  std::vector<int> sort_index(const T *data, int nelems )
  {
    std::vector<int> index(nelems);
    for (int i=0; i<nelems; i++)
      index[i] = i;
    return sort_index(data, index);
  }
}

#endif /* sort_index_hh */
