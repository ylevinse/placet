#ifndef file_buffered_stream_hh
#define file_buffered_stream_hh

#include "file_stream.hh"
#include "fifo_stream.hh"

#define BUFFER_SIZE (1L<<18)

class File_Buffered_OStream : public OStream {

  File_OStream	stream;
  FIFO_Stream	buffer;
	
protected:

  bool writable() const { return stream && buffer.free() > 0; }

public:

  File_Buffered_OStream(const char *path, size_t size = BUFFER_SIZE ) : stream(path), buffer(size) {}
  File_Buffered_OStream(int fd, size_t size = BUFFER_SIZE ) : stream(fd), buffer(size) {}
  File_Buffered_OStream(size_t size = BUFFER_SIZE ) : buffer(size) {}
  ~File_Buffered_OStream() { stream << buffer; }
	
  void flush() { stream << buffer; buffer.clear(); }

  bool open(const char *path, int oflag = O_CREAT | O_WRONLY, int mode = 0644 ) { return stream.open(path, oflag, mode); }
  bool open(int fd ) { return stream.open(fd); }
  
  int fd() { return stream.fd(); }

  void close()
  {
    flush(); 
    stream.close();
  }

  // write methods
#define WRITE(TYPE)				\
  size_t write(const TYPE *ptr, size_t n )	\
  {						\
    size_t __t = 0;				\
    while(__t<n) {				\
      __t += buffer.write(ptr+__t,n-__t);	\
      if (__t < n) {				\
	flush();				\
      }						\
    }						\
    return __t;					\
  }						\
  size_t write(const TYPE &ref )		\
  {						\
    for (;;) {					\
      if (buffer.write(ref))			\
	break;					\
      flush();					\
    }						\
    return 1;					\
  }
  
  WRITE(char)
  WRITE(bool)
  WRITE(float)
  WRITE(double)
  WRITE(long double)
  WRITE(signed short)
  WRITE(signed int)
  WRITE(signed long int)
  WRITE(unsigned short)
  WRITE(unsigned int)
  WRITE(unsigned long int)
#undef WRITE

    };

class File_Buffered_IStream : public IStream {

  File_IStream	stream;
  FIFO_Stream	buffer;
		
protected:

  bool readable() const { return stream && buffer.size() > 0; }

public:

  File_Buffered_IStream(const char *path, size_t size = BUFFER_SIZE ) : stream(path), buffer(size) {}
  File_Buffered_IStream(int fd, size_t size = BUFFER_SIZE ) : stream(fd), buffer(size) {}
  File_Buffered_IStream(size_t size = BUFFER_SIZE ) : buffer(size) {}

  ~File_Buffered_IStream() {}

  bool open(const char *path, int oflag = O_RDONLY ) { buffer.clear(); return stream.open(path, oflag); }
  bool open(int fd ) { return stream.open(fd); }

  int fd() { return stream.fd(); }

  void close()
  {
    stream.close();
  }

  // read methods
#define READ(TYPE)				\
  size_t read(TYPE *ptr, size_t n )		\
  {						\
    size_t __t = 0;				\
    while(__t<n) {				\
      __t += buffer.read(ptr+__t,n-__t);	\
      if (__t < n) {				\
	(void)(stream >> buffer);		\
      }						\
    }						\
    return __t;					\
  }						\
  size_t read(TYPE &ref )			\
  {						\
    for (;;) {					\
      if (buffer.read(ref))			\
	break;					\
      (void)(stream >> buffer);			\
    }						\
    return 1;					\
  }

  READ(char)
  READ(bool)
  READ(float)
  READ(double)
  READ(long double)
  READ(signed short)
  READ(signed int)
  READ(signed long int)
  READ(unsigned short)
  READ(unsigned int)
  READ(unsigned long int)
#undef READ

    };

class File_Buffered_IOStream : public IOStream {

  File_IOStream	stream;
  FIFO_Stream	buffer;
	
protected:

  bool writable() const { return stream || buffer.free() > 0; }
  bool readable() const { return stream || buffer.size() > 0; }

public:

  File_Buffered_IOStream(const char *path, size_t size = BUFFER_SIZE ) : stream(path), buffer(size) {}
  File_Buffered_IOStream(int fd, size_t size = BUFFER_SIZE ) : stream(fd), buffer(size) {}
  File_Buffered_IOStream(size_t size = BUFFER_SIZE ) : buffer(size) {}
  ~File_Buffered_IOStream() { stream << buffer; }

  bool open(const char *path )	{ buffer.clear(); return stream.open(path); }
  bool open(int fd )		{ return stream.open(fd); }
	
  void close()
  {
    flush(); 
    stream.close();
  }

  // write methods
#define WRITE(TYPE)				\
  size_t write(const TYPE *ptr, size_t n )	\
  {						\
    size_t __t = 0;				\
    while(__t<n) {				\
      __t += buffer.write(ptr+__t,n-__t);	\
      if (__t < n) {				\
	flush();				\
      }						\
    }						\
    return __t;					\
  }						\
  size_t write(const TYPE &ref )		\
  {						\
    for (;;) {					\
      if (buffer.write(ref))			\
	break;					\
      flush();					\
    }						\
    return 1;					\
  }
  
  WRITE(char)
  WRITE(bool)
  WRITE(float)
  WRITE(double)
  WRITE(long double)
  WRITE(signed short)
  WRITE(signed int)
  WRITE(signed long int)
  WRITE(unsigned short)
  WRITE(unsigned int)
  WRITE(unsigned long int)
#undef WRITE

  // read methods
#define READ(TYPE)				\
    size_t read(TYPE *ptr, size_t n )		\
  {						\
    size_t __t = 0;				\
    while(__t<n) {				\
      __t += buffer.read(ptr+__t,n-__t);	\
      if (__t < n) {				\
	(void)(stream >> buffer);		\
      }						\
    }						\
    return __t;					\
  }						\
    size_t read(TYPE &ref )			\
  {						\
    for (;;) {					\
      if (buffer.read(ref))			\
	break;					\
      (void)(stream >> buffer);			\
    }						\
    return 1;					\
  }

  READ(char)
  READ(bool)
  READ(float)
  READ(double)
  READ(long double)
  READ(signed short)
  READ(signed int)
  READ(signed long int)
  READ(unsigned short)
  READ(unsigned int)
  READ(unsigned long int)
#undef READ

    };

#endif /* file_buffered_stream_hh */
