#ifndef fifo_stream_hh
#define fifo_stream_hh

#include <cstring>
#include "stream.hh"

class FIFO_Stream : public IOStream {

  char *buffer;
  size_t size_;
  size_t in_;
  size_t out_;

protected:

  bool writable() const { return buffer!=NULL && free()>0; }
  bool readable() const { return buffer!=NULL && size()>0; }
	
public:

  FIFO_Stream(const FIFO_Stream &fifo ) : size_(fifo.size_), in_(fifo.in_), out_(fifo.out_) { buffer=new char[size_]; memcpy(buffer,fifo.buffer,size_); }
  FIFO_Stream(size_t _s = 262144) : in_(0), out_(0)
  {	
    // size_ must be a power of 2
    if (_s&(_s-1)) {
      size_=1;	
      while(size_<_s)
	size_<<=1;
    } else size_=_s;
    buffer = new char[size_];
  }

  ~FIFO_Stream() { delete []buffer; }
	
  FIFO_Stream &operator=(const FIFO_Stream &fifo )
  {
    if (this!=&fifo) {
      if (fifo.size_!=size_) {
	delete []buffer;
	buffer=new char[size_=fifo.size_];
      }
      memcpy(buffer,fifo.buffer,size_);
      in_ = fifo.in_;
      out_ = fifo.out_;
    }
    return *this;
  }
  inline operator bool() const { return buffer!=NULL; }

  bool resize(size_t _s )
  {
    size_t _size_;
    if (_s&(_s-1)) {
      _size_ = 1;	
      while(_size_<_s)
	size_ <<= 1;
    } else {
      _size_=_s;
    }
    in_ = out_ = 0;
    if (size_!=_size_) {
      delete []buffer;
      buffer = new char[size_=_size_];
    }
    return buffer != NULL;
  }
	
  inline void clear() { in_ = out_ = 0; }

  inline char *c_ptr() { return buffer; }
  inline const char *c_ptr() const { return buffer; }

  inline void in(int i ) { in_ = i; }
	
  inline size_t in() const { return in_&(size_-1); }
  inline size_t out() const { return out_&(size_-1); }

  inline size_t free() const { return size_-in_+out_; }
  inline size_t size() const { return in_-out_; }
  inline size_t capacity() const { return size_; }
		
#define WRITE(TYPE)							\
  inline size_t write(const TYPE *ptr, size_t n )			\
  {									\
    size_t len=std::min(n,free()/sizeof(TYPE))*sizeof(TYPE);		\
    size_t l=std::min(len,size_-in());					\
    memcpy(buffer+in(),reinterpret_cast<const char*>(ptr),l);		\
    memcpy(buffer,reinterpret_cast<const char*>(ptr)+l,len-l);		\
    in_+=len;								\
    return len/sizeof(TYPE);						\
  }									\
  inline size_t write(const TYPE &ref )					\
  {									\
    if (free()<sizeof(TYPE)) return 0;					\
    size_t l=std::min(sizeof(TYPE),size_-in());				\
    memcpy(buffer+in(),reinterpret_cast<const char*>(&ref),l);		\
    memcpy(buffer,reinterpret_cast<const char*>(&ref)+l,sizeof(TYPE)-l); \
    in_+=sizeof(TYPE);							\
    return 1;								\
  }
  
  WRITE(char)
  WRITE(bool)
  WRITE(float)
  WRITE(double)
  WRITE(long double)
  WRITE(signed short)
  WRITE(signed int)
  WRITE(signed long int)
  WRITE(unsigned short)
  WRITE(unsigned int)
  WRITE(unsigned long int)
#undef WRITE

#define READ(TYPE)							\
  inline size_t read(TYPE *ptr, size_t n )				\
  {									\
    n=std::min(n,size()/sizeof(TYPE));					\
    if (n==0) return 0;							\
    size_t l=std::min(n*sizeof(TYPE),size_-out());			\
    memcpy(reinterpret_cast<char*>(ptr),buffer+out(),l);		\
    memcpy(reinterpret_cast<char*>(ptr)+l,buffer,n*sizeof(TYPE)-l);	\
    out_+=n*sizeof(TYPE);						\
    return n;								\
  }									\
  inline size_t read(TYPE &ref )					\
  {									\
    if (size()<sizeof(TYPE)) return 0;					\
    size_t l=std::min(sizeof(TYPE),size_-out());			\
    memcpy(reinterpret_cast<char*>(&ref),buffer+out(),l);		\
    memcpy(reinterpret_cast<char*>(&ref)+l,buffer,sizeof(TYPE)-l);	\
    out_+=sizeof(TYPE);							\
    return 1;								\
  }
  
  READ(char)
  READ(bool)
  READ(float)
  READ(double)
  READ(long double)
  READ(signed short)
  READ(signed int)
  READ(signed long int)
  READ(unsigned short)
  READ(unsigned int)
  READ(unsigned long int)
#undef READ

  friend IStream &operator>>(IStream &stream, FIFO_Stream &fifo )
  {
    size_t len = fifo.free();
    if (len>0) {
      size_t in = fifo.in();
      size_t l = std::min(len,fifo.size_-in);
      size_t l_ = stream.read(fifo.buffer+in,l);
      if (l_ == l) {
	l_ += stream.read(fifo.buffer,len-l);
      }
      fifo.in_ += l_;
    }
    return stream;
  }

  friend OStream &operator<<(OStream &stream, const FIFO_Stream &fifo )
  {
    size_t len = fifo.size();
    if (len>0) {
      size_t out = fifo.out();
      size_t l = std::min(len,fifo.size_-out);
      stream.write(fifo.buffer+out,l);
      stream.write(fifo.buffer,len-l);
    }
    return stream;
  }
  
};

#endif /* fifo_stream_hh */
