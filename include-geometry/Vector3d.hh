#ifndef VECTOR3D_HH
#define VECTOR3D_HH

/*

  Author: Andrea Latina

*/

#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cmath>

struct Vector3d {

  double x, y, z;
  
  Vector3d(double _x, double _y, double _z ) : x(_x), y(_y), z(_z) {}
  Vector3d() : x(0.0), y(0.0), z(0.0) {}
  
  ~Vector3d() {}
  
  double rho() const { return sqrt(x*x + y*y + z*z); }
  double theta() const
  {
    double _rho=rho();
    return (1.+_rho>1.) ? acos(z/_rho) : 0.;
  }
  
  double phi() const { return atan2(y, x); }
  
  double normalize();
  
  friend double abs(const Vector3d &v )	{ return v.rho(); }
  friend double norm(const Vector3d &v ) { return v.x*v.x + v.y*v.y + v.z*v.z; }
  
  const Vector3d &operator  = (const Vector3d &v ) { x = v.x; y = v.y; z = v.z; return *this; }
  const Vector3d &operator += (const Vector3d &v ) { x += v.x; y += v.y; z += v.z; return *this; }
  const Vector3d &operator -= (const Vector3d &v ) { x -= v.x; y -= v.y; z -= v.z; return *this; }
  const Vector3d &operator *= (double a ) { x *= a; y *= a; z *= a; return *this; }
  const Vector3d &operator /= (double a ) { x /= a; y /= a; z /= a; return *this; }
  const Vector3d &operator ^= (const Vector3d &v ) { return *this = *this ^ v; }
  
  friend bool operator == (const Vector3d &a, const Vector3d &b ) { return a.x == b.x && a.y == b.y && a.x == b.z; }
  friend bool operator != (const Vector3d &a, const Vector3d &b ) { return a.x != b.x || a.y != b.y || a.z != b.z; }
  
  friend Vector3d operator + (const Vector3d &a, const Vector3d &b ) { return Vector3d(a.x + b.x, a.y + b.y, a.z + b.z); }
  friend Vector3d operator - (const Vector3d &a, const Vector3d &b ) { return Vector3d(a.x - b.x, a.y - b.y, a.z - b.z); }
  friend Vector3d operator - (const Vector3d &a ) { return Vector3d(-a.x, -a.y, -a.z); }
  friend double operator * (const Vector3d &a, const Vector3d &b ) { return a.x * b.x + a.y * b.y + a.z * b.z; }
  friend Vector3d operator * (const Vector3d &a, double d ) { return Vector3d(a.x * d, a.y * d, a.z * d); }
  friend Vector3d operator * (double d, const Vector3d &a ) { return Vector3d(a.x * d, a.y * d, a.z * d); }
  friend Vector3d operator / (const Vector3d &a, double d ) { return Vector3d(a.x / d, a.y / d, a.z / d); }
  friend Vector3d operator ^ (const Vector3d &a, const Vector3d &b ) { return Vector3d(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x); }
  
  friend std::ostream &operator << (std::ostream &stream, const Vector3d &a )
  {
    double _x = fabs(a.x) > std::numeric_limits<double>::epsilon() ? a.x : 0.;
    double _y = fabs(a.y) > std::numeric_limits<double>::epsilon() ? a.y : 0.;
    double _z = fabs(a.z) > std::numeric_limits<double>::epsilon() ? a.z : 0.;
    return stream << _x << ' ' << _y << ' ' << _z; 
  }
  
};

namespace {
  
  Vector3d polar(double rho, double theta, double phi ) 
  {  
    double s_t, c_t;
    double s_p, c_p;
#ifdef sincos
    sincos(theta, &s_t, &c_t);
    sincos(phi, &s_p, &c_p);
#else
    s_t = sin(theta);
    c_t = cos(theta);
    s_p = sin(phi);
    c_p = cos(phi);
#endif
    return Vector3d(rho * s_t * c_p, rho * s_t * s_p, rho * c_t); 
  }
  
}

#endif /* VECTOR3D_HH */
