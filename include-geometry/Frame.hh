#ifndef FRAME_HH
#define FRAME_HH

/*

  Author: Andrea Latina

*/

#include "Rotation.hh"
#include "Vector3d.hh"

class Frame {

  Vector3d origin;
  Rotation axes;

public:
  
  Frame(double x, double y, double z, double phi_x, double theta_z, double phi_z ) : origin(x, y, z), axes(phi_x, theta_z, phi_z) {}
  Frame(double x, double y, double z, const Rotation &_axes ) : origin(x, y, z), axes(_axes) {}
  Frame(const Vector3d &_origin, double phi_x, double theta_z, double phi_z ) : origin(_origin), axes(phi_x, theta_z, phi_z) {}
  Frame(const Vector3d &_origin, const Rotation &_axes ) : origin(_origin), axes(_axes) {}
  explicit Frame(const Rotation &_axes ) : origin(0, 0, 0), axes(_axes) {}	
  explicit Frame(const Vector3d &_origin ) : origin(_origin) {}
  Frame() : origin(0, 0, 0) {}
  
  ~Frame() {}
  
  void set_origin(const Vector3d &_origin ) { origin = _origin; }
  void set_axes(const Rotation &_axes )	{ axes = _axes; }
  void set_euler_angles(const std::valarray<double> &angles ) { axes.set_euler_angles(angles); }
  
  const Rotation &get_axes() const { return axes; }
  const Vector3d &get_origin() const { return origin; }
  Matrix3d get_rotation() const	{ return axes.get_column_matrix(); }
  
  const Frame &operator  = (const Rotation &_axes ) { origin = Vector3d(0, 0, 0); axes = _axes; return *this; } 
  
  const Frame &operator *= (const Frame &a ) { return *this = *this * a; }
  const Frame &operator /= (const Frame &a ) { return *this = *this / a; }
  
  friend Frame operator * (const Frame &a, const Frame &b ) { return Frame(a.origin + a.axes * b.origin, a.axes * b.axes); }
  
  friend Vector3d operator * (const Frame &a, const Vector3d &b ) { return a.axes * b + a.origin; }
  friend Axis operator * (const Frame &a, const Axis &b ) { return Axis(a.axes * b); }
  
  friend Frame operator / (const Frame &a, const Frame &b ) { return Frame(inverse(b.axes) * (a.origin - b.origin), inverse(b.axes) * a.axes); }
  
  friend Vector3d operator / (const Vector3d &a, const Frame &b ) { return inverse(b.axes) * (a - b.origin); }
  friend Axis operator / (const Axis &a, const Frame &b ) { return Axis(inverse(b.axes) * a); }
  
  friend std::ostream &operator << (std::ostream &stream, const Frame &f ) { return stream << f.origin << std::endl << f.axes << std::endl; }
  
};

#endif /* FRAME_HH */
