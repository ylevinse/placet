#ifndef MATRIX3D_HH
#define MATRIX3D_HH

/*

  Author: Andrea Latina

*/

#include "Vector3d.hh"

class Matrix3d {
  
  double data[9];
  
public:
  
  Matrix3d(const Matrix3d &m )	{ for (size_t i = 0; i < 9; i++) data[i] = m.data[i];	}
  Matrix3d(Vector3d a, Vector3d b, Vector3d c )
  {
    data[0] = a.x; data[1] = a.y; data[2] = a.z;
    data[3] = b.x; data[4] = b.y; data[5] = b.z;
    data[6] = c.x; data[7] = c.y; data[8] = c.z; 
  }
	
  Matrix3d() {}

  static Matrix3d star(const Vector3d &v );
  static Matrix3d Identity3d();

  void clear() { for(size_t i = 0; i < 9; i++) data[i] = 0; }
	
  const double 	*operator [] (size_t i ) const { return data + i * 3; }
  double *operator [] (size_t i ) { return data + i * 3; }

  friend double det(const Matrix3d &m )	{ return m[0][0] * det(m, 0, 0) - m[0][1] * det(m, 0, 1) + m[0][2] * det(m, 0, 2); }
  friend double	det(const Matrix3d &m, size_t i, size_t j )					
  {
    size_t a, b, c, d;	
    // a b
    // c d
    
    if (i == 0)	{ a = b = 3; c = d = 6; }
    else if (i == 1) { a = b = 0; c = d = 6; }
    else { a = b = 0; c = d = 3; }
	
    if (j == 0)	{ a += 1; b += 2; c +=1; d += 2; }
    else if (j == 1) { b += 2; d += 2; }
    else { b += 1; d += 1; }

    return m.data[a] * m.data[d] - m.data[b] * m.data[c];
  }
	
  friend double	trace(const Matrix3d &m )
  {
    return m.data[0] + m.data[4] + m.data[8];
  }

  friend Matrix3d transpose(const Matrix3d &m )
  {
    Matrix3d r; 
    for(size_t i = 0; i < 3; i++) 
      for(size_t j = 0; j < 3; j++) 
	r[i][j] = m[j][i]; 
    return r; 
  }
  
  friend Matrix3d inverse(const Matrix3d &m )
  {
    Matrix3d r;
    double det_m_ij, det_m;
    det_m  = m[0][0] * (det_m_ij = det(m, 0, 0)); r[0][0] =  det_m_ij;		
    det_m -= m[0][1] * (det_m_ij = det(m, 0, 1)); r[1][0] = -det_m_ij;			
    det_m += m[0][2] * (det_m_ij = det(m, 0, 2)); r[2][0] =  det_m_ij;		
    for(size_t i = 1; i < 3; i++)
      for(size_t j = 0; j < 3; j++)
	r[j][i] = (i + j) & 1 ? -det(m, i, j) : det(m, i, j);
    return r / det_m;
  }

  const Matrix3d &operator += (const Matrix3d &b ) { for (size_t i = 0; i < 9; i++) data[i] += b.data[i]; return *this; }
  const Matrix3d &operator -= (const Matrix3d &b ) { for (size_t i = 0; i < 9; i++) data[i] -= b.data[i]; return *this; }
  const Matrix3d &operator *= (const Matrix3d &b ) { return *this = *this * b; }
  const Matrix3d &operator /= (const Matrix3d &b ) { return *this = *this * inverse(b); }
  const Matrix3d &operator *= (double b ) { for (size_t i = 0; i < 9; i++) data[i] *= b; return *this; }	
  const Matrix3d &operator /= (double b ) { for (size_t i = 0; i < 9; i++) data[i] /= b; return *this; }
	
  friend Matrix3d operator + (Matrix3d a, const Matrix3d &b ) { for (size_t i = 0; i < 9; i++) a.data[i] += b.data[i]; return a; }
  friend Matrix3d operator - (Matrix3d a, const Matrix3d &b ) { for (size_t i = 0; i < 9; i++) a.data[i] -= b.data[i];	return a; }
  friend Matrix3d operator * (double b, Matrix3d a ) { for (size_t i = 0; i < 9; i++) a.data[i] *= b; return a; }
  friend Matrix3d operator * (Matrix3d a, double b ) { for (size_t i = 0; i < 9; i++) a.data[i] *= b; return a; }
  friend Matrix3d operator / (Matrix3d a, double b ) { for (size_t i = 0; i < 9; i++) a.data[i] /= b; return a; }
  friend Matrix3d operator / (const Matrix3d &a, const Matrix3d &b ) { return a * inverse(b); }

  friend Matrix3d operator * (const Matrix3d &a, const Matrix3d &b )
  {
    Matrix3d r;
    for(size_t i = 0; i < 3; i++)
      for(size_t j = 0; j < 3; j++) {
	double t = a.data[i * 3 ] * b.data[j];
	for(size_t k = 1; k < 3; k++)
	  t += a.data[i * 3 + k] * b.data[k * 3 + j];
	r.data[i * 3 + j] = t;
      }
    return r;
  }
  
  friend Vector3d operator * (const Matrix3d &a, const Vector3d &v )
  {
    return Vector3d(a.data[0] * v.x + a.data[1] * v.y + a.data[2] * v.z,
		    a.data[3] * v.x + a.data[4] * v.y + a.data[5] * v.z,
		    a.data[6] * v.x + a.data[7] * v.y + a.data[8] * v.z);
  }
  
  friend std::ostream &operator << (std::ostream &stream, const Matrix3d &m )
  {
    for(size_t i = 0; i < 3; i++) {
      stream << '(';
      for(size_t j = 0; j < 2; j++)
	stream << m[i][j] << ",\t";	
      stream << m[i][2] << "\t)\n";	
    }
    return stream << std::endl;
  }

};

inline Matrix3d Matrix3d::star(const Vector3d &v )
{ 
  return Matrix3d(Vector3d(	   0,	-v.z,	  v.y	),
		  Vector3d(	 v.z,	   0,	 -v.x	),
		  Vector3d(	-v.y,	 v.x,	    0	)); 
}

inline Matrix3d Matrix3d::Identity3d()
{
  return Matrix3d(Vector3d(1, 0, 0), 
		  Vector3d(0, 1, 0), 
		  Vector3d(0, 0, 1)); 
}

#endif /* MATRIX3D_HH */
