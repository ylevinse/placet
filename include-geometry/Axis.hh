#ifndef AXIS_HH
#define AXIS_HH

/*

  Author: Andrea Latina

*/

#include <iostream>
#include <valarray>
#include <cmath>

#include "Matrix3d.hh"

#define Axis_X Axis(M_PI / 2., 0.)
#define Axis_Y Axis(M_PI / 2., M_PI / 2.)
#define Axis_Z Axis(0., 0.)

struct Axis {

  double theta;
  double phi;
	
  explicit Axis(const Vector3d &v ) : theta(v.theta()), phi(v.phi()) {}
  Axis(double _theta, double _phi ) : theta(_theta), phi(_phi) {}
  Axis() {}
  
  ~Axis() {}
  
  double x() const { return sin(theta) * cos(phi); } 			  
  double y() const { return sin(theta) * sin(phi); } 			  
  double z() const { return cos(theta); }		  			  
  
  operator Vector3d() const 
  {
    double s_t, c_t;
    double s_p, c_p;
#ifdef sincos
    sincos(theta, &s_t, &c_t);
    sincos(phi, &s_p, &c_p);
#else
    s_t = sin(theta);
    c_t = cos(theta);
    s_p = sin(phi);
    c_p = cos(phi);
#endif
    return Vector3d(s_t * c_p, s_t * s_p, c_t);
  }  
 
  friend Axis operator - (const Axis &a ) { return Axis(fmod(M_PI - a.theta, M_PI), fmod(a.phi + M_PI, 2.*M_PI)); }
  
  friend double operator * (const Axis &a, const Axis &b )
  {
    double theta_m = b.theta - a.theta;
    double theta_p = b.theta + a.theta;
    double phi_m = b.phi - a.phi;
    return (+ cos(theta_m + phi_m) 
	    + cos(theta_m - phi_m)
	    - cos(theta_p + phi_m) 
	    - cos(theta_p - phi_m) 
	    + 2 * cos(theta_m) 
	    + 2 * cos(theta_p)) / 4;
  }
  
  friend Vector3d operator ^ (const Axis &a, const Axis &b )
  {
    double s_t1, c_t1;
    double s_p1, c_p1;
#ifdef sincos
    sincos(a.theta, &s_t1, &c_t1);
    sincos(a.phi, &s_p1, &c_p1);
#else
    s_t1 = sin(a.theta);
    c_t1 = cos(a.theta);
    s_p1 = sin(a.phi);
    c_p1 = cos(a.phi);
#endif
    double s_t2, c_t2;
    double s_p2, c_p2;
#ifdef sincos
    sincos(b.theta, &s_t2, &c_t2);
    sincos(b.phi, &s_p2, &c_p2);
#else
    s_t2 = sin(b.theta);
    c_t2 = cos(b.theta);
    s_p2 = sin(b.phi);
    c_p2 = cos(b.phi);
#endif
    return Vector3d (s_p1 * s_t1 * c_t2 - s_p2 * c_t1 * s_t2,
		     c_p2 * c_t1 * s_t2 - c_p1 * s_t1 * c_t2,
		     s_t1 * s_t2 * (c_p1 * s_p2 - s_p1 * c_p2));
  }
  
  friend Vector3d operator * (double x, const Axis &a )	{ return ::polar(x, a.theta, a.phi); }
  friend Vector3d operator * (const Axis &a, double x )	{ return ::polar(x, a.theta, a.phi); }
  
  friend std::ostream &operator << (std::ostream &stream, const Axis &a );
  friend std::istream &operator >> (std::istream &stream, Axis &a );
};

#endif /* AXIS_HH */
