#include "placet_print.h"
#include "placet_cout.hh"

#include "defaults.h"
#include "config.h"

#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cstring>

struct PRINT_DATA print_data;

/* help routines */
namespace {
  int print_char(FILE *file,char *s,int n)
  {
    for (int i=0;i<n;i++) placet_fprintf(INFO,file,"%s",s);
    return 0;
  }

  /**
   * @brief fputs() with verbosity level
   * 
   * Use this instead of fputs(). If you want to print to normal screen,
   * use verbosity INFO. Other levels include ERROR, WARNING, VERBOSE, 
   * VERYVERBOSE, and DEBUG.
   * @author Yngve Inntjore Levinsen
   * @return 0 if string isn't printed, otherwise returns value from fputs().
   * @see VERBOSITY
   */
  int placet_fputs(VERBOSITY verbosity, const char* __s, FILE* __stream )
  {
    int ret=0;
    if (verbosity<=print_data.verbosity) ret=fputs( __s, __stream);
    if (verbosity==ERROR) {
      print_data.num_errors++;
      if (print_data.exit_on_error) {exit(1);}
    } else if (verbosity==WARNING) print_data.num_warnings++;
    return ret;
  }
}


VERBOSITY get_verbosity() { 
  return print_data.verbosity; 
};

void set_verbosity(VERBOSITY verbosity_new) { 
  print_data.verbosity=verbosity_new;
}

static int print_centerline(FILE *file, const char *str, const char *delim, int n )
{
  placet_fputs(INFO,delim, file);
  print_char(file," ",(n-2*strlen(delim)-strlen(str))/2);
  placet_fputs(INFO,str, file);
  print_char(file," ",n - 2 * strlen(delim) - strlen(str) - (n-2*strlen(delim)-strlen(str))/2);
  placet_fputs(INFO,delim, file);
  placet_fputs(INFO,"\n",file);
  return 0;
}

int print_copy()
{
    print_char(stdout,"*",80);
    placet_printf(INFO,"\n");

#ifndef HTGEN
    const char * version_string = "PLACET Version No " PACKAGE_VERSION;
#else
    const char * version_string = "PLACET-HTGEN Version No " PACKAGE_VERSION;
#endif
    print_centerline(stdout, "", "**", 80);
    print_centerline(stdout, version_string, "**", 80);
    print_centerline(stdout, "written by D. Schulte", "**", 80);
    print_centerline(stdout, "contributions from", "**", 80);
#ifndef HTGEN
    print_centerline(stdout, "A. Latina, N. Leros,", "**", 80);
#else
    print_centerline(stdout, "A. Latina, N. Leros, H. Burkhardt,", "**", 80);
#endif
    print_centerline(stdout, "P. Eliasson, E. Adli,", "**", 80);
    print_centerline(stdout, "B. Dalena, J. Snuverink,","**",80);
    print_centerline(stdout, "Y. Levinsen, J. Esberg","**",80);
    print_centerline(stdout, "", "**", 80);
    print_centerline(stdout, "THIS VERSION INFO:", "**", 80);
#if OCTAVE_INTERFACE == 1
    print_centerline(stdout, "Octave interface enabled ", "**", 80);
#else
    print_centerline(stdout, "Octave interface disabled", "**", 80);
#endif
#if PYTHON_INTERFACE == 1
    print_centerline(stdout, "Python interface enabled ", "**", 80);
#else
    print_centerline(stdout, "Python interface disabled", "**", 80);
#endif
#if MPI_MODULE == 1
    print_centerline(stdout, "MPI module enabled ", "**", 80);
#else
    print_centerline(stdout, "MPI module disabled", "**", 80);
#endif
    print_centerline(stdout, "", "**", 80);
    print_centerline(stdout, "Submit bugs reports / feature requests to", "**",80);
    print_centerline(stdout, "https://its.cern.ch/jira/browse/TCPLACET", "**",80);
    print_centerline(stdout, "", "**", 80);

    print_char(stdout,"*",80);
    placet_printf(INFO,"\n");
    return 0;
}
int print_help(char *prog_name) {
    placet_cout << ALWAYS << "Usage: "<< prog_name<<" [options] [script]" << std::endl;
    placet_cout << ALWAYS << std::endl;
    placet_cout << ALWAYS << "Available options:" << std::endl;
    placet_cout << ALWAYS <<  "  -s"   << "\tset verbosity level to SILENT (only ERROR messages)" << std::endl;
    placet_cout << ALWAYS <<  "  -v"   << "\tset verbosity level to VERBOSE" << std::endl;
    placet_cout << ALWAYS <<  "  -vv"  << "\tset verbosity level to VERY VERBOSE" << std::endl;
    placet_cout << ALWAYS <<  "  -dbg" << "\tset verbosity level to DEBUG (most output)" << std::endl;
    placet_cout << ALWAYS <<  "  -error" << "\tPLACET exits when ERROR is found" << std::endl;
    placet_cout << ALWAYS <<  "  -w"   << "\tuse tk/wish" << std::endl;
    placet_cout << ALWAYS <<  "  -h,--help" << "\tPrint this help message and exit" << std::endl;
    placet_cout << ALWAYS << "By default, info, warning and error messages are displayed" << std::endl;
    placet_cout << ALWAYS << "If no script is given, PLACET runs interactively" << std::endl;
    placet_cout << ALWAYS << "" << endmsg;
    return 0;
}

int placet_fprintf(VERBOSITY verbosity, FILE *stream, const char *format, ... )
{
  int ret=0;
  // print if verbosity level is matched or if printed to file
  if (verbosity<=print_data.verbosity || (stream!=stdout && stream!=stderr)) {
    va_list ap;
    va_start(ap, format);
    ret=vfprintf(stream, format, ap);
    va_end(ap);
  }
  if (verbosity==ERROR) {
    print_data.num_errors++;
    if (print_data.exit_on_error) {exit(1);}
  } else if (verbosity==WARNING) print_data.num_warnings++;
  return ret;
}

int placet_printf(VERBOSITY verbosity, const char *format, ... )
{
  int ret=0;
  if (verbosity<=print_data.verbosity) {
    va_list ap;
    va_start(ap, format);
    if (verbosity==ERROR || verbosity==WARNING) {
      // print to stderr
      vfprintf(stderr, format, ap);
    } else {
      // print to stdout
      ret=vprintf(format, ap);
    }
    va_end(ap);
  }
  if (verbosity==ERROR) {
    print_data.num_errors++;
    if (print_data.exit_on_error) {exit(1);}    
  } else if (verbosity==WARNING) {print_data.num_warnings++;}
  return ret;
}

char* print_double(const char *format,double v)
{
  static char buffer[1024];
  snprintf(buffer,1024,format,v);
  return buffer;
}

char* print_int(const char *format,int v)
{
  static char buffer[1024];
  snprintf(buffer,1024,format,v);
  return buffer;
}

void my_error(char* err_rout)
{
  //placet_printf(ERROR,"An error has occured in %s\n",err_rout);
  placet_cout << ERROR << "An error has occured in " << err_rout << endmsg;
  exit(1);
}
