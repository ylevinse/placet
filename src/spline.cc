#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <fstream>
#include <vector>

#include <tcl.h>
#include <tk.h>

/* Function prototypes */

#include "spline.h"

#include "placet_cout.hh"

Tcl_HashTable SplineTable;

SPLINE::SPLINE(double x[],int xscale,double y[],int yscale,int nr_points):
  n(nr_points),xscal(xscale), yscal(yscale)
{
  tab=new spline_tab_entry[n];

  int i;

  /// determine in which way input values are ordered
  if (x[0]<x[n-1]) { // ordered from small to large
    for (i=0;i<n;i++){
      switch (xscal){
      case 0:
	tab[i].x=x[i];
	break;
      case 1:
	if (x[i]<=0.0) {
	  placet_cout << ERROR << "spline, not able to take log of " << i << "th value of x: " << x[i] << endmsg;
	  exit(1);
	}
	tab[i].x=log(x[i]);
	break;
      }
      switch (yscal){
      case 0:
	tab[i].y=y[i];
	break;
      case 1:
	if (y[i]<=0.0) {
	  placet_cout << ERROR << "spline, not able to take log of " << i << "th value of y: " << y[i] << endmsg;
	  exit(1);
	}
	tab[i].y=log(y[i]);
	break;
      }	    
    }
  }
  else { // ordered from large to small
    for (i=0;i<n;i++){
      switch (xscal){
      case 0:
	tab[n-1-i].x=x[i];
	break;
      case 1:
	if (x[i]<=0.0) {
	  placet_cout << ERROR << "spline, not able to take log of " << i << "th value of x: " << x[i] << endmsg;
	  exit(1);
	}
	tab[n-1-i].x=log(x[i]);
	break;
      }
      switch (yscal){
      case 0:
	tab[n-1-i].y=y[i];
	break;
      case 1:
	if (y[i]<=0.0) {
	  placet_cout << ERROR << "spline, not able to take log of " << i << "th value of y: " << y[i] << endmsg;
	  exit(1);
	}
	tab[n-1-i].y=log(y[i]);
	break;
      }	    
    }
  }

  sety2();
}

void SPLINE::sety2() {
  double u[n];

  tab[0].y2=0.0; /// lower boundary condition is set to "natural"
  u[0]=0.0;

  /// decomposition loop of the tridiagonal algorithm. y2 and u are used for temporary storage of the decomposed factors
  for (int i=1;i<n-1;i++){
    double sig=(tab[i].x-tab[i-1].x)
      /(tab[i+1].x-tab[i-1].x);
    double p=1.0/(sig*tab[i-1].y2+2.0);
    tab[i].y2=(sig-1.0)*p;
    u[i]=(6.0*((tab[i+1].y-tab[i].y)
	       /(tab[i+1].x-tab[i].x)
	       -(tab[i].y-tab[i-1].y)
	       /(tab[i].x-tab[i-1].x))
	  /(tab[i+1].x-tab[i-1].x)-sig*u[i-1])*p;
  }
  tab[n-1].y2=0.0; /// upper boundary condition is set to "natural"

  /// backsubstitution loop of the tridagonal algorithm
  for (int i=n-2;i>=0;i--){
    tab[i].y2=tab[i].y2*tab[i+1].y2+u[i];
  }
}

double
SPLINE::eval(double x)
{
  int kmin,kmax,kpoint;
  double a,b,w;
  
  kmin=0;
  kmax=n-1;
  switch(xscal){
  case 0:
    break;
  case 1:
    x=log(x);
    break;
  }
  if (x>tab[kmax].x){
    if (yscal){
      return exp(tab[kmax].y);
    }
    else{      return tab[kmax].y;
    }
  }
  if (x<tab[0].x){
    if (yscal) {
      return exp(tab[0].y);
    }
    else {
      return tab[0].y;
    }
  }
  while (kmax-kmin>1){
    kpoint=(kmax+kmin)/2;
    if (tab[kpoint].x>x){
      kmax=kpoint;
    }
    else{
      kmin=kpoint;
    }
  }
  w=tab[kmax].x-tab[kmin].x;
  a=(tab[kmax].x-x)/w;
  b=(x-tab[kmin].x)/w;
  x=a*tab[kmin].y+b*tab[kmax].y+
    (a*(a*a-1.0)*tab[kmin].y2
     +b*(b*b-1.0)*tab[kmax].y2)*w*w/6.0;
  switch (yscal){
  case 0:
    return x;
  case 1:
    return exp(x);
  }
  return x;
}

void SPLINE::get_table(double x0,double dx,double y[],int n)
{
  int i;
  for (i=0;i<n;i++) {
    y[i]=eval(x0+i*dx);
  }
}

SPLINE::~SPLINE()
{
  delete[] tab;
  tab=NULL;
}

/**********************************************************************/
/*                      spline_list (not a command)                   */
/**********************************************************************/

SPLINE* get_spline(const char * splinename) {
  Tcl_HashEntry *entry=Tcl_FindHashEntry(&SplineTable,splinename);
  if (entry==NULL) {
    placet_cout << ERROR << "Spline '" << splinename << "' not found" << endmsg;
    return NULL;
  }
  SPLINE* spline=(SPLINE*)Tcl_GetHashValue(entry);
  return spline;
}

SPLINE* spline_list(char *v[],int n,int xaxis,int yaxis)
{
  double *x,*y;
  char *point;
  int i;
  
  x=(double*)alloca(sizeof(double)*n);
  y=(double*)alloca(sizeof(double)*n);
  for (i=0;i<n;i++){
    x[i]=strtod(v[i],&point);
    y[i]=strtod(point,&point);
  }
  SPLINE* spline = new SPLINE(x,xaxis,y,yaxis,n);
  return spline;
}

/******   Tcl/Tk commands   ******/

/**********************************************************************/
/*                      SplineEval                                    */
/**********************************************************************/

int tk_SplineEval(ClientData clientdata,Tcl_Interp *interp,int /*argc*/,
		  char *argv[])
{
  double x,y;
  char *point;
  x=strtod(argv[1],&point);
  y=((SPLINE*)clientdata)->eval(x);
  char buf[20];
  snprintf(buf,20,"%g",y);
  Tcl_SetResult(interp,buf,TCL_VOLATILE);
  return TCL_OK;
}


/**********************************************************************/
/*                      SplineCreate                                  */
/**********************************************************************/

int tk_SplineCreate(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error,ok;
  Tcl_HashEntry *entry;
  int n,xaxis=0,yaxis=0,sign=0;
  SPLINE *spline;
  char **v;
  char *filename=NULL;
  
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&filename,
     (char*)"Name of the file where the spline is defined (optional)"},
    {(char*)"-sign",TK_ARGV_INT,(char*)NULL,(char*)&sign,
     (char*)"Sign of spline is flipped, only for file (default 0)"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"This command creates a spline."},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"Usage: SplineCreate name data."},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"  name: name of the spline to be created"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"  data: data points for the spline in the form"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"    {{x1 y1} {x2 y2} ...}"},
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,(char*)NULL,
     (char*)"A tcl-command with the spline name is created to evaluate the spline"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (filename) {
    if (argc!=2){
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "Usage: ",argv[0]," name -file filename\n",NULL);
      return TCL_ERROR;
    }
    std::ifstream f;
    f.open(filename);
    if (f.fail()) {
      placet_cout << ERROR << "File '" << filename << "' not found" << endmsg;
    }

    unsigned int size = 1024; // initial spline size, increased when file is larger
    std::vector<double> x_vec(size),y_vec(size);
    unsigned int count = 0;
    while (f.good()) {
      f >> x_vec[count] >> y_vec[count];
      placet_cout << DEBUG << x_vec[count] << " " << y_vec[count] << endmsg;
      if (count >=1 && x_vec[count] < x_vec[count-1]) placet_cout << WARNING << "Spline " << argv[1] << " from file " << filename << " not continuously increasing in x" << endmsg;
      count++;
      if (count>=size) {size*=2; x_vec.resize(size); y_vec.resize(size);}
      // skip whitespace
      f >> std::ws;
    }
    placet_cout << VERBOSE << "Spline '" << argv[1] << "' created with " << count << " points" << endmsg;
    f.close();

    if (sign) {
      for(int i=0; i<count; i++) y_vec[i] = -y_vec[i];
    }

    spline = new SPLINE(&x_vec[0],xaxis,&y_vec[0],yaxis,count);
  }
  else { 
    if (argc!=3){
      Tcl_AppendResult(interp,"Error in \n",argv[0],"\n",
		       "Usage: ",argv[0]," name value_list\n",NULL);
      return TCL_ERROR;
    }
    
    if(error=Tcl_SplitList(interp,argv[2],&n,&v)){
      return error;
    }
    spline=spline_list(v,n,xaxis,yaxis);
  }

  Tcl_CreateCommand(interp,argv[1],&tk_SplineEval,spline,
		       NULL);
  entry=Tcl_CreateHashEntry(&SplineTable,argv[1],&ok);
  if (ok){
    Tcl_SetHashValue(entry,spline);
    return TCL_OK;
  }
  return TCL_ERROR;
}

int
Spline_Init(Tcl_Interp *interp)
{
  Tcl_InitHashTable(&SplineTable,TCL_STRING_KEYS);
  Tcl_CreateCommand(interp,"SplineCreate",&tk_SplineCreate,
		       NULL,NULL);
  Tcl_PkgProvide(interp,"Spline","1.0");
  return TCL_OK;
}
