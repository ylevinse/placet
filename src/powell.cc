#include <cmath>

#include "powell.h"
#include "placet_print.h"

double POWELL::r_sign(double x,double s)
{
  if (s>0.0) return fabs(x);
  if (s<0.0) return -fabs(x);
  return 0.0;
}

double POWELL::brent(double ax,double bx,double cx,double (POWELL::*f)(double),
		 double tol,double *xmin)
{
    /* System generated locals */
    double r__1;

    /* Local variables */
    static int iter;
    static double a, b, d__, e, p, q, r__, u, v, w, x, etemp, fv, fw, fx, fu, 
	    xm, tol1, tol2;

    if (ax<cx) {a=ax;} else {a=cx; }
    if (ax>cx) {b=ax;} else {b=cx; }
    v = bx;
    w = v;
    x = v;
    e = (double)0.;
    fx = (this->*f)(x);
    fv = fx;
    fw = fx;
    for (iter = 1; iter <= 100; ++iter) {
	xm = (a + b) * (double).5;
	tol1 = tol * fabs(x) + (double)1e-10;
	tol2 = tol1 * (double)2.;
	if ((r__1 = x - xm, fabs(r__1)) <= tol2 - (b - a) * (double).5) {
	    goto L3;
	}
	if (fabs(e) > tol1) {
	    r__ = (x - w) * (fx - fv);
	    q = (x - v) * (fx - fw);
	    p = (x - v) * q - (x - w) * r__;
	    q = (q - r__) * (double)2.;
	    if (q > (double)0.) {
		p = -p;
	    }
	    q = fabs(q);
	    etemp = e;
	    e = d__;
	    if (fabs(p) >= (r__1 = q * (double).5 * etemp, fabs(r__1)) || p <= 
		    q * (a - x) || p >= q * (b - x)) {
		goto L1;
	    }
	    d__ = p / q;
	    u = x + d__;
	    if (u - a < tol2 || b - u < tol2) {
		r__1 = xm - x;
		d__ = r_sign(tol1,r__1);
	    }
	    goto L2;
	}
L1:
	if (x >= xm) {
	    e = a - x;
	} else {
	    e = b - x;
	}
	d__ = e * (double).381966;
L2:
	if (fabs(d__) >= tol1) {
	    u = x + d__;
	} else {
	    u = x + r_sign(tol1,d__);
	}
	fu = (this->*f)(u);
	if (fu <= fx) {
	    if (u >= x) {
		a = x;
	    } else {
		b = x;
	    }
	    v = w;
	    fv = fw;
	    w = x;
	    fw = fx;
	    x = u;
	    fx = fu;
	} else {
	    if (u < x) {
		a = u;
	    } else {
		b = u;
	    }
	    if (fu <= fw || w == x) {
		v = w;
		fv = fw;
		w = u;
		fw = fu;
	    } else if (fu <= fv || v == x || v == w) {
		v = u;
		fv = fu;
	    }
	}
    }
    placet_printf(WARNING,"Brent exceed maximum iterations.\n");
L3:
    *xmin = x;
    return fx;
} /* brent_ */

double POWELL::f1dim(double x)
{
    /* Local variables */
    int j;
    double xt[50];
    for (j=0;j<f1com.ncom;j++) {
	xt[j] = f1com.pcom[j] + x * f1com.xicom[j];
    }
    return (*f1com.func)(xt);
}

void POWELL::mnbrak(double *ax,double *bx,double *cx,double *fa,double *fb,double *fc,
		    double (POWELL::*func)(double))
{
    /* System generated locals */
    double d__1, d__2, d__3, d__4;

    /* Local variables */
    static double ulim, q, r__, u, fu, dum;
    *fa = (this->*func)(*ax);
    *fb = (this->*func)(*bx);
    if (*fb > *fa) {
	dum = *ax;
	*ax = *bx;
	*bx = dum;
	dum = *fb;
	*fb = *fa;
	*fa = dum;
    }
    *cx = *bx + (*bx - *ax) * 1.618034;
    *fc = (this->*func)(*cx);
L1:
    if (*fb >= *fc) {
	r__ = (*bx - *ax) * (*fb - *fc);
	q = (*bx - *cx) * (*fb - *fa);
/* Computing MAX */
	d__3 = (d__1 = q - r__, fabs(d__1));
	d__2 = (d__3>1e-20) ? d__3 : 1e-20 ;
	d__4 = q - r__;
	u = *bx - ((*bx - *cx) * q - (*bx - *ax) * r__) / (r_sign(d__2,d__4) * (float)2.);
	ulim = *bx + (*cx - *bx) * 100.;
	if ((*bx - u) * (u - *cx) > 0.0) {
	    fu = (this->*func)(u);
	    if (fu < *fc) {
		*ax = *bx;
		*fa = *fb;
		*bx = u;
		*fb = fu;
		goto L1;
	    } else if (fu > *fb) {
		*cx = u;
		*fc = fu;
		goto L1;
	    }
	    u = *cx + (*cx - *bx) * 1.618034;
	    fu = (this->*func)(u);
	} else if ((*cx - u) * (u - ulim) > (float)0.) {
	    fu = (this->*func)(u);
	    if (fu < *fc) {
		*bx = *cx;
		*cx = u;
		u = *cx + (*cx - *bx) * 1.618034;
		*fb = *fc;
		*fc = fu;
		fu = (this->*func)(u);
	    }
	} else if ((u - ulim) * (ulim - *cx) >= (float)0.) {
	    u = ulim;
	    fu = (this->*func)(u);
	} else {
	    u = *cx + (*cx - *bx) * 1.618034;
	    fu = (this->*func)(u);
	}
	*ax = *bx;
	*bx = *cx;
	*cx = u;
	*fa = *fb;
	*fb = *fc;
	*fc = fu;
	goto L1;
    }
    return;
} /* mnbrak_ */

int POWELL::linmin(double *p,double *xi,int *n,double *fret)
{
    /* System generated locals */
    int i__1;

    /* Local variables */
    static double xmin;
    static int j;
    static double fa, fb, ax, bx, fx;
    static double xx;

    /* Parameter adjustments */
    --xi;
    --p;
    /* Function Body */
    f1com.ncom = *n;
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	f1com.pcom[j - 1] = p[j];
	f1com.xicom[j - 1] = xi[j];
    }
    ax = 0.0;
    xx = 1.0;
    bx = 2.0;
    mnbrak(&ax,&xx,&bx,&fa,&fx,&fb,&POWELL::f1dim);
    *fret=brent(ax,xx,bx,&POWELL::f1dim,c_b13,&xmin);
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	xi[j] = xmin * xi[j];
	p[j] += xi[j];
    }
    return 0;
}

void
POWELL::powell(double p[],double xi[],int n,int np,double ftol,
       int *iter,double *fret)
{
    /* System generated locals */
    int xi_dim1, xi_offset, i__1; //i__2
    double d__1, d__2;

    /* Builtin functions */

    /* Local variables */
    static int ibig;
    static double fptt;
    static int i__, j;
    static double t, fp, pt[50];
    static double del, xit[50], ptt[50];

    /* Parameter adjustments */
    xi_dim1 = np;
    xi_offset = xi_dim1 + 1;
    xi -= xi_offset;
    --p;

    /* Function Body */
    *fret = (*f1com.func)(&p[1]);
    for (j = 1; j <= n; ++j) {
	pt[j - 1] = p[j];
    }
    *iter = 0;
L1:
    ++(*iter);
    fp = *fret;
    ibig = 0;
    del =0.0;
    for (i__ = 1; i__ <= n; ++i__) {
      for (j = 1; j <= n; ++j) {
	xit[j - 1] = xi[j + i__ * xi_dim1];
      }
      linmin(&p[1], xit, &n, fret);
      if ((d__1 = fp - *fret, fabs(d__1)) > del) {
	del = (d__1 = fp - *fret, fabs(d__1));
	ibig = i__;
      }
    }
    if ((d__1 = fp - *fret, fabs(d__1)) * (float)2. <= ftol * (fabs(fp) + fabs(*
									       fret))) {
      return;
    }
    if (*iter == 200) {
      placet_printf(WARNING,"Powell exceeding maximum iterations.\n");
      return;
    }
    i__1 = n;
    for (j = 1; j <= i__1; ++j) {
      ptt[j - 1] = p[j] * (float)2. - pt[j - 1];
      xit[j - 1] = p[j] - pt[j - 1];
      pt[j - 1] = p[j];
      /* L14: */
    }
    fptt = (*f1com.func)(ptt);
    if (fptt >= fp) {
      goto L1;
    }
    /* Computing 2nd power */
    d__1 = fp - *fret - del;
    /* Computing 2nd power */
    d__2 = fp - fptt;
    t = (fp-*fret*2.0+fptt)*2.0*(d__1*d__1) - del * (
						     d__2 * d__2);
    if (t >=0.0) {
      goto L1;
    }
    linmin(&p[1], xit, &n, fret);
    i__1 = n;
    for (j = 1; j <= i__1; ++j) {
      xi[j + ibig * xi_dim1] = xit[j - 1];
      /* L15: */
    }
    goto L1;
} /* powell_ */

