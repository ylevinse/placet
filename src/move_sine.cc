#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <tcl.h>
#include <tk.h>


#define PLACET
#include "placet.h"
#include "structures_def.h"

//#include "random.h"
#include "random.hh"
#include "move_sine.h"

extern INTER_DATA_STRUCT inter_data;


typedef struct{
  double ampl,freq,time;
} MOVE_SINE_STRUCT;


static MOVE_SINE_STRUCT move_sine_data;

int tk_MoveInitSine(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
                        char *argv[])
{
  int error,i,j,first_init=1;
  int nlist,*v;
  double amp=0.0,freq=0.0,t=0.0,FI,step;
  char *i_el_list=NULL,**w;

  Tk_ArgvInfo table[]={
    {(char*)"-amplitude",TK_ARGV_FLOAT,(char*)NULL,(char*)&amp,
     (char*)"Amplitude of the sine movement in micron"},
    {(char*)"-frequency",TK_ARGV_FLOAT,(char*)NULL,(char*)&freq,
     (char*)"Frequency of the sine movement in Hz"},
    {(char*)"-time",TK_ARGV_FLOAT,(char*)NULL,(char*)&t,
     (char*)"Initial Time in s"},
    {(char*)"-separated_element_list",TK_ARGV_STRING,(char*)NULL,
             (char*)&i_el_list,
     (char*)"Default NULL. Separated list of elements (see option -option_sep_list)."},
    {(char*)"-first_init",TK_ARGV_INT,(char*)NULL,(char*)&first_init,
     (char*)"First initialisation of random generator (default if 1), if 0 use at the last working stage of move_sine routines"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
 
  if (i_el_list) {
    if(error=Tcl_SplitList(interp,i_el_list,&nlist,&w)) {
      return error;
    }

    v=(int*)malloc(sizeof(int)*(nlist+1));
    for (i=0;i<nlist;i++){
       if (error=Tcl_GetInt(interp,w[i],&v[i])) {
         return error;
       }
     } 
  }

  /// Store amplitude and frequency in the data structure
  
  move_sine_data.ampl = amp;
  move_sine_data.freq = freq;

  move_sine_data.time = t;

  /// Define a working data structure for generator and the inital one 
  /// is also stored (move_sine_data.init is copied to move_sine_data.work). 



  if(first_init==1)Instrumentation.SetSeed();   // set default

 
  /// Move elements only. Treat the spearated list according 
  /// to the option

  
  j = 0;
 
  for (i=0;i<inter_data.beamline->n_elements;i++){
    FI = Instrumentation.Uniform() ;
    step = amp * cos(TWOPI*(freq*t+FI));
    if (i_el_list) {
      if (i != v[j]) {
        inter_data.beamline->element[i]->offset.y += step; 
      } 
      else j++; 
    }  
    else inter_data.beamline->element[i]->offset.y += step; 
  }
   
  return TCL_OK;
}

//

int tk_MoveStepSine(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
                        char *argv[])
{
  int error,i,j,ig=1;
  int nlist,*v;
  double amp,freq,t=0.0,FI,time,step;
  double amb,apb;
  char *i_el_list=NULL,**w;

  Tk_ArgvInfo table[]={
    {(char*)"-time",TK_ARGV_FLOAT,(char*)NULL,(char*)&t,
     (char*)"Incremented Time step in s"},
   {(char*)"-separated_element_list",TK_ARGV_STRING,(char*)NULL,
            (char*)&i_el_list,
     (char*)"Default NULL. Separated list of elements which don't move."},
    {(char*)"-rndm5_init",TK_ARGV_INT,(char*)NULL,(char*)&ig,
     (char*)"Use the initialised random generator (default if 1), if 0 use at the last working stage of MOVE'X'SINE routines"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  if (i_el_list) {
    if(error=Tcl_SplitList(interp,i_el_list,&nlist,&w)) {
      return error;
    }

    v=(int*)malloc(sizeof(int)*(nlist+1));
    for (i=0;i<nlist;i++){
       if (error=Tcl_GetInt(interp,w[i],&v[i])) {
         return error;
       }
    }
  } 

  /// Get the initial data structure for random generator obtained 
  /// in MoveInitSine (This is by default !!!)
  
  if(ig==1)Instrumentation.SetSeed();
 

  /// Move elements according to Sine function for each element
  // FI is the phase of one element

  amp = move_sine_data.ampl;
  freq = move_sine_data.freq;
  time = move_sine_data.time;

  j = 0;
  if(t>0.) {

    for (i=0;i<inter_data.beamline->n_elements;i++) {

      FI = Instrumentation.Uniform();

// step -> sinA(T+t)-sinB(T)

      amb = -PI*freq*t;
      apb = -amb + TWOPI*(freq*time+FI);
      step = amp*2.0*sin(amb)*sin(apb);

      if (i_el_list) {
        if (i != v[j]) {
          inter_data.beamline->element[i]->offset.y += step; 
        } 
        else j++; 
      }  
      else inter_data.beamline->element[i]->offset.y += step;
    }
  }

 // increment the time !!!

  move_sine_data.time += t;

  return TCL_OK;
}
