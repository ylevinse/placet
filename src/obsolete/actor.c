#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "actor.h"

#define N 10000
#define NB 280
#define M 13

void
init(double k0[],double k1[],double c[],double s[],int n,double dt)
{
  int i;
  double tmp;
  for (i=0;i<n;++i){
    tmp=exp(-k1[i]*dt);
     c[i]=cos(k0[i]*dt)*tmp;
     s[i]=sin(k0[i]*dt)*tmp;
  }
}

void move(double c[],double s[],double dc[],double ds[],int n)
{
  int i;
  double tmp;

  for (i=0;i<n;++i){
    tmp=dc[i]*c[i]+ds[i]*s[i];
    s[i]=-ds[i]*c[i]+dc[i]*s[i];
    c[i]=tmp;
  }
}

void move2(double c[],double s[],double k0[],double k1[],int n,double dt)
{
  int i;
  double tmp,dc,ds;

  for (i=0;i<n;++i){
    tmp=exp(-k1[i]*dt);
    dc=cos(k0[i]*dt)*tmp;
    ds=sin(k0[i]*dt)*tmp;
    tmp=dc*c[i]+ds*s[i];
    s[i]=-ds*c[i]+dc*s[i];
    c[i]=tmp;
  }
}

main()
{
  double **k0,**k1,**dc,**ds,**c,**s,**dc1,**ds1;
  int i,j,ib;
  clock_t t0,t1;

  t0=clock();

  k0=(double**)malloc(sizeof(double*)*N);
  k1=(double**)malloc(sizeof(double*)*N);
  dc=(double**)malloc(sizeof(double*)*N);
  ds=(double**)malloc(sizeof(double*)*N);
  dc1=(double**)malloc(sizeof(double*)*N);
  ds1=(double**)malloc(sizeof(double*)*N);
  c=(double**)malloc(sizeof(double*)*N);
  s=(double**)malloc(sizeof(double*)*N);
  for (i=0;i<N;++i){
    k0[i]=(double*)malloc(sizeof(double)*M);
    k1[i]=(double*)malloc(sizeof(double)*M);
    dc[i]=(double*)malloc(sizeof(double)*M);
    ds[i]=(double*)malloc(sizeof(double)*M);
    dc1[i]=(double*)malloc(sizeof(double)*M);
    ds1[i]=(double*)malloc(sizeof(double)*M);
    c[i]=(double*)malloc(sizeof(double)*M);
    s[i]=(double*)malloc(sizeof(double)*M);
  }
  for(j=0;j<N;++j){
    for(i=0;i<M;++i){
      k0[j][i]=(i+1);
      k1[j][i]=(i+1);
      c[j][i]=1.0;
      s[j][i]=0.0;
    }
  }
  for(i=0;i<N;++i){
    init(k0[i],k1[i],dc[i],ds[i],M,0.01);
    init(k0[i],k1[i],dc1[i],ds1[i],M,0.01);
  }
  for (ib=0;ib<NB;++ib){
    //    init(k0[i],k1[i],c[i],s[i],M,0.01);
    for(i=0;i<N;++i){
      //            init(k0[i],k1[i],c[i],s[i],M,0.01);
      //            init(k0[i],k1[i],c[0],s[0],M,0.01);
      //      init(k0[i],k1[i],dc[i],ds[i],M,0.01);
            move(c[i],s[i],dc1[i],ds1[i],M);
	    //      move2(c[i],s[i],k0[i],k1[i],M,0.01); 
      for(j=0;j<41;++j){
	//	move(c[0],s[0],dc[0],ds[0],M);
	move(c[i],s[i],dc[i],ds[i],M);
      }
    }
    t1=clock();
    printf("%g\n",(double)(t1-t0));
    t0=t1;
  }
  exit(0);
}
