#include <cstdlib>
#include "octave_embed.h" 
#include "placet_cout.hh"

void octave_init(int /*argc*/, char * /*argv*/[]) {
  placet_cout << ERROR << ": placet-development has no octave integration, call placet-octave instead" << endmsg;
  exit(1);
}
int octave_call(const char * /*string*/ ) {return 0;}
