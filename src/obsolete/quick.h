#ifndef h_QUICK
#define h_QUICK
extern double *quick_s,*quick_c,quick_i,quick_f;
extern int quick_n,quick_n2,quick_n4;

void quick_start();

inline void
sin_cos(double x,double *s,double *c)
{
  int i;
  //  i=lrint(x*quick_i);
  i=int(x*quick_i+0.5);
  x-=i*quick_f;
  i=i%quick_n;
  *s=quick_s[i]+x*(quick_c[i]-x*(0.5*quick_s[i]
				 +0.166666666666666667*x*quick_c[i]));
  *c=quick_c[i]-x*(quick_s[i]+x*(0.5*quick_c[i]
				 -0.166666666666666667*x*quick_s[i]));
}

inline void
sin_cos2(double x,double *s,double *c)
{
  int i,i1;
  //  i=lrint(x*quick_i);
  i=int(x*quick_i+0.5);
  x-=i*quick_f;
  i=i%quick_n;
  i1=(i+quick_n4)%quick_n;
  *s=quick_s[i]+x*(quick_s[i1]-x*(0.5*quick_s[i]
				  +0.166666666666666667*x*quick_s[i1]));
  *c=quick_s[i1]-x*(quick_s[i]+x*(0.5*quick_s[i1]
				  -0.166666666666666667*x*quick_s[i]));
}
#endif
