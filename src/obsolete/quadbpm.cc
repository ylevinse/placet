QUADBPM *quadbpm_make(double length,double strength,int field)
{
  QUADBPM *element;
  element=new QUADBPM;
  element->length=length;
  element->v1=strength;
  element->type=QUADBPM;
#ifdef TWODIM
  element->help=(double*)xmalloc(sizeof(double)*2);
  element->help[0]=0.0;
  element->help[1]=0.0;
#else
  element->help=(double*)xmalloc(sizeof(double)*1);
  element->help[0]=0.0;
#endif
  return element;
}
  
void quadbpm_step(ELEMENT *e,BEAM *bunch)
{
  double tmp,k0,k,l,ksqrt,s,c;
  double eps=1e-200,wgtsum=0.0,signal_y=0.0,signal_x=0.0;
  int i;
  R_MATRIX r;
#ifdef TWODIM
  R_MATRIX rxx;
#endif
  QUADBPM *element=(QUADBPM*)e;
  
  l=0.5*element->length;
  if (l>eps){
    k0=0.5*element->v1/l;
    // warning: needs to be corrected for BPM error 
    if (fabs(k0)<eps){
      ((BPM*)element)->step(bunch);
      return;
    }
    for (i=0;i<bunch->slices;i++){
      k=k0/bunch->particle[i].energy;
      if (k>0.0){
	ksqrt=sqrt(k);
	c=cos(ksqrt*l);
	s=sin(ksqrt*l);
	r.r11=c;
	r.r12=s/ksqrt;
	r.r21=-s*ksqrt;
	r.r22=c;
	mult_M_M(&r,&r,&r);
	/ *first half step**** /
	    tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=-s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;
   
	signal_y+=tmp*bunch->particle[i].wgt;
	wgtsum+=bunch->particle[i].wgt;
   
	/ *second half step***** /
	    tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=-s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;
#ifdef TWODIM
	c=cosh(ksqrt*l);
	s=sinh(ksqrt*l);
	rxx.r11=c;
	rxx.r12=s/ksqrt;
	rxx.r21=s*ksqrt;
	rxx.r22=c;
	mult_M_M(&rxx,&rxx,&rxx);
	/ *first half step***** /
	    tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
   
	signal_x+=tmp*bunch->particle[i].wgt;
   
	/ *second half step***** /
	    tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
   
	mult_M_M(&rxx,bunch->sigma_xx+i,bunch->sigma_xx+i);
	transpose_M(&rxx);
	mult_M_M(bunch->sigma_xx+i,&rxx,bunch->sigma_xx+i);
	mult_M_M(&r,bunch->sigma_xy+i,bunch->sigma_xy+i);      
	mult_M_M(bunch->sigma_xy+i,&rxx,bunch->sigma_xy+i);
#endif
	mult_M_M(&r,bunch->sigma+i,bunch->sigma+i);
	transpose_M(&r);
	mult_M_M(bunch->sigma+i,&r,bunch->sigma+i);
      }
      else{
	ksqrt=sqrt(-k);
	c=cosh(ksqrt*l);
	s=sinh(ksqrt*l);
	r.r11=c;
	r.r12=s/ksqrt;
	r.r21=s*ksqrt;
	r.r22=c;
	mult_M_M(&r,&r,&r);
   
	/ * first half step ******* /
	    tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;

	signal_y+=tmp*bunch->particle[i].wgt;
	wgtsum+=bunch->particle[i].wgt;

	/ * second half step ******** /
	    tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;
#ifdef TWODIM
	c=cos(ksqrt*l);
	s=sin(ksqrt*l);
	rxx.r11=c;
	rxx.r12=s/ksqrt;
	rxx.r21=-s*ksqrt;
	rxx.r22=c;
	mult_M_M(&rxx,&rxx,&rxx);
	/ *first half step **** /
	    tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=-s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
   
	signal_x+=tmp*bunch->particle[i].wgt;

	/ * second half step ***** /
	    tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=-s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
   
	mult_M_M(&rxx,bunch->sigma_xx+i,bunch->sigma_xx+i);
	transpose_M(&rxx);
	mult_M_M(bunch->sigma_xx+i,&rxx,bunch->sigma_xx+i);
	mult_M_M(&r,bunch->sigma_xy+i,bunch->sigma_xy+i);      
	mult_M_M(bunch->sigma_xy+i,&rxx,bunch->sigma_xy+i);
#endif
	mult_M_M(&r,bunch->sigma+i,bunch->sigma+i);
	transpose_M(&r);
	mult_M_M(bunch->sigma+i,&r,bunch->sigma+i);
      }
    }
  }
  else{
    k0=element->v1;
    if (fabs(k0)<eps){
      ((BPM*)element)->step(bunch);
      return;
    }
    for (i=0;i<bunch->slices;i++){
      k*=2.0;
      k=k0/bunch->particle[i].energy;
      r.r11=1.0;
      r.r12=0.0;
      r.r21=-k;
      r.r22=1.0;
      bunch->particle[i].yp-=k*bunch->particle[i].y;
      signal_y+=bunch->particle[i].y*bunch->particle[i].wgt;
      wgtsum+=bunch->particle[i].wgt;
#ifdef TWODIM
      rxx.r11=1.0;
      rxx.r12=0.0;
      rxx.r21=k;
      rxx.r22=1.0;
      bunch->particle[i].xp+=k*bunch->particle[i].x;
      signal_x+=bunch->particle[i].x*bunch->particle[i].wgt;
      mult_M_M(&rxx,bunch->sigma_xx+i,bunch->sigma_xx+i);
      transpose_M(&rxx);
      mult_M_M(bunch->sigma_xx+i,&rxx,bunch->sigma_xx+i);
      mult_M_M(&r,bunch->sigma_xy+i,bunch->sigma_xy+i);      
      mult_M_M(bunch->sigma_xy+i,&rxx,bunch->sigma_xy+i);
#endif
      mult_M_M(&r,bunch->sigma+i,bunch->sigma+i);
      transpose_M(&r);
      mult_M_M(bunch->sigma+i,&r,bunch->sigma+i);
    }
  }
  signal_y/=wgtsum;
  element->info1=signal_y-element->help[0];
#ifdef TWODIM
  signal_x/=wgtsum;
  element->info2=signal_x-element->help[1];
#endif
}
   
void quadbpm_step_4d_0(ELEMENT *e,BEAM *bunch)
{
  double tmp,k0,k,l,ksqrt,s,c;
  double eps=1e-200,wgtsum=0.0,signal_y=0.0,signal_x=0.0;
  int i;
  QUADBPM *element=(QUADBPM*)e;
   
  l=0.5*element->length;
  if (l>eps){
    k0=0.5*element->v1/l;
    if (fabs(k0)<eps){
      ((BPM*)element)->step_4d_0(bunch);
      return;
    }
    for (i=0;i<bunch->slices;i++){
      k=k0/bunch->particle[i].energy;
      if (k>0.0){
	ksqrt=sqrt(k);
	c=cos(ksqrt*l);
	s=sin(ksqrt*l);
	/ *first half step **** /
	    tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=-s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;
   
	signal_y+=tmp*bunch->particle[i].wgt;
	wgtsum+=bunch->particle[i].wgt;
   
	/ *second half step  **** /
	    tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=-s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;
#ifdef TWODIM
	c=cosh(ksqrt*l);
	s=sinh(ksqrt*l);
	/ *first half step  **** /
	    tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
   
	signal_x+=tmp*bunch->particle[i].wgt;
   
	/ *second half step   ***** / 
	    tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
#endif
      }
      else{
	ksqrt=sqrt(-k);
	c=cosh(ksqrt*l);
	s=sinh(ksqrt*l);
	/ * first half step ******* /
	    tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;

	signal_y+=tmp*bunch->particle[i].wgt;
	wgtsum+=bunch->particle[i].wgt;
   
	/ * second half step ****** /
	    tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;
#ifdef TWODIM
	c=cos(ksqrt*l);
	s=sin(ksqrt*l);
	/ *first half step ***** /
	    tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=-s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
   
	signal_x+=tmp*bunch->particle[i].wgt;
   
	/ * second half step ***** /
	    tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=-s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
   
#endif
      }
    }
  }
  else{
    k0=element->v1;
    if (fabs(k0)<eps){
      ((BPM*)element)->step(bunch);
      return;
    }
    for (i=0;i<bunch->slices;i++){
      k*=2.0;
      k=k0/bunch->particle[i].energy;
      bunch->particle[i].yp-=k*bunch->particle[i].y;
      signal_y+=bunch->particle[i].y*bunch->particle[i].wgt;
      wgtsum+=bunch->particle[i].wgt;
#ifdef TWODIM
      bunch->particle[i].xp+=k*bunch->particle[i].x;
      signal_x+=bunch->particle[i].x*bunch->particle[i].wgt;
#endif
    }
  }
  signal_y/=wgtsum;
  element->info1=signal_y-element->help[0];
#ifdef TWODIM
  signal_x/=wgtsum;
  element->info2=signal_x-element->help[1];
#endif
}
  
void quadbpm_step_half(ELEMENT *e,BEAM *bunch)
{
  double tmp,k0,k,l,ksqrt,s,c;
  double eps=1e-200;
  int i;
  R_MATRIX r;
#ifdef TWODIM
  R_MATRIX rxx;
#endif
  QUADBPM *element=(QUADBPM*)e;
  l=0.5*element->length;
  if (l>eps){
    k0=0.5*element->v1/l;
    if (fabs(k0)<eps){
      drift_step_half(element,bunch);
      return;
    }
    for (i=0;i<bunch->slices;i++){
      k=k0/bunch->particle[i].energy;
      if (k>0.0){
	ksqrt=sqrt(k);
	c=cos(ksqrt*l);
	s=sin(ksqrt*l);
	r.r11=c;
	r.r12=s/ksqrt;
	r.r21=-s*ksqrt;
	r.r22=c;
	tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=-s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;
#ifdef TWODIM
	c=cosh(ksqrt*l);
	s=sinh(ksqrt*l);
	rxx.r11=c;
	rxx.r12=s/ksqrt;
	rxx.r21=s*ksqrt;
	rxx.r22=c;
	tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
	mult_M_M(&rxx,bunch->sigma_xx+i,bunch->sigma_xx+i);
	transpose_M(&rxx);
	mult_M_M(bunch->sigma_xx+i,&rxx,bunch->sigma_xx+i);
	mult_M_M(&r,bunch->sigma_xy+i,bunch->sigma_xy+i);      
	mult_M_M(bunch->sigma_xy+i,&rxx,bunch->sigma_xy+i);
#endif
	mult_M_M(&r,bunch->sigma+i,bunch->sigma+i);
	transpose_M(&r);
	mult_M_M(bunch->sigma+i,&r,bunch->sigma+i);
      }
      else{
	ksqrt=sqrt(-k);
	c=cosh(ksqrt*l);
	s=sinh(ksqrt*l);
	r.r11=c;
	r.r12=s/ksqrt;
	r.r21=s*ksqrt;
	r.r22=c;
	tmp=bunch->particle[i].y*c+bunch->particle[i].yp*s/ksqrt;
	bunch->particle[i].yp=s*ksqrt*bunch->particle[i].y
	  +bunch->particle[i].yp*c;
	bunch->particle[i].y=tmp;
#ifdef TWODIM
	c=cos(ksqrt*l);
	s=sin(ksqrt*l);
	rxx.r11=c;
	rxx.r12=s/ksqrt;
	rxx.r21=-s*ksqrt;
	rxx.r22=c;
	tmp=bunch->particle[i].x*c+bunch->particle[i].xp*s/ksqrt;
	bunch->particle[i].xp=-s*ksqrt*bunch->particle[i].x
	  +bunch->particle[i].xp*c;
	bunch->particle[i].x=tmp;
	mult_M_M(&rxx,bunch->sigma_xx+i,bunch->sigma_xx+i);
	transpose_M(&rxx);
	mult_M_M(bunch->sigma_xx+i,&rxx,bunch->sigma_xx+i);
	mult_M_M(&r,bunch->sigma_xy+i,bunch->sigma_xy+i);      
	mult_M_M(bunch->sigma_xy+i,&rxx,bunch->sigma_xy+i);
#endif
	mult_M_M(&r,bunch->sigma+i,bunch->sigma+i);
	transpose_M(&r);
	mult_M_M(bunch->sigma+i,&r,bunch->sigma+i);
      }
    }
  }
  else{
    k0=0.5*element->v1;
    if (fabs(k0)<eps){
      drift_step_half(element,bunch);
      return;
    }
    for (i=0;i<bunch->slices;i++){
      k=k0/bunch->particle[i].energy;
      r.r11=1.0;
      r.r12=0.0;
      r.r21=-k;
      r.r22=1.0;
      bunch->particle[i].yp-=k*bunch->particle[i].y;
#ifdef TWODIM
      rxx.r11=1.0;
      rxx.r12=0.0;
      rxx.r21=k;
      rxx.r22=1.0;
      bunch->particle[i].xp+=k*bunch->particle[i].x;
      mult_M_M(&rxx,bunch->sigma_xx+i,bunch->sigma_xx+i);
      transpose_M(&rxx);
      mult_M_M(bunch->sigma_xx+i,&rxx,bunch->sigma_xx+i);
      mult_M_M(&r,bunch->sigma_xy+i,bunch->sigma_xy+i);      
      mult_M_M(bunch->sigma_xy+i,&rxx,bunch->sigma_xy+i);
#endif
      mult_M_M(&r,bunch->sigma+i,bunch->sigma+i);
      transpose_M(&r);
      mult_M_M(bunch->sigma+i,&r,bunch->sigma+i);
    }
  }
}
