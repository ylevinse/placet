/*
  The routine steps through a cavity using second order transport
*/

void CAVITY::sigma_step(R_MATRIX *sigma, double delta )
{
  double lndelta,lndelta_delta;
  /* include the rotation due to the endfields ****/
#ifdef END_FIELDS
#ifdef CAV_PRECISE
  if (delta>0.01) {
    lndelta=0.5*log1p(delta);
    lndelta_delta=lndelta/delta;
  } else {
    lndelta_delta=0.5*(1.0-delta*(0.5+delta*0.3308345316809));
    lndelta=lndelta_delta*delta;
  }
#else
  lndelta=0.5*delta;
  lndelta_delta=0.5;
#endif
  /* Transfer matrix with endfield kicks */
  R_MATRIX r1,r2;
  r1.r11=1.0-lndelta;
  r1.r12=2.0*geometry.length*lndelta_delta;
  r1.r21=-0.5*delta/(geometry.length*(1.0+delta))*lndelta;
  r1.r22=(1.0+lndelta)/(1.0+delta);
  r2.r11=r1.r11;
  r2.r12=r1.r21;
  r2.r21=r1.r12;
  r2.r22=r1.r22;
#else
#ifdef CAV_PRECISE
  if (delta>0.01) {
    r1.r12=geometry.length*log1p(delta)/delta;
  } else {
    r1.r12=geometry.length*(1.0-delta*(0.5+delta*0.3308345316809));
  }
  r2.r21=r1.r12;
#endif
  r1.r11=1.0;
  r1.r21=0.0;
  r2.r12=0.0;
  r2.r11=1.0;
  r1.r22=1.0/(1.0+delta);
  r2.r22=r1.r22;
#endif
  mult_M_M(&r1,sigma,sigma);
  mult_M_M(sigma,&r2,sigma);
}
