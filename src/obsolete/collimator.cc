void
beam_bin_poles(BEAM *beam,double *r[],int np)
{
  int i,j,k=0,l;
  double x,y;

  for (i=0;i<beam->slices_per_bunch;++i) {
    for (l=0;l<2*np;++l) {
      r[i][2*l]=0.0;
      r[i][2*l+1]=0.0;
    }
    for (j=0;j<beam->particle_number[i];++j){
      x=beam->particle[k].x;
      y=beam->particle[k].y;
      for (l=0;l<np;++l) {
	r[i][2*l]+=beam->particle[k].wgt*x;
	r[i][2*l+1]+=beam->particle[k].wgt*y;
	x=x*beam->particle[k].x-y*beam->particle[k].y;
	y=y*beam->particle[k].x+x*beam->particle[k].y;
      }
      k++;
    }
    fflush(stdout);
  }
}

void
beam_kick_poles(BEAM *beam,double *r[],int np)
{
  int i,j,k=0,l;
  double kx,ky,x,y;
  for (i=0;i<beam->slices_per_bunch;++i){
    for (j=0;j<beam->particle_number[i];++j){
      kx=0.0;
      ky=0.0;
      x=1.0;
      y=0.0;
      for (l=0;l<np;++l) {
	kx+=r[i][2*l]*x-r[i][2*l+1]*y;
	ky+=r[i][2*l+1]*x+r[i][2*l]*y;
	x=x*beam->particle[k].x-y*beam->particle[k].y;
	y=y*beam->particle[k].x+x*beam->particle[k].y;
      }
      beam->particle[k].xp+=kx/beam->particle[k].energy;
      beam->particle[k].yp+=ky/beam->particle[k].energy;
      k++;
    }
  }
}
