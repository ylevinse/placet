#include <cstring>

#include <getopt.h>
#include <libgen.h>

#include "beamline.hh"
#include "beam.hh"

#include "file_buffered_stream.hh"
#include "socket.hh"
#include "stopwatch.hh"

#include "quadrupole.hh"
#include "multipole.hh"
#include "sbend.hh"
#include "drift.hh"

int main(int argc, char **argv )
{
  char *beamline_name = NULL;
  char *beam_name = NULL;
  char *fifo_name = NULL;
  bool help = false;
  {
    struct option options[] = {
      {"help", 0, 0, 0},
      {"version", 0, 0, 0},
      {"beamline", 1, 0, 0},
      {"beam", 1, 0, 0},
      {"fifo", 1, 0, 0},
      {0, 0, 0, 0}
    };

    while (true) {
      int option_idx = 0, c = getopt_long_only(argc, argv, "", options, &option_idx);
      
      if (c==-1)	break;
      if (c==0) {
	switch (option_idx) {
	case 0:	fprintf(stdout, "\nSix Dimensional Tracking Routine (" __DATE__ ")\n\n"
			"USAGE:\t%s [OPTIONS]\n\n"
			"OPTIONS:\n"
			"  --help                    Display this help\n"
			"  --version                 Display version information\n"
			"  --fifo=name               Use fifo `name\' for input/output\n"
			"  --beam=filename           Input particle set\n"
			"  --beamline=filename       Input file with the beamline\n\n", argv[0]);
	  help = true;
	  break;
	  
	case 1:	fprintf(stdout, "\n%s\n(" __DATE__ ")\n\n", argv[0]); help = true; break;
	case 2:	beamline_name = optarg; break;
	case 3:	beam_name = optarg; break;
	case 4: fifo_name = beamline_name = beam_name = optarg; break;
	  
	default: fprintf(stderr, "unrecognized option `%s\'\n", options[option_idx].name); break;
	}
      }
    }
  }
  if (beamline_name && beam_name) {
    bool loop = fifo_name != NULL;
    do {
      File_IStream file;
      if (file.open(beamline_name)) {
	if (loop) system("clear");
	Beamline beamline;
	try {
	  file >> beamline;
	  if (!loop) file.close();
	} catch (Error &msg ) {
	  std::cerr << "error:\t" << msg << std::endl;
	  return 0;
	}
	std::cerr << "\nthis beamline is composed by:\n\n  "
		  << beamline.get_list_of<Quadrupole>().size() << "\tquadrupoles\n  "
		  << beamline.get_list_of<Multipole>().size() << "\tmultipoles\n  "
		  << beamline.get_list_of<SBend>().size() << "\tsbends\n  "
		  << beamline.get_list_of<Drift>().size() << "\tdrifts\n\n";
	if (loop || file.open(beam_name)) {
	  ParticleSet beam;
	  file >> beam;
	  file.close();
	  Stopwatch timer;
	  timer.start();
	  for (Beamline::element_itr itr=beamline.begin(); itr!=beamline.end(); ++itr) {
	    const Element *element_ptr = *itr;
	    //						element_ptr->transport_synrad(beam);
	    element_ptr->transport(beam);
	  }
	  timer.stop();
	  std::cerr << timer << "\n";
	  if (fifo_name) {
	    File_Buffered_OStream fifo(fifo_name);
	    fifo << beam;
	  } else {
	    for (size_t i=0; i<beam.size(); i++) {
	      fprintf(stdout,"%.15g %g %g %g %g %g\n", beam[i].energy,
		      beam[i].x,
		      beam[i].y,
		      beam[i].z,
		      beam[i].xp,
		      beam[i].yp);
	    }
	  }
	} else {
	  fprintf(stderr, "error:\tcannot open input beam file `%s\'\n", beam_name);
	  break;
	}
      } else {			
	fprintf(stderr, "error:\tcannot open beamline file `%s\'\n", beamline_name);
	break;
      }
    } while(loop);
  } else if (!help) {	
    fprintf(stderr, "try:\t%s --help\n", basename(argv[0]));
  }
  return 0;
}
