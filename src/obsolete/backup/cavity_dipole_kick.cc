#define CAV_MULTI_ANGLE
#define EQUAL_SLICE

#include "rndm.h"

void CAVITY::fill_z_mode_xy(double *x, double *y, int n)
{
  if (yz!=NULL) {
    for (int i=0;i<n;i++){
      xz[i]=0.0;
    }
  } else {
    double dz=geometry.length/n;
    double z=-0.5*(geometry.length-dz);
    for (int i=0;i<n;i++){
      x[i]=0.0;
      y[i]=0.0;
    }
  }
}

//#include "dipole_obsolete.cpp"

/* applies the transverse wakefield kick */

void
dipole_kick_n(CAVITY *element,BEAM *beam,double length)
{
  double tmp,factor,x,y,half_length;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0,nstep;
  INJECT_BEAM_PARAM* param;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m;
  double rndm_scale,ds,dc;
  double *multi_y,*multi_x,*multi_y_b,*multi_x_b,*longfact;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,
    *rho_a,*rho_b,pos_yb,pos_xb,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2,*rho_xp,*rho_yp,z_mode,d_z;
  DRIVE_DATA *cavity_data;
#ifdef EQUAL_SLICE
  double *ds0,*dc0,*w,*xz,*yz;
#endif  

  n_macro=beam->macroparticles;
  cavity_data=beam->drive_data;
  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  w=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  rho_yp=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  rho_xp=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  half_length=0.5*length;
  
  factor=beam->transv_factor*length*beam->factor;

  rho_y=beam->rho_y[0];
#ifdef TWODIM
  rho_x=beam->rho_x[0];
#endif
  if (beam->bunches==1) {
      p=beam->field[0].kick;
      for (j=0;j<beam->slices_per_bunch;j++){
	  rho_y[j]=0.0;
#ifdef TWODIM
	  rho_x[j]=0.0;
#endif
	  for (i_m=0;i_m<n_macro;i_m++){
	      rho_y[j]+=beam->particle[j*n_macro+i_m].y
		  *beam->particle[j*n_macro+i_m].wgt;
#ifdef TWODIM
	      rho_x[j]+=beam->particle[j*n_macro+i_m].x
		  *beam->particle[j*n_macro+i_m].wgt;
#endif
	  }
	  sumy=0.0;
#ifdef TWODIM
	  sumx=0.0;
#endif
	  for (i=0;i<=j;i++){
	      sumy+=rho_y[i] * *p;
#ifdef TWODIM
	      sumx+=rho_x[i] * *p;
#endif
	      p++;
	  }
	  for (i_m=0;i_m<n_macro;i_m++){
	      beam->particle[j*n_macro+i_m].yp+=sumy
		  *factor/beam->particle[j*n_macro+i_m].energy;
#ifdef TWODIM
	      beam->particle[j*n_macro+i_m].xp+=sumx
		  *factor/beam->particle[j*n_macro+i_m].energy;
#endif
	  }
      }
      return;
  }

  /* Initialise wakefields */

  rndm_scale=element->v_tesla;

  param=beam->drive_data->param.inject;

  nstep=element->id->n;

  multi_y=(double*)alloca(sizeof(double)*nstep);
  multi_x=(double*)alloca(sizeof(double)*nstep);
  multi_y_b=(double*)alloca(sizeof(double)*nstep);
  multi_x_b=(double*)alloca(sizeof(double)*nstep);
  longfact=(double*)alloca(sizeof(double)*nstep);
#ifdef EQUAL_SLICE
  ds0=(double*)alloca(sizeof(double)*nstep);
  dc0=(double*)alloca(sizeof(double)*nstep);
  xz=(double*)alloca(sizeof(double)*nstep);
  yz=(double*)alloca(sizeof(double)*nstep);
#endif

  for (step=0;step<nstep;step++){
    multi_y[step]=0.0;
    multi_y_b[step]=0.0;
#ifdef TWODIM
    multi_x[step]=0.0;
    multi_x_b[step]=0.0;
#endif
    longfact[step]=element->id->a0[step]*(param->charge*1.6e-19*1e12)
      *1e-6*1e-3;
#ifdef EQUAL_SLICE
    sincos(rndm_scale*(beam->z_position[1]-beam->z_position[0])
	   *TWOPI*1e-6/element->id->lambda[step],ds0+step,dc0+step);
    /*
    ds0[step]=sin(rndm_scale*(beam->z_position[1]-beam->z_position[0])
		  *TWOPI*1e-6/element->id->lambda[step]);
    dc0[step]=cos(rndm_scale*(beam->z_position[1]-beam->z_position[0])
		  *TWOPI*1e-6/element->id->lambda[step]);
		  */
#endif
  }
  
  element->fill_z_mode_xy(xz,yz,nstep);

  /* loop over all bunches */
  
  d_z=length/nstep;

  for (k=0;k<beam->bunches;k++){
    
    /* apply attenuation */
    
    if (k>0){
      for (step=0;step<nstep;step++){
	tmp=exp(-PI*(param->bunch[k].z-param->bunch[k-1].z)*1e-6/
		(element->id->lambda[step]
		 *element->id->Q[step]));
	multi_y[step]*=tmp;
	multi_y_b[step]*=tmp;
#ifdef TWODIM
	multi_x[step]*=tmp;
	multi_x_b[step]*=tmp;
#endif
      }
    }
    
    m=k*beam->slices_per_bunch;
    p=beam->field[0].kick;
    for (j=0;j<beam->slices_per_bunch;j++){
      w[j]=0.0;
      rho_y[j]=0.0;
      rho_yp[j]=0.0;
#ifdef TWODIM
      rho_x[j]=0.0;
      rho_xp[j]=0.0;
#endif
      for (i_m=0;i_m<n_macro;i_m++){
	w[j]+=beam->particle[(m+j)*n_macro+i_m].wgt;
	rho_y[j]+=beam->particle[(m+j)*n_macro+i_m].y
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef CAV_MULTI_ANGLE
	rho_yp[j]+=beam->particle[(m+j)*n_macro+i_m].yp
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
#ifdef TWODIM
	rho_x[j]+=beam->particle[(m+j)*n_macro+i_m].x
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef CAV_MULTI_ANGLE
	rho_xp[j]+=beam->particle[(m+j)*n_macro+i_m].xp
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
#endif
      }
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      kick_y[j]=sumy;
#ifdef TWODIM
      kick_x[j]=sumx;
#endif
    }
    z_mode=-0.5*(length-d_z);
    for (step=0;step<nstep;step++) {
      pos_y=0.0;
      pos_yb=0.0;
#ifdef TWODIM
      pos_x=0.0;
      pos_xb=0.0;
#endif
      sincos(rndm_scale*beam->z_position[m]
	     *TWOPI*1e-6/element->id->lambda[step],&s,&c);
      /*
      s=sin(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/element->id->lambda[step]);
      c=cos(rndm_scale*beam->z_position[m]
	    *TWOPI*1e-6/element->id->lambda[step]);
	    */
#ifdef EQUAL_SLICE
      ds=ds0[step];
      dc=dc0[step];
#else
      sincos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	     *TWOPI*1e-6/element->id->lambda[step],&ds,&dc);
      /*
      ds=sin(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	     *TWOPI*1e-6/element->id->lambda[step]);
      dc=cos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
	     *TWOPI*1e-6/element->id->lambda[step]);
	     */
#endif
      for (j=0;j<beam->slices_per_bunch;j++){
#ifdef CAV_MULTI_ANGLE
	pos_y+=(rho_y[j]+z_mode*rho_yp[j]-w[j]*yz[step])*c;
	pos_yb-=(rho_y[j]+z_mode*rho_yp[j]-w[j]*yz[step])*s;
#ifdef TWODIM
	pos_x+=(rho_x[j]+z_mode*rho_xp[j]-w[j]*xz[step])*c;
	pos_xb-=(rho_x[j]+z_mode*rho_xp[j]-w[j]*xz[step])*s;
#endif
#else
	pos_y+=rho_y[j]*c;
	pos_yb-=rho_y[j]*s;
#ifdef TWODIM
	pos_x+=rho_x[j]*c;
	pos_xb-=rho_x[j]*s;
#endif
#endif
	kick_y[j]+=(multi_y[step]*s+multi_y_b[step]*c)*longfact[step];
#ifdef TWODIM
	kick_x[j]+=(multi_x[step]*s+multi_x_b[step]*c)*longfact[step];
#endif
	tmp=dc*c-ds*s;
	s=ds*c+dc*s;
	c=tmp;
      }
      multi_y[step]+=pos_y;
      multi_y_b[step]+=pos_yb;
#ifdef TWODIM
      multi_x[step]+=pos_x;
      multi_x_b[step]+=pos_xb;
#endif
      z_mode+=d_z;
    }
    for (j=0;j<beam->slices_per_bunch;j++) {
      for (i_m=0;i_m<n_macro;i_m++){
	beam->particle[(m+j)*n_macro+i_m].yp+=
	  kick_y[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#ifdef TWODIM
	beam->particle[(m+j)*n_macro+i_m].xp+=
	  kick_x[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#endif
      }
    }
  }
}

void
dipole_kick(CAVITY *element,BEAM *beam,double length)
{
  double tmp,factor,x,y,half_length;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0;
  INJECT_BEAM_PARAM* param;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m,nstep;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force;
  double *multi_y,*multi_x,*multi_y_b,*multi_x_b,*longfact;
  double *rho_a,*rho_b,pos_yb,pos_xb,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2,*rho_xp,*rho_yp,z_mode,d_z;
  double *w,w_s,w_sp,*xz,*yz,*ds0,*dc0;
  DRIVE_DATA *cavity_data;
  
  n_macro=beam->macroparticles;
  if (n_macro>1) {
      dipole_kick_n(element,beam,length);
      return;
  }

  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  w=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  rho_yp=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  rho_xp=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  half_length=0.5*length;
  cavity_data=beam->drive_data;  
  factor=beam->transv_factor*length*beam->factor;

  rho_y=beam->rho_y[0];
#ifdef TWODIM
  rho_x=beam->rho_x[0];
#endif

  if (beam->bunches==1) {
      p=beam->field[0].kick;
      for (j=0;j<beam->slices_per_bunch;j++){
	  rho_y[j]=0.0;
#ifdef TWODIM
	  rho_x[j]=0.0;
#endif
	  rho_y[j]+=beam->particle[j].y
	      *beam->particle[j].wgt;
#ifdef TWODIM
	  rho_x[j]+=beam->particle[j].x
	      *beam->particle[j].wgt;
#endif
	  sumy=0.0;
#ifdef TWODIM
	  sumx=0.0;
#endif
	  for (i=0;i<=j;i++){
	      sumy+=rho_y[i] * *p;
#ifdef TWODIM
	      sumx+=rho_x[i] * *p;
#endif
	      p++;
	  }
	  beam->particle[j].yp+=sumy
	      *factor/beam->particle[j].energy;
#ifdef TWODIM
	  beam->particle[j].xp+=sumx
	      *factor/beam->particle[j].energy;
#endif
      }
      return;
  }

  /* Initialise wakefields */

  rndm_scale=element->v_tesla;

  param=beam->drive_data->param.inject;

  nstep=element->id->n;

  multi_y=(double*)alloca(sizeof(double)*nstep);
  multi_x=(double*)alloca(sizeof(double)*nstep);
  multi_y_b=(double*)alloca(sizeof(double)*nstep);
  multi_x_b=(double*)alloca(sizeof(double)*nstep);
  longfact=(double*)alloca(sizeof(double)*nstep);
#ifdef EQUAL_SLICE
  ds0=(double*)alloca(sizeof(double)*nstep);
  dc0=(double*)alloca(sizeof(double)*nstep);
  xz=(double*)alloca(sizeof(double)*nstep);
  yz=(double*)alloca(sizeof(double)*nstep);
#endif

  for (step=0;step<nstep;step++){
    multi_y[step]=0.0;
    multi_y_b[step]=0.0;
#ifdef TWODIM
    multi_x[step]=0.0;
    multi_x_b[step]=0.0;
#endif
    longfact[step]=element->id->a0[step]*(param->charge*1.6e-19*1e12)
      *1e-6*1e-3;
#ifdef EQUAL_SLICE
    sincos(rndm_scale*(beam->z_position[1]-beam->z_position[0])
	   *TWOPI*1e-6/element->id->lambda[step],ds0+step,dc0+step);
    /*
    ds0[step]=sin(rndm_scale*(beam->z_position[1]-beam->z_position[0])
		  *TWOPI*1e-6/element->id->lambda[step]);
    dc0[step]=cos(rndm_scale*(beam->z_position[1]-beam->z_position[0])
		  *TWOPI*1e-6/element->id->lambda[step]);
		  */
#endif
  }

  element->fill_z_mode_xy(xz,yz,nstep);

  /* loop over all bunches */
  
  d_z=length/nstep;

  for (k=0;k<beam->bunches;k++){
    
    /* apply attenuation */
    
      if (k>0){
	  for (step=0;step<nstep;step++){
	    tmp=exp(-PI*(param->bunch[k].z-param->bunch[k-1].z)*1e-6/
		    (element->id->lambda[step]
		     *element->id->Q[step]));
	    multi_y[step]*=tmp;
	    multi_y_b[step]*=tmp;
#ifdef TWODIM
	    multi_x[step]*=tmp;
	    multi_x_b[step]*=tmp;
#endif
	  }
      }
    
      m=k*beam->slices_per_bunch;
      p=beam->field[0].kick;
      for (j=0;j<beam->slices_per_bunch;j++){
	//nanu
	w[j]=beam->particle[m+j].wgt;
	  rho_y[j]=beam->particle[m+j].y
	      *beam->particle[m+j].wgt;
#ifdef CAV_MULTI_ANGLE
	  rho_yp[j]=beam->particle[m+j].yp
	      *beam->particle[m+j].wgt;
#endif
#ifdef TWODIM
	  rho_x[j]=beam->particle[m+j].x
	      *beam->particle[m+j].wgt;
#ifdef CAV_MULTI_ANGLE
	  rho_xp[j]=beam->particle[m+j].xp
	      *beam->particle[m+j].wgt;
#endif
#endif
	  sumy=0.0;
#ifdef TWODIM
	  sumx=0.0;
#endif
	  for (i=0;i<=j;i++){
	      sumy+=rho_y[i] * *p;
#ifdef TWODIM
	      sumx+=rho_x[i] * *p;
#endif
	      p++;
	  }
	  kick_y[j]=sumy;
#ifdef TWODIM
	  kick_x[j]=sumx;
#endif
      }
      z_mode=-0.5*(length-d_z);
      for (step=0;step<nstep;step++) {
	  pos_y=0.0;
	  pos_yb=0.0;
#ifdef TWODIM
	  pos_x=0.0;
	  pos_xb=0.0;
#endif
	  sincos(rndm_scale*beam->z_position[m]
		 *TWOPI*1e-6/element->id->lambda[step],&s,&c);
	  /*
	  s=sin(rndm_scale*beam->z_position[m]
		*TWOPI*1e-6/element->id->lambda[step]);
	  c=cos(rndm_scale*beam->z_position[m]
		*TWOPI*1e-6/element->id->lambda[step]);
		*/
#ifdef EQUAL_SLICE
	  ds=ds0[step];
	  dc=dc0[step];
#else
	  sincos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
		 *TWOPI*1e-6/element->id->wake->lambda[step],&ds,&dc);
	  /*
	  ds=sin(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
		 *TWOPI*1e-6/element->id->wake->lambda[step]);
	  dc=cos(rndm_scale*(beam->z_position[m+1]-beam->z_position[m])
		     *TWOPI*1e-6/element->id->wake->lambda[step]);
		     */
#endif
	  for (j=0;j<beam->slices_per_bunch;j++){
#ifdef CAV_MULTI_ANGLE
	      pos_y+=(rho_y[j]+z_mode*rho_yp[j]-w[j]*yz[step])*c;
	      pos_yb-=(rho_y[j]+z_mode*rho_yp[j]-w[j]*yz[step])*s;
#ifdef TWODIM
	      pos_x+=(rho_x[j]+z_mode*rho_xp[j]-w[j]*xz[step])*c;
	      pos_xb-=(rho_x[j]+z_mode*rho_xp[j]-w[j]*xz[step])*s;
#endif
#else
	      pos_y+=rho_y[j]*c;
	      pos_yb-=rho_y[j]*s;
#ifdef TWODIM
	      pos_x+=rho_x[j]*c;
	      pos_xb-=rho_x[j]*s;
#endif
#endif
	      kick_y[j]+=(multi_y[step]*s+multi_y_b[step]*c)*longfact[step];
#ifdef TWODIM
	      kick_x[j]+=(multi_x[step]*s+multi_x_b[step]*c)*longfact[step];
#endif
	      tmp=dc*c-ds*s;
	      s=ds*c+dc*s;
	      c=tmp;
	  }
	  multi_y[step]+=pos_y;
	  multi_y_b[step]+=pos_yb;
#ifdef TWODIM
	  multi_x[step]+=pos_x;
	  multi_x_b[step]+=pos_xb;
#endif
	  z_mode+=d_z;
      }
      for (j=0;j<beam->slices_per_bunch;j++) {
	  beam->particle[m+j].yp+=
	      kick_y[j]*factor/beam->particle[m+j].energy;
#ifdef TWODIM
	  beam->particle[m+j].xp+=
	      kick_x[j]*factor/beam->particle[m+j].energy;
#endif
      }
  }
}


/* applies the transverse wakefield kick */

void
dipole_kick_long(CAVITY *element,BEAM *beam,double length)
{
  double tmp,factor,x,y;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0,nstep;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m,l;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,*multi_y,*multi_x,
    *rho_a,*rho_b,*multi_y_b,pos_yb,pos_xb,*longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,
    *multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  
  nstep=element->id->n;

  multi_y=(double*)alloca(sizeof(double)*nstep);
  multi_x=(double*)alloca(sizeof(double)*nstep);
  multi_y_b=(double*)alloca(sizeof(double)*nstep);
  multi_x_b=(double*)alloca(sizeof(double)*nstep);
  longfact=(double*)alloca(sizeof(double)*nstep);
  n_macro=beam->macroparticles;

  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  /* loop over all bunches */
  
  for (k=0;k<beam->bunches;k++){
    
    m=k*beam->slices_per_bunch;
    p=beam->field[0].kick;
    rho_y=beam->rho_y[k];
#ifdef TWODIM
    rho_x=beam->rho_x[k];
#endif
    for (j=0;j<beam->slices_per_bunch;j++){
      rho_y[j]=0.0;
#ifdef TWODIM
      rho_x[j]=0.0;
#endif
      for (i_m=0;i_m<n_macro;i_m++){
	rho_y[j]+=beam->particle[(m+j)*n_macro+i_m].y
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef TWODIM
	rho_x[j]+=beam->particle[(m+j)*n_macro+i_m].x
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
      }
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      kick_y[j]=sumy;
#ifdef TWODIM
      kick_x[j]=sumx;
#endif
    }
    for (j=k-1;j>=0;j--){
      force=beam->force[k-j]+beam->slices_per_bunch;
      rho_y=beam->rho_y[j];
#ifdef TWODIM
      rho_x=beam->rho_x[j];
#endif
      for (i=0;i<beam->slices_per_bunch;i++){
	  sumy=kick_y[i];
#ifdef TWODIM
	  sumx=kick_x[i];
#endif
	  for (l=0;l<beam->slices_per_bunch;l++){
	      sumy+=force[l]*rho_y[l];
#ifdef TWODIM
	      sumx+=force[l]*rho_x[l];
#endif
	  }
	  kick_y[i]=sumy;
#ifdef TWODIM
	  kick_x[i]=sumx;
#endif
	  force--;
      }
    }

    for (j=0;j<beam->slices_per_bunch;j++) {
      for (i_m=0;i_m<n_macro;i_m++){
	beam->particle[(m+j)*n_macro+i_m].yp+=
	  kick_y[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#ifdef TWODIM
	beam->particle[(m+j)*n_macro+i_m].xp+=
	  kick_x[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#endif
      }
    }
  }
}

void
dipole_kick_long_fft(CAVITY *element,BEAM *beam,double length)
{
  double tmp,factor,x,y;
  double *kick_y,y_off,tmpy,*kick_x;
  static int step=0,nstep;
  int i,j,k,m,n,j0,m2,n_macro,nf,nadd,ks,i_m,l;
  double rndm_scale,ds,dc;
  double sumy,sumx,*p,pos_x,pos_y,*rho_y,*force,*multi_y,*multi_x,
    *rho_a,*rho_b,*multi_y_b,pos_yb,pos_xb,*longfact,s,c,*af,*bf,
    *rho_x,*rho_yl,*rho_xl,sumyl,sumxl,*along,*rho_a_x,*rho_b_x,
    *multi_x_b,tmpx,
    *rho_a2,*rho_b2,pos_y2,pos_yb2,tmpy2;
  
  nstep=element->id->n;

  multi_y=(double*)alloca(sizeof(double)*nstep);
  multi_x=(double*)alloca(sizeof(double)*nstep);
  multi_y_b=(double*)alloca(sizeof(double)*nstep);
  multi_x_b=(double*)alloca(sizeof(double)*nstep);
  longfact=(double*)alloca(sizeof(double)*nstep);
  n_macro=beam->macroparticles;

  kick_y=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  kick_x=(double*)alloca(sizeof(double)*beam->slices_per_bunch);

  factor=beam->transv_factor*length*beam->factor;

  /* Initialise wakefields */

  /* loop over all bunches */
  
  for (k=0;k<beam->bunches;k++){
    
    m=k*beam->slices_per_bunch;
    p=beam->field[0].kick;
    rho_y=beam->rho_y[k];
#ifdef TWODIM
    rho_x=beam->rho_x[k];
#endif
    for (j=0;j<beam->slices_per_bunch;j++){
      rho_y[j]=0.0;
#ifdef TWODIM
      rho_x[j]=0.0;
#endif
      for (i_m=0;i_m<n_macro;i_m++){
	rho_y[j]+=beam->particle[(m+j)*n_macro+i_m].y
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#ifdef TWODIM
	rho_x[j]+=beam->particle[(m+j)*n_macro+i_m].x
	  *beam->particle[(m+j)*n_macro+i_m].wgt;
#endif
      }
      sumy=0.0;
#ifdef TWODIM
      sumx=0.0;
#endif
      for (i=0;i<=j;i++){
	sumy+=rho_y[i] * *p;
#ifdef TWODIM
	sumx+=rho_x[i] * *p;
#endif
	p++;
      }
      kick_y[j]=sumy;
#ifdef TWODIM
      kick_x[j]=sumx;
#endif
    }
    for (j=k-1;j>=0;j--){
      force=beam->force[k-j]+beam->slices_per_bunch;
      rho_y=beam->rho_y[j];
#ifdef TWODIM
      rho_x=beam->rho_x[j];
#endif
      for (i=0;i<beam->slices_per_bunch;i++){
	  sumy=kick_y[i];
#ifdef TWODIM
	  sumx=kick_x[i];
#endif
	  for (l=0;l<beam->slices_per_bunch;l++){
	      sumy+=force[l]*rho_y[l];
#ifdef TWODIM
	      sumx+=force[l]*rho_x[l];
#endif
	  }
	  kick_y[i]=sumy;
#ifdef TWODIM
	  kick_x[i]=sumx;
#endif
	  force--;
      }
    }

    for (j=0;j<beam->slices_per_bunch;j++) {
      for (i_m=0;i_m<n_macro;i_m++){
	beam->particle[(m+j)*n_macro+i_m].yp+=
	  kick_y[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#ifdef TWODIM
	beam->particle[(m+j)*n_macro+i_m].xp+=
	  kick_x[j]*factor/beam->particle[(m+j)*n_macro+i_m].energy;
#endif
      }
    }
  }
}
