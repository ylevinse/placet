#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double PI;

typedef struct{
  double y;
} PARTICLE;

typedef struct{
  int slices_per_bunch,bunches;
  double* z_position;
  PARTICLE *particle;
} BEAM;

beam_init(BEAM *beam)
{
  int i,j,k=0;
  double z;
  beam->slices_per_bunch=10;
  beam->bunches=2000;
  z=0.0;
  beam->z_position=malloc(sizeof(double)*20000);
  beam->particle=malloc(sizeof(PARTICLE)*20000);
  for (j=0;j<2000;j++){
    for (i=0;i<10;i++){
      beam->z_position[k]=z;
      beam->particle[k].y=sin(2.0*PI*z/30.0);
      z+=1.0;
      k++;
    }
    z+=20.0;
  }
}

wavelength_extract(BEAM *beam,double lambda,double *cr,double *sr)
{
  int ibunch,islice,nbunch,nslice,k=0;
  double c=1.0,s=0.0,c1,s1,c2,s2,sum_s=0.0,sum_c=0.0,dz,tmp;

  nbunch=beam->bunches;
  nslice=beam->slices_per_bunch;
  dz=beam->z_position[1]-beam->z_position[0];
  dz*=2.0*PI/lambda;
  c2=cos(dz);
  s2=sin(dz);
  dz=beam->z_position[nslice]-beam->z_position[nslice-1]
    -(beam->z_position[1]-beam->z_position[0]);
  dz*=2.0*PI/lambda;
  c1=cos(dz);
  s1=sin(dz);
  for (ibunch=0;ibunch<nbunch;ibunch++){
    for (islice=0;islice<nslice;islice++){
      sum_s+=s*beam->particle[k].y;
      sum_c+=c*beam->particle[k].y;
      tmp=c2*s-s2*c;
      c=c2*c+s2*s;
      s=tmp;
      k++;
    }
    tmp=c1*s-s1*c;
    c=c1*c+s1*s;
    s=tmp;
  }
  *cr=sum_c;
  *sr=sum_s;
}

wavelength_spectrum_extract(BEAM *beam,double l0,double l1,int n,int flag,
			    FILE *file)
{
  int i;
  double dl,l,c,s;
  switch(flag){
  case 0:
    if (n>1){
      dl=(l1-l0)/(double)(n-1);
    }
    else{
      dl=0.0;
    }
    if (file) {
      for (i=0;i<n;i++){
	l=l0+dl*(double)i;
	wavelength_extract(beam,l,&c,&s);
	fprintf(file,"%g %g %g\n",l,c,s);
      }
    }
    else{
      for (i=0;i<n;i++){
	l=l0+dl*(double)i;
	wavelength_extract(beam,l,&c,&s);
	printf("%g %g %g\n",l,c,s);
      }
    }
    break;
  }
}

main()
{
  BEAM beam;
  int i;
  PI=acos(-1.0);
  beam_init(&beam);
  wavelength_spectrum_extract(&beam,2.0,100.0,1000,0,NULL);
}
