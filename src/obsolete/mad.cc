#include <cstdlib>
#include <cmath>
#include <cstring>

#include <tcl.h>
#include <tk.h>

#include "placet.h"
#include "structures_def.h"

#include "lattice.h"
#include "girder.h"
#include "matrix.h"
#include "mad.h"

extern INTER_DATA_STRUCT inter_data;

void
MAD::set_file(char *n)
{
  int m;
  if (n) {
    m=strlen(n)+1;
    file=new char [m];
    strcpy(file,n);
  }
  else {
    file=n;
  }
}

void
list_particle(PARTICLE* p)
{
  printf("%g %g %g %g\n",p->x,p->y,p->xp,p->yp);
}

void
mad_get_particle(PARTICLE* p)
{
  //  printf("%g %g %g %g\n",p->x,p->y,p->xp,p->yp);
}

void
mad_open()
{
}

void
mad_close()
{
}

void
mad_begin()
{
}

void
mad_end()
{
}

void
mad_list_preamble()
{
}

void
mad_result_init()
{
}

void
mad_list_particles(BEAM *beam)
{
  int i,j,nmax=10000,nlast,niter,k=0,k2=0;

  niter=(int)((beam->slices-1)/nmax);
  nlast=beam->slices-niter*nmax;
  //  printf("%d %d\n",niter,nlast);
  mad_begin();
  for (j=0;j<niter;j++){
    mad_open();
    mad_list_preamble();
    for (i=0;i<nmax;i++){
      list_particle(beam->particle+k);
      k++;
    }
    mad_close();
    mad_result_init();
    for (i=0;i<nmax;i++){
      mad_get_particle(beam->particle+k2);
      k2++;
    }   
  }
  mad_open();
  mad_list_preamble();
  for (i=0;i<nlast;i++){
    list_particle(beam->particle+k);
      k++;
  }
  mad_close();
  mad_result_init();
  for (i=0;i<nmax;i++){
    mad_get_particle(beam->particle+k2);
    k2++;
  }
  mad_end();
}

void
MAD::set_lattice(char *n)
{
  int m;
  if (n) {
    m=strlen(n)+1;
    lattice=new char [m];
    strcpy(lattice,n);
  }
  else {
    lattice=n;
  }
}

int tk_Mad(ClientData clientdata,Tcl_Interp *interp,int argc,
	   char *argv[])
{
  int error;
  MAD *element;
  char *file=NULL,*lattice=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&file,
     (char*)"name of the lattice file"},
    {(char*)"-lattice",TK_ARGV_STRING,(char*)NULL,(char*)&lattice,
     (char*)"a list describing the lattice"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <Mad>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  if (check_beamline_changeable(interp,argv[0],error)) return TCL_ERROR;

  element=new MAD();
  element->type=SPECIAL;
  element->callback=NULL;
  element->length=0.0;
  element->d_x=0.0;
  element->d_y=0.0;

  if (file) {
    if (lattice) {
      sprintf(interp->result,
	      "only one out of -lattice and -file can be specified in Mad");
      return TCL_ERROR;
    }
  }
  else {
    if (lattice==NULL) {
      sprintf(interp->result,
	      "must specify either -lattice or -file in Mad");
      return TCL_ERROR;
    }
  }

  inter_data.girder->add_element(element);
  element->set_file(file);
  element->set_lattice(lattice);
  return TCL_OK;
}

void
MAD::step_4d_0(BEAM* beam)
{
  int i;

  printf("MAD0: %s %s\n",this->lattice,this->file);
}

void
MAD::step_4d(BEAM* beam)
{
  int i;

  printf("MAD: %s %s\n",this->lattice,this->file);
}

void
MAD::list(FILE* f)
{
  if (this->file) {
    fprintf(f,"Mad -file %s\n",this->get_file());
  }
  else {
    fprintf(f,"Mad -lattice %s\n",this->get_lattice());
  }
}
