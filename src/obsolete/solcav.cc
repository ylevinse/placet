#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "malloc.h"
#include "matrix.h"
#include "placeti3.h"
#include "solcav.h"
#include "solenoid.h"
#include "wakekick.h"

extern WAKEFIELD_DATA_STRUCT wakefield_data;

/* the routine steps the particle PARTICLE a distance length in a solenoidal
   field bz assuming a relative acceleration delta */

#ifdef TWODIM

ELEMENT*
solcav_make(double length,double gradient,double bz,double phase)
{
  ELEMENT *element;
  element=element_make(length,0);
  element->type=SOLCAV;
  element->v1=gradient;
  element->v2=bz;
  element->v5=phase*PI/180.0;
  element->help=(double*)xmalloc(sizeof(double)*2);
  element->help[0]=0.0;
  element->help[1]=0.0;
  return element;
}

void
solcav_particle_step(PARTICLE *particle,double length,double delta,double bz)
{
  double eps=1e-20,rscal=3.33333;
  double phi,xp,yp,s,c,lambda_bar,k_bar;

  xp=particle->xp;
  yp=particle->yp;
  k_bar=bz/(particle->energy*rscal);

  particle->energy*=(1.0+delta);
  //printf("%g %g\n",particle->energy,delta);
  /* if the energy change is significant calculate effective length */
  if (fabs(delta)>1e-6){
    length*=log(1.0+delta)/delta;
  }
  
  /* if phase advance is not significant do normal structure */
  if (eps>k_bar*k_bar){
    particle->x+=xp*length;
    particle->y+=yp*length;
    particle->xp*=1.0/(1.0+delta);
    particle->yp*=1.0/(1.0+delta);
    return;
  }

  phi=length*k_bar;
  s=sin(phi);
  c=cos(phi);
  lambda_bar=1.0/k_bar;
  particle->x+=lambda_bar*(s*xp-(1.0-c)*yp);
  particle->y+=lambda_bar*(s*yp+(1.0-c)*xp);
  particle->xp=c*xp-s*yp;
  particle->yp=c*yp+s*xp;
  particle->xp*=1.0/(1.0+delta);
  particle->yp*=1.0/(1.0+delta);
}

void
cavity_particle_step_end(PARTICLE *particle,double gradient)
{
  particle->xp-=0.5*gradient*particle->x/particle->energy;
  particle->yp-=0.5*gradient*particle->y/particle->energy;
}

void
solcav_particle_step_end(PARTICLE *particle,double gradient,
			 double bz)
{
  solenoid_particle_step_end(particle,bz);
  cavity_particle_step_end(particle,gradient);
}

/*
  the routine tracks the beam matrix through the end fields of a structure
   sigma,
   sigma_xx,
   sigma_xy: beam matrix
   energy: energy of the particle
   d_g: variation of the gradient
*/

void
cavity_sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double d_g)
{
  R_MATRIX r1,r2;

#ifdef END_FIELDS
  r1.r11=1.0;
  r1.r12=0.0;
  r1.r21=-0.5*d_g/energy;
  r1.r22=1.0;
  r2.r11=r1.r11;
  r2.r12=r1.r21;
  r2.r21=r1.r12;
  r2.r22=r1.r22;
  mult_M_M(&r1,sigma,sigma);
  mult_M_M(sigma,&r2,sigma);
  mult_M_M(&r1,sigma_xx,sigma_xx);
  mult_M_M(sigma_xx,&r2,sigma_xx);
  mult_M_M(&r1,sigma_xy,sigma_xy);
  mult_M_M(sigma_xy,&r2,sigma_xy);
#endif
}

/*
  the routine tracks the beam matrix through the end fields of a structure in
  a solenoid
   sigma,
   sigma_xx,
   sigma_xy: beam matrix
   energy: energy of the particle
   d_g: variation of the gradient
   d_bz: variation of the solenoidal field
*/

void
solcav_sigma_step_end_new(R_MATRIX *sigma,R_MATRIX *sigma_xx,
			  R_MATRIX *sigma_xy,
			  double energy,double d_g,double d_bz)
{
  R_MATRIX r1,r2,r3;
  double k,rscal=3.33333;

  k=0.5*d_bz/(energy*rscal);
  r1.r11=1.0;
  r1.r12=0.0;
  r1.r21=-0.5*d_g/energy;
  r1.r22=1.0;
  r2.r11=0.0;
  r2.r12=0.0;
  r2.r21=-k;
  r2.r22=0.0;
  r3.r11=0.0;
  r3.r12=0.0;
  r3.r21=k;
  r3.r22=0.0;
  M_mult_4(&r1,&r2,&r3,&r1,sigma,sigma_xy,sigma_xx);
}

void
solcav_sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double d_g,double d_bz)
{
  /*
    include the matrix due to the solenoidendfields
  */

  solenoid_sigma_step_end(sigma,sigma_xx,sigma_xy,energy,d_bz);

  /*
    Transfer matrix for the structure end fields
  */

  cavity_sigma_step_end(sigma,sigma_xx,sigma_xy,energy,d_g);
}

void
solcav_sigma_step(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double length,double delta,double bz)
{
  double eps=1e-20,rscal=3.33333;
  double phi,s,c,lambda_bar,k_bar;
  R_MATRIX r1,r2,r3;

  k_bar=bz/(energy*rscal);

  /* if the energy change is significant calculate effective length */
  if (delta>1e-6){
    length*=log(1.0+delta)/delta;
  }
  
  phi=length*k_bar;

  /* if phase advance is not significant do normal structure */

  if (eps>phi*phi){
    r1.r11=1.0;
    r1.r12=length;
    r1.r11=0.0;
    r1.r11=1.0/(1.0+delta);
    r2.r11=r1.r11;
    r2.r12=r1.r21;
    r2.r21=r1.r12;
    r2.r22=r1.r22;
    mult_M_M(sigma,&r2,sigma);
    mult_M_M(&r1,sigma,sigma);
    mult_M_M(sigma_xx,&r2,sigma_xx);
    mult_M_M(&r1,sigma_xx,sigma_xx);
    mult_M_M(sigma_xy,&r2,sigma_xy);
    mult_M_M(&r1,sigma_xy,sigma_xy);
    return;
  }

  s=sin(phi);
  c=cos(phi);
  lambda_bar=1.0/k_bar;

  r1.r11=1.0;
  r1.r12=s*lambda_bar;
  r1.r21=0.0;
  r1.r22=c/(1.0+delta);

  r2.r11=0.0;
  r2.r12=-(1.0-c)*lambda_bar;
  r2.r21=0.0;
  r2.r22=-s/(1.0+delta);

  r3.r11=-r2.r11;
  r3.r12=-r2.r12;
  r3.r21=-r2.r21;
  r3.r22=-r2.r22;
  M_mult_4(&r1,&r2,&r3,&r1,sigma,sigma_xy,sigma_xx);
  return;
}

void
solcav_step(ELEMENT *element,BEAM *beam)
{
  double factor,de0,delta,length,half_length,length_i;
  double *de,s_long,c_long,*s_beam,*c_beam;
  int i;

  factor=beam->factor;
  length=element->length;
  half_length=0.5*length;
  length_i=1.0/length;
  de=beam->acc_field[element->field];
  s_beam=beam->s_long[0];
  c_beam=beam->c_long[0];
  s_long=sin(element->v5);
  c_long=cos(element->v5);

/*
  Move beam to the centre of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field[0].de[i/beam->macroparticles];
    delta=half_length*de0/beam->particle[i].energy;
    solcav_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			  beam->particle[i].energy,de0,element->v2);
    solcav_sigma_step(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
		      beam->particle[i].energy,length,2.0*delta,
		      element->v2);
    solcav_particle_step_end(beam->particle+i,de0,element->v2);
    solcav_particle_step(beam->particle+i,half_length,delta,element->v2);
  }

/*
  Apply the wakefield kick
*/

  if(wakefield_data.transv){
    wake_kick(element,beam,length);
  }

/*
  Move beam to the end of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field[0].de[i/beam->macroparticles];
    delta=half_length*de0/beam->particle[i].energy;
    solcav_particle_step(beam->particle+i,half_length,delta,element->v2);
    solcav_particle_step_end(beam->particle+i,-de0,-element->v2);
    solcav_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			  beam->particle[i].energy,-de0,-element->v2);
  }
}

/*
  The routine steps through a cavity using first order transport
*/

void solcav_step_4d_0(ELEMENT *element,BEAM *beam)
{
  double factor,de0,delta,length,half_length,length_i;
  double *de,s_long,c_long,*s_beam,*c_beam;
  int i;

  factor=beam->factor;
  length=element->length;
  half_length=0.5*length;
  length_i=1.0/length;
  de=beam->acc_field[element->field];
  s_beam=beam->s_long[0];
  c_beam=beam->c_long[0];
  s_long=sin(element->v5);
  c_long=cos(element->v5);

/*
  Move beam to the centre of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field[0].de[i/beam->macroparticles];
    delta=half_length*de0/beam->particle[i].energy;
    solcav_particle_step_end(beam->particle+i,de0,element->v2);
    solcav_particle_step(beam->particle+i,half_length,delta,element->v2);
  }

/*
  Apply the wakefield kick
*/

  if(wakefield_data.transv){
    wake_kick(element,beam,length);
  }

/*
  Move beam to the end of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field[0].de[i/beam->macroparticles];
    delta=half_length*de0/beam->particle[i].energy;
    solcav_particle_step(beam->particle+i,half_length,delta,element->v2);
    solcav_particle_step_end(beam->particle+i,-de0,-element->v2);
  }
}

void solcav_step_twiss(ELEMENT *element,BEAM *beam,FILE *file,double step0,int j,
		    double s,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int))
{
  double length;
  int i,nstep,istep;
  double *de,de0,factor,half_length,delta,s_long,c_long,*s_beam,*c_beam;

  factor=beam->factor;
  nstep=(int)(element->length/step0)+1;
  length=element->length/(nstep);
  half_length=0.5*length;
  de=beam->acc_field[element->field];
  s_beam=beam->s_long[0];
  c_beam=beam->c_long[0];
  s_long=sin(element->v5);
  c_long=cos(element->v5);

  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
    +factor*beam->field[0].de[i/beam->macroparticles];
    cavity_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			  beam->particle[i].energy,de0);
  }
  callback(file,beam,j,s,n1,n2);
  for (istep=0;istep<nstep;istep++){
    s+=length;
    for (i=0;i<beam->slices;i++){
      de0=(c_beam[i/beam->macroparticles]*c_long
	   +s_beam[i/beam->macroparticles]*s_long)*element->v1
	+factor*beam->field[0].de[i/beam->macroparticles];
      delta=length*de0/beam->particle[i].energy;
      solenoid_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			      beam->particle[i].energy,element->v2);
      solcav_sigma_step(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			beam->particle[i].energy,length,delta,
			element->v2);
      beam->particle[i].energy+=de0*length;
      solenoid_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			      beam->particle[i].energy,-element->v2);
    }
    callback(file,beam,j,s,n1,n2);
  }
  for (i=0;i<beam->slices;i++){
    de0=(c_beam[i/beam->macroparticles]*c_long
	 +s_beam[i/beam->macroparticles]*s_long)*element->v1
      +factor*beam->field[0].de[i/beam->macroparticles];
    cavity_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			  beam->particle[i].energy,-de0);
  }
}

#else

ELEMENT* solcav_make(double length,double gradient,double bz,double phase)
{
  return NULL;
}

void solcav_particle_step(PARTICLE *particle,double length,double delta,double bz)
{
}

void cavity_particle_step_end(PARTICLE *particle,double gradient)
{
}

void solcav_particle_step_end(PARTICLE *particle,double gradient,
			 double bz)
{
}

void cavity_sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double d_g)
{
}

void solcav_sigma_step_end_new(R_MATRIX *sigma,R_MATRIX *sigma_xx,
			  R_MATRIX *sigma_xy,
			  double energy,double d_g,double d_bz)
{
}

void solcav_sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double d_g,double d_bz)
{
}

void solcav_sigma_step(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double length,double delta,double bz)
{
}

void solcav_step(ELEMENT *element,BEAM *bunch)
{
}

/*
  The routine steps through a cavity using first order transport
*/

void solcav_step_4d_0(ELEMENT *element,BEAM *bunch)
{
}

void solcav_step_twiss(ELEMENT *element,BEAM *beam,FILE *file,double step0,
		       int j,double s,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int))
{
}

#endif
