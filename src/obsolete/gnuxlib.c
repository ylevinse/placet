#include <stdlib.h>
#include <stdio.h>
#include <math.h>

static double xmin=0.0,ymin=0.0;
static double xs=500.0,ys=500.0;

get_xy(char buffer[],int *x,int *y)
{
  double xm=4096.0,ym=4096.0;
  char tmp[5],*point;
  tmp[0]=buffer[1];
  tmp[1]=buffer[2];
  tmp[2]=buffer[3];
  tmp[3]=buffer[4];
  tmp[4]='\0';
  *x=(int)(strtod(tmp,&point)/xm*xs+xmin);
  tmp[0]=buffer[5];
  tmp[1]=buffer[6];
  tmp[2]=buffer[7];
  tmp[3]=buffer[8];
  tmp[4]='\0';
  *y=(int)(ys-strtod(tmp,&point)/ym*ys+ymin);
}

main(int argc,char *argv[])
{
  char buffer[1024],*point,tmp[5],side[3];
  int x,y,i=0,t_adj=1,xold,yold,ltype=0,width=1,scale=0;
  char *colour[]={"red","green","blue"};
  char *wname;

  wname=argv[1];
  switch(argc){
  case 6:
    xmin=strtod(argv[2],&point);
    ymin=strtod(argv[3],&point);
    xs=strtod(argv[4],&point);
    ys=strtod(argv[5],&point);
    break;
  case 4:
    xs=strtod(argv[2],&point);
    ys=strtod(argv[3],&point);
    break;
  case 2:
    scale=1;
    xs=4096.0;
    ys=4096.0;
    break;
  otherwise:
    exit(1);
  }
  point=gets(buffer);
  while (point&&(buffer[0]!='E')){
    switch(buffer[0]){
    case 'L':
      ltype=strtol(buffer+1,&point,10);
      point=gets(buffer);
      break;
    case 'J':
      t_adj=strtol(buffer+1,&point,10);
      point=gets(buffer);
      break;
    case 'M':
      get_xy(buffer,&x,&y);
      xold=x; yold=y;
      if (scale) {
	printf("%s create line %d*$cmx %d*$cmy ",wname,x,y);
      }
      else {
	printf("%s create line %d %d ",wname,x,y);
      }
      point=gets(buffer);
      while(buffer[0]=='V') {
	get_xy(buffer,&x,&y);
	if (scale) {
	  printf("%d*$cmx %d*$cmy ",x,y);
	}
	else {
	  printf("%d %d ",x,y);
	}
	point=gets(buffer);
      }
      if ((xold==x)&&(yold==y)){
	printf("%d %d ",x+1,y+1);
      }
      if (ltype>=0){
	printf("-fill %s\n",colour[ltype]);
      }
      else{
	printf("-width %d\n",abs(ltype)+1);
      }
      break;
    case 'T':
      get_xy(buffer,&x,&y);
      side[1]='\0';
      side[2]='\0';
      switch(t_adj){
	case 1:
	  side[0]='w';
	  break;
	case 2:
	  side[0]='e';
	  break;
      default:
	side[0]='c';
	break;
      }
      if (scale) {
	printf("%s create text %d*$cmx %d*$cmy -text {%s} -anchor %s\n",wname,
	       x,y,buffer+9,side);
      }
      else {
	printf("%s create text %d %d -text {%s} -anchor %s\n",wname,x,y,
	       buffer+9,side);
      }
      point=gets(buffer);
      break;
    default:
      point=gets(buffer);
      break;
    }
  }
  exit(0);
}
