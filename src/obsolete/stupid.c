#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <tcl.h>
#include <tk.h>

#include "memory.h"
#include "histogram.h"

int
Tcl_Histogram_add(ClientData clientData,Tcl_Interp *interp,int argc,
		  char *argv[])
{
  HISTOGRAM *histogram;
  double x,y;
  char *point;

  histogram=(HISTOGRAM*)clientData;
  if (argc!=1) {
    return TCL_ERROR;
  }
  x=strtod(argv[0],&point);
  y=strtod(point,&point);
  histogram_add(histogram,x,y);
  //  printf("%g %g\n",x,y);
  return TCL_OK;
}

int
Tcl_Histogram_integrate(ClientData clientData,Tcl_Interp *interp,int argc,
			char *argv[])
{
  int error=TCL_OK;
  HISTOGRAM *histogram;
  char *point;
  int i=1;
  Tk_ArgvInfo table[]={
    {(char*)"-direction",TK_ARGV_INT,(char*)NULL,
     (char*)&i,
     (char*)"direction of integration (defaults to 1)"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  histogram=(HISTOGRAM*)clientData;
  if (argc!=1) {
    return TCL_ERROR;
  }
  histogram_integrate(histogram,i);
  return TCL_OK;
}

int
Tcl_Histogram_print(ClientData clientData,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  HISTOGRAM *histogram;
  FILE *file;

  histogram=(HISTOGRAM*)clientData;
  switch (argc) {
  case 0:
    file=stdout;
    break;
  case 1:
    file=fopen(argv[0],"w");
    break;
  default:
    return TCL_ERROR;
  }
  histogram_fprint(file,histogram);
  if (argc==1) fclose(file);
  return TCL_OK;
}

int
Tcl_Histogram_eval(ClientData clientData,Tcl_Interp *interp,int argc,
		   char *argv[])
{
  double x,y;
  char *point;

  if (argc<2){
    return TCL_ERROR;
  }
  if (!strcmp("add",argv[1])){
    return Tcl_Histogram_add(clientData,interp,argc-2,argv+2);
  }
  if (!strcmp("print",argv[1])){
    return Tcl_Histogram_print(clientData,interp,argc-2,argv+2);
  }
  if (!strcmp("integrate",argv[1])){
    return Tcl_Histogram_integrate(clientData,interp,argc-2,argv+2);
  }
  // include error handling
  return TCL_ERROR;
}

int
Tcl_Histogram(ClientData clientData,Tcl_Interp *interp,int argc,char *argv[])
{
  HISTOGRAM *histogram;
  double xmin,xmax;
  int n,error;
  int log=0;
  char *point;
  Tk_ArgvInfo table[]={
    {(char*)"-n",TK_ARGV_INT,(char*)NULL,
     (char*)&n,
     (char*)"number of intervals for x-axis"},
    {(char*)"-xmin",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&xmin,
     (char*)"minimal value of x-axis"},
    {(char*)"-xmax",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&xmax,
     (char*)"maximal value of x-axis"},
    {(char*)"-log",TK_ARGV_INT,(char*)NULL,
     (char*)&log,
     (char*)"switch for logarithmic x-axis"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=2){
    Tcl_SetResult(interp,"Too many arguments to <Histogram>",TCL_VOLATILE);
    return TCL_ERROR;
  }
  log++;
  //  printf("name %s\n",argv[1]);
  histogram=malloc(sizeof(HISTOGRAM));
  histogram_make(histogram,log,xmin,xmax,n,argv[1]);
  Tcl_CreateCommand(interp,argv[1],Tcl_Histogram_eval,(ClientData)histogram,
		    (Tcl_CmdDeleteProc*)NULL);
  //  printf("%s\n%g %g %d\n",argv[2],xmin,xmax,n);
  return TCL_OK;
}

int
Histogram_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"Histogram",Tcl_Histogram,(ClientData)NULL,
		    (Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"Histogram","1.0");
  return TCL_OK;
}
