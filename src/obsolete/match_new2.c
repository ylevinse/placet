#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <tcl.h>
#include <tk.h>

#ifndef PI
#define PI 3.141592653589793
#endif

#ifndef max
#define max(a,b) (((a)<(b))?(b):(a))
#endif

#ifndef min
#define min(a,b) (((a)>(b))?(b):(a))
#endif

#define RNDM_EPS 6e-8

static struct
{
  float u[97],c,cd,cm;
  int i,j;
} rndm_match_store;

void rndmst_match(int na1,int na2,int na3, int nb1)
{
  int i,j,nat;
  float s,t;
  rndm_match_store.i=96;
  rndm_match_store.j=32;
  for (i=0;i<97;i++)
    {
      s=0.0;
      t=0.5;
      for (j=0;j<24;j++)
	{
	  nat=(((na1*na2) % 179)*na3) % 179;
	  na1=na2;
	  na2=na3;
	  na3=nat;
	  nb1=(53*nb1+1) % 169;
	  if ((nb1*nat) % 64 >= 32)
	    {
	      s+=t;
	    }
	  t*=0.5;
	}
      rndm_match_store.u[i]=s;
    }
  rndm_match_store.c=    362436.0/16777216.0;
  rndm_match_store.cd=  7654321.0/16777216.0;
  rndm_match_store.cm= 16777213.0/16777216.0;
}

float rndm_match()
{
  float temp;

/* for (;;){*/
  temp=rndm_match_store.u[rndm_match_store.i]-rndm_match_store.u[rndm_match_store.j];
  if (temp<0.0)
    {
      temp+=1.0;
    }
  rndm_match_store.u[rndm_match_store.i]=temp;
  if (--rndm_match_store.i<0) rndm_match_store.i=96;
  if (--rndm_match_store.j<0) rndm_match_store.j=96;
  rndm_match_store.c-=rndm_match_store.cd;
  if (rndm_match_store.c<0.0)
    {
      rndm_match_store.c+=rndm_match_store.cm;
    }
  temp-=rndm_match_store.c;
  if (temp<0.0)
    {
      return temp+1.0;
    }
  else
    {
/*      if (temp>0.0) */
        return temp;
    }
/*}*/
}

static struct{
  int iset;
  float v1,v2;
} gasdev_match_data;

float gasdev_match()
{
  float r;
  if (gasdev_match_data.iset==0)
    {
      for (;;)
	{
          gasdev_match_data.v1=2.0*rndm_match()-1.0;
          gasdev_match_data.v2=2.0*rndm_match()-1.0;
          r=gasdev_match_data.v1*gasdev_match_data.v1
	+gasdev_match_data.v2*gasdev_match_data.v2;
          if ((r<=1.0) && (r!=0))
	    {
	      break;
	    }
        }
      gasdev_match_data.iset=1;
      r=sqrt(-2.0*log((double)r)/r);
      gasdev_match_data.v1*=r;
      gasdev_match_data.v2*=r;
      return gasdev_match_data.v1;
    }
  else
    {
      gasdev_match_data.iset=0;
      return gasdev_match_data.v2;
    }
}

#undef RNDM_EPS

#include "amoeba2.c"

mkquad(double rk,double rl,double r[])
{
    double sign1, sign2;
    int i;
    double rkabs, c1, c2, s1, s2, rksqrt;

    for (i=0;i<16;i++) {
      r[i]=0.0;
    }
    rkabs=fabs(rk);
    rksqrt=sqrt(rkabs);
    if (rk>0.0) {
	c1=cos(rksqrt*rl);
	s1=sin(rksqrt*rl);
	c2=cosh(rksqrt*rl);
	s2=sinh(rksqrt*rl);
	sign1=-1.0;
	sign2=1.0;
    } else {
	c1=cosh(rksqrt*rl);
	s1=sinh(rksqrt*rl);
	c2=cos(rksqrt*rl);
	s2=sin(rksqrt*rl);
	sign1=1.0;
	sign2=-1.0;
    }
    r[0]=c1;
    r[1]=s1/rksqrt;
    r[4]=s1*rksqrt*sign1;
    r[5]=c1;
    r[10]=c2;
    r[11]=s2/rksqrt;
    r[14]=s2*rksqrt*sign2;
    r[15]=c2;
}

mkdrift(double rl,double r[])
{
  int i;
  
  for (i=0;i<16;i++) {
    r[i]=0.0;
  }
  r[0]=1.0;
  r[1]=rl;
  r[5]=1.0;
  r[10]=1.0;
  r[11]=rl;
  r[15]=1.0;
}

mattransp(double r[])
{
  int i,j;
  double tmp;
  for (i=1;i<4;i++){
    for (j=0;j<i;j++){
      tmp=r[i*4+j];
      r[i*4+j]=r[i+4*j];
      r[i+4*j]=tmp;
    }
  }
}

matcopy(double r1[],double r2[])
{
  int i;
  for (i=0;i<16;i++){
    r2[i]=r1[i];
  }
}

matmul(double r1[],double r2[],double r3[])
{
  int i,j,k;
  double rhelp[16],sum;
  
  for (k=0;k<4;k++) {
    for (j=0;j<4;j++) {
      sum=0.0;
      for (i=0;i<4;i++) {
	sum += r1[i+k*4]*r2[j+4*i];
      }
      rhelp[j+4*k]=sum;
    }
  }
  for (j=0;j<16;j++) {
    r3[j]=rhelp[j];
  }
}

print_r(double r[])
{
  int i,j;
  for (j=0;j<4;j++){
    for (i=0;i<4;i++){
      printf("%g ",r[i+j*4]);
    }  
    printf("\n");
  }
  printf("\n");
}

/*
infodo(double rk1,double rl1,double rk2,double rl2,double d,
       double *alpha1,double *alpha2,double *beta1,double *beta2,
       double *rmu1,double *rmu2)
{
    double rtot[16],drift,rd[16],rq[16];
    double cos1, cos2;

    drift=d-0.5*(rl1+rl2);
    mkquad(rk2,rl2,rq);
    mkdrift(drift,rd);
    matmul(rq,rd,rtot);
    matmul(rd,rtot,rtot);
    mkquad(rk1,0.5*rl1,rq);
    matmul(rtot,rq,rtot);
    matmul(rq,rtot,rtot);
    cos1=0.5*(rtot[0]+rtot[5]);
    cos2=0.5*(rtot[10]+rtot[15]);
    if (cos1>1.0) {
      printf("error in infodo: abs(cos1)>1.0\n");
      exit(-1);
    }
    if (cos2>1.0) {
      printf("error in infodo: abs(cos2)>1.0\n");
      exit(-1);
    }
    *rmu1 = acos(cos1);
    *rmu2 = acos(cos2);
    if (rtot[1]<0.0) {
	*rmu1 = 2.0*PI-(*rmu1);
    }
    if (rtot[11]<0.0) {
	*rmu2 = 2.0*PI-(*rmu2);
    }
    *alpha1 = (rtot[0]-rtot[5])*0.5/sin(*rmu1);
    *alpha2 = (rtot[10]-rtot[15])*0.5/sin(*rmu2);
    *beta1 = rtot[1]/sin(*rmu1);
    *beta2 = rtot[11]/sin(*rmu2);
}
*/

struct{
  double alpha1_0,alpha2_0,beta1_0,beta2_0,mu1_0,mu2_0;
  double l[5],d1,d2,k1,l1,k2,l2,k11,k12,k21,k22;
  double rbeam[16];
} match_data;

mkbeam(double alpha1,double beta1,double alpha2,double beta2,double r[])
{
  int i;
  for (i=0;i<16;i++){
    r[i]=0.0;
  }
  r[0]=beta1;
  r[1]=-alpha1;
  r[4]=-alpha1;
  r[5]=(1.0+alpha1*alpha1)/beta1;
  r[10]=beta2;
  r[11]=-alpha2;
  r[14]=-alpha2;
  r[15]=(1.0+alpha2*alpha2)/beta2;
}

void
mkunit(double r[])
{
  int i;
  for(i=0;i<16;i++){
    r[i]=0.0;
  }
  for(i=0;i<4;i++){
    r[i*5]=1.0;
  }
}

struct {
  int *element,*copy;
  double *length,*K,*K_max;
  int n_elements;
  int *change;
  int n_changes;
  double *alpha1_x,*alpha2_x,*beta1_x,*beta2_x,
    *alpha1_y,*alpha2_y,*beta1_y,*beta2_y,
    *energy,*e_wgt;
  int n_energies;
} match_lattice;

double match_section(double x[])
{
  double alpha1,alpha2,beta1,beta2,mu1,mu2,delta=0.0,tmp;
  double rtot[16],rq[16],rd[16],rbeam[16],rtrans[16];
  double s1=1.01,s2=0.99,sl=1e4,w1=0.1,w2=0.1,beta_wgt=0.1,
    alpha_wgt=1.0;
  int do_diff=0;
  int i,j;
  int n_elements,n_changes,n_energies;
  double *K,*length;

  x++;
  n_elements=match_lattice.n_elements;
  n_energies=match_lattice.n_energies;
  n_changes=match_lattice.n_changes;
  K=(double*)alloca(sizeof(double)*n_elements);
  length=(double*)alloca(sizeof(double)*n_elements);
  for (j=0;j<n_elements;j++){
    length[j]=match_lattice.length[j];
    K[j]=match_lattice.K[j];
  }
  for (i=0;i<n_changes;i++){
    if (match_lattice.change[i]<0) {
      length[-match_lattice.change[i]]=x[i];
      if (x[i]<0.0) delta+=x[i]*x[i]*1e2;
    }
    else {
      K[match_lattice.change[i]]=x[i];
    }
  }
  for (i=0;i<n_elements;i++){
    if (match_lattice.copy[i]>=0) K[i]=K[match_lattice.copy[i]];
  }
  /* nominal beam */
  for (i=0;i<n_energies;i++){
    mkunit(rtot);
    for (j=0;j<n_elements;j++){
      switch (match_lattice.element[j]) {
	/* Drift */
      case 0:
	mkdrift(length[j],rd);
	break;
	/* Quadrupole */
      case 1:
	mkquad(K[j]/match_lattice.energy[i],length[j],rd);
	//	printf("K[%d]=%g\n",j,K[j]);
	break;
      }
      //      print_r(rd);
      matmul(rd,rtot,rtot);
    }
    mkbeam(match_lattice.alpha1_x[i],match_lattice.beta1_x[i],
	   match_lattice.alpha1_y[i],match_lattice.beta1_y[i],rbeam);
    matmul(rtot,rbeam,rbeam);
    matcopy(rtot,rtrans);
    mattransp(rtrans);
    matmul(rbeam,rtrans,rbeam);
    tmp=match_lattice.beta2_x[i]-rbeam[0];
    delta+=tmp*tmp*match_lattice.e_wgt[i];
    tmp=match_lattice.beta2_y[i]-rbeam[10];
    delta+=tmp*tmp*match_lattice.e_wgt[i];
    tmp=match_lattice.alpha2_x[i]-rbeam[1];
    delta+=3.0*tmp*tmp*match_lattice.e_wgt[i];
    tmp=match_lattice.alpha2_y[i]-rbeam[11];
    delta+=3.0*tmp*tmp*match_lattice.e_wgt[i];
    //    printf("%g %g %g %g\n",rbeam[0],rbeam[10],rbeam[1],rbeam[11]);
  }
  /*  printf("%g %g %g %g\n",
	 match_lattice.beta2_x[i]-rbeam[0],
	 match_lattice.beta2_y[i]-rbeam[10],
	 match_lattice.alpha2_x[i]-rbeam[1],
	 match_lattice.alpha2_y[i]-rbeam[11]);*/

  if (delta<1e100) return delta;
  return 1e100;
}

match_init(int n_elements, int n_energies, int n_changes)
{
  int i;
  match_lattice.n_elements=n_elements;
  match_lattice.element=(int*)malloc(sizeof(int)*match_lattice.n_elements);
  match_lattice.copy=(int*)malloc(sizeof(int)*match_lattice.n_elements);
  for (i=0;i<match_lattice.n_elements;i++) match_lattice.copy[i]=-1;
  match_lattice.length=(double*)malloc(sizeof(double)
				       *match_lattice.n_elements);
  match_lattice.K=(double*)malloc(sizeof(double)*match_lattice.n_elements);
  match_lattice.n_energies=n_energies;
  match_lattice.energy=(double*)malloc(sizeof(double)
					 *match_lattice.n_energies);
  match_lattice.e_wgt=(double*)malloc(sizeof(double)
				      *match_lattice.n_energies);
  match_lattice.alpha1_x=(double*)malloc(sizeof(double)
					 *match_lattice.n_energies);
  match_lattice.alpha2_x=(double*)malloc(sizeof(double)
					 *match_lattice.n_energies);
  match_lattice.alpha1_y=(double*)malloc(sizeof(double)
					 *match_lattice.n_energies);
  match_lattice.alpha2_y=(double*)malloc(sizeof(double)
					 *match_lattice.n_energies);
  match_lattice.beta1_x=(double*)malloc(sizeof(double)
					*match_lattice.n_energies);
  match_lattice.beta1_y=(double*)malloc(sizeof(double)
					*match_lattice.n_energies);
  match_lattice.beta2_x=(double*)malloc(sizeof(double)
					*match_lattice.n_energies);
  match_lattice.beta2_y=(double*)malloc(sizeof(double)
					*match_lattice.n_energies);
  match_lattice.n_changes=n_changes;
  match_lattice.change=(int*)malloc(sizeof(int)*match_lattice.n_changes);
}

match_fill(int i,double alpha1_x,double beta1_x,double alpha1_y,double beta1_y,
	   double alpha2_x,double beta2_x,double alpha2_y,double beta2_y,
	   double energy,double e_wgt)
{
  match_lattice.alpha1_x[i]=alpha1_x;
  match_lattice.alpha2_x[i]=alpha2_x;
  match_lattice.alpha1_y[i]=alpha1_y;
  match_lattice.alpha2_y[i]=alpha2_y;
  match_lattice.beta1_x[i]=beta1_x;
  match_lattice.beta2_x[i]=beta2_x;
  match_lattice.beta1_y[i]=beta1_y;
  match_lattice.beta2_y[i]=beta2_y;
  match_lattice.energy[i]=energy;
  match_lattice.e_wgt[i]=e_wgt;
}

match(double alpha_x1[],double beta_x1[],double alpha_y1[],double beta_y1[],
      double alpha_x2[],double beta_x2[],double alpha_y2[],double beta_y2[],
      double e[],int n_energies,
      int element[],double length[],double K[],int copy[],int n_elements,
      int change[],int n_change,
      double kf[])
{
  double **x,*y;
  double ymin,yminold;
  int n,i,j,iter,niter=20;

  rndmst_match(12,34,56,78);

  match_init(n_elements,n_energies,n_change);

  for (i=0;i<n_energies;i++) {
    match_fill(i,
	       alpha_x1[i],beta_x1[i],alpha_y1[i],beta_y1[i],
	       alpha_x2[i],beta_x2[i],alpha_y2[i],beta_y2[i],
	       e[i],1.0);
  }
  n=match_lattice.n_changes;

  for (i=0;i<n_change;i++){
    match_lattice.change[i]=change[i];
  }
  for(i=0;i<n_elements;i++){
    match_lattice.element[i]=element[i];
    match_lattice.length[i]=length[i];
    match_lattice.K[i]=K[i];
    match_lattice.copy[i]=copy[i];
  }

  /* prepare data storage */

  y=(double*)malloc(sizeof(double)*n);
  x=(double**)malloc(sizeof(double*)*(n+1));
  for (i=0;i<=n;i++){
    x[i]=(double*)malloc(sizeof(double)*n);
  }

  for (j=0;j<=n;j++){
    for (i=0;i<n;i++){
      if (match_lattice.change[i]<0) {
	x[j][i]=match_lattice.length[-match_lattice.change[i]];
      }
      else {
	x[j][i]=match_lattice.K[match_lattice.change[i]];
      }
      if (i==j) {
	x[j][i]*=0.5;
      }
    }
    x[j]--;
    y[j]=match_section(x[j]);
  }
  y--;
  x--;
  amoeba2(x,y,n,1e-20,&match_section,&niter);
  y++;
  x++;
  for (i=0;i<=n;i++){
    (x[i])++;
  }

  ymin=1e300;
  for (i=0;i<=n;i++){
    for (j=0;j<n;j++){
      //      printf("%g ",x[i][j]);
    }
    //    printf("%g\n",y[i]);
    if (y[i]<ymin){
      ymin=y[i];
      for (j=0;j<n;j++){
	kf[j]=x[i][j];
      }
    }
  }
  return;
  yminold=ymin;
  while ((yminold>1e-5)&&(iter<20)){
    for (j=1;j<=n;j++){
      for (i=1;i<n;i++){
	x[j][i]=kf[i+1];
	x[j][i]*=1.0+0.3*gasdev_match();
      }
      y[j]=match_section(x[j]);
    }

    y--;
    for (i=0;i<n;i++){
      (x[i])--;
    }
    x--;
    amoeba2(x,y,n,1e-20,&match_section,&niter);
    y++;
    for (i=0;i<n;i++){
      (x[i])++;
    }
    x++;
    
    ymin=1e300;
    for (i=0;i<n;i++){
      for (j=0;j<n;j++){
	printf("%g ",x[i][j]);
      }
      printf("%g\n",y[i]);
      if (y[i]<ymin){
	ymin=y[i];
      }
    }
    if (ymin<yminold){
      yminold=ymin;
      ymin=1e300;
      for (i=0;i<n;i++){
	if (y[i]<ymin){
	  ymin=y[i];
	  for (j=0;j<5;j++){
	    kf[j]=x[i][j+1];
	  }
	}
      }
    }
    iter++;
  }
}

int
Tcl_MatchOptics(ClientData clientData,Tcl_Interp *interp,int argc,char *argv[])
{
  Tcl_DString res;
  char **v,**v2,*twiss_list,*lattice_list,*fit_list;
  double *alpha_x1,*beta_x1,*alpha_y1,*beta_y1,
    *alpha_x2,*beta_x2,*alpha_y2,*beta_y2,*e,
    *length,*K,
    *kf;
  char buffer[255];
  int *element,*copy,*change;
  int i,n,n2,error,n_elements,n_change;
  Tk_ArgvInfo table[]={
    {(char*)"-lattice",TK_ARGV_STRING,(char*)NULL,
     (char*)&lattice_list,
     (char*)"Lattice description"},
    {(char*)"-twiss",TK_ARGV_STRING,(char*)NULL,
     (char*)&twiss_list,
     (char*)"List of Twiss parameters for different energies"},
    {(char*)"-fit_list",TK_ARGV_STRING,(char*)NULL,
     (char*)&fit_list,
     (char*)"List of parameters to be fitted"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1){
    Tcl_SetResult(interp,"Too many arguments to <",TCL_VOLATILE);
    Tcl_AppendResult(interp,argv[0],">:\n",NULL);
    for (i=1;i<argc;i++){
      Tcl_AppendResult(interp,argv[i],NULL);
    }
    return TCL_ERROR;
  } 

  /*
    Read the Twiss parameters for the different energies
   */

  if (error=Tcl_SplitList(interp,twiss_list,&n,&v)) return error;
  alpha_x1=(double*)alloca(sizeof(double)*n);
  beta_x1=(double*)alloca(sizeof(double)*n);
  alpha_y1=(double*)alloca(sizeof(double)*n);
  beta_y1=(double*)alloca(sizeof(double)*n);
  alpha_x2=(double*)alloca(sizeof(double)*n);
  beta_x2=(double*)alloca(sizeof(double)*n);
  alpha_y2=(double*)alloca(sizeof(double)*n);
  beta_y2=(double*)alloca(sizeof(double)*n);
  e=(double*)alloca(sizeof(double)*n);
  for(i=0;i<n;i++){
    if (error=Tcl_SplitList(interp,v[i],&n2,&v2)) return error;
    if (n2!=9) {
      return TCL_ERROR;
    }
    if (error=Tcl_GetDouble(interp,v2[0],alpha_x1+i)) return error;
    if (error=Tcl_GetDouble(interp,v2[1],beta_x1+i)) return error;
    if (error=Tcl_GetDouble(interp,v2[2],alpha_y1+i)) return error;
    if (error=Tcl_GetDouble(interp,v2[3],beta_y1+i)) return error;
    if (error=Tcl_GetDouble(interp,v2[4],alpha_x2+i)) return error;
    if (error=Tcl_GetDouble(interp,v2[5],beta_x2+i)) return error;
    if (error=Tcl_GetDouble(interp,v2[6],alpha_y2+i)) return error;
    if (error=Tcl_GetDouble(interp,v2[7],beta_y2+i)) return error;
    if (error=Tcl_GetDouble(interp,v2[8],e+i)) return error;
  }

  /*
    Read the lattice parameters
   */
  if (error=Tcl_SplitList(interp,lattice_list,&n_elements,&v)) return error;
  element=(int*)alloca(sizeof(int)*n_elements);
  copy=(int*)alloca(sizeof(int)*n_elements);
  length=(double*)alloca(sizeof(double)*n_elements);
  K=(double*)alloca(sizeof(double)*n_elements);
  for (i=0;i<n_elements;i++){
    if (error=Tcl_SplitList(interp,v[i],&n2,&v2)) return error;
    if (!strcmp(v2[0],"q")) {
      if (n2<3) {
	return TCL_ERROR;
      }
      element[i]=1;
      if (error=Tcl_GetDouble(interp,v2[1],length+i)) return error;
      if (error=Tcl_GetDouble(interp,v2[2],K+i)) return error;
      copy[i]=-1;
      if (n2>3) {
	if (n2!=4) {
	  return TCL_ERROR;
	}
	if (error=Tcl_GetInt(interp,v2[3],copy+i)) return error;	
      }
      else {
      }
    }
    else {
      if (!strcmp(v2[0],"d")) {
	element[i]=0;
	copy[i]=-1;
	if (n2!=2) {
	  return TCL_ERROR;
	}
      if (error=Tcl_GetDouble(interp,v2[1],length+i)) return error;
      }
      else {
	return TCL_ERROR;
      }
    }
  }
  if (error=Tcl_SplitList(interp,fit_list,&n_change,&v)) return error;
  change=(int*)alloca(sizeof(int)*n_change);
  kf=(double*)alloca(sizeof(double)*n_change);
  for (i=0;i<n_change;i++){
      if (error=Tcl_GetInt(interp,v[i],change+i)) return error;
  }

  match(alpha_x1,beta_x1,alpha_y1,beta_y1,alpha_x2,beta_x2,alpha_y2,beta_y2,
	e,n,
	element,length,K,copy,n_elements,
	change,n_change,
	kf);
  Tcl_DStringInit(&res);
  for (i=0;i<n_change;i++) {
    sprintf(buffer,"%g",kf[i]);
    Tcl_DStringAppendElement(&res,buffer);
  }
  Tcl_DStringResult(interp,&res);
  return TCL_OK;
}

int
Optics_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"MatchOptics",Tcl_MatchOptics,(ClientData)NULL,
		    (Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"Optics","1.0");
  return TCL_OK;
}
