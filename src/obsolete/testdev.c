int tk_SetRfGradientSingle(ClientData clientdata,Tcl_Interp *interp,int argc,
			   char *argv[])
{
  int error=0;
  char *point,**v,*name;
  BEAM *beam;
  int jt=0,i,j,nb,np,m,n,nm,k,l;
  double *de,*de0,de_centre,de_scale,derf,debeam,lambda,*s_beam,*c_beam,c,
    dephase,z_pos;

  if (argc>4){
    Tcl_SetResult(interp,"Too many arguments to <SetRfGradientSingle>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (argc<4){
    Tcl_SetResult(interp,"Not enough arguments to <SetRfGradientSingle>",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }

  beam=get_beam(argv[1]);
  l=strtol(argv[2],&point,10);
  if(error=Tcl_SplitList(interp,argv[3],&n,&v)) return error;
  np=beam->slices_per_bunch;
  nm=beam->macroparticles;
  nb=beam->bunches;
  de0=beam->field->de;
  lambda=injector_data.lambda;
  s_beam=beam->s_long[l];
  c_beam=beam->c_long[l];
  for (j=0;j<nb;j++){
    m=j*np;
    if (j<n) {
      derf=strtod(v[j],&point);
      debeam=strtod(point,&point);
      dephase=strtod(point,&point);
    }
    for (i=0;i<np;i++) {
      z_pos=(dephase/180.0+beam->z_position[m+i]*1e-6/lambda*2.0)*PI;
      c=cos(z_pos);
      c_beam[m+i]=derf*c;
      s_beam[m+i]=derf*sin(z_pos);
      if (l==0) {
	de0[m+i]+=debeam*cos(beam->z_position[m+i]*1e-6/lambda*2.0*PI);
      }
    }
  }
  free((char*)v);
  return TCL_OK;
}
