#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <tcl.h>
#include <tk.h>

double
function_eval(char *f,double *x,int n)
{
  int i;
  return 0.0;
}

int
tk_MinimiseEmittance(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  char *f=NULL,*s=NULL,*d=NULL,**v;
  double *x,**x1,*xbase,fres,tol=1e-12;
  int n,i,j,nd,iter;
  int list=0;
  char buffer[100];
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: MinimiseEmittance options"},
    {(char*)"-function",TK_ARGV_STRING,(char*)NULL,
     (char*)&f,
     (char*)"Function to minimise"},
    {(char*)"-start",TK_ARGV_STRING,(char*)NULL,
     (char*)&s,
     (char*)"Starting point"},
    {(char*)"-delta",TK_ARGV_STRING,(char*)NULL,
     (char*)&d,
     (char*)"Differences from starting point"},
    {(char*)"-tolerance",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&tol,
     (char*)"Tolerance for stopping"},
    {(char*)"-list",TK_ARGV_INT,(char*)NULL,
     (char*)&list,
     (char*)"If not 0 function takes a list as argument"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }
  if (!f) {
    Tcl_SetResult(interp,"You need to provide a function for the minimisation",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if(error=Tcl_SplitList(interp,s,&n,&v)) {
    return error;
  }
  x=(double*)alloca(sizeof(double)*n);
  x1=(double**)alloca(sizeof(double*)*(n+1));
  xbase=(double*)alloca(sizeof(double)*(n+1)*n);
  for (i=0;i<n;i++){
    x[i]=strtod(v[i],NULL);
  }
  for (i=0;i<n+1;i++){
    x1[i]=xbase+n*i;
    for (j=0;j<n;j++){
      x1[i][j]=x[j];
    }
  }
  if (d) {
    free(v);
    if(error=Tcl_SplitList(interp,d,&nd,&v)) {
      return error;
    }
    if (nd!=n) {
      Tcl_AppendResult(interp,"mist",NULL);
      return TCL_ERROR;
    }
    if (0) {
      for (i=0;i<n;i++){
	for (j=0;j<n;j++){
	  x1[i+1][j]=0.0;
	}
	x1[i+1][i]=strtod(v[i],NULL);
      }
    }
    else {
      for (i=0;i<n;i++){
	x1[i+1][i]+=strtod(v[i],NULL);
      }
    }
  }
  else {
    if (0) {
      for (i=0;i<n;i++){
	for (j=0;j<n;j++){
	  x1[i+1][j]=0.0;
	}
	x1[i+1][i]=1.0;
      }
    }
    else {
      for (i=0;i<n;i++){
	x1[i+1][i]+=1.0;
      }
    }
  }
  double (*fun)(double *);
  if (list) {
    function_common.p=function_eval_init(interp,f,n);
    fun=&func;
  }
  else {
    function_common.p=function_eval_init2(interp,f,n);
    fun=&func2;
  }
  function_common.n=n;
  function_common.f=f;
  function_common.interp=interp;
  POWELL powe(fun);
  powe.powell(xbase,xbase+n,n,n,tol,&iter,&fres);
  Tcl_ResetResult(interp);
  for(i=0;i<n;i++){
    snprintf(buffer,100,"%g",x1[0][i]);
    //    printf("%g\n",x1[0][i]);
    fflush(stdout);
    Tcl_AppendElement(interp,buffer);
  }
  free(v);
  return TCL_OK;
}

int
tk_MinimiseFunction(ClientData clientdata,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  char *f=NULL,*s=NULL,**v,**v1;
  char buffer[1000];
  double *x;
  int n,m,i;
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: MatchList options"},
    {(char*)"-function",TK_ARGV_STRING,(char*)NULL,
     (char*)&f,
     (char*)"Function to minimise"},
    {(char*)"-start",TK_ARGV_STRING,(char*)NULL,
     (char*)&s,
     (char*)"Starting point"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,TK_ARGV_NO_LEFTOVERS))
      !=TCL_OK){
    return error;
  }
  if (!f) {
    Tcl_SetResult(interp,"You need to provide a function for the minimisation",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if(error=Tcl_SplitList(interp,s,&n,&v)) {
    return error;
  }
  sprintf(buffer,"%s {",f);
  for (i=0;i<n;i++){
    sprintf(buffer,"%s ",v[i]);
  }
  sprintf(buffer,"}");
  printf("%s\n",buffer);
  Tcl_Eval(interp,buffer);
  printf(">> %s\n",interp->result);
  return TCL_OK;
}

int
Minimise_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"MinimiseFunction",tk_MinimiseFunction,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"Minimise","0.1");
  return TCL_OK;
}
