#ifndef TWODIM
static double max_y(const BEAM *bunch)
{
  double ymax=0.0;
  for (int i=0;i<bunch->slices;i++){
    if (bunch->particle[i].wgt > 2*std::numeric_limits<double>::epsilon()) {
      double tmp=fabs(bunch->particle[i].y)+switches.envelope_cut*sqrt(bunch->sigma[i].r11);
      if (tmp>ymax)
	ymax=tmp;
    }
  }
  return ymax;
}

static double max_y_el(const BEAM *bunch,ELEMENT *el)
{
  double ymax=0.0;
  for (int i=0;i<bunch->slices;i++){
    if( bunch->particle[i].wgt > 2*std::numeric_limits<double>::epsilon() ) {
      double tmp=fabs(bunch->particle[i].y-el->offset.y)+
	switches.envelope_cut*sqrt(bunch->sigma[i].r11);
      if (tmp>ymax) { 
	ymax=tmp;
	//	i_max = i;
      }
    }
  }
  //placet_printf(INFO,"max value at slice: %d\n", i_max);
  return ymax;
}
#endif
