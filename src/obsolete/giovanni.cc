void resistive_intermediate(double yp,double zp,double diffz,double blength,double gam,double*Np,double* deltayarray, double &kickyrwt,double &kickyrwf)const;
{
	      // 	      kickyrwt_xa=0.;
	      // 	      kickyrwt_xb=0.;
	      // unused:    int ind_slice=int(floor((blength/2-zp)/diffz));
	      /*	      
  	      kickyrwt=0.;
	      kickyrwf=0.;
	      kickyrwt2=0.;
	      kickyrwf2=0.;
	      double bb_loc;
	      for(int imain=0;imain<=imax;imain++) {
	      bb_loc=in_height-(in_height-fin_height)/(double)imax*(double)imain;
	      s0_loc=pow((2*bbbb_loc/Z0/sigma),1./3.);
	      double cap_gam_loc=C_LIGHT*tau/s0_loc;
	      findpoles(cap_gam_loc);
	      double coeffrwt=64*RE*(taper_length/(double)imax)/gam/(bbbb_loc*bbbb_loc);
	      double coeffrwt2=64*RE*(taper_length/(double)imax)/gam*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
	      // kmain=0 is the head of the bunch 
	      for(int kmain=0;kmain<ind_slice;kmain++) {
	      kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*
	      _res_sr_ac2;
	      kickyrwt_xa+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain])*
	      _res_sr_ac2;
	      kickyrwt_xb+=Np[kmain]*coeffrwt*(0.411)*
	      _res_sr_ac2;
	      kickyrwt2+=Np[kmain]*coeffrwt2*(0.822*deltayarray[kmain]+0.411*yp)*
	      intmed22(blength/2-zp, (double)kmain*diffz, blength);
	      } 
	      }
	      
	      if(flat_length>0.) {
	      double coeffrwf=32*RE*flat_length/gam/(bbbb_loc*bbbb_loc);
	      double coeffrwf2=32*RE*flat_length/gam*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
	      for(int kmain=0;kmain<ind_slice;kmain++) {   
	      kickyrwf+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain]+0.411*yp)*
	      _res_sr_ac2;
	      kickyrwf2+=Np[kmain]*coeffrwf2*(0.822*deltayarray[kmain]+0.411*yp)*
	      intmed22(blength/2-zp, (double)kmain*diffz, blength);
	      }   
	      }
	      */
}
