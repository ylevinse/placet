#include <stdlib.h>
#include <stdio.h>

#include <tcl.h>
#include <tk.h>

double pos[200000],sigma[200000];

int tk_Temp(ClientData clientdata,Tcl_Interp *interp,int argc,
	    char *argv[])
{
  int start=0,bunch,i,j,k,ds=3;
  double ymin,ymax;
  char *point,buffer[1000];
  bunch=strtol(argv[1],&point,10);
  k=bunch*101;
  Tcl_Eval(interp,".c delete all");
  for (j=0;j<5;j++){
    for (i=0;i<101;i++){
      ymin=100.0-0.01*(pos[k]+sigma[k]);
      ymax=100.0-0.01*(pos[k]-sigma[k]);
      sprintf(buffer,".c create rectangle %d %d %d %d -fill red\n",
	      start,(int)ymin,start+ds,(int)ymax); 
      Tcl_Eval(interp,buffer);
      k++;
      start+=ds;
    }
    start+=50;
  }
  return TCL_OK;
}

int Tcl_AppInit(Tcl_Interp *interp)
{
  if (Tcl_Init(interp)){
    printf("%s\n",interp->result);
  }

  if (Tk_Init(interp)){
    printf("%s\n",interp->result);
  }

  Tcl_CreateCommand(interp,"Temp",&tk_Temp,NULL,
		    NULL);

  return TCL_OK;
}

main(int argc,char *argv[])
{
  int n=0;
  FILE *file;
  char buffer[1000],*point;
  file=fopen("bunchlong.dat","r");
  while (fgets(buffer,1000,file)){
    pos[n]=strtod(buffer,&point);
    pos[n]=strtod(point,&point);
    pos[n]=strtod(point,&point);
    sigma[n]=strtod(point,&point);
    sigma[n]=strtod(point,&point);
    n++;
  }
  printf("%d\n",n);
  fclose(file);
  Tk_Main(argc,argv,&Tcl_AppInit);
}
