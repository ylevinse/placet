#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define N 10000

main()
{
  int i,j,n,m,k=0,l;
  double *x,*sx,*c,*mx;
  FILE *f;
  char *p,b[N];

  f=fopen("test.data","r");
  p=fgets(b,N,f);
  n=strtol(p,&p,10);
  m=strtol(p,&p,10);
  printf("%d %d\n",n,m);
  x=(double*)malloc(sizeof(double)*n*m);
  mx=(double*)malloc(sizeof(double)*m);
  sx=(double*)malloc(sizeof(double)*m);
  c=(double*)malloc(sizeof(double)*m*m);
  for(j=0;j<m;++j){
    sx[j]=0.0;
  }
  for (i=0;i<n;++i){
    p=fgets(b,N,f);
    for(j=0;j<m;++j){
      x[k]=strtod(p,&p);
      mx[j]+=x[k++];
    }
  }
  fclose(f);
  for(j=0;j<m;++j){
    mx[j]/=(double)n;
  }
  k=0;
  for (i=0;i<n;++i){
    for(j=0;j<m;++j){
      x[k++]-=mx[j];
    }
  }
  for (i=0;i<m*m;++i){
    c[i]=0.0;
  }
  for (i=0;i<n;++i){
    l=0;
    for(j=0;j<m;++j){
      for(k=0;k<m;++k){
	c[l++]+=x[i*m+j]*x[i*m+k];
      }
    }
  }
  for (i=0;i<m*m;++i){
    c[i]/=(double)n;
  }
  for(i=0;i<m;++i){
    sx[i]=sqrt(c[i*(m+1)]);
    if (sx[i]<1e-10) sx[i]=1e-10;
    printf("%g\n",sx[i]);
  }
  k=0;
  for (i=0;i<m;++i) {
    for (j=0;j<m-1;++j) {
      printf("\t%12g ",c[k++]/(sx[i]*sx[j]));
    }
    printf("\t%12g\n",c[k++]/(sx[i]*sx[j]));
  }
  exit(0);
}
