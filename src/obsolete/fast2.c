#include <stdlib.h>
#include <stdio.h>
#include <math.h>

main(int argc,char *argv[])
{
  FILE *f;
  char line[1024],*point;
  int n,i,k=0;
  double x,xmin;

  f=fopen(argv[1],"r");
  n=strtol(argv[2],&point,10);
  xmin=strtod(argv[3],&point);
  while (point=fgets(line,1024,f)){
    for (i=0;i<=n;i++){
      x=strtod(point,&point);
    }
    if ((x*xmin>0.0)&&(fabs(x)>fabs(xmin))){
      printf("%d\n",k);
    }
    k++;
  }
  fclose(f);
}
