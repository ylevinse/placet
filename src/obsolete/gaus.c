#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tmp.c"

double gauss_fill_f(double x)
{
  return exp(-0.5*x*x);
}

gauss_fill(double x[],int n,double xmin,double xmax)
{
  int i;
  double xlow,xhigh,dx,xscale=0.0;

  xlow=xmin;
  dx=(xmax-xmin)/(double)n;
  for (i=0;i<n;i++){
      xhigh=xlow+dx;
      qromb(&gauss_fill_f,xlow,xhigh,&(x[i]));
      xscale+=x[i];
      xlow=xhigh;
  }
  xscale=1.0/xscale;
  for (i=0;i<n;i++){
    x[i]*=xscale;
  }
}

main()
{
  int i;
  double x[20];
  gauss_fill(x,20,-3.0,3.0);
  for (i=0;i<20;i++){
    printf("%d %g\n",i,x[i]);
  }
}
