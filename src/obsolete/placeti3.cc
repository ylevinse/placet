void adapt(BEAMLINE *beamline,BEAM *bunch,double response[])
{
  int i,nquad;
  double c=0.00002,move;
  BPM **bpm=beamline->bpm;
  nquad=beamline->n_quad;
  bunch_track_0(beamline,bunch,0,beamline->n_elements);
  for (i=1;i<nquad-2;i++){
    move=-c*((bpm[i+1])->get_bpm_y_reading_exact()+(bpm[i-1])->get_bpm_y_reading_exact()
	     -(2.0+response[i+1])*(bpm[i])->get_bpm_y_reading_exact());
/*    printf("%g %g\n",beamline->quad[i+1]->offset.y,move);*/
    //element_add_offset(beamline->quad[i+1],-move,0.0);
    beamline->quad[i+1]->vary_vcorr(-move);
  }
}

void adapt_find_response(BEAMLINE *beamline,BEAM *bunch0,BEAM *workbunch,
		    double response[])
{
  int i,ipos,j;
  double bpm0;
  ELEMENT **element;
  ipos=0;
  element=beamline->element;
  for (i=0;i<beamline->n_quad;i++){
    j=ipos;
    while (!element[j]->is_quad()){
      element[j++]->track(bunch0);
    }
    bunch_copy_0(bunch0,workbunch);
    ipos=j;
    while (!element[j]->is_bpm()){
      element[j++]->track_0(workbunch);
    }
    element[j]->track_0(workbunch);
    bpm0=element[j]->get_bpm_y_reading_exact();
    j=ipos;
    //element_add_offset(element[j],1.0,0.0);
    element[j]->vary_vcorr(1.0);
    bunch_copy_0(bunch0,workbunch);
    while (!element[j]->is_bpm()){
      element[j++]->track_0(workbunch);
    }
    element[j]->track_0(workbunch);
    response[i]=-(element[j]->get_bpm_y_reading_exact()-bpm0);
    //element_add_offset(element[ipos],-1.0,0.0);
    element[ipos]->vary_vcorr(-1.0);
    element[ipos++]->track(bunch0);
  }
  bunch_track(beamline,bunch0,ipos,beamline->n_elements);
}

void beam_delete_total(BEAM *bunch)
{
  int i;
  //printf("EA: entering beam delete total\n");
  quad_kick_data_delete(bunch->quad_kick_data);
  free(bunch->drive_data->bf);
  free(bunch->drive_data->af);
  free(bunch->drive_data->blong);
  free(bunch->drive_data->along);
  free(bunch->drive_data->factor_long);
  free(bunch->drive_data->factor_kick);
#ifdef TWODIM
  free(bunch->drive_data->xposb);
  free(bunch->drive_data->xpos);
#endif
  free(bunch->drive_data->yoff);
  free(bunch->drive_data->yposb);
  free(bunch->drive_data->ypos);
  free(bunch->drive_data);
  free(bunch->z_position);
  free(bunch->help);
  free(bunch->force);
  for (i=0;i<bunch->bunches;i++){
    free(bunch->rho_y[i]);
  }
  free(bunch->rho_y);
  free(bunch->sigma);
#ifdef TWODIM
  for (i=0;i<bunch->bunches;i++){
    free(bunch->rho_x[i]);
  }
  free(bunch->rho_x);
  free(bunch->sigma_xx);
  free(bunch->sigma_xy);
#endif
  free(bunch->particle);
  field_delete(bunch->field);
  free(bunch->particle_number);
#ifdef HTGEN
  free(bunch->particle_number_sec);
#endif
  free(bunch);
}

void beam_info(BEAM *bunch,int n1,int n2,int flag,double yoff,double ypoff,
	       BEAMINFO *beaminfo)
{
  int i;
  double sum11,sum12,sum22,y,yp,ym,ypm,wgtsum,wgt,ymax,tmp;

  ym=0.0;
  ypm=0.0;
  wgtsum=0.0;
  ymax=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    ym+=wgt*bunch->particle[i].y;
    ypm+=wgt*bunch->particle[i].yp;
    if((tmp=fabs(bunch->particle[i].y))>ymax) ymax=tmp;
  }
  ym/=wgtsum;
  ypm/=wgtsum;
  beaminfo->y_mean=ym;
  beaminfo->yp_mean=ypm;
  beaminfo->y_max=ymax;
  switch(flag){
  case 0:
    break;
  case 1:
    ym=yoff;
    ypm=ypoff;
    break;
  }
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    y=bunch->particle[i].y-ym;
    yp=bunch->particle[i].yp-ypm;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
    sum12+=(y*yp+bunch->sigma[i].r21)*wgt;
    sum22+=(yp*yp+bunch->sigma[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  beaminfo->sigma_y=sqrt(sum11);
  return;
}

void beam_plot(BEAMLINE *beamline,BEAM *bunch,char name[])
{
  int i,j=0;
  FILE *file;
  if (name!=NULL){
    file=fopen(name,"w");
  }
  else{
    file=stdout;
  }
  for (i=0;i<beamline->n_elements;i++){
    beamline->element[i]->track_0(bunch);
    if (beamline->element[i]->is_bpm()){
      fprintf(file,"%d %g %g\n",j++,bunch_get_offset_y(bunch),
	      bunch_get_offset_yp(bunch));
    }
  }
  fclose(file);
}

void beamline_bin_divide_strange(BEAMLINE *beamline,int /*nq*/,int /*interleave*/,
			    BIN **bin,int *bin_number)
{
  int i,quad,bpm;
  *bin_number=0;
  for (i=2;i<beamline->n_elements;i++){
    if (beamline->element[i]->is_quad()){
      bin[*bin_number]=bin_make(1,1);
      quad=i; bpm=i-2;
      bin_set_elements(bin[*bin_number],&quad,1,&bpm,1,1);
      bin[*bin_number]->start=bpm;
      bin[*bin_number]->stop=quad+1;
      bin[*bin_number]->a0[0]=-1.0;
      bin[*bin_number]->a1[0]=0.0;
      bin[*bin_number]->a2[0]=0.0;
      bin_finish(bin[*bin_number],0);
      (*bin_number)++;
    }
  }
  placet_printf(INFO,"beamline divided into %d bins\n",*bin_number);
}

void beamline_bpm_switch_on(BEAMLINE *beamline) 	 
{ 	 
  int i,n; 	 
  ELEMENT **element; 	 
  n=beamline->n_elements; 	 
  element=beamline->element; 	 
  for (i=0;i<n;i++){ 	 
    if (element[i]->type==BPMDRIFT){ 	 
      element[i]->type=BPM; 	 
    } 	 
  } 	 
}

double beam_size_axis(BEAM *bunch)
{
  int i,n;
  double sum11,y,ym,wgtsum,wgt;

  n=bunch->slices;
  ym=0.0;
  wgtsum=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    ym+=wgt*bunch->particle[i].y;
  }
  sum11=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    y=bunch->particle[i].y;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
  }
  sum11/=wgtsum;
  return sqrt(sum11);
}

void
bin_correct(BEAMLINE *beamline,int flag,BIN *bin) // old version, now bin_correct_wgt renamed to bin_correct
{
  bin_correct_wgt(beamline,flag,bin);
  return;
  /*
  int i;
  double cor[BIN_MAX_QUAD],step;
#ifdef TWODIM
  double cor_x[BIN_MAX_QUAD],step_x;
#endif

  if (flag){
    if (bin->a){
      correct(bin->a,bin->indx,bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,
	      corr.w,bin->b0,bin->b1,bin->b2,bin->t0,bin->t1,bin->t2,
	      corr.pwgt,cor);
    }
#ifdef TWODIM
    if (bin->a_x){
      correct(bin->a_x,bin->indx_x,bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,
	      bin->nbpm_x,corr.w,bin->b0_x,bin->b1_x,bin->b2_x,
	      bin->t0_x,bin->t1_x,bin->t2_x,corr.pwgt,cor_x);
    }
#endif
  }
  else{
    if (bin->a){
      correct_0(bin->a,bin->indx,bin->a0,bin->nq,bin->nbpm,bin->b0,
		bin->t0,corr.pwgt,cor);
    }
#ifdef TWODIM
    if (bin->a_x){
      correct_0(bin->a_x,bin->indx_x,bin->a0_x,bin->nq_x,bin->nbpm_x,bin->b0_x,
		bin->t0_x,corr.pwgt,cor_x);
    }
#endif
  }
  if (bin->a){
    for (i=0;i<bin->qlast;i++){
      step=(cor[i]+bin->cor0[i])*bin->gain;
      bin->cor0[i]+=cor[i];
      bin->cor0[i]*=bin->dtau;
      if (errors.quad_step_size>0.0){
	step=rint(step/errors.quad_step_size)*errors.quad_step_size;
      }
      step+=errors.quad_move_res*gasdev();
      //element_add_offset(beamline->element[bin->quad[i]],step,0.0);
      beamline->element[bin->quad[i]]->correct_y(step);
    }
  }
#ifdef TWODIM
  if(bin->a_x){
    for(i=0;i<bin->qlast;i++){
      step_x=(cor_x[i]+bin->cor0_x[i])*bin->gain;
      bin->cor0_x[i]+=cor_x[i];
      bin->cor0_x[i]*=bin->dtau;
      if (errors.quad_step_size>0.0){
	step_x=rint(step_x/errors.quad_step_size)*errors.quad_step_size;
      }
      step_x+=errors.quad_move_res*gasdev();
      //element_add_offset_x(beamline->element[bin->quad_x[i]],step_x,0.0);
      beamline->element[bin->quad_x[i]]->correct_x(step_x);
    }
  }
#endif
  */
}

void bin_correct_2(BEAMLINE *beamline,int flag,BIN *bin)
{
  int i;
  double cor[BIN_MAX_QUAD],step;
  if (flag){
    correct_2(bin->a,bin->indx,bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,
	      corr.w,bin->b0,bin->b1,bin->b2,corr.pwgt,cor);
  }
  else{
    correct_0(bin->a,bin->indx,bin->a0,bin->nq,bin->nbpm,bin->b0,bin->t0,
	      corr.pwgt,cor);
  }
  for (i=0;i<bin->qlast;i++){
    step=cor[i]*bin->gain;
    if (errors.quad_step_size>0.0){
	step=rint(step/errors.quad_step_size)*errors.quad_step_size;
    }
    step+=errors.quad_move_res*gasdev();
    //element_add_offset(beamline->element[bin->quad[i]],step,0.0);
    beamline->element[bin->quad[i]]->correct_y(step);
    /*   if (i>0) element_add_offset(beamline->element[bin->quad[i]-2],
		       step,0.0);
		       */
  }
}

/* has to be completed */

/*
void bin_correct_ballistic(BEAMLINE *beamline,BIN *bin)
{
  double cor[BIN_MAX_BPM],step;
  int i;

  correct_0(bin->a,bin->indx,bin->a0,bin->nq,bin->nbpm,bin->b0,bin->t0,
	    corr.pwgt,cor);
  step=cor[0]*bin->gain+errors.quad_move_res*gasdev();
  element_add_offset(beamline->element[bin->quad[0]],step,0.0);
  for (i=0;i<bin->nbpm;i++){
    element_add_offset(beamline->element[bin->bpm[i]],
		       beamline->element[bin->bpm[i]]->info1,0.0);
  }
}
*/

/*
** As the previous function implements now the 'abstract' correction schema, 
** bin_correct_dipole() is no longer required
*/

void bin_correct_dipole(BEAMLINE *beamline,int flag,BIN *bin)
{
  int i;
  double cor[BIN_MAX_QUAD],step;
#ifdef TWODIM
  double cor_x[BIN_MAX_QUAD],step_x;
#endif

  if (flag){
    if (bin->a){
      correct(bin->a,bin->indx,bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,
	      corr.w,bin->b0,bin->b1,bin->b2,bin->t0,bin->t1,bin->t2,
	      corr.pwgt,cor);
    }
#ifdef TWODIM
    if (bin->a_x){
      correct(bin->a_x,bin->indx_x,bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,
	      bin->nbpm_x,corr.w,bin->b0_x,bin->b1_x,bin->b2_x,
	      bin->t0_x,bin->t1_x,bin->t2_x,corr.pwgt,cor_x);
    }
#endif
  }
  else{
    if (bin->a){
      correct_0(bin->a,bin->indx,bin->a0,bin->nq,bin->nbpm,bin->b0,bin->t0,
		corr.pwgt,cor);
    }
#ifdef TWODIM
    if (bin->a_x){
      correct_0(bin->a_x,bin->indx_x,bin->a0_x,bin->nq_x,bin->nbpm_x,bin->b0_x,
		bin->t0_x,corr.pwgt,cor_x);
    }
#endif
  }
  if (bin->a){
    for (i=0;i<bin->qlast;i++){
      step=cor[i]*bin->gain;
      dipole_add_strength_y(beamline->element[bin->quad[i]],step);
    }
  }
#ifdef TWODIM
  if(bin->a_x){
    for(i=0;i<bin->qlast;i++){
      step_x=cor_x[i]*bin->gain;
      dipole_add_strength_x(beamline->element[bin->quad[i]],step_x);
    }
  }
#endif
}

/* Corrects the positions of the BPMs two elements in front of the quadrupoles
 */

void bin_correct_nlc_2(BEAMLINE *beamline,int flag,BIN *bin)
{
  int i;
  double cor[BIN_MAX_QUAD],step;
  if (flag){
    correct(bin->a,bin->indx,bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,corr.w,
	    bin->b0,bin->b1,bin->b2,bin->t0,bin->t1,bin->t2,corr.pwgt,cor);
  }
  else{
    correct_0(bin->a,bin->indx,bin->a0,bin->nq,bin->nbpm,bin->b0,bin->t0,
	      corr.pwgt,cor);
  }
  for (i=0;i<bin->qlast;i++){
    step=cor[i]*bin->gain;
    if (errors.quad_step_size>0.0){
	step=rint(step/errors.quad_step_size)*errors.quad_step_size;
    }
    step+=errors.quad_move_res*gasdev();
    //if (i>0) element_add_offset(beamline->element[bin->quad[i]-2],step,0.0);
    if (i>0) beamline->element[bin->quad[i]-2]->correct_y(step);
  }
}

void bin_finish(BIN *bin,int flag) // old version, now bin_finish_wgt renamed to bin_finish
{
  bin_finish_wgt(bin,flag);
  return;
  /*
  int i,n;
  double *a0,*a1,*a2,d;
  double *a0_x,*a1_x,*a2_x;
  n=bin->nq*bin->nbpm;
  a0=bin->a0;
  a1=bin->a1;
  a2=bin->a2;
  if (flag){
    for (i=0;i<bin->nbpm;++i) {
      bin->t1[i]-=bin->t0[i];
      bin->t2[i]-=bin->t0[i];
#ifdef TWODIM
      bin->t1_x[i]-=bin->t0_x[i];
      bin->t2_x[i]-=bin->t0_x[i];
#endif
    }
    for (i=0;i<n;i++){
      *a1++ -= *a0;
      *a2++ -= *a0++;
    }
  }

#ifdef TWODIM
  n=bin->nq_x*bin->nbpm_x;
  a0_x=bin->a0_x;
  a1_x=bin->a1_x;
  a2_x=bin->a2_x;
  if (flag){
    for (i=0;i<n;i++){
      *a1_x++ -= *a0_x;
      *a2_x++ -= *a0_x++;
    }
  }
#endif

  if (flag){
    if (bin->a){
      hessian(bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,corr.pwgt,corr.w0,
	      corr.w,corr.w2,bin->a);
    }
#ifdef TWODIM
    if(bin->a_x){
      hessian(bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,bin->nbpm_x,corr.pwgt,
	      corr.w0,corr.w,corr.w2,bin->a_x);
    }
#endif
  }
  else{
    if (bin->a){
      hessian_0(bin->a0,bin->nq,bin->nbpm,corr.pwgt,bin->a);
    }
#ifdef TWODIM
    if(bin->a_x){
      hessian_0(bin->a0_x,bin->nq_x,bin->nbpm_x,corr.pwgt,bin->a_x);
    }
#endif
  }
  if(bin->a){
    ludcmp(bin->a,bin->nq,bin->indx,&d);
  }
#ifdef TWODIM
  if(bin->a_x){
    ludcmp(bin->a_x,bin->nq_x,bin->indx_x,&d);
  }
#endif
  */
}

void bin_finish_2(BIN *bin,int /*flag*/)
{
  double d;
  hessian_2(bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,corr.pwgt,corr.w0,
	    corr.w,bin->a);
  ludcmp(bin->a,bin->nq,bin->indx,&d);
}

void bin_finish_train(BIN *bin,int flag)
{
  int i,n;
  double *a0,*a1,*a2,d;
  double *a0_x,*a1_x,*a2_x;
  n=bin->nq*bin->nbpm;
  a0=bin->a0;
  a1=bin->a1;
  a2=bin->a2;
  if (flag){
    for (i=0;i<n;i++){
      *a1++ -= *a0;
      *a2++ -= *a0++;
    }
  }

#ifdef TWODIM
  n=bin->nq_x*bin->nbpm_x;
  a0_x=bin->a0_x;
  a1_x=bin->a1_x;
  a2_x=bin->a2_x;
  if (flag){
    for (i=0;i<n;i++){
      *a1_x++ -= *a0_x;
      *a2_x++ -= *a0_x++;
    }
  }
#endif

  if (flag){
    if (bin->a){
      hessian(bin->a0,bin->a1,bin->a2,bin->nq,bin->nbpm,corr.pwgt,corr.w0,
	      corr.w,corr.w2,bin->a);
    }
#ifdef TWODIM
    if(bin->a_x){
      hessian(bin->a0_x,bin->a1_x,bin->a2_x,bin->nq_x,bin->nbpm_x,corr.pwgt,
	      corr.w0,corr.w,corr.w2,bin->a_x);
    }
#endif
  }
  else{
    if (bin->a){
      hessian_0(bin->a0,bin->nq,bin->nbpm,corr.pwgt,bin->a);
    }
#ifdef TWODIM
    if(bin->a_x){
      hessian_0(bin->a0_x,bin->nq_x,bin->nbpm_x,corr.pwgt,bin->a_x);
    }
#endif
  }
  if(bin->a){
    ludcmp(bin->a,bin->nq,bin->indx,&d);
  }
#ifdef TWODIM
  if(bin->a_x){
    ludcmp(bin->a_x,bin->nq_x,bin->indx_x,&d);
  }
#endif
}

void bin_list(BEAMLINE *beamline,BIN **bin,int nbin,char *name)
{
  FILE *file;
  int i;

  if (name==NULL){
    file=stdout;
  }
  else{
    file=fopen(name,"w");
  }
  for (i=0;i<nbin;i++){
    bin_print(file,beamline,bin[i],i);
  }
  if (name!=NULL){
    fclose(file);
  }
}

void
bin_measure_drive(BEAMLINE *beamline,BEAM *bunch,int flag,BIN *bin)
{
  const double dy=1.0;
  double *b;
#ifdef TWODIM
  double *b_x;
#endif
  int *quad,*bpm,nq,nb;
  int i,iq,ib;
  ELEMENT **element;

  nb=bin->nbpm;
  bpm=bin->bpm;
  element=beamline->element;

  bunch_track_0(beamline,bunch,bin->start,bin->stop);

  for (ib=0;ib<nb;ib++){
    bin->b0[ib]=element[bpm[ib]]->help[0]+errors.bpm_resolution*gasdev();
    bin->b1[ib]=element[bpm[ib]]->help[1]+errors.bpm_resolution*gasdev();
    bin->b2[ib]=element[bpm[ib]]->help[2]+errors.bpm_resolution*gasdev();
  }
#ifdef TWODIM
  nb=bin->nbpm_x;
  bpm=bin->bpm_x;
  for (ib=0;ib<nb;ib++){
    bin->b0_x[ib]=element[bpm[ib]]->help[0]+errors.bpm_resolution*gasdev();
    bin->b1_x[ib]=element[bpm[ib]]->help[1]+errors.bpm_resolution*gasdev();
    bin->b2_x[ib]=element[bpm[ib]]->help[2]+errors.bpm_resolution*gasdev();
  }
#endif
}

void
bin_measure_full(BEAMLINE *beamline,BEAM *bunch,int flag,BIN *bin)
{
  //  const double dy=1.0;
  double *b;
#ifdef TWODIM
  double *b_x;
#endif
  int *bpm,nb;
  int ib;
  ELEMENT **element;

  switch (flag){
  case 0:
    b=bin->b0;
#ifdef TWODIM
    b_x=bin->b0_x;
#endif
    break;
  case 1:
    b=bin->b1;
#ifdef TWODIM
    b_x=bin->b1_x;
#endif
    break;
  case 2:
    b=bin->b2;
#ifdef TWODIM
    b_x=bin->b2_x;
#endif
    break;
  default:
    placet_cout << ERROR << "bin_measure_full: flag not existing: " << flag << endmsg;
    exit(1);
    break;
  }
  nb=bin->nbpm;
  bpm=bin->bpm;
  element=beamline->element;

  bunch_track_0(beamline,bunch,bin->start,bin->stop);

  for (ib=0;ib<nb;ib++){
    b[ib]=element[bpm[ib]]->get_bpm_y_reading_exact()+errors.bpm_resolution*gasdev();
  }
#ifdef TWODIM
  nb=bin->nbpm_x;
  bpm=bin->bpm_x;
  for (ib=0;ib<nb;ib++){
    b_x[ib]=element[bpm[ib]]->get_bpm_x_reading_exact()+errors.bpm_resolution*gasdev();
  }
#endif
}

void
bin_measure_full_noacc(BEAMLINE *beamline,BEAM *bunch,int flag,BIN *bin)
{
  //  const double dy=1.0;
  double *b;
#ifdef TWODIM
  double *b_x;
#endif
  int *bpm,nb;
  int ib;
  ELEMENT **element;

  switch (flag){
  case 0:
    b=bin->b0;
#ifdef TWODIM
    b_x=bin->b0_x;
#endif
    break;
  case 1:
    b=bin->b1;
#ifdef TWODIM
    b_x=bin->b1_x;
#endif
    break;
  case 2:
    b=bin->b2;
#ifdef TWODIM
    b_x=bin->b2_x;
#endif
    break;
  default:
    placet_cout << ERROR << "bin_measure_full_noacc: flag not existing: " << flag << endmsg;
    exit(1);
    break;
  }
  nb=bin->nbpm;
  bpm=bin->bpm;
  element=beamline->element;

  bunch_track_0_noacc(beamline,bunch,bin->start,bin->stop);

  for (ib=0;ib<nb;ib++){
    b[ib]=element[bpm[ib]]->get_bpm_y_reading_exact()+errors.bpm_resolution*gasdev();
  }
#ifdef TWODIM
  nb=bin->nbpm_x;
  bpm=bin->bpm_x;
  for (ib=0;ib<nb;ib++){
    b_x[ib]=element[bpm[ib]]->get_bpm_x_reading_exact()+errors.bpm_resolution*gasdev();
  }
#endif
}

void bin_measure_noerr(BEAMLINE *beamline,BEAM *bunch,int flag,BIN *bin)
{
  //  const double dy=1.0;
  double *b;
  int *bpm,nb;
  int ib;
  ELEMENT **element;

  switch (flag){
  case 0:
    b=bin->b0;
    break;
  case 1:
    b=bin->b1;
    break;
  case 2:
    b=bin->b2;
    break;
  }
  nb=bin->nbpm;
  bpm=bin->bpm;
  element=beamline->element;

  bunch_track_0(beamline,bunch,bin->start,bin->stop);

  for (ib=0;ib<nb;ib++){
    b[ib]=element[bpm[ib]]->get_bpm_y_reading_exact();
  }
}

void bin_measure_zero(BEAMLINE * /*beamline*/,BEAM * /*bunch*/,int flag,BIN *bin)
{
  //  const double dy=1.0;
  double *b;
  int nb;
  int ib;

  switch (flag){
  case 0:
    b=bin->b0;
    break;
  case 1:
    b=bin->b1;
    break;
  case 2:
    b=bin->b2;
    break;
  }
  nb=bin->nbpm;

  for (ib=0;ib<nb;ib++){
    b[ib]=0.0;
  }
}

void bin_response_drive(BEAMLINE *beamline,BEAM *bunch0,BEAM *bunch,int flag,
		   BIN *bin)
{
  const double dy=5.0;
  double *a0,*a1,*a2,bpm0[BIN_MAX_BPM],bpm1[BIN_MAX_BPM],bpm2[BIN_MAX_BPM],
    dy_inv;
#ifdef TWODIM
  double *a_x,bpm0_x[BIN_MAX_BPM];
#endif
  int *quad,*bpm,nq,nb;
  int i,iq,ib;
  ELEMENT **element;

  nb=bin->nbpm;
  nq=bin->nq;
  bpm=bin->bpm;
  quad=bin->quad;
  element=beamline->element;

  bunch_copy_0(bunch0,bunch);
  bunch_track_0(beamline,bunch,bin->start,bin->stop);

  a0=bin->a0;
  a1=bin->a1;
  a2=bin->a2;
  for (ib=0;ib<nb;ib++){
    bpm0[ib]=element[bpm[ib]]->help[0];
    bpm1[ib]=element[bpm[ib]]->help[1];
    bpm2[ib]=element[bpm[ib]]->help[2];
  }

  dy_inv=1.0/dy;
  for (iq=0;iq<nq;iq++){
    element_add_offset(element[quad[iq]],dy,0.0);
    bunch_copy_0(bunch0,bunch);
    bunch_track_0(beamline,bunch,bin->start,bin->stop);
    for (ib=0;ib<nb;ib++){
      *a0++=(element[bpm[ib]]->help[0]-bpm0[ib])*dy_inv;
      *a1++=(element[bpm[ib]]->help[1]-bpm1[ib])*dy_inv;
      *a2++=(element[bpm[ib]]->help[2]-bpm2[ib])*dy_inv;
    }
    element_add_offset(element[quad[iq]],-dy,0.0);
  }
}

void bmag_plot(BEAMLINE *beamline,char *beam_name,char *name)
{
  ELEMENT **element;
  int i,j=0,nc;
  double eps,beta,alpha,eps0,beta0,alpha0;
  FILE *file;
  double s=0.0,e;
  BEAM *bunch0,*bunch;

  file=fopen(beam_name,"r");
  fclose(file);
  bunch=bunch_remake(bunch0); // bunch0 is uninitialised! JS
  beam_copy(bunch0,bunch);
  nc=bunch->slices/2;
  file=fopen(name,"w");
  element=beamline->element;
  while(element[j]!=NULL){
    s+=element[j]->get_length();
    if (QUADRUPOLE *quad=element[j]->quad_ptr()){
      quad->step_half(bunch);
      eps=0.0;
      beta=0.0;
      alpha=0.0;
      for (i=0;i<bunch->slices;i++){
	eps+=(bunch->sigma[i].r11*bunch->sigma[i].r22
	  -bunch->sigma[i].r12*bunch->sigma[i].r21)*bunch->particle[i].wgt;
	beta+=bunch->sigma[i].r11*bunch->particle[i].wgt;
	alpha-=bunch->sigma[i].r12*bunch->particle[i].wgt;
      }
      eps=sqrt(eps);
      beta/=eps;
      alpha/=eps;
      eps0=(bunch->sigma[nc].r11*bunch->sigma[nc].r22
	  -bunch->sigma[nc].r12*bunch->sigma[nc].r21);
      beta0=bunch->sigma[nc].r11;
      alpha0=-bunch->sigma[nc].r12;
      eps0=sqrt(eps0);
      beta0/=eps0;
      alpha0/=eps0;
      e=bunch->particle[nc].energy;
      fprintf(file,"%d %g %g %g %g %g %g %g\n",j,s,e,beta0,alpha0,beta,alpha,
	      quad->get_strength()/quad->get_length());
      quad->step_half(bunch);
    }
    else{
      element[j]->track(bunch);
//      element_step(element[j],bunch);
    }
    j++;
  }
  fclose(file);
  beam_delete(bunch);
}

void bpm_filter_prepare(double filter[],BEAM *bunch)
{
  int i;
  double sum=0.0;
  for (i=0;i<bunch->slices;i++){
    sum+=filter[i]*bunch->particle[i].wgt;
  }
  sum=1.0/sum;
  for (i=0;i<bunch->slices;i++){
    filter[i]*=sum;
  }
}

void bpm_plot(BEAMLINE *beamline,char name[])
{
  int i,j=0;
  FILE *file;
  if (name!=NULL){
    file=fopen(name,"w");
  }
  else{
    file=stdout;
  }
  for (i=0;i<beamline->n_elements;i++){
    if (beamline->element[i]->is_bpm()){
      fprintf(file,"%d %g %g\n",j++,beamline->element[i]->offset.y,
	      beamline->element[i]->offset.yp);
    }
  }
  fclose(file);
}

void bpm_prepare_samples(BEAMLINE *beamline,int nsamples)
{
  int i;
  for (i=0;i<beamline->n_elements;i++){
    if (beamline->element[i]->is_bpm()){
      if (beamline->element[i]->help){
	beamline->element[i]->help=(double*)realloc(beamline->element[i]->help,
						    sizeof(double)*nsamples);
      }
      else{
	beamline->element[i]->help=(double*)xmalloc(sizeof(double)*nsamples);
      }
    }
  }
}

/* NO error in bpm resolution */

void bpm_step(ELEMENT *element,BEAM *bunch)
{
  double tmp,wsum=0.0,sum;
  R_MATRIX r,rt;
  int i,j,imin,imax,n;
  ((BPM*)element)->info1=0.0;
#ifdef TWODIM
  ((BPM*)element)->info2=0.0;
#endif
  printf("help\n");
  fflush(stdout);
  exit(13);
  r.r11=1.0;
  r.r12=element->get_length();
  r.r21=0.0;
  r.r22=1.0;
  rt.r11=1.0;
  rt.r12=0.0;
  rt.r21=element->get_length();
  rt.r22=1.0;
  switch (bunch->drive_data->do_filter) {
  case 1:
    for (i=0;i<bunch->slices;i++){
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
      tmp=bunch->particle[i].wgt*bunch->filter[i];
      wsum+=tmp;
      element->info1+=bunch->particle[i].y*tmp;
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
#ifdef TWODIM
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
      element->info2+=bunch->particle[i].x*tmp;
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
#endif
      mult_M_M(&r,bunch->sigma+i,bunch->sigma+i);
#ifdef TWODIM
      mult_M_M(&r,bunch->sigma_xx+i,bunch->sigma_xx+i);
      mult_M_M(&r,bunch->sigma_xy+i,bunch->sigma_xy+i);
#endif
      //      transpose_M(&r);
      mult_M_M(bunch->sigma+i,&rt,bunch->sigma+i);
#ifdef TWODIM
      mult_M_M(bunch->sigma_xx+i,&rt,bunch->sigma_xx+i);
      mult_M_M(bunch->sigma_xy+i,&rt,bunch->sigma_xy+i);
#endif
    }
    break;
  case 0:
    for (i=0;i<bunch->slices;i++){
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
      tmp=bunch->particle[i].wgt;
      wsum+=tmp;
      element->info1+=bunch->particle[i].y*tmp;
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
#ifdef TWODIM
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
      element->info2+=bunch->particle[i].x*tmp;
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
#endif
      mult_M_M(&r,bunch->sigma+i,bunch->sigma+i);
#ifdef TWODIM
      mult_M_M(&r,bunch->sigma_xx+i,bunch->sigma_xx+i);
      mult_M_M(&r,bunch->sigma_xy+i,bunch->sigma_xy+i);
#endif
      //      transpose_M(&r);
      mult_M_M(bunch->sigma+i,&rt,bunch->sigma+i);
#ifdef TWODIM
      mult_M_M(bunch->sigma_xx+i,&rt,bunch->sigma_xx+i);
      mult_M_M(bunch->sigma_xy+i,&rt,bunch->sigma_xy+i);
#endif
    }
    break;
  case 2:
  // scd work (adapt for twodim)
    for (i=0;i<bunch->slices;i++){
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
      mult_M_M(&r,bunch->sigma+i,bunch->sigma+i);
#ifdef TWODIM
      mult_M_M(&r,bunch->sigma_xx+i,bunch->sigma_xx+i);
      mult_M_M(&r,bunch->sigma_xy+i,bunch->sigma_xy+i);
#endif
      //      transpose_M(&r);
      mult_M_M(bunch->sigma+i,&rt,bunch->sigma+i);
#ifdef TWODIM
      mult_M_M(bunch->sigma_xx+i,&rt,bunch->sigma_xx+i);
      mult_M_M(bunch->sigma_xy+i,&rt,bunch->sigma_xy+i);
#endif
    }
    for (j=0;j<bunch->drive_data->bpm_samples;j++){
      wsum=0.0;
      sum=0.0;
      imin=bunch->drive_data->bpm_min[j];
      imax=bunch->drive_data->bpm_max[j];
      for (i=imin;i<imax;i++){
	tmp=bunch->particle[i].wgt;
	wsum+=tmp;
	sum+=tmp*bunch->particle[i].y;
      }
      element->help[j]=sum/wsum;
//printf("bpm %g %g ",element->help[j],wsum);
    }
//printf("\n");
    for (i=0;i<bunch->slices;i++){
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
#ifdef TWODIM
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
      element->info2+=bunch->particle[i].x*bunch->particle[i].wgt;
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
#endif
    }
    break;
  }
  element->info1/=wsum;
#ifdef TWODIM
  element->info2/=wsum;
#endif
}

void bpm_step_0(ELEMENT *element,BEAM *bunch)
{
  double tmp,wsum=0.0,sum;
  int i,j,imin,imax,n;
  printf("help\n");
  fflush(stdout);
  exit(13);
  element->info1=0.0;
#ifdef TWODIM
  element->info2=0.0;
#endif
  switch (bunch->drive_data->do_filter){
  case 1:
    for (i=0;i<bunch->slices;i++){
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
      tmp=bunch->particle[i].wgt*bunch->filter[i];
      wsum+=tmp;
      element->info1+=bunch->particle[i].y*tmp;
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
#ifdef TWODIM
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
      element->info2+=bunch->particle[i].x*tmp;
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
#endif
    }
    break;
  case 0:
    for (i=0;i<bunch->slices;i++){
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
      tmp=bunch->particle[i].wgt;
      wsum+=tmp;
      element->info1+=bunch->particle[i].y*tmp;
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
#ifdef TWODIM
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
      element->info2+=bunch->particle[i].x*bunch->particle[i].wgt;
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
#endif
    }
    break;
  case 2:
    // scd work (adapt for twodim)
    for (i=0;i<bunch->slices;i++){
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
    }
    for (j=0;j<bunch->drive_data->bpm_samples;j++){
      wsum=0.0;
      sum=0.0;
      imin=bunch->drive_data->bpm_min[j];
      imax=bunch->drive_data->bpm_max[j];
      for (i=imin;i<imax;i++){
	tmp=bunch->particle[i].wgt;
	wsum+=tmp;
	sum+=bunch->particle[i].y*tmp;
      }
      element->help[j]=sum/wsum;
    }
    for (i=0;i<bunch->slices;i++){
      (bunch->particle)[i].y+=0.5*element->get_length()*(bunch->particle)[i].yp;
#ifdef TWODIM
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
      element->info2+=bunch->particle[i].x*bunch->particle[i].wgt;
      (bunch->particle)[i].x+=0.5*element->get_length()*(bunch->particle)[i].xp;
#endif
    }
    element->info1=wsum*(element->help[2]+element->help[1]+element->help[0]);
    break;
  }
  element->info1/=wsum;
#ifdef TWODIM
  element->info2/=wsum;
#endif
}

/* NO error in bpm resolution */

/*scdadapt to TWODIM*/

void bpm_stepback_0(ELEMENT *element,BEAM *bunch)
{
  if (BPM *bpm=element->bpm_ptr()) bpm->set_y_position(0.0);
  for (int i=0;i<bunch->slices;i++){
    bunch->particle[i].y-=0.5*element->get_length()*(bunch->particle)[i].yp;
    if (BPM *bpm=element->bpm_ptr()) bpm->add_y_position(bunch->particle[i].y*bunch->particle[i].wgt);
    bunch->particle[i].y-=0.5*element->get_length()*(bunch->particle)[i].yp;
  }
}

void bunch_bpm_filter_init(BEAM * /*bunch*/)
{
}

void bunch_bpm_filter_add(BEAM *bunch,double f,int i)
{
  bunch->filter[i]=f;
}

void bunch_bpm_filter_finish(BEAM *bunch)
{
  int i;
  double sum=0.0;
  bunch->drive_data->do_filter=1;
  for (i=0;i<bunch->slices;i++){
    sum+=bunch->filter[i]*bunch->particle[i].wgt;
  }
  sum=1.0/sum;
  for (i=0;i<bunch->slices;i++){
    bunch->filter[i]*=sum;
  }
}

void bunch_bpm_filter_on(BEAM *bunch)
{
  bunch->drive_data->do_filter=1;
}

void bunch_bpm_filter_off(BEAM *bunch)
{
  bunch->drive_data->do_filter=0;
}

void bunch_copy_0(BEAM *bunch1,BEAM *bunch2) 	 
{ 	 
  int i; 	 
  if (bunch1->slices!=bunch2->slices){ 	 
    my_error("bunch_copy_0"); 	 
  } 	 
  /*
    Check if the quadrupolar wakefields are used. 	 
    If so, beam matrices will be modyfied, so they need to be copied. 	 
  */ 	 
  if (quad_wake.on) { 	 
    beam_copy(bunch1,bunch2); 	 
    return; 	 
  } 	 
  memcpy(bunch2->particle,bunch1->particle,sizeof(PARTICLE)*bunch1->slices); 	 
  //  for (i=0;i<bunch1->slices;i++){ 	 
  //    bunch2->particle[i]=bunch1->particle[i]; 	 
  //  } 	 
  /* 	 
	 If longitudinal position variation is allowed, copy the corresponding 	 
	 data. Otherwise just copy the pointer to the data. 	 
  */ 	 
#ifdef LONGITUDINAL 	 
  memcpy(bunch2->z_position,bunch1->z_position,sizeof(double)*bunch1->slices); 	 
#else 	 
  bunch2->z_position=bunch1->z_position; 	 
#endif 	 
  bunch2->rho_y=bunch1->rho_y; 	 
#ifdef TWODIM 	 
  bunch2->rho_x=bunch1->rho_x; 	 
#endif 	 
  bunch2->force=bunch1->force; 	 
  bunch2->help=bunch1->help; 	 
  bunch2->filter=bunch1->filter; 	 
  bunch2->field=bunch1->field; 	 
  bunch2->acc_field=bunch1->acc_field; 	 
  bunch2->factor=bunch1->factor; 	 
  bunch2->drive_data=bunch1->drive_data; 	 
  bunch2->which_field=bunch1->which_field; 	 
  bunch2->s_long=bunch1->s_long; 	 
  bunch2->c_long=bunch1->c_long; 	 
  bunch2->particle_number=bunch1->particle_number; 	 
}

void bunch_feedback(ELEMENT *element,BEAM *bunch)
{
  double y=0.0,yp=0.0,wgt=0.0;
  int i,n;
  n=bunch->slices;
  for (i=0;i<n;i++){
    y+=bunch->particle[i].y*bunch->particle[i].wgt;
    yp+=bunch->particle[i].yp*bunch->particle[i].wgt;
    wgt+=bunch->particle[i].wgt;
  }
  y/=wgt;
  y-=element->offset.y;
  yp/=wgt;
  yp-=element->offset.yp;
  for (i=0;i<n;i++){
    bunch->particle[i].y-=y;
    bunch->particle[i].yp-=yp;
  }
}

void bunch_fprint(FILE *file,BEAM *bunch)
{
  int i;
  for (i=0;i<bunch->slices;i++){
    fprintf(file,"%d %g %g\n",i,(bunch->particle)[i].y,(bunch->particle)[i].yp);
  }
}

void bunch_plot_save(BEAM *bunch)
{
  int i;
  FILE *bunchfile;
  for (i=0;i<bunch->slices;i++){
    fprintf(bunchfile,"%g %g\n",bunch->particle[i].y,
	    sqrt(bunch->sigma[i].r11));
  }
}

void bunch_reload(BEAMLINE *beamline,BEAM *bunch,char *name)
{
  FILE *file;
  int i,n,k,nmacro=1,j;
  double wgtsum,ecut=2.0,sigma_e=0.014*9.0,delta,fact;
  float tmp1,tmp2,tmp3;
  double betax,alphax,epsx;
  double betay,alphay,epsy;
  char buffer[100],*point;

  file=fopen("match.ini","r");
  if (!file) std::cout << "No such file: match.ini" << std::endl;
  point=fgets(buffer,100,file);
  epsx=strtod(point,&point);
  betax=strtod(point,&point);
  alphax=strtod(point,&point);
  point=fgets(buffer,100,file);
  epsy=strtod(point,&point);
  betay=strtod(point,&point);
  alphay=strtod(point,&point);
  epsx*=EMASS/9.0*1e12*EMITT_UNIT;
  epsy*=EMASS/9.0*1e12*EMITT_UNIT;
  fclose(file);
  file=fopen(name,"r");
  if (!file) std::cout << "No such file: " << name << std::endl;
  fscanf(file,"%d",&n);
  /* scd to update for number of phases */
  fscanf(file,"%g",&tmp1);
  wgtsum=0.0;
  for (i=0;i<n;i++){
    fscanf(file,"%g %g %g",&tmp1,&tmp2,&tmp3);
    for (k=0;k<nmacro;k++){
      if (nmacro>1){
	delta=ecut*(2.0*k/(double)(nmacro-1)-1.0);
	bunch_set_slice_energy(bunch,0,i,k,9.0+delta*sigma_e);
	fact=exp(-0.5*delta*delta);
      }
      else{
	bunch_set_slice_energy(bunch,0,i,k,9.0);
	fact=1.0;
      }
      bunch_set_slice_wgt(bunch,0,i,k,(double)tmp3*fact);
      wgtsum+=tmp3*fact;
      for (j=0;j<beamline->n_phases;j++){
	bunch->acc_field[j][i]=rf_data.gradient*rf_data.cavlength
	*cos(tmp1/(rf_data.lambda*1e6)*2.0*PI+beamline->phase[j]*PI/180.0);
      }
#ifdef TWODIM
      bunch_set_slice_x(bunch,0,i,k,0.0);
      bunch_set_slice_xp(bunch,0,i,k,0.0);
      bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,epsx);
      bunch_set_sigma_xy(bunch,0,i,k);
#endif
      bunch_set_slice_y(bunch,0,i,k,zero_point);
      bunch_set_slice_yp(bunch,0,i,k,0.0);
      bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,epsy);
      bunch->field->de[i]=tmp2*1e-3;
    }
  }
  /* scd new 27.6.97 */
  for (i=0;i<n*(n+1)/2;i++){
    fscanf(file,"%g",&tmp3);
    field_set_kick(bunch->field,i,tmp3*1e-3);
  }
  /* scd end */
  fclose(file);
  for (i=0;i<n*nmacro;i++){
    bunch->particle[i].wgt/=wgtsum;
  }
#ifdef TWODIM
  printf("emitt_x= %g %g\n",emitt_x(bunch),wgtsum);
#endif
  printf("emitt_y= %g %g\n",emitt_y(bunch),wgtsum);
}

void bunch_save(char *name,BEAM *bunch)
{
  int i,j,m,n;
  FILE *file;
  double ym,ypm,wgt,xm,xpm;
  file=fopen(name,"w");
  ym=0.0;
  ypm=0.0;
#ifdef TWODIM
  xm=0.0;
  xpm=0.0;
#endif
  wgt=0.0;
  for (i=0;i<bunch->slices;i++){
    wgt+=bunch->particle[i].wgt;
    ym+=bunch->particle[i].y*bunch->particle[i].wgt;
    ypm+=bunch->particle[i].yp*bunch->particle[i].wgt;
#ifdef TWODIM
    xm+=bunch->particle[i].x*bunch->particle[i].wgt;
    xpm+=bunch->particle[i].xp*bunch->particle[i].wgt;
#endif
  }
  ym/=wgt;
  ypm/=wgt;
ym=0.0;
ypm=0.0;
#ifdef TWODIM
  xm/=wgt;
  xpm/=wgt;
xm=0.0;
xpm=0.0;
#endif
m=0; 
 n=bunch->slices/bunch->bunches;
 for (j=0;j<bunch->bunches;j++){ 
   for (i=0;i<n;i++){
#ifdef TWODIM
    fprintf(file,"%d %g %g %g %g %g %g %g %.16g\n",m,bunch->particle[m].energy,
	    (bunch->particle)[m].x-xm,bunch->particle[m].xp-xpm,
	    sqrt(bunch->sigma_xx[m].r11),
	    (bunch->particle)[m].y-ym,bunch->particle[m].yp-ypm,
	    sqrt(bunch->sigma[m].r11),bunch->z_position[m/bunch->macroparticles]);
#else
    fprintf(file,"%d %g %g %g %g %g %.16g\n",m,bunch->particle[m].energy,
	    (bunch->particle)[m].y-ym,bunch->particle[m].yp-ypm,
	    sqrt(bunch->sigma[m].r11),bunch->sigma[m].r12,
					   bunch->z_position[m/bunch->macroparticles]);
#endif
    m++;
   }
   fprintf(file,"\n");
 }
  fclose(file);
}

void bunch_set_bpm_sample(BEAM *bunch,int isample,int imin,int imax)
{
  bunch->drive_data->bpm_min[isample]=imin;
  bunch->drive_data->bpm_max[isample]=imax;
}

void bunch_set_offset(BEAM *bunch,double y,double yp)
{
//   for (int i=0;i<bunch->bunches;i++){
//     for (int j=0;j<bunch->slices_per_bunch;j++){
//       for (int k=0;k<bunch->macroparticles;k++){
// 	bunch_set_slice_y(bunch,i,j,k,y);
// 	bunch_set_slice_yp(bunch,i,j,k,yp);
//       }
//     }
//   }
  for (int i=0;i<bunch->slices;i++){
    bunch->particle[i].y=y;
    bunch->particle[i].yp=yp;
  }
}

void bunch_set_offset_x(BEAM *bunch,double x,double xp)
{
#ifdef TWODIM
//   int i,j,k;
//   for (i=0;i<bunch->bunches;i++){
//     for (j=0;j<bunch->slices_per_bunch;j++){
//       for (k=0;k<bunch->macroparticles;k++){
// 	bunch_set_slice_x(bunch,i,j,k,x);
// 	bunch_set_slice_xp(bunch,i,j,k,xp);
//       }
//     }
//   }
  for (int i=0;i<bunch->slices;i++){
    bunch->particle[i].x=x;
    bunch->particle[i].xp=xp;
  }
#endif
}

void bunch_set_samples(BEAM *bunch,int nsamples)
{
  bunch->drive_data->bpm_samples=nsamples;
  bunch->drive_data->bpm_min=(int*)xmalloc(sizeof(int)*nsamples);
  bunch->drive_data->bpm_max=(int*)xmalloc(sizeof(int)*nsamples);
  bunch->drive_data->do_filter=2;
}

void bunch_shift_phase(BEAMLINE *beamline,BEAM *bunch,double delta)
{
  int i,j;
  for (i=0;i<bunch->slices;i++){
    for (j=0;j<beamline->n_phases;j++){
      bunch->acc_field[j][i]=rf_data.gradient*rf_data.cavlength
	*cos(bunch->z_position[i]/(rf_data.lambda*1e6)*2.0*PI
	     +(beamline->phase[j]+delta)*PI/180.0);
    }
  }
}

double bunch_test(BUMP *bump,BEAM *bunch)
{
  int i,n;
  double x=0.0,xp=0.0;
  double sigx2=0.0,sigxxp=0.0,sigxp2=0.0;
  n=bunch->slices;
  for (i=0;i<n;i++){
    x+=bunch->particle[i].y*bunch->particle[i].wgt;
    xp+=bunch->particle[i].yp*bunch->particle[i].wgt;
  }
  for (i=0;i<n;i++){
    sigx2+=(bunch->particle[i].y-x)*(bunch->particle[i].y-x)
      *bunch->particle[i].wgt;
    sigxp2+=(bunch->particle[i].yp-xp)*(bunch->particle[i].yp-xp)
      *bunch->particle[i].wgt;
    sigxxp+=(bunch->particle[i].yp-xp)*(bunch->particle[i].y-x)
      *bunch->particle[i].wgt;
  }
  return bump->gamma*sigx2+2.0*bump->alpha*sigxxp+bump->beta*sigxp2;
}

void bunch_track_compare(BEAMLINE *beamline,BEAM *b0,BEAM *b1,BEAM *b2) 	 
{ 	 
  int n_el,n_sl; 	 
  int i_el,i_sl; 	 
  double bpm0,bpm1,bpm2; 	 
	  	 
  n_el=beamline->n_elements; 	 
  n_sl=b0->slices; 	 
  for (i_el=0;i_el<n_el;i_el++){ 	 
    if ((beamline->element[i_el]->is_quad()) 	 
	||(beamline->element[i_el]->is_quadbpm())) 	 
      quadrupoles_set(beamline,quad_set0); 	 
    beamline->element[i_el]->step(b0); 	 
    //    element_step(beamline->element[i_el],b0); 	 
    if((beamline->element[i_el]->is_bpm()) 	 
       ||(beamline->element[i_el]->is_quadbpm())) 	 
      bpm0=beamline->element[i_el]->info1; 	 
	  	 
    if ((beamline->element[i_el]->is_quad()) 	 
	||(beamline->element[i_el]->is_quadbpm())) 	 
      quadrupoles_set(beamline,quad_set1); 	 
    beamline->element[i_el]->step(b1); 	 
    //    element_step(beamline->element[i_el],b1); 	 
    if((beamline->element[i_el]->is_bpm()) 	 
       ||(beamline->element[i_el]->is_quadbpm())) 	 
      bpm1=beamline->element[i_el]->info1; 	 
	  	 
    if ((beamline->element[i_el]->is_quad()) 	 
	||(beamline->element[i_el]->is_quadbpm())) 	 
      quadrupoles_set(beamline,quad_set2); 	 
    beamline->element[i_el]->step(b2); 	 
    //    element_step(beamline->element[i_el],b2); 	 
    if((beamline->element[i_el]->is_bpm()) 	 
       ||(beamline->element[i_el]->is_quadbpm())) 	 
      bpm2=beamline->element[i_el]->info1; 	 
    if((beamline->element[i_el]->is_bpm()) 	 
       ||(beamline->element[i_el]->is_quadbpm())){ 	 
      printf("%g %g %g\n",bpm0,bpm1,bpm2); 	 
    } 	 
  } 	 
}

void call_fortran(char *name,char *input)
{
    FILE *file;
    file=popen(name,"w");
    fprintf(file,input);
    fflush(file);
    pclose(file);
}

void cavity_field_set_next(BEAMLINE *beamline,int *icav,int field)
{
  int i;
  i=*icav;
  CAVITY *cav;
  while(!(cav=beamline->element[i]->cavity_ptr())) {
    i++;
  }
  *icav=i+1;
  cav->set_field(field);
}

/*scdadapt to TWODIM*/ 	 
void cavity_stepback_0(ELEMENT *element,BEAM *bunch) 	 
{ 	 
  double tmp,factor; 	 
  int i,j; 	 
  double sum,*p; 	 
  factor=bunch->factor; 	 
  /* scd update!!****/ 	 
  p=bunch->field->kick; 	 
  for (j=0;j<bunch->slices;j++){ 	 
    sum=0.0; 	 
    for (i=0;i<=j;i++){ 	 
      sum+=bunch->particle[i].y * *p; 	 
      p++; 	 
    } 	 
    bunch->particle[j].yp-=sum*factor/bunch->particle[j].energy; 	 
  } 	 
  for (i=0;i<bunch->slices;i++){ 	 
    tmp=1.0/(1.0-bunch->acc_field[element->field][i] 	 
	     /bunch->particle[i].energy); 	 
    (bunch->particle)[i].yp*=tmp; 	 
    (bunch->particle)[i].y-=element->get_length()*(bunch->particle)[i].yp; 	 
    (bunch->particle)[i].energy-=bunch->acc_field[element->field][i]; 	 
  } 	 
}

void
correct_2(double a[],int indx[],double a0[],double a1[],double a2[],
	  int nq,int nbpm,double /*w*/,double bpm0[],double bpm1[],double bpm2[],
	  double /*pwgt*/,double qcorr[])
{
  right_side_2(a0,a1,a2,nq,nbpm,corr.w0,corr.w,bpm0,bpm1,bpm2,qcorr);

  /*
    matrices should be transponated before calling these functions but a is
   symmetric anyway
  */

  lubksb(a,nq,indx,qcorr);
}

void divide_length(BEAMLINE *beamline,BIN **bin,int nbin)
{
  double s=0.0;
  int i,j,n;
  i=0;
  for(j=0;j<nbin;j++){
    n=bin[j]->start;
    for (;i<n;i++){
      s+=beamline->element[i]->get_length();
    }
    bin[j]->s_pos=s;
  }
}

void drift_step(ELEMENT *element,BEAM *bunch)
{
  double length;
  R_MATRIX r1,r2;
  int i,n;
  double const eps=1e-200;
  length=element->get_length();
  if (placet_switch.first_order||bunch->particle_beam) {
      drift_step_0(element,bunch);
      return;
  }
  if (length*length<eps) return;
  r1.r11=1.0;
  r1.r12=length;
  r1.r21=0.0;
  r1.r22=1.0;
  r2.r11=1.0;
  r2.r12=0.0;
  r2.r21=length;
  r2.r22=1.0;
  for (i=0;i<bunch->slices;i++){
    bunch->particle[i].y+=length*bunch->particle[i].yp;
#ifdef TWODIM
    bunch->particle[i].x+=length*bunch->particle[i].xp;
#endif
    mult_M_M(&r1,bunch->sigma+i,bunch->sigma+i);
    mult_M_M(bunch->sigma+i,&r2,bunch->sigma+i);
#ifdef TWODIM
    mult_M_M(&r1,bunch->sigma_xx+i,bunch->sigma_xx+i);
    mult_M_M(bunch->sigma_xx+i,&r2,bunch->sigma_xx+i);
    mult_M_M(&r1,bunch->sigma_xy+i,bunch->sigma_xy+i);
    mult_M_M(bunch->sigma_xy+i,&r2,bunch->sigma_xy+i);
#endif
#ifdef EARTH_FIELD
    bunch->particle[i].yp+=earth_field.y*element->get_length()
	/bunch->particle[i].energy;
#ifdef TWODIM
    bunch->particle[i].xp+=earth_field.x*element->get_length()
	/bunch->particle[i].energy;
#endif
#endif
  }
}

/*scdadapt to TWODIM*/

void drift_stepback_0(ELEMENT *element,BEAM *bunch)
{
  int i;
  for (i=0;i<bunch->slices;i++){
    (bunch->particle)[i].y-=element->get_length()*(bunch->particle)[i].yp;
  }
}

/* scd adjust to new cavity model */

void drift_track_0(ELEMENT *element,BEAM *bunch)
{
  double const eps=1e-200;
  int i;
  if (element->get_length()*element->get_length()<eps) {
      return;
  }
  for (i=0;i<bunch->slices;i++){
      bunch->particle[i].y+=element->get_length()*bunch->particle[i].yp;
#ifdef TWODIM
      bunch->particle[i].x+=element->get_length()*bunch->particle[i].xp;
#endif
#ifdef EARTH_FIELD
      bunch->particle[i].yp+=earth_field.y*element->get_length()
	  /bunch->particle[i].energy;
#ifdef TWODIM
      bunch->particle[i].xp+=earth_field.x*element->get_length()
	  /bunch->particle[i].energy;
#endif
#endif
  }
}

/* Calculates the beamloading for the drivebeam */

void drive_longitudinal(BEAM *bunch,double lambda,double charge)
{
  int i,j,n;
  double beta_g,r_over_q,l_cav,power,power_sin=0.0,power_cos=0.0,power_sum=0.0;
  double loss,l_vis,dist,dist_max,scale,k;
  
  //EA SINGLE MODE ONLY (only first mode used) - NO 
  r_over_q=drive_data.r_over_q[0];
  l_cav=drive_data.l_cav*1e6;
  //EA SINGLE MODE ONLY (only first mode used)
  beta_g=drive_data.beta_group_l[0];
  n=bunch->slices;
  k=2.0*PI/lambda;
  dist_max=l_cav*(1.0-beta_g)/fabs(beta_g);
  //EA SINGLE MODE ONLY (only first mode used)
  scale=1e-9*(charge*1.602e-19)*C/drive_data.lambda_longitudinal[0]
    *2.0*PI/l_cav;
  for (j=0;j<n;j++){
    loss=0.5*l_cav*r_over_q*bunch->particle[j].wgt;
    for (i=j-1;i>=0;i--){
      dist=bunch->z_position[j]-bunch->z_position[i];
      if (dist>dist_max) break;
      l_vis=l_cav-beta_g/(1.0-beta_g)*dist;
      loss+=l_vis*r_over_q*bunch->particle[i].wgt*cos(dist*k);
    }
    bunch->field->de[j]=-loss*scale;
  }
  for (i=bunch->slices/bunch->macroparticles-bunch->slices_per_bunch;
       i<bunch->slices/bunch->macroparticles;i++){
    power=0.0;
    for (j=i*bunch->macroparticles;j<(i+1)*bunch->macroparticles;j++){
      power-=bunch->field->de[i]*bunch->particle[j].wgt;
    }
    power_sum+=power;
    power_sin+=power*sin(2.0*PI*bunch->z_position[j]/lambda);
    power_cos+=power*cos(2.0*PI*bunch->z_position[j]/lambda);
  }
  power=sqrt(power_sin*power_sin+power_cos*power_cos);
  scale=C/bunch->drive_data->param.drive->distance
	 *l_cav*1e-6*1.602e-10*bunch->drive_data->param.drive->charge*1e-6;
  printf("power %g %g\n",power,power*scale);
  printf("%g %g %g\n",power_sin*scale,power_cos*scale,power_sum*scale);
}

void element_set_offset_to_x(ELEMENT *element,double x,double xp)
{
  element->offset.x=x;
  element->offset.xp=xp;
}

/*scdadapt to TWODIM***/ 	 
	  	 
void element_stepback_0(ELEMENT *element,BEAM *bunch) 	 
{ 	 
  int i; 	 
#ifdef LONGITUDINAL 	 
  if (bunch->macroparticles==1){ 	 
    for (i=0;i<bunch->slices;i++){ 	 
      bunch->z_position[i]+=element->get_length()*0.5e-6 	 
	*bunch->particle[i].yp*bunch->particle[i].yp; 	 
    } 	 
  } 	 
#endif 	 
  for (i=0;i<bunch->slices;i++){ 	 
    bunch->particle[i].y-=element->offset.y; 	 
    bunch->particle[i].yp-=element->offset.yp; 	 
  } 	 
  switch (element->type){ 	 
  case QUAD: 	 
    quadrupole_stepback_0(element,bunch); 	 
    break; 	 
  case QUADBPM: 	 
    quadrupole_stepback_0(element,bunch); 	 
    break; 	 
  case DRIFT: 	 
  case BPMDRIFT: 	 
    drift_stepback_0(element,bunch); 	 
    break; 	 
  case CAV: 	 
    cavity_stepback_0(element,bunch); 	 
    break; 	 
  case BPM: 	 
    bpm_stepback_0(element,bunch); 	 
    break; 	 
  } 	 
  for (i=0;i<bunch->slices;i++){ 	 
    bunch->particle[i].y+=element->offset.y; 	 
    bunch->particle[i].yp+=element->offset.yp; 	 
  } 	 
}

void element_step_fdbk(ELEMENT *element,BEAM *bunch) 	 
{ 	 
  int i; 	 
  double offset; 	 
#ifdef LONGITUDINAL 	 
  if (bunch->macroparticles==1) { 	 
    for (i=0;i<bunch->slices;i++){ 	 
      bunch->z_position[i]+=element->get_length()*0.5e-6 	 
	*bunch->particle[i].yp*bunch->particle[i].yp; 	 
      bunch->z_position[i]+=element->get_length()*0.5*1e6 	 
	*(EMASS*EMASS)/(bunch->particle[i].energy 	 
			*bunch->particle[i].energy); 	 
    } 	 
  } 	 
#endif 	 
  if ((element->type!=DRIFT)&&(element->type!=BPMDRIFT)){ 	 
    offset=0.5*element->get_length()*element->offset.yp-element->offset.y; 	 
    for (i=0;i<bunch->slices;i++){ 	 
      bunch->particle[i].y+=offset; 	 
      bunch->particle[i].yp-=element->offset.yp; 	 
    } 	 
  } 	 
  switch (element->type){ 	 
  case FDBK: 	 
    feedback_step(element,bunch); 	 
    break; 	 
  case QUAD: 	 
    exit(1); 	 
    /* 	 
	 #ifdef TWODIM 	 
	 bunch_rotate(bunch,element->tilt()); 	 
	 #endif 	 
	 quadrupole_step(element,bunch); 	 
	 #ifdef TWODIM 	 
	 bunch_rotate(bunch,element->tilt()); 	 
	 #endif 	 
    ***/ 	 
    break; 	 
  case QUADBPM: 	 
#ifdef TWODIM 	 
    bunch_rotate(bunch,element->tilt()); 	 
#endif 	 
    quadbpm_step(element,bunch); 	 
#ifdef TWODIM 	 
    bunch_rotate(bunch,element->tilt()); 	 
#endif 	 
    break; 	 
  case _DIPOLE: 	 
    dipole_step(element,bunch); 	 
    break; 	 
  case DRIFT: 	 
  case BPMDRIFT: 	 
    drift_step(element,bunch); 	 
    break; 	 
  case CAV: 	 
    exit(1); 	 
    //    cavity_step(element,bunch); 	 
    break; 	 
  case CAV_PETS: 	 
    exit(1); 	 
    //    cavity_step_pets(element,bunch); 	 
    break; 	 
  case BPM: 	 
#ifdef TWODIM 	 
    bunch_rotate(bunch,element->tilt()); 	 
#endif 	 
    //    bpm_step(element,bunch); 	 
#ifdef TWODIM 	 
    bunch_rotate(bunch,element->tilt()); 	 
#endif 	 
    break; 	 
  case SOLENOID: 	 
    solenoid_step(element,bunch); 	 
    break; 	 
  } 	 
  if ((element->type!=DRIFT)&&(element->type!=BPMDRIFT)){ 	 
    offset=0.5*element->get_length()*element->offset.yp+element->offset.y; 	 
    for (i=0;i<bunch->slices;i++){ 	 
      bunch->particle[i].y+=offset; 	 
      bunch->particle[i].yp+=element->offset.yp; 	 
    } 	 
  } 	 
#ifdef EARTH_FIELD 	 
  for(i=0;i<bunch->slices;i++){ 	 
    bunch->particle[i].yp+=earth_field.y*element->get_length() 	 
      /bunch->particle[i].energy; 	 
#ifdef TWODIM 	 
    bunch->particle[i].xp+=earth_field.x*element->get_length() 	 
      /bunch->particle[i].energy; 	 
#endif 	 
  } 	 
#endif 	 
}

void element_step_rf(ELEMENT *element,BEAM *bunch) 	 
{ 	 
  int i; 	 
  double offset,offsetx; 	 
#ifdef LONGITUDINAL 	 
  if (bunch->macroparticles==1) { 	 
    for (i=0;i<bunch->slices;i++){ 	 
      bunch->z_position[i]+=element->get_length()*0.5e-6 	 
	*bunch->particle[i].yp*bunch->particle[i].yp; 	 
      bunch->z_position[i]+=element->get_length()*0.5*1e6 	 
	*(EMASS*EMASS)/(bunch->particle[i].energy 	 
			*bunch->particle[i].energy); 	 
    } 	 
  } 	 
#endif 	 
  if ((element->type!=DRIFT)&&(element->type!=BPMDRIFT)){ 	 
    offset=0.5*element->get_length()*element->offset.yp-element->offset.y; 	 
#ifdef TWODIM 	 
    offsetx=0.5*element->get_length()*element->offset.xp-element->offset.x; 	 
#endif 	 
    for (i=0;i<bunch->slices;i++){ 	 
      bunch->particle[i].y+=offset; 	 
      bunch->particle[i].yp-=element->offset.yp; 	 
#ifdef TWODIM 	 
      bunch->particle[i].x+=offsetx; 	 
      bunch->particle[i].xp-=element->offset.xp; 	 
#endif 	 
    } 	 
  } 	 
  switch (element->type){ 	 
  case QUAD: 	 
    exit(1); 	 
    break; 	 
  case QUADBPM: 	 
#ifdef TWODIM 	 
    bunch_rotate_0(bunch,element->tilt()); 	 
#endif 	 
    quadbpm_track_0(element,bunch); 	 
#ifdef TWODIM 	 
    bunch_rotate_0(bunch,element->tilt()); 	 
#endif 	 
    break; 	 
  case _DIPOLE: 	 
    dipole_track_0(element,bunch); 	 
    break; 	 
  case DRIFT: 	 
  case BPMDRIFT: 	 
    drift_track_0(element,bunch); 	 
    break; 	 
  case CAV: 	 
    cavity_step_rf_0(element,bunch); 	 
    break; 	 
  case BPM: 	 
#ifdef TWODIM 	 
    bunch_rotate_0(bunch,element->tilt()); 	 
#endif 	 
    //    bpm_track_0(element,bunch); 	 
#ifdef TWODIM 	 
    bunch_rotate_0(bunch,element->tilt()); 	 
#endif 	 
    break; 	 
  case SOLENOID: 	 
    solenoid_track_0(element,bunch); 	 
    break; 	 
  } 	 
  if ((element->type!=DRIFT)&&(element->type!=BPMDRIFT)){ 	 
    offset=0.5*element->get_length()*element->offset.yp+element->offset.y; 	 
#ifdef TWODIM 	 
    offsetx=0.5*element->get_length()*element->offset.xp+element->offset.x; 	 
#endif 	 
    for (i=0;i<bunch->slices;i++){ 	 
      bunch->particle[i].y+=offset; 	 
      bunch->particle[i].yp+=element->offset.yp; 	 
#ifdef TWODIM 	 
      bunch->particle[i].x+=offsetx; 	 
      bunch->particle[i].xp+=element->offset.xp; 	 
#endif 	 
    } 	 
  } 	 
#ifdef EARTH_FIELD 	 
  for(i=0;i<bunch->slices;i++){ 	 
    bunch->particle[i].yp+=earth_field.y*element->get_length() 	 
      /bunch->particle[i].energy; 	 
#ifdef TWODIM 	 
    bunch->particle[i].xp+=earth_field.x*element->get_length() 	 
      /bunch->particle[i].energy; 	 
#endif 	 
  } 	 
#endif 	 
}

double emitt_x_old(BEAM *bunch)
{
  int i,n;
  double sum11,sum12,sum22,x,xp,xm,xpm,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  n=bunch->slices;
  xm=0.0;
  xpm=0.0;
  wgtsum=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    xm+=wgt*bunch->particle[i].x;
    xpm+=wgt*bunch->particle[i].xp;
  }
  xm/=wgtsum;
  xpm/=wgtsum;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  if (placet_switch.first_order||bunch->particle_beam) {
    for (i=0;i<n;i++){
      wgt=bunch->particle[i].wgt;
      x=bunch->particle[i].x-xm;
      xp=bunch->particle[i].xp-xpm;
      sum11+=x*x*wgt;
      sum12+=x*xp*wgt;
      sum22+=xp*xp*wgt;
    }
  }
  else {
    for (i=0;i<n;i++){
      wgt=bunch->particle[i].wgt;
      x=bunch->particle[i].x-xm;
      xp=bunch->particle[i].xp-xpm;
      sum11+=(x*x+bunch->sigma_xx[i].r11)*wgt;
      sum12+=(x*xp+bunch->sigma_xx[i].r21)*wgt;
      sum22+=(xp*xp+bunch->sigma_xx[i].r22)*wgt;
    }
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  return sqrt(sum11*sum22-sum12*sum12)
    *(bunch->particle)[bunch->slices/2].energy
    /EMASS*1e-5;
}

/* calculates the emittance of the bunch scaling the positions of the centres
of the slices by factor */

double emitt_y_factor(BEAM *bunch,double factor)
{
  int i,n;
  double sum11,sum12,sum22,y,yp,ym,ypm,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  n=bunch->slices;
  ym=0.0;
  ypm=0.0;
  wgtsum=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    ym+=wgt*bunch->particle[i].y;
    ypm+=wgt*bunch->particle[i].yp;
  }
  ym/=wgtsum;
  ypm/=wgtsum;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    y=(bunch->particle[i].y-ym)*factor;
    yp=(bunch->particle[i].yp-ypm)*factor;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
    sum12+=(y*yp+bunch->sigma[i].r21)*wgt;
    sum22+=(yp*yp+bunch->sigma[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  return sqrt(sum11*sum22-sum12*sum12)
    *(bunch->particle)[bunch->slices/2].energy
    /EMASS*1e-5;
}

double emitt_y_region(BEAM *bunch,int n1,int n2,double y0,double yp0,
		     double *off)
{
  int i;
  double sum11,sum12,sum22,y,yp,ym,ypm,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  ym=0.0;
  ypm=0.0;
  wgtsum=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    ym+=wgt*bunch->particle[i].y;
    ypm+=wgt*bunch->particle[i].yp;
  }
  ym/=wgtsum;
  ypm/=wgtsum;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=n1;i<n2;i++){
    wgt=bunch->particle[i].wgt;
    y=bunch->particle[i].y-y0;
    yp=bunch->particle[i].yp-yp0;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
    sum12+=(y*yp+bunch->sigma[i].r21)*wgt;
    sum22+=(yp*yp+bunch->sigma[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  *off=ym-y0;
  return sqrt(sum11*sum22-sum12*sum12)
    *(bunch->particle)[(n1+n2)/2].energy/EMASS*1e-5;
}

double emitt_y_tail(BEAM *bunch)
{
  int i,n;
  double sum11,sum12,sum22,y,yp,ym,ypm,wgtsum,wgt;
  R_MATRIX sigma0;

  sigma0.r11=0.0;
  sigma0.r12=0.0;
  sigma0.r21=0.0;
  sigma0.r22=0.0;
  n=bunch->slices;
  ym=0.0;
  ypm=0.0;
  wgtsum=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    ym+=wgt*bunch->particle[i].y;
    ypm+=wgt*bunch->particle[i].yp;
  }
  ym/=wgtsum;
  ypm/=wgtsum;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  for (i=0;i<n/5;i++){
    wgt=bunch->particle[i].wgt;
    y=bunch->particle[i].y-ym;
    yp=bunch->particle[i].yp-ypm;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
    sum12+=(y*yp+bunch->sigma[i].r21)*wgt;
    sum22+=(yp*yp+bunch->sigma[i].r22)*wgt;
  }
  for (i=n-n/5;i<n;i++){
    wgt=bunch->particle[i].wgt;
    y=bunch->particle[i].y-ym;
    yp=bunch->particle[i].yp-ypm;
    sum11+=(y*y+bunch->sigma[i].r11)*wgt;
    sum12+=(y*yp+bunch->sigma[i].r21)*wgt;
    sum22+=(yp*yp+bunch->sigma[i].r22)*wgt;
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  return sqrt(sum11*sum22-sum12*sum12)
    *(bunch->particle)[bunch->slices/2].energy/EMASS*1e-5;
}

double emitt_y_old(BEAM *bunch)
{
  int i,n;
  double sum11,sum12,sum22,y,yp,ym,ypm,wgtsum,wgt;

  n=bunch->slices;
  ym=0.0;
  ypm=0.0;
  wgtsum=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    ym+=wgt*bunch->particle[i].y;
    ypm+=wgt*bunch->particle[i].yp;
  }
  ym/=wgtsum;
  ypm/=wgtsum;
  //ym=0.0;
  //ypm=0.0;
  sum11=0.0;
  sum12=0.0;
  sum22=0.0;
  if (placet_switch.first_order||bunch->particle_beam) {
    for (i=0;i<n;i++){
      wgt=bunch->particle[i].wgt;
      y=bunch->particle[i].y-ym;
      yp=bunch->particle[i].yp-ypm;
      sum11+=y*y*wgt;
      sum12+=y*yp*wgt;
      sum22+=yp*yp*wgt;
    }
  }
  else {
    for (i=0;i<n;i++){
      wgt=bunch->particle[i].wgt;
      y=bunch->particle[i].y-ym;
      yp=bunch->particle[i].yp-ypm;
      sum11+=(y*y+bunch->sigma[i].r11)*wgt;
      sum12+=(y*yp+bunch->sigma[i].r12)*wgt;
      sum22+=(yp*yp+bunch->sigma[i].r22)*wgt;
    }
  }
  sum11/=wgtsum;
  sum12/=wgtsum;
  sum22/=wgtsum;
  return sqrt(sum11*sum22-sum12*sum12)
    *(bunch->particle)[bunch->slices/2].energy/EMASS*1e-5;
}

/* to be finished*/
double emitt_z(BEAM *bunch)
{
  int i,n;
  double sumee,sumez,sumzz,emean,zmean,e,z,wgt,wgtsum;

  sumee=0.0;
  sumzz=0.0;
  sumez=0.0;
  n=bunch->slices;
  emean=0.0;
  zmean=0.0;
  wgtsum=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    wgtsum+=wgt;
    emean+=wgt*bunch->particle[i].energy;
    zmean+=wgt*bunch->z_position[i];
  }
  emean/=wgtsum;
  zmean/=wgtsum;
  sumee=0.0;
  sumez=0.0;
  sumzz=0.0;
  for (i=0;i<n;i++){
    wgt=bunch->particle[i].wgt;
    e=bunch->particle[i].energy-emean;
    z=bunch->z_position[i]-zmean;
    sumee+=e*e*wgt;
    sumez+=e*z*wgt;
    sumzz+=z*z*wgt;
  }
  printf("%g %g %g\n",sumee,sumez,sumzz);
  sumee/=wgtsum;
  sumez/=wgtsum;
  sumzz/=wgtsum;
  return sqrt(sumee*sumzz-sumez*sumez)/emean;
}

double energy_spread_spline(BEAM *bunch)
{
  struct{
    SPLINE spline;
  } energy_spread_data;
  
  int i,n,n_centre;
  double min,max;
  double x[1000],y[1000];
  n=bunch->slices;
  n_centre=n/2;
  min=1e300;
  max=-1e300;
  for (i=0;i<n;i++){
    x[i]=(double)i;
    y[i]=bunch->particle[i].energy;
  }
  spline_init(x,0,y,0,n,&(energy_spread_data.spline));
  for (i=n_centre;i<n;i++){
    if (bunch->particle[i].energy<min) min=bunch->particle[i].energy;
  }
  return (max-min)/bunch->particle[n_centre].energy;
}

FIELD *field_make_2(int n)
{
  FIELD *field;
  int i;
  field=(FIELD*)xmalloc(sizeof(FIELD));
  field->de=(double*)xmalloc(sizeof(double)*n);
  for (i=0;i<n;i++){
    field->de[i]=0.0;
  }
  field->kick=NULL;
  return field;
}

void feedback_correct(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
		 BEAM *workbunch,int dist)
{
  int ipos,i;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    emitt_store(i,bunch);
    if ((i+1)%dist==0){
      bunch_copy_0(bunch,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      bin_correct(beamline,0,bin[i]);
//      bunch_feedback(beamline->element[bin[i]->start],bunch);
    } 
    ipos=bin[i]->start;
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
  emitt_store(nbin,bunch);
}

void feedback_correct_2(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch,
			BEAM *workbunch,int loop[],int /*nloop*/)
{
  int ipos,i,iloop=0;
  ipos=0;
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch,ipos,bin[i]->start);
    emitt_store(i,bunch);
    if (i==loop[iloop]){
      bunch_copy_0(bunch,workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      bin_correct(beamline,0,bin[i]);
      iloop++;
    } 
    ipos=bin[i]->start;
  }
  bunch_track(beamline,bunch,ipos,beamline->n_elements);
  emitt_store(nbin,bunch);
}

void feedback_correct_jitter(BEAMLINE *beamline,BIN **bin,int nbin,BEAM *bunch0,
			BEAM *bunch1,BEAM *workbunch,int dist)
{
  int ipos,i;
  ipos=0;
  bunch_copy_0(bunch0,bunch1);
  for (i=0;i<bunch1->slices;i++){
    bunch1->particle[i].y=1.0;
  }
  for (i=0;i<nbin;i++){
    bunch_track(beamline,bunch0,ipos,bin[i]->start);
    bunch_track_0(beamline,bunch1,ipos,bin[i]->start);
    emitt_store(i,bunch0);
    if ((i+1)%dist==0){
      bunch_join_0(bunch0,bunch1,gasdev(),workbunch);
      bin_measure(beamline,workbunch,0,bin[i]);
      bin_correct(beamline,0,bin[i]);
    }
    ipos=bin[i]->start;
  }
  bunch_track(beamline,bunch0,ipos,beamline->n_elements);
  emitt_store(nbin,bunch0);
}

/*scdadapt to TWODIM*/
void feedback_step(ELEMENT *element,BEAM *bunch)
{
  int i,n;
  double y=0.0,yp=0.0;
  n=bunch->slices;
  for (i=0;i<n;i++){
    y+=bunch->particle[i].y*bunch->particle[i].wgt;
    yp+=bunch->particle[i].yp*bunch->particle[i].wgt;
  }
  for (i=0;i<n;i++){
    bunch->particle[i].yp-=yp;
    bunch->particle[i].y-=y+element->get_length()*bunch->particle[i].yp;
  }
  printf("d_y= %g\n",element->offset.y);
}

void field_adjust(BEAM *bunch)
{
  int j,k,l;
  double wgt,*p;
  p=bunch->field->kick;
  /*
  for (i=0;i<bunch->bunches;i++){
    for (j=0;j<bunch->slices_per_bunch;j++){
      for (l=0;l<=j;l++){
	wgt=0.0;
	for (k=0;k<bunch->macroparticles;k++){
	  wgt+=bunch->particle[(i*bunch->slices_per_bunch+l)
			      *bunch->macroparticles+k].wgt;
	}
	*p/=wgt;
	p++;
      }
    }
  }
  */
  for (j=0;j<bunch->slices_per_bunch;j++){
    for (l=0;l<=j;l++){
      wgt=0.0;
      for (k=0;k<bunch->macroparticles;k++){
	wgt+=bunch->particle[l*bunch->macroparticles+k].wgt;
      }
      *p/=wgt;
      p++;
    }
  }
}

/** recalculates the scaling factors for the lattice and changes the
accelerating gradients */

void field_reread(FIELD **field,double phase[],double scale[],int n,char *string)
{
  FILE *file;
  int i,nslice,j;
  float tmp1,tmp2,tmp3,rf;
  file=fopen(string,"r");
  if (!file) {
    placet_cout << ERROR << "No such file: " << string << endmsg;
    exit(1);
  }
  fscanf(file,"%d",&nslice);
  fscanf(file,"%g",&tmp1);
  tmp1*=rf_data.cavlength;
  for (j=0;j<n;j++){
    scale[j]=(rf_data.cavlength*rf_data.gradient
	      *cos(phase[j]*PI/180.0)+tmp1*1e-3)
      /(rf_data.cavlength*rf_data.gradient);
  }
  for (i=0;i<nslice;i++){
    fscanf(file,"%g %g %g",&tmp1,&tmp2,&tmp3);
    for (j=0;j<n;j++){
      rf=rf_data.gradient*rf_data.cavlength
	*cos(tmp1/(rf_data.lambda*1e6)*TWOPI+phase[j]*PI/180.0);
      field_set_de(field[j],i,rf);
    }
  }
  fclose(file);
}

void field_set_de(FIELD *field,int n,double grad)
{
  field->de[n]=grad;
}

/* workbunch should be the same length as the normal bunch. It is used to 	 
	    avoid too many memory (de-) allocations */ 	 

void find_response(BEAMLINE *beamline,BEAM *bunch,BEAM *workbunch, 	 
		   int i_el,int nlast,double res[],int *n) 	 
{ 	 
  ELEMENT *point; 	 
  double dy=1.0; 	 
	  	 
  point=beamline->element[i_el]; 	 
  bunch_copy_0(bunch,workbunch); 	 
  element_add_offset(point,dy,0.0); 	 
  point->track_0(workbunch); 	 
  //  element_step_0(point,workbunch); 	 
  element_add_offset(point,-dy,0.0); 	 
  i_el++; 	 
  //  element_print(stdout,point); 	 
  for (*n=0;i_el<nlast;i_el++){ 	 
    beamline->element[i_el]->track_0(workbunch); 	 
    //    element_step_0(beamline->element[i_el],workbunch); 	 
    if (beamline->element[i_el]->is_bpm()){ 	 
      res[(*n)++]=beamline->element[i_el]->info1; 	 
    } 	 
  } 	 
} 	 

void fwrite_element(FILE *file,ELEMENT *element) 	 
{ 	 
  switch (element->type){ 	 
  case QUAD: 	 
    fprintf(file,"QUAD %g\n",0.0); 	 
    break; 	 
  case QUADBPM: 	 
    fprintf(file,"QUADBPM %g\n",0.0); 	 
    break; 	 
  } 	 
} 	 

void
hessian_2(double a0[],double a1[],double a2[],int nq,int nbpm,double pwgt,
	  double w0,double w,double a[])
{
  int i,j,k;
  double sum0,sum1;
  for (j=0;j<nq;j++){
    for (i=0;i<nq;i++){
      sum0=0.0;
      sum1=0.0;
      for (k=0;k<nbpm;k++){
/*scd check*/
	sum0+=0.0*a0[k+i*nbpm]*a0[k+j*nbpm]+0.0*a1[k+i*nbpm]*a1[k+j*nbpm]+
	  a2[k+i*nbpm]*a2[k+j*nbpm];
	sum1+=(a1[k+i*nbpm]-a0[k+i*nbpm])*(a1[k+i*nbpm]-a0[k+i*nbpm])
	  +(a2[k+i*nbpm]-a0[k+i*nbpm])*(a2[k+i*nbpm]-a0[k+i*nbpm])
	  +(a2[k+i*nbpm]-a1[k+i*nbpm])*(a2[k+i*nbpm]-a1[k+i*nbpm]);
      }
      a[i+j*nq]=2.0*(w0*sum0+w*sum1);
    }
    a[j+j*nq]+=2.0*pwgt;
  }
}
	 	 
void
hessian_wgt_0(double a0[],int nq,int nbpm,double pwgt[],double a[])
{
  int i,j,k;
  double sum0;
  for (j=0;j<nq;j++){
    for (i=0;i<nq;i++){
      sum0=0.0;
      for (k=0;k<nbpm;k++){
	sum0+=a0[k+i*nbpm]*a0[k+j*nbpm];
      }
      a[i+j*nq]=2.0*sum0;
    }
    a[j+j*nq]+=2.0*pwgt[j];
  }
}

double* kick_make(int n)
{
  return (double*)xmalloc(sizeof(double)*n*(n+1)/2); 
}

void lattice_rescale(BEAMLINE *beamline,double phase[],double estep[],double e0)
{
  double e,scal[10],e_scal;
  int isect,i,j,kscal=0,icav=0,iq,new_lattice=1;

  field_reread(field_cav,phase,scal,4,"beam.dat");
  iq=0;
  e=e0;
  e_scal=e0;
  for (isect=0;isect<lattice_data.nsect;isect++){
    for (i=0;i<lattice_data.nquad[isect];i++){
      quad_set0[iq]=e*quad_strength[iq];
      iq++;
      if (new_lattice){
	switch(lattice_data.nlong[isect]){
	case 1:
	  e+=3.0*rf_data.cavlength*rf_data.gradient*scal[kscal];
	  e_scal+=3.0*rf_data.cavlength*rf_data.gradient;
	  cavity_field_set_next(beamline,&icav,kscal);
	  cavity_field_set_next(beamline,&icav,kscal);
	  cavity_field_set_next(beamline,&icav,kscal);
	  break;
	case 2:
	  e+=2.0*rf_data.cavlength*rf_data.gradient*scal[kscal];
	  e_scal+=2.0*rf_data.cavlength*rf_data.gradient;
	  cavity_field_set_next(beamline,&icav,kscal);
	  cavity_field_set_next(beamline,&icav,kscal);
	  break;
	case 3:
	  e+=1.0*rf_data.cavlength*rf_data.gradient*scal[kscal];
	  e_scal+=1.0*rf_data.cavlength*rf_data.gradient;
	  cavity_field_set_next(beamline,&icav,kscal);
	  break;
	default:
	  break;
	}
      }
      else{
	if (!lattice_data.nlong[isect]){
	  e+=rf_data.cavlength*rf_data.gradient*2.0*scal[kscal];
	  e_scal+=rf_data.cavlength*rf_data.gradient*2.0;
	  cavity_field_set_next(beamline,&icav,kscal);
	  cavity_field_set_next(beamline,&icav,kscal);
	}
      }
      for (j=0;j<lattice_data.ngirder[isect];j++){
	e+=(rf_data.cavlength*rf_data.gradient)*4.0*scal[kscal];
	e_scal+=(rf_data.cavlength*rf_data.gradient)*4.0;
	cavity_field_set_next(beamline,&icav,kscal);
	cavity_field_set_next(beamline,&icav,kscal);
	cavity_field_set_next(beamline,&icav,kscal);
	cavity_field_set_next(beamline,&icav,kscal);
      }
      check_kscal(&kscal,estep,e_scal*0.98);
    }
  }
  placet_printf(INFO,"energy after rescaling: %g\n",e);
  quadrupoles_set(beamline,quad_set0);
}

void longrange_fill(BEAM *bunch,double lambda,double delta_z,double dist)
{
  int i,j,l,nmacro,nbunch,nslice,p=0,ntmp;
  double phi,phi0,a,b;

  ntmp=(int)floor(dist/lambda);
  dist-=ntmp*lambda;
  dist*=2.0*PI/lambda;
  delta_z=2.0*PI*delta_z/lambda;
  nbunch=bunch->bunches;
  nslice=bunch->slices_per_bunch;
  nmacro=bunch->macroparticles;
  phi0=0.5*PI;
  for (j=0;j<nbunch;j++){
    if (bunch->drive_data->empty_buckets){
      if (((j%bunch->drive_data->bunches_per_train)==0)&&(j))
	phi0+=bunch->drive_data->empty_bucket_number*dist;
    }
    phi=phi0;
    for (i=0;i<nslice;i++){
      a=sin(phi);
      b=cos(phi);
      for (l=0;l<nmacro;l++){
	bunch->drive_data->af[p]=a;
	bunch->drive_data->bf[p]=b;
	/*printf("%d %d %g %g %g %g\n",j,i,phi0,phi,a,b);*/
	p++;
      }
      phi+=delta_z;
    }
    phi0+=dist;
  }
}

void longrange_fill_band(BEAM *bunch,double lambda0,double delta_z0,double dist0,
		    double band,int nfreq)
{
  int i,j,l,nmacro,nbunch,nslice,p=0,ntmp,ifreq;
  double phi,phi0,a,b,lambda,dist,delta_z;

  for (ifreq=0;ifreq<nfreq;ifreq++){
    lambda=lambda0*(1.0+band*((double)ifreq/(double)(nfreq-1)-0.5));
    ntmp=(int)floor(dist0/lambda);
    dist=dist0-ntmp*lambda;
    dist*=2.0*PI/lambda;
    nbunch=bunch->bunches;
    delta_z=2.0*PI*delta_z0/lambda;
    nslice=bunch->slices_per_bunch;
    nmacro=bunch->macroparticles;
    phi0=0.5*PI;
    phi0=0.0;
    for (j=0;j<nbunch;j++){
      if (bunch->drive_data->empty_buckets){
	if (((j%bunch->drive_data->bunches_per_train)==0)&&(j))
	    phi0+=bunch->drive_data->empty_bucket_number*dist;
      }
      phi=phi0;
      for (i=0;i<nslice;i++){
	a=sin(phi);
	b=cos(phi);
	for (l=0;l<nmacro;l++){
	  bunch->drive_data->af[p]=a;
	  bunch->drive_data->bf[p]=b;
	  /*printf("%d %d %g %g %g %g\n",j,i,phi0,phi,a,b);*/
	  p++;
	}
	phi+=delta_z;
      }
      phi0+=dist;
    }
  }
}

static struct{
  double espread,ecut;
  int nmacro;
} make_bunch_data;

BEAM *make_bunch(BEAMLINE *beamline,char *name)
{
  FILE *file;
  BEAM *bunch;
  int i,n,k,nmacro,j;
  double wgtsum,ecut,sigma_e,delta,fact;
  float tmp1,tmp2,tmp3;
  double betax,alphax,epsx;
  double betay,alphay,epsy=0.5*EMASS/9.0*1e12*EMITT_UNIT;
  char buffer[100],*point;
  double *store,elow,ehigh,de,sumwgt;
  int nbunches=1;

  sigma_e=make_bunch_data.espread;
  ecut=make_bunch_data.ecut;
  nmacro=make_bunch_data.nmacro;
  file=fopen("match.ini","r");
  if (!file) std::cout << "No such file: match.ini" << std::endl;
  point=fgets(buffer,100,file);
  epsx=strtod(point,&point);
  betax=strtod(point,&point);
  alphax=strtod(point,&point);
  point=fgets(buffer,100,file);
  epsy=strtod(point,&point);
  betay=strtod(point,&point);
  alphay=strtod(point,&point);
  /*
  fscanf(file,"%Lg %Lg %Lg",&epsx,&betax,&alphax);
  fscanf(file,"%Lg %Lg %Lg",&epsy,&betay,&alphay);
  */
  epsx*=EMASS/9.0*1e12*EMITT_UNIT;
  epsy*=EMASS/9.0*1e12*EMITT_UNIT;
  fclose(file);
  file=fopen(name,"r");
  if (!file) std::cout << "No such file: " << name << std::endl;
  fscanf(file,"%d",&n);
  /* scd to update for number of phases */
  /*  bunch=bunch_make(1,n,nmacro,10);*/
  bunch=bunch_make(nbunches,n,nmacro,10,0);
  fscanf(file,"%g",&tmp1);
  wgtsum=0.0;
  de=2.0*ecut/(double)(nmacro);
  if (gsl_integration_workspace *w = gsl_integration_workspace_alloc (1000))
  {
	gsl_function F;

	F.function = (double (*) (double, void *))&gauss;
	F.params = NULL;

	if (nmacro>1)
	{
		store=(double*)alloca(sizeof(double)*nmacro);
		sumwgt=0.0;
		elow=-ecut;
		for (k=0;k<nmacro;k++)
		{
			ehigh=elow+de;
			printf("%g %g\n",elow,ehigh);
			store[k]=0.0;

			double error;

			gsl_integration_qag(&F, elow, ehigh, 0, 1e-6, 1000, GSL_INTEG_GAUSS41, w, &(store[k]), &error); 

			elow = ehigh;
			sumwgt += store[k];
		}
		for (k=0;k<nmacro;k++)
		{
			store[k]/=sumwgt;
			printf("wgt[%d]: %g\n",k,store[k]);
		}
	}		
  	gsl_integration_workspace_free(w);
  }

  for (i=0;i<n;i++){
    fscanf(file,"%g %g %g",&tmp1,&tmp2,&tmp3);
    for (k=0;k<nmacro;k++){
      if (nmacro>1){
	delta=ecut*(2.0*k/(double)(nmacro-1)-1.0);
delta=-ecut+((double)k+0.5)*de;
	bunch_set_slice_energy(bunch,0,i,k,9.0+delta*sigma_e);
	/*	fact=exp(-0.5*delta*delta);*/
fact=store[k];
      }
      else{
	bunch_set_slice_energy(bunch,0,i,k,9.0);
	fact=1.0;
      }
      bunch_set_slice_wgt(bunch,0,i,k,(double)tmp3*fact);
      wgtsum+=tmp3*fact;
#ifdef TWODIM
      bunch_set_slice_x(bunch,0,i,k,0.0);
      bunch_set_slice_xp(bunch,0,i,k,0.0);
      bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,epsx);
      bunch_set_sigma_xy(bunch,0,i,k);
#endif
      bunch_set_slice_y(bunch,0,i,k,zero_point);
      bunch_set_slice_yp(bunch,0,i,k,0.0);
      bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,epsy);
      bunch->field->de[i]=tmp2*1e-3;
    }
    for (j=0;j<beamline->n_phases;j++){
      bunch->acc_field[j][i]=rf_data.gradient
	*cos(tmp1/(rf_data.lambda*1e6)*2.0*PI+beamline->phase[j]*PI/180.0);
      bunch->z_position[i]=tmp1;
    }
  }
  /* scd new 27.6.97 */
  for (i=0;i<n*(n+1)/2;i++){
    fscanf(file,"%g",&tmp3);
    field_set_kick(bunch->field,i,tmp3*1e-3);
  }
  /* scd end */
  fclose(file);
  for (i=0;i<n*nmacro;i++){
    bunch->particle[i].wgt/=wgtsum;
  }
  if (nmacro>1){
    field_adjust(bunch);
  }

  bunch->which_field=1;
  if (nmacro>1) bunch->which_field=2;

#ifdef TWODIM
  printf("emitt_x= %g %g\n",emitt_x(bunch),wgtsum);
#endif
  printf("emitt_y= %g %g\n",emitt_y(bunch),wgtsum);
  return bunch;
}

void make_bunch_init(double espread,double ecut,int nmacro)
{
  make_bunch_data.espread=espread;
  make_bunch_data.ecut=ecut;
  make_bunch_data.nmacro=nmacro;
}

/* creates the beam for the drivebeam */

BEAM *make_multi_bunch_drive(BEAMLINE * /*beamline*/,char *name,
			     DRIVE_BEAM_PARAM *param)
{
  FILE *file;
  BEAM *bunch;
  int i,n,k,nmacro,j,j1,nbunches;
  double wgtsum,ecut,sigma_e,delta,fact;
  float tmp1,tmp2,tmp3;
  double betax,alphax,epsx;
  double betay,alphay,epsy;
  double *store,elow,ehigh,de,sumwgt;
  double a0,charge,e0;
  double attenuation;

  nbunches=param->n_bunch;
  a0=drive_data.a0[0];
  e0=param->e0;
  sigma_e=param->espread;
  nmacro=param->n_macro;
  charge=param->charge;
  ecut=param->ecut;

  /*
  file=fopen("match.ini","r");
  point=fgets(buffer,100,file);
  epsx=strtod(point,&point);
  betax=strtod(point,&point);
  alphax=strtod(point,&point);
  point=fgets(buffer,100,file);
  epsy=strtod(point,&point);
  betay=strtod(point,&point);
  alphay=strtod(point,&point);
  fclose(file);
  */

  epsx=param->emittx;
  epsy=param->emitty;
  alphax=param->alphax;
  alphay=param->alphay;
  betax=param->betax;
  betay=param->betay;

  epsx*=EMASS/e0*1e12*EMITT_UNIT;
  epsy*=EMASS/e0*1e12*EMITT_UNIT;

  file=fopen(name,"r");
  if (!file) {
    std::cout << "No such file: " << name << std::endl;
    exit(1);
  }
  fscanf(file,"%d",&n);
  /* scd to update for number of phases */
  bunch=bunch_make(nbunches,n,nmacro,1,0);

  bunch->drive_data->param.drive=(DRIVE_BEAM_PARAM*)
    xmalloc(sizeof(DRIVE_BEAM_PARAM));
  *(bunch->drive_data->param.drive)=*param;

  fscanf(file,"%g",&tmp1);
  wgtsum=0.0;
  
  de=2.0*ecut/(double)(nmacro);
  if (nmacro>1){
    store=(double*)xmalloc(sizeof(double)*nmacro);
    sumwgt=0.0;
    elow=-ecut;
    for (k=0;k<nmacro;k++){
      ehigh=elow+de;
      printf("%g %g\n",elow,ehigh);
      store[k]=0.0;
//      qromb(&gauss,elow,ehigh,&(store[k]));
//scd check
store[k]=gauss_bin(elow,ehigh);
      elow=ehigh;
      sumwgt+=store[k];
    }
    for (k=0;k<nmacro;k++){
      store[k]/=sumwgt;
      printf("wgt[%d]: %g\n",k,store[k]);
    }
  }

  for (i=0;i<n;i++){
    fscanf(file,"%g %g %g",&tmp1,&tmp2,&tmp3);
    for (k=0;k<nmacro;k++){
      if (nmacro>1){
	delta=-ecut+((double)k+0.5)*de;
	bunch_set_slice_energy(bunch,0,i,k,e0+delta*sigma_e);
	fact=store[k];
      }
      else{
	bunch_set_slice_energy(bunch,0,i,k,e0);
	fact=1.0;
      }
      bunch_set_slice_wgt(bunch,0,i,k,(double)tmp3*fact);
      wgtsum+=tmp3*fact;
#ifdef TWODIM
      bunch_set_slice_x(bunch,0,i,k,0.0);
      bunch_set_slice_xp(bunch,0,i,k,0.0);
      bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,epsx);
      bunch_set_sigma_xy(bunch,0,i,k);
#endif
      bunch_set_slice_y(bunch,0,i,k,zero_point);
      bunch_set_slice_yp(bunch,0,i,k,0.0);
      bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,epsy);
    }
    bunch->acc_field[0][i]=0.0;
    bunch->field->de[i]=tmp2*1e-3;
    bunch->z_position[i]=-tmp1;
  }
  fclose(file);

  for (i=0;i<n*nmacro;i++){
    bunch->particle[i].wgt/=wgtsum;
  }

  k=n*nmacro;
  if (param->z0) {
    for (j=1;j<nbunches;j++){
      for (i=0;i<n*nmacro;i++){
	bunch->z_position[k]=bunch->z_position[i]+param->z0[j]
	  +j*param->distance*1e6;
	bunch->particle[k]=bunch->particle[i];
	bunch->sigma[k]=bunch->sigma[i];
#ifdef TWODIM
	bunch->sigma_xx[k]=bunch->sigma_xx[i];
	bunch->sigma_xy[k]=bunch->sigma_xy[i];
#endif
	k++;
      }
    }
    for (i=0;i<n*nmacro;i++){
      bunch->z_position[i]+=param->z0[0];
    }
  }
  else{
    for (j=1;j<nbunches;j++){
      for (i=0;i<n*nmacro;i++){
	bunch->z_position[k]=bunch->z_position[i]+j*param->distance*1e6;
	bunch->particle[k]=bunch->particle[i];
	bunch->sigma[k]=bunch->sigma[i];
#ifdef TWODIM
	bunch->sigma_xx[k]=bunch->sigma_xx[i];
	bunch->sigma_xy[k]=bunch->sigma_xy[i];
#endif
	k++;
      }
    }
  }
  
  j1=std::min(param->n_ramp,nbunches);
  k=0;
  fact=param->s_ramp;
  for(j=0;j<j1;j++){
    for (i=0;i<n*nmacro;i++){
      bunch->particle[k].wgt*=fact;
      k++;
    }
    if (((j+1)%param->ramp_step)==0){
      fact+=(1.0-param->s_ramp)*(double)param->ramp_step/(double)param->n_ramp;
    }
  }

  k=n;
  for (j=1;j<nbunches;j++){
    for (i=0;i<n;i++){
      bunch->acc_field[0][k]=0.0;
      k++;
    }
  }

  //EA SINGLE MODE ONLY (only first mode used) - NO
  drive_longitudinal(bunch,drive_data.lambda_longitudinal[0]*1e6,charge);

  bunch->drive_data->longrange_max=1;

  bunch->drive_data->along[0]=a0*(charge*1.6e-19*1e12)*1e-6*1e-3;

  fact=1.0;
  attenuation=exp(-param->distance/drive_data.lambda_transverse[0]
		  *PI/drive_data.q_value[0]
		  *1.0/(1.0-drive_data.beta_group_t[0]));
  printf("Attenuation %g\n",attenuation);
  printf("fact[%d] %g\n",0,bunch->drive_data->along[0]);
  for (i=1;i<nbunches;i++){
    fact*=attenuation;
    bunch->drive_data->along[i]=bunch->drive_data->along[0]
      *(1.0-fabs(drive_data.beta_group_t[0])*param->distance*i
	/(drive_data.l_cav*(1.0-drive_data.beta_group_t[0])))*fact;
    printf("fact[%d] %g\n",i,bunch->drive_data->along[i]);
    if (bunch->drive_data->along[i]<=0.0) break;
  }
  bunch->drive_data->longrange_max=i-1;
  printf("longrange_max=%d\n",i-1);

  /* scd new 26.1.98*/
  if(param->resist){ 
    for (i=1;i<nbunches;i++){
      /* scd check order of magnitude */
      bunch->drive_data->blong[i]=resist_wall_transv((double)i*param->distance)
	*param->charge*1.602e-19*1e-9;
    }
  }

  if (nmacro>1){
    field_adjust(bunch);
    free(store);
  }
  if (param->resist){
    bunch->which_field=13;
  }
  else{
    bunch->which_field=112;
  }

  bunch->drive_data->ypos2=(double*)xmalloc(sizeof(double)*bunch->bunches);
  bunch->drive_data->yposb2=(double*)xmalloc(sizeof(double)*bunch->bunches);
  drive_data.speed=drive_data.beta_group_t[0]/(1.0-drive_data.beta_group_t[0])
    *1e-6/drive_data.l_cav;

  bunch->drive_data->empty_buckets=0;
  bunch->drive_data->bunches_per_train=bunch->bunches;

  longrange_fill(bunch,drive_data.lambda_transverse[0],
		 1e-6*fabs(bunch->z_position[1]-bunch->z_position[0]),
		 param->distance);
  /*
  for (i=0;i<nbunches*n;i++){
    bunch->z_position[i]=0.0;
  }
  */
  return bunch;
}

GIRDER* make_quad_girder_start(double qstr,double phase)
{
  ELEMENT *element;
  GIRDER *girder;
  double girderlength=1.615,bpmlength=0.083,quadlength=0.686;
  double driftlength;
  double z;

  girder=girder_make(girderlength);
  z=-0.5*girderlength;

  z+=0.5*bpmlength;
  z+=0.5*bpmlength;

  z+=0.5*0.04;
  z+=0.5*0.04;

  element=new QUADRUPOLE(0.5*quadlength,0.5*qstr*quadlength);
  z+=0.75*quadlength;
  girder_add_element(girder,element,z);
  z+=0.25*quadlength;

  element=new DRIFT(0.04*0.5*driftlength); // driftlength is uninitialised! JS
  z+=0.5*(0.04+0.5*driftlength);
  girder_add_element(girder,element,z);
  z+=0.5*(0.04+0.5*driftlength);

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder_add_element(girder,element,z);
  z+=0.5*rf_data.cavlength;

  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder_add_element(girder,element,z);
  z+=0.5*driftlength;

  element=new CAVITY(rf_data.cavlength,0,phase);
  z+=0.5*rf_data.cavlength;
  girder_add_element(girder,element,z);
  z+=0.5*rf_data.cavlength;

  driftlength=rf_data.girderdrift+rf_data.cavdrift;
  element=new DRIFT(driftlength);
  z+=0.5*driftlength;
  girder_add_element(girder,element,z);
  z+=0.5*driftlength;

  return girder;
}

GIRDER* make_quad_girder_end(double qstr)
{
  ELEMENT *element;
  double girderlength=1.615,bpmlength=0.083; // ,quadlength=0.303;
  GIRDER *girder=girder_make(girderlength);

  double z=-0.5*girderlength;

  if ((bpm_data.all==0)||((qstr>0.0)&&(bpm_data.all>0))
      ||((qstr<0.0)&&(bpm_data.all<0))){
    element=new BPM(bpmlength);
  }
  else{
    element=new BPM(bpmlength);
  }
  z+=0.5*bpmlength;
  girder_add_element(girder,element,z);
  z+=0.5*bpmlength;

  return girder;
}

void measure_quadrupoles(BEAMLINE *beamline,double *mean,double *sigma)
{
  double s=0.0,s2=0.0;
  int i,n;
  n=beamline->n_quad;
  for (i=0;i<n;i++){
    s+=beamline->quad[i]->offset.y;
    s2+=beamline->quad[i]->offset.y*beamline->quad[i]->offset.y;
  }
  s/=(double)n;
  s2/=(double)n;
  printf("%g %g\n",s,sqrt(std::max(0.0,s2-s*s)));
  *mean=s;
  *sigma=sqrt(s2-s*s);
}

void print_r(double r[])
{
  int i,j;
  for (j=0;j<4;j++){
    for (i=0;i<4;i++){
      printf("%g ",r[i+j*4]);
    }  
    printf("\n");
  }
  printf("\n");
}

void quadrupole_plot(BEAMLINE *beamline,char name[])
{
  int i,j=0;
  FILE *file;
  if (name!=NULL){
    file=fopen(name,"w");
  }
  else{
    file=stdout;
  }
  for (i=0;i<beamline->n_elements;i++){
    if (beamline->element[i]->is_quad()){
      fprintf(file,"%d %g %g\n",j++,beamline->element[i]->offset.y,
	      beamline->element[i]->offset.yp);
    }
  }
  fclose(file);
}

void
quadrupole_restore_positions(BEAMLINE *beamline,double s[])
{
  int i,n;
  n=beamline->n_quad;
  for (i=0;i<n;i++){
    beamline->quad[i]->offset.y=s[2*i];
    beamline->quad[i]->offset.yp=s[2*i+1];
  }
}

void quadrupole_save_positions(BEAMLINE *beamline,double s[])
{
  int i,n;
  n=beamline->n_quad;
  for (i=0;i<n;i++){
    s[2*i]=beamline->quad[i]->offset.y;
    s[2*i+1]=beamline->quad[i]->offset.yp;
  }
}

void quadrupoles_bin_set_error_abs(BEAMLINE *beamline,BIN *bin,
				   double strength[],double strength0[],
				   double sigma)
{
  int i,j;

  j=bin->quad[0];
  if (QUADRUPOLE *quad=beamline->element[j]->quad_ptr()) {
    quad->set_strength(strength0[quad->get_number()]*quad->get_length());
  }
  for (i=1;i<bin->nq;i++){
    j=bin->quad[i];
    if (QUADRUPOLE *quad=beamline->element[j]->quad_ptr()) {
      quad->set_strength(strength[quad->get_number()]*quad->get_length()*(1.0+sigma*gasdev()));
    }
  }
}

#ifdef TWODIM
void quadrupoles_bin_set_position_error_x(BEAMLINE *beamline,BIN *bin,
				     double position_x[],double position_y[],
				     int direction)
{
  int i,j;
  if (direction>0){
    for (i=0;i<bin->nq;i++){
      j=bin->quad[i];
      beamline->element[j]->offset.x+=position_x[beamline->element[j]->get_number()];
      beamline->element[j]->offset.y+=position_y[beamline->element[j]->get_number()];
    }
  }
  else{
    for (i=0;i<bin->nq;i++){
      j=bin->quad[i];
      beamline->element[j]->offset.x+=-position_x[beamline->element[j]->get_number()];
      beamline->element[j]->offset.y+=-position_y[beamline->element[j]->get_number()];
    }
  }
}
#endif

void quadrupoles_field_error(double quad0[],double quad[],int n,double sigma)
{
  int i;
  for (i=0;i<n;i++){
    quad[i]=quad0[i]*(1.0+sigma*gasdev());
  }
}

void quadrupoles_print(BEAMLINE *beamline,char *name)
{
  FILE *file;
  int i;
  file=fopen(name,"w");
  for (i=0;i<beamline->n_quad;i++){
    fprintf(file,"%d %g %g\n",i,beamline->quad[i]->offset.y,
	    beamline->quad[i]->offset.yp);
  }
  fclose(file);
}

void quadrupoles_save(BEAMLINE *beamline,double strength[])
{
  int i;
  for (i=0;i<beamline->n_quad;i++){
    strength[i]=beamline->quad[i]->get_strength();
  }
}

void quadrupoles_set_error(BEAMLINE *beamline,double strength[],double sigma,
		      double cut)
{
  int i;
  double tmp;
  for (i=0;i<beamline->n_quad;i++){
    while (fabs(tmp=gasdev())>cut) ;
    beamline->quad[i]->set_strength(strength[i]*beamline->quad[i]->get_length()*(1.0+sigma*tmp));
  }
}

void
quadrupoles_set_position_error(BEAMLINE *beamline,double position[],
			       int direction)
{
  int i;
  if (direction>0){
    for (i=0;i<beamline->n_quad;i++){
      beamline->quad[i]->offset.y+=position[i];
    }
  }
  else{
    for (i=0;i<beamline->n_quad;i++){
      beamline->quad[i]->offset.y+=-position[i];
    }
  }
}

void
right_side(double a0[],double a1[],double a2[],int nq,int nbpm,double w0,
	   double w,double w2,double bs0[],double bs1[],double bs2[],
	   double xr[])
{
  double sum0,sum1,sum2;
  int i,j;
  for (j=0;j<nq;j++){
    sum0=0.0;
    sum1=0.0;
    sum2=0.0;
    for (i=0;i<nbpm;i++){
      sum0+=a0[i+j*nbpm]*bs0[i];
      sum1+=a1[i+j*nbpm]*(bs1[i]-bs0[i]);
      sum2+=a2[i+j*nbpm]*(bs2[i]-bs0[i]);
      /*      sum1+=a1[i+j*nbpm]*(bs2[i]-bs1[i]);*/
    }
    xr[j]=-2.0*(w0*sum0+w*(sum1+w2*sum2));
  }
}

void
right_side_0(double a0[],int nq,int nbpm,double bs0[],double xr[])
{
  double sum0;
  int i,j;
  for (j=0;j<nq;j++){
    sum0=0.0;
    for (i=0;i<nbpm;i++){
      sum0+=a0[i+j*nbpm]*bs0[i];
    }
    xr[j]=-2.0*sum0;
  }
}

void
right_side_2(double a0[],double a1[],double a2[],int nq,int nbpm,double w0,
	     double w,double bs0[],double bs1[],double bs2[],double xr[])
{
  double sum0,sum1;
  int i,j;
  for (j=0;j<nq;j++){
    sum0=0.0;
    sum1=0.0;
    for (i=0;i<nbpm;i++){
      sum0+=0.0*a0[i+j*nbpm]*bs0[i]+0.0*a1[i+j*nbpm]*bs1[i]
	+a2[i+j*nbpm]*bs2[i];
      sum1+=(a1[i+j*nbpm]-a0[i+j*nbpm])*(bs1[i]-bs0[i])
	+(a2[i+j*nbpm]-a0[i+j*nbpm])*(bs2[i]-bs0[i])
	+(a2[i+j*nbpm]-a1[i+j*nbpm])*(bs2[i]-bs1[i]);
    }
    xr[j]=-2.0*(w0*sum0+w*sum1);
  }
}

/*
  Change the calculation of the average Twiss parameters.
 */
void twiss_emitt(FILE *file,int j,double s,BEAM *bunch,double k)
{
  double beta=0.0,alpha=0.0,beta0=0.0,alpha0=0.0,e;
  double beta_x=0.0,alpha_x=0.0,beta0_x=0.0,alpha0_x=0.0;
  if (bunch->particle_beam) {
    double wgtsum=0.0,xsum=0.0,ysum=0.0,xpsum=0.0,ypsum=0.0,esum=0.0;
    double sxxsum=0.0,sxxpsum=0.0,sxpxpsum=0.0,syysum=0.0,syypsum=0.0,sypypsum=0.0;
    for (int i=0;i<bunch->slices;++i){
      const PARTICLE &particle=bunch->particle[i];
      double energy=fabs(particle.energy);
      double wgt=fabs(particle.wgt);
      wgtsum+=wgt;
      xsum+=wgt*particle.x;
      xpsum+=wgt*particle.xp;
      ysum+=wgt*particle.y;
      ypsum+=wgt*particle.yp;
      esum+=wgt*energy;
      sxxsum+=wgt*(particle.x*particle.x);
      sxxpsum+=wgt*(particle.x*particle.xp);
      sxpxpsum+=wgt*(particle.xp*particle.xp);
      syysum+=wgt*(particle.y*particle.y);
      syypsum+=wgt*(particle.y*particle.yp);
      sypypsum+=wgt*(particle.yp*particle.yp);
    }
    xsum/=wgtsum;
    xpsum/=wgtsum;
    ysum/=wgtsum;
    ypsum/=wgtsum;
    esum/=wgtsum;
    sxxsum/=wgtsum;
    sxxpsum/=wgtsum;
    sxpxpsum/=wgtsum;
    syysum/=wgtsum;
    syypsum/=wgtsum;
    sypypsum/=wgtsum;
    double emitt_x=sqrt(sxxsum*sxpxpsum-sxxpsum*sxxpsum);
    double emitt_y=sqrt(syysum*sypypsum-syypsum*syypsum);
    beta_x=sxxsum/emitt_x;
    beta=syysum/emitt_y;
    alpha_x=-sxxpsum/emitt_x;
    alpha=-syypsum/emitt_y;
    e=wgtsum;
  } else {
    double eps=0.0,eps_x=0.0;
    int nc=bunch->slices/2;
    for (int i=0;i<bunch->slices;i++){
      eps+=(bunch->sigma[i].r11*bunch->sigma[i].r22
	    -bunch->sigma[i].r12*bunch->sigma[i].r21)*bunch->particle[i].wgt;
      beta+=bunch->sigma[i].r11*bunch->particle[i].wgt;
      alpha-=bunch->sigma[i].r12*bunch->particle[i].wgt;
#ifdef TWODIM
      eps_x+=(bunch->sigma_xx[i].r11*bunch->sigma_xx[i].r22
	      -bunch->sigma_xx[i].r12*bunch->sigma_xx[i].r21)
        *bunch->particle[i].wgt;
      beta_x+=bunch->sigma_xx[i].r11*bunch->particle[i].wgt;
      alpha_x-=bunch->sigma_xx[i].r12*bunch->particle[i].wgt;
#endif
    }
    eps=sqrt(eps);
    beta/=eps;
    alpha/=eps;
    double eps0=(bunch->sigma[nc].r11*bunch->sigma[nc].r22
	  -bunch->sigma[nc].r12*bunch->sigma[nc].r21);
    beta0=bunch->sigma[nc].r11;
    alpha0=-bunch->sigma[nc].r12;
    eps0=sqrt(eps0);
    beta0/=eps0;
    alpha0/=eps0;
#ifdef TWODIM
    eps_x=sqrt(eps_x);
    beta_x/=eps_x;
    alpha_x/=eps_x;
    double eps0_x=(bunch->sigma_xx[nc].r11*bunch->sigma_xx[nc].r22
	    -bunch->sigma_xx[nc].r12*bunch->sigma_xx[nc].r21);
    beta0_x=bunch->sigma_xx[nc].r11;
    alpha0_x=-bunch->sigma_xx[nc].r12;
    eps0_x=sqrt(eps0_x);
    beta0_x/=eps0_x;
    alpha0_x/=eps0_x;
#endif
    e=bunch->particle[nc].energy;
  }
  if (file) {
#ifdef TWODIM
    fprintf(file,"%d %g %g %g %g %g %g %g %g %g %g %g\n",j,s,e,
	    beta0_x,alpha0_x,beta_x,alpha_x,
	    beta0,alpha0,beta,alpha,k);
#else
    fprintf(file,"%d %g %g %g %g %g %g %g\n",j,s,e,beta0,alpha0,beta,alpha,
	    k);
#endif

  }
  else {  
    char dummy[255];
#ifdef TWODIM
    snprintf(dummy,255,"{%d %g %g %g %g %g %g %g %g %g %g %g}\n",j,s,e,
	    beta0_x,alpha0_x,beta_x,alpha_x,
	    beta0,alpha0,beta,alpha,k);
#else
    snprintf(dummy,255,"{%d %g %g %g %g %g %g %g}\n",j,s,e,
	    beta0,alpha0,beta,alpha,k);
#endif
    interp_append(dummy);
  }
}

/*
  Change the calculation of the average Twiss parameters.
 */

void twiss_emitt_full(FILE *file,int j,double s,BEAM *bunch,double /*k*/)
{
  int nc;
  double eps0,beta0,alpha0,e;
#ifdef TWODIM
  double eps0_x,beta0_x,alpha0_x;
#endif
  char dummy[255];

  nc=bunch->slices/2;

  eps0=(bunch->sigma[nc].r11*bunch->sigma[nc].r22
	-bunch->sigma[nc].r12*bunch->sigma[nc].r21);
  beta0=bunch->sigma[nc].r11;
  alpha0=-bunch->sigma[nc].r12;
  eps0=sqrt(eps0);
  beta0/=eps0;
  alpha0/=eps0;
#ifdef TWODIM
  eps0_x=(bunch->sigma_xx[nc].r11*bunch->sigma_xx[nc].r22
	  -bunch->sigma_xx[nc].r12*bunch->sigma_xx[nc].r21);
  beta0_x=bunch->sigma_xx[nc].r11;
  alpha0_x=-bunch->sigma_xx[nc].r12;
  eps0_x=sqrt(eps0_x);
  beta0_x/=eps0_x;
  alpha0_x/=eps0_x;
#endif
  e=bunch->particle[nc].energy;
  if (file) {
#ifdef TWODIM
    fprintf(file,"%d %g %g %g %g %g %g %g %g %g %g %g %g\n",j,s,e,
	    bunch->sigma_xx[nc].r11,
	    bunch->sigma_xx[nc].r12,
	    bunch->sigma_xx[nc].r22,
	    bunch->sigma_xy[nc].r11,
	    bunch->sigma_xy[nc].r12,
	    bunch->sigma_xy[nc].r21,
	    bunch->sigma_xy[nc].r22,
	    bunch->sigma[nc].r11,
	    bunch->sigma[nc].r12,
	    bunch->sigma[nc].r22);
#else
    fprintf(file,"%d %g %g %g %g %g\n",j,s,e,
	    bunch->sigma[nc].r11,
	    bunch->sigma[nc].r12,
	    bunch->sigma[nc].r22);
#endif
  }
  else {
#ifdef TWODIM
    snprintf(dummy,255,"%d %g %g %g %g %g %g %g %g %g %g %g %g\n",j,s,e,
	    bunch->sigma_xx[nc].r11,
	    bunch->sigma_xx[nc].r12,
	    bunch->sigma_xx[nc].r22,
	    bunch->sigma_xy[nc].r11,
	    bunch->sigma_xy[nc].r12,
	    bunch->sigma_xy[nc].r21,
	    bunch->sigma_xy[nc].r22,
	    bunch->sigma[nc].r11,
	    bunch->sigma[nc].r12,
	    bunch->sigma[nc].r22);
#else
    snprintf(dummy,255,"%d %g %g %g %g %g\n",j,s,e,
	    bunch->sigma[nc].r11,
	    bunch->sigma[nc].r12,
	    bunch->sigma[nc].r22);
#endif
    interp_append(dummy);
  }
}

void twiss_plot_full(BEAMLINE *beamline,BEAM *bunch0,char *name)
{
  ELEMENT **element;
  int j=0;
  FILE *file=NULL;
  double s=0.0;
  BEAM *bunch;

  bunch=bunch_remake(bunch0);
  beam_copy(bunch0,bunch);
  if (name) file=fopen(name,"w");
  else {file=stdout;}
  element=beamline->element;
  twiss_emitt(file,0,0.0,bunch,0.0);
  while(element[j]!=NULL){
    s+=0.5*element[j]->get_length();
    if (QUADRUPOLE *quad=element[j]->quad_ptr()){
      quad->step_half(bunch);
      twiss_emitt_full(file,j,s,bunch,quad->get_strength()/quad->get_length());
      quad->step_half(bunch);
    }
    else{
      element[j]->track(bunch);
    }
    s+=0.5*element[j]->get_length();
    j++;
  }
  twiss_emitt_full(file,j-1,s,bunch,0.0);
  if (name) fclose(file);
  beam_delete(bunch);
}

