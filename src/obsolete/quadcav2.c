  double factor,de0,length,half_length;
  double *de,tmp;
  int i;
  R_MATRIX r1,r2;

  factor=beam->factor;
  length=element->length;
  half_length=0.5*length;
  de=beam->acc_field[element->field];

/*
  Move beam to the centre of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=de[i/beam->macroparticles]*element->v1
      +factor*beam->field->de[i/beam->macroparticles];
#ifdef CAVITY_2
    quadcav_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			   beam->particle[i].energy,de0);
#endif
    quadcav_coefficients2(de0,0.5*element->v2,half_length,
			  beam->particle[i].energy,
			  &r1,&r2,30);
    tmp=beam->particle[i].y;
    beam->particle[i].y*=r1.r11;
    beam->particle[i].y+=r1.r12*beam->particle[i].yp;
    beam->particle[i].yp*=r1.r22;
    beam->particle[i].yp+=r1.r21*tmp;
#ifdef TWODIM
    beam->particle[i].x*=r2.r11;
    beam->particle[i].x+=r2.r12*beam->particle[i].xp;
    beam->particle[i].xp*=r2.r22;
    beam->particle[i].xp+=r2.r21*tmp;
#endif
#ifdef CAVITY_2
    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
    transpose_M(beam->sigma+i);
    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
#ifdef TWODIM
    mult_M_M(&r2,beam->sigma_xx+i,beam->sigma_xx+i);
    transpose_M(beam->sigma_xx+i);
    mult_M_M(&r2,beam->sigma_xx+i,beam->sigma_xx+i);
    transpose_M(&r1);
    mult_M_M(beam->sigma_xy+i,&r2,beam->sigma_xy+i);
    mult_M_M(&r1,beam->sigma_xy+i,beam->sigma_xy+i);
#endif
#endif
    beam->particle[i].energy+=half_length*de0;
  }

/*
  Apply the wakefield kick
*/

  if(wakefield_data.transv){
    wake_kick(element,beam,length);
  }

/*
  Move beam to the end of the structure
*/

  for (i=0;i<beam->slices;i++){
    de0=de[i/beam->macroparticles]*element->v1
      +factor*beam->field->de[i/beam->macroparticles];
    quadcav_coefficients2(de0,0.5*element->v2,half_length,
			  beam->particle[i].energy,
			  &r1,&r2,30);
    tmp=beam->particle[i].y;
    beam->particle[i].y*=r1.r11;
    beam->particle[i].y+=r1.r12*beam->particle[i].yp;
    beam->particle[i].yp*=r1.r22;
    beam->particle[i].yp+=r1.r21*tmp;
#ifdef TWODIM
    beam->particle[i].x*=r2.r11;
    beam->particle[i].x+=r2.r12*beam->particle[i].xp;
    beam->particle[i].xp*=r2.r22;
    beam->particle[i].xp+=r2.r21*tmp;
#endif
#ifdef CAVITY_2
    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
    transpose_M(beam->sigma+i);
    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
#ifdef TWODIM
    mult_M_M(&r2,beam->sigma_xx+i,beam->sigma_xx+i);
    transpose_M(beam->sigma_xx+i);
    mult_M_M(&r2,beam->sigma_xx+i,beam->sigma_xx+i);
    transpose_M(&r1);
    mult_M_M(beam->sigma_xy+i,&r2,beam->sigma_xy+i);
    mult_M_M(&r1,beam->sigma_xy+i,beam->sigma_xy+i);
#endif
#endif
    beam->particle[i].energy+=half_length*de0;
#ifdef CAVITY_2
    quadcav_sigma_step_end(beam->sigma+i,beam->sigma_xx+i,beam->sigma_xy+i,
			   beam->particle[i].energy,-de0);
#endif
    /*
    transpose_M(&r1);
    print_M(&r1);
printf("%g\n",r1.r11*r1.r22-r1.r12*r1.r21);
    quadcav_coefficients(de0,0.5*element->v2,half_length,
			 beam->particle[i].energy,
			 &r1,&r2);
    print_M(&r1);
printf("%g %g\n",(beam->particle[0].energy-de0*half_length)/beam->particle[0].energy,r1.r11*r1.r22-r1.r12*r1.r21);
printf("\n");
    */
  }
