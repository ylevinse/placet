double Comp_Emitt_0_old(char *b_1,int center)
{
  int i,n,n1=0;
  double *x1,*xp1,*y1,*yp1,*w1,*z;
  double *rx11_1,*rx22_1,*ry11_1,*ry22_1;
  double emitt,emitt0;
  double ym=0.0,sym=0.0,wsum=0.0,mry11=0.0,mry22=0.0;
  FILE *f1;
  char *point,buffer[1000];
  char ch;

  f1=fopen(b_1,"r");

  while(!feof(f1)){ 
    fgets(buffer,1000,f1);
    sscanf(buffer,"%c",&ch);
    if(isxdigit(ch)||ch=='-') n1 += 1;
  }
  rewind(f1);


  n=n1;
  z=(double*)malloc(sizeof(double)*n);
  w1=(double*)malloc(sizeof(double)*n);
  x1=(double*)malloc(sizeof(double)*n);
  xp1=(double*)malloc(sizeof(double)*n);
  y1=(double*)malloc(sizeof(double)*n);
  yp1=(double*)malloc(sizeof(double)*n);

  rx11_1=(double*)malloc(sizeof(double)*n);
  rx22_1=(double*)malloc(sizeof(double)*n);
  ry11_1=(double*)malloc(sizeof(double)*n);
  ry22_1=(double*)malloc(sizeof(double)*n);


  for(i=0;i<n;i++){

    point=fgets(buffer,1000,f1);
    z[i]=strtod(point,&point);
    w1[i]=strtod(point,&point);
    strtod(point,&point);
    x1[i]=strtod(point,&point);
    xp1[i]=strtod(point,&point);
    y1[i]=strtod(point,&point);
    yp1[i]=strtod(point,&point);

    rx11_1[i]=strtod(point,&point);
    strtod(point,&point);
    rx22_1[i]=strtod(point,&point);
    ry11_1[i]=strtod(point,&point);
    strtod(point,&point);
    ry22_1[i]=strtod(point,&point);

    if(center==0){
      ym += w1[i]*y1[i];
      sym += w1[i]*yp1[i];

    }

    wsum += w1[i];
    mry11 += w1[i]*sqrt(ry11_1[i]);
    mry22 +=  w1[i]*sqrt(ry22_1[i]);
    
  }
   
  mry11 /= wsum;
  mry22 /= wsum;

  /// Compute the emittance when the beam is brought back to the beamline

  if(center==0){
    ym /= wsum;
    sym /= wsum;

    for(i=0;i<n;i++){
      y1[i] -= ym;
      yp1[i] -= sym; 
    }
  }

  for(i=0;i<n;i++){
    y1[i] /= mry11;
    yp1[i] /= mry22; 
  }    

  fclose(f1);

  emitt = emittance_calc_0(w1,n,y1,yp1);

  for(i=0;i<n;i++){
    y1[i]=0.0;
    yp1[i]=0.0;
  }

  emitt0 = emittance_calc_0(w1,n,y1,yp1);

  free(z);
  free(w1);
  free(x1);
  free(xp1);
  free(y1);
  free(yp1);
  free(rx11_1);
  free(rx22_1);
  free(ry11_1);
  free(ry22_1);

  return (emitt-emitt0)/emitt0;
}

double emittance_calc_0(double w[],int n,
			double y[],double yp[])
{
  int i;
  double syy=0.0,syyp=0.0,sypyp=0.0,wsum=0.0;

  for(i=0;i<n;i++){
    wsum+=w[i];
    syy+=w[i]*(1.0+y[i]*y[i]);
    syyp+=w[i]*(y[i]*yp[i]);
    sypyp+=w[i]*(1.0+yp[i]*yp[i]);
  }
  syy/=wsum;
  syyp/=wsum;
  sypyp/=wsum;
  return sqrt(syy*sypyp-syyp*syyp);
}
