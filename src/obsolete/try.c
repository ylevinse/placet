#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

step_rf(double g1[],double g2[],double h1[],double h2[],int n1,int n2)
{
  int i;
  for (i=n2-1;i>0;i--){
    g2[i]=g2[i-1];
    h2[i]=h2[i-1];
  }
  g2[0]=h1[n1-1];
  h2[0]=g1[n1-1];
  for (i=n1-1;i>0;i--){
    g1[i]=g1[i-1];
    h1[i]=h1[i-1];
  }
}

feed_rf(double g1[],double h1[],double g0,double h0)
{
  g1[0]=g0;
  h1[0]=h0;
}

step_beam(double g1[],double g2[],double h1[],double h2[],int n1,int n2,
	  double ch1,double ch2,double *e1,double *e2)
{
  int i;
  double s1=0.0,s2=0.0;
  for(i=0;i<n1;i++){
    s1+=g1[i];
    s2+=h1[i];
    g1[i]-=ch1;
    h1[i]-=ch2;
  }
  for(i=0;i<n2;i++){
    s1+=g2[i];
    s2+=h2[i];
    g2[i]-=ch1;
    h2[i]-=ch2;
  }
  *e1=s1;
  *e2=s2;
}

step(double g1[],double g2[],double h1[],double h2[],int n1,int n2,
     double ch1[],double ch2[],double g0[],double h0[],int nstep,
     double e1[],double e2[])
{
  int i;
  for (i=0;i<n1;i++){
    g1[i]=0.0;
    h1[i]=0.0;
  }
  for (i=0;i<n2;i++){
    g2[i]=0.0;
    h2[i]=0.0;
  }
  for(i=0;i<nstep;i++){
    step_rf(g1,g2,h1,h2,n1,n2);
    feed_rf(g1,h1,g0[i],h0[i]);
    step_beam(g1,g2,h1,h2,n1,n2,ch1[i],ch2[i],e1+i,e2+i);
  }
}

profile(double g1[],double g2[],double h1[],double h2[],int n1,int n2)
{
  int i;
  for(i=0;i<n1;i++){
    printf("%g %g\n",g1[i],h1[i]);
  }
  for(i=0;i<n2;i++){
    printf("%g %g\n",g2[i],h2[i]);
  }
}

#define N1 28
#define N2 28
#define N 1000
#define NF 50
#define ND1 0
#define ND2 0
#define A 0.0

main(int argc,char *argv[])
{
  int i;
  double g1[N1],g2[N2],h1[N1],h2[N2];
  double ch1[N],ch2[N],p1[N],p2[N],e1[N],e2[N];

  for (i=0;i<300;i++){
    ch1[i]=1.0;
    ch2[i]=0.0;
  }
  for (i=300;i<300+NF;i++){
    ch1[i]=(299+NF-i)/(double)NF;
    ch2[i]=(i-300)/(double)NF;
  }
  for (i=300+NF;i<N;i++){
    ch1[i]=0.0;
    ch2[i]=1.0;
  }

  for (i=0;i<300;i++){
    p1[i]=200.0;
    p2[i]=200.0;
  }
  for (i=300;i<300+ND1;i++){
    p1[i]=200.0-(double)(i-300)/(double)ND1*A;
    p2[i]=200.0+(double)(i-300)/(double)ND1*A;
  }
  for (i=300+ND1;i<300+ND1+ND2;i++){
    p1[i]=200.0-(double)(300+ND1+ND2-i)/(double)(ND2)*A;
    p2[i]=200.0+(double)(300+ND1+ND2-i)/(double)(ND2)*A;
  }
  for (i=300+ND1+ND2;i<N;i++){
    p1[i]=200.0;
    p2[i]=200.0;
  }

  step(g1,g2,h1,h2,N1,N2,ch1,ch2,p1,p2,N,e1,e2);
  //profile(g1,g2,h1,h2,N1,N2);
  //exit(0);
  for(i=0;i<N;i++){
    if (ch1[i]==0.0){
      e1[i]=0.0;
    }
    if (ch2[i]==0.0){
      e2[i]=0.0;
    }
    printf("%g %g %g %g\n",e1[i],e2[i],ch1[i],ch2[i]);
  }
  exit(0);
}
