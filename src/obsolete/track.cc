void
bunch_backtrack_0(BEAMLINE * /*beamline*/,BEAM * /*bunch*/,int /*start*/,int /*stop*/)
{
  placet_cout << ERROR << "function " << __FUNCTION__ << " is not implemented in this version" << endmsg;
/*
  ELEMENT **element;
  element=beamline->element;
  start--;
  for (;start>=stop;start--){
    element_stepback_0(element[start],bunch);
  }
*/
}

void bunch_track_callback(BEAMLINE *beamline,BEAM *beam,int start,int stop)
{
  ELEMENT **element=beamline->element;
  for (;start<stop;start++){
    element[start]->track(beam);
    if (element[start]->callback) 
      (*(element[start]->callback->funct))(beam,start,
					   element[start]->callback->interp,
					   element[start]->callback->script);
  }
}

void
bunch_track_gradient(BEAMLINE *beamline,BEAM *beam,int start,int stop,
		     double gradient)
{
  for (;start<stop;start++){
    ELEMENT *element=beamline->element[start];
    if (CAVITY *cavity=dynamic_cast<CAVITY*>(element)) {
      double tmp=cavity->get_gradient();
      cavity->set_gradient(gradient*tmp);
      cavity->track(beam);
      cavity->set_gradient(tmp); 
    } else {
      element->track(beam);
    }
  }
}

void bunch_track_line(BEAMLINE *beamline,BEAM *bunch,int do_emitt)
{
  FILE *file,*file2;
  int n_el,n_sl;
  int i_el,i_sl;
  double y0,yp0,w0;
  static int cavnum=0;

  n_el=beamline->n_elements;
  n_sl=bunch->slices;
  inter_data.bunch=bunch;
  for (i_el=0;i_el<n_el;i_el++){
    beamline->element[i_el]->step(bunch);
    / * scdxxx * /
    if (is_cavity(beamline->element[i_el])){
      if (cavnum==0){
      	bunch_save("tmp.dat",bunch);
      }
      cavnum++;
    }
    if (do_emitt){
      / *      if (i_el%10==0){ * /
      if (beamline->element[i_el]->is_bpm()){
	y0=0.0; yp0=0.0; w0=0.0;
	for(i_sl=0;i_sl<n_sl;i_sl++){
	  y0+=bunch->particle[i_sl].y;
	  yp0+=bunch->particle[i_sl].yp;
	  w0+=bunch->particle[i_sl].wgt;
	}
	y0/=w0; yp0/=w0;
	for(i_sl=0;i_sl<n_sl;i_sl++){
	  fprintf(file,"%g %g %g\n",bunch->particle[i_sl].y-y0,
		  bunch->particle[i_sl].yp-yp0,
		  bunch->particle[i_sl].energy);
	}
	fprintf(file2,"%g\n",emitt_y(bunch));
      }
    }
  }
}

void bunch_track_line_0(BEAMLINE *beamline,BEAM *bunch,int output)
{
  FILE *file,*file2;
  int n_el,n_sl;
  int i_el,i_sl;
  double y0,yp0,w0;

  n_el=beamline->n_elements;
  n_sl=bunch->slices;
  inter_data.bunch=bunch;
  if (output){
    file=fopen("tmp.line","w");
    file2=fopen("emitt.line","w");
    fprintf(file,"%d %d\n",n_el,n_sl);
  }
  for (i_el=0;i_el<n_el;i_el++){
    beamline->element[i_el]->track_0(bunch);
    if (output){
      / *      if (i_el%10==0){* /
      if (beamline->element[i_el]->is_bpm()){
	y0=0.0; yp0=0.0; w0=0.0;
	for(i_sl=0;i_sl<n_sl;i_sl++){
	  y0+=bunch->particle[i_sl].y;
	  yp0+=bunch->particle[i_sl].yp;
	  w0+=bunch->particle[i_sl].wgt;
	}
	y0/=w0; yp0/=w0;
	for(i_sl=0;i_sl<n_sl;i_sl++){
	  fprintf(file,"%g %g %g\n",bunch->particle[i_sl].y-y0,
		  bunch->particle[i_sl].yp-yp0,
		  bunch->particle[i_sl].energy);
	}
	fprintf(file2,"%g\n",emitt_y(bunch));
      }
    }
  }
  if (output){
    fclose(file);
    fclose(file2);
  }
}

void
bunch_track_noacc(BEAMLINE *beamline,BEAM *beam,int start,int stop)
{
  for (;start<stop;start++){
    ELEMENT *element=beamline->element[start];
    if (CAVITY *cavity=element->cavity_ptr()) {
      double tmp=cavity->get_gradient();
      cavity->set_gradient(0.0);
      cavity->track(beam);
      cavity->set_gradient(tmp); 
    } else {
      element->track(beam);
    }
  }
}

void emitt_clear()
{
  int i,j;
  j=emitt_data.nswitch;
  for (i=0;i<emitt_data.nmax[j];i++){
    emitt_data.e[j][i]=0.0;
    emitt_data.e2[j][i]=0.0;
    emitt_data.ym[j][i]=0.0;
    emitt_data.ysig[j][i]=0.0;
#ifdef TWODIM
    emitt_data.ex[j][i]=0.0;
    emitt_data.ex2[j][i]=0.0;
    emitt_data.xm[j][i]=0.0;
    emitt_data.xsig[j][i]=0.0;
#endif
    emitt_data.n[j][i]=0;
  }
}

void emitt_clear_2()
{
  int i,j,k,n;
  j=emitt_data_2.nswitch;
  n=emitt_data_2.nbunch[j];
  for (i=0;i<emitt_data_2.nmax[j];i++){
    for (k=0;k<n;k++){
      emitt_data_2.e[j][i*n+k]=0.0;
      emitt_data_2.e2[j][i*n+k]=0.0;
      emitt_data_2.ym[j][i*n+k]=0.0;
      emitt_data_2.ysig[j][i*n+k]=0.0;
#ifdef TWODIM
      emitt_data_2.ex[j][i*n+k]=0.0;
      emitt_data_2.ex2[j][i*n+k]=0.0;
      emitt_data_2.xm[j][i*n+k]=0.0;
      emitt_data_2.xsig[j][i*n+k]=0.0;
#endif
    }
    emitt_data_2.n[j][i]=0;
    emitt_data_2.etot_y[j][i]=0.0;
#ifdef TWODIM
    emitt_data_2.etot_x[j][i]=0.0;
#endif
  }
}
