void drive_longitudinal_2(BEAM *bunch,double lambda,double charge)
{
  int i,j,n,l;
  double beta_g,r_over_q,l_cav,power=0.0;
  double loss,l_vis,dist,dist_max,scale,k,*wgt,tmp,emax=0.0;

  n=bunch->slices/bunch->macroparticles;
  wgt=(double*)alloca(sizeof(double)*bunch->bunches*bunch->slices_per_bunch);
  l=0;
  for (i=0;i<n;i++){
    tmp=0.0;
    for (j=0;j<bunch->macroparticles;j++){
      tmp+=bunch->particle[l].wgt;
      l++;
    }
    wgt[i]=tmp;
  }
  //EA SINGLE MODE ONLY (only first mode used) - NO
  r_over_q=drive_data.r_over_q[0];
  l_cav=drive_data.l_cav*1e6;
  //EA SINGLE MODE ONLY (only first mode used)
  beta_g=drive_data.beta_group_l[0];
  k=2.0*PI/lambda;
  dist_max=l_cav*(1.0-beta_g)/fabs(beta_g);
  //EA SINGLE MODE ONLY (only first mode used)
  scale=1e-9*(charge*1.602e-19)*C/drive_data.lambda_longitudinal[0]
    *2.0*PI/l_cav;
  scale/=(1.0-beta_g);
  for (j=0;j<n;j++){
    loss=0.5*l_cav*r_over_q*wgt[j];
    for (i=j-1;i>=0;i--){
      dist=bunch->z_position[j]-bunch->z_position[i];
      if (dist>dist_max) break;
      l_vis=l_cav-beta_g/(1.0-beta_g)*dist;
      loss+=l_vis*r_over_q*wgt[i]*cos(dist*k);
    }
    bunch->field->de[j]=-loss*scale;
  }
  for (i=bunch->slices-bunch->slices_per_bunch;
       i<n;i++){
    power-=bunch->field->de[i]*wgt[i];
    emax=min(emax,bunch->field->de[i]);
  }
  printf("power %g %g\n",power,power*C/bunch->drive_data->param.drive->distance
	 *l_cav*1e-6*1.602e-10*bunch->drive_data->param.drive->charge*1e-6);
  printf("maxloss %g\n",emax*l_cav*1e-6);
}

void longrange_fill_2(BEAM *bunch,double lambda,double delta_z,double dist)
{
  int i,j,nbunch,nslice,p=0,ntmp;
  double phi,phi0,a,b;

  ntmp=(int)floor(dist/lambda);
  dist-=ntmp*lambda;
  dist*=2.0*PI/lambda;
  delta_z=2.0*PI*delta_z/lambda;
  nbunch=bunch->bunches;
  nslice=bunch->slices_per_bunch;
  phi0=0.5*PI;
  for (j=0;j<nbunch;j++){
    phi=phi0;
    for (i=0;i<nslice;i++){
      a=sin(phi);
      b=cos(phi);
      bunch->drive_data->af[p]=a;
      bunch->drive_data->bf[p]=b;
      p++;
      phi+=delta_z;
    }
    phi0+=dist;
  }
}

void longrange_fill_long(BEAM *bunch,double lambda,double delta_z,double dist)
{
  int i,j,nbunch,nslice,p=0,ntmp;
  double phi,phi0,a,b;

  ntmp=(int)floor(dist/lambda);
  dist-=ntmp*lambda;
  dist*=2.0*PI/lambda;
  delta_z=2.0*PI*delta_z/lambda;
  nbunch=bunch->bunches;
  nslice=bunch->slices_per_bunch;
  phi0=0.5*PI;
  for (j=0;j<nbunch;j++){
    phi=phi0;
    for (i=0;i<nslice;i++){
      a=sin(phi);
      b=cos(phi);
      bunch->drive_data->al[p]=a;
      bunch->drive_data->bl[p]=b;
      p++;
      phi+=delta_z;
    }
    phi0+=dist;
  }
}

/* to allow for different bunch spacing */

void longrange_fill_long_new(BEAM *bunch,double lambda)
{
  int i,j,nbunch,nslice,p=0;
  double phi,phi0,a,b;

  lambda*=1e6;
  nbunch=bunch->bunches;
  nslice=bunch->slices_per_bunch;
  phi0=0.5*PI;
  for (j=0;j<nbunch;j++){
    for (i=0;i<nslice;i++){
      phi=bunch->z_position[j*nslice+i]*TWOPI/lambda+phi0;
      a=sin(phi);
      b=cos(phi);
      bunch->drive_data->al[p]=a;
      bunch->drive_data->bl[p]=b;
      p++;
    }
  }
}

/* for variable bunch spacing */

void longrange_fill_new(BEAM *beam,double lambda)
{
  int i,j,nbunch,nslice,p=0;
  double phi0,a,b,k;

  lambda*=1e6;
  phi0=0.5*PI;
  nbunch=beam->bunches;
  nslice=beam->slices_per_bunch;
  k=TWOPI/lambda;
  for (j=0;j<nbunch;j++){
    for (i=0;i<nslice;i++){
      sin_cos(beam->z_position[p]*k+phi0,&a,&b);
      beam->drive_data->af[p]=a;
      beam->drive_data->bf[p]=b;
      p++;
    }
  }
}

void longrange_fill_old(BEAM *beam,double lambda)
{
  int i,j,nbunch,nslice,p=0;
  double phi,phi0,a,b;

  lambda*=1e6;
  phi0=0.5*PI;
  nbunch=beam->bunches;
  nslice=beam->slices_per_bunch;
  for (j=0;j<nbunch;j++){
    for (i=0;i<nslice;i++){
      phi=beam->z_position[j*nslice+i]*2.0*PI/lambda+phi0;
      a=sin(phi);
      b=cos(phi);
      beam->drive_data->af[p]=a;
      beam->drive_data->bf[p]=b;
      p++;
    }
  }
}

/* calculates the RF kick for a particle at final position r2 with an angle
   rp in a cavity of length L and gradient grad
*/

double rf_kickno(double grad,double L,double r2,double /*rp*/,double /*d1*/,double /*d2*/)
{
  int norder;
  norder=drive_data.rf_order;
  return grad*L*(norder)*pow(r2,(double)(norder-1));
}

double rf_kickxxx(double grad,double L,double r2,double rp)
{
  int norder;
  norder=drive_data.rf_order;
  return 0.5*grad*L*(norder)*(pow(r2,(double)(norder-1))
			      +pow(r2-rp*L,(double)(norder-1)));
}

double rf_kick(double grad,double L,double r2,double rp,double s1,double s2)
{
  double dr,eps=1e-6;
  L=s2-s1;
  dr=rp*L;
  if (dr*dr<eps){
    return grad*L*(drive_data.rf_order)*pow(r2,drive_data.rf_order-1);
  }
  return grad*L
    *(pow(r2,drive_data.rf_order)
      -pow(r2-dr,drive_data.rf_order))/dr;
}

void rf_kick_fill(BEAM *beam,double lambda,double charge)
{
  int i,j,n,l,norder;
  double beta_g,r_over_q,l_cav,a;
  double loss,l_vis,dist,dist_max,scale,k,*wgt,tmp;
  norder=drive_data.rf_order-1;
  a=drive_data.rf_a0;
  placet_printf(INFO,"lambda= %g  charge= %g\n",lambda,charge);
  n=beam->slices/beam->macroparticles;
  wgt=(double*)alloca(sizeof(double)*n);
  l=0;
  for (i=0;i<n;i++){
    tmp=0.0;
    for (j=0;j<beam->macroparticles;j++){
      tmp+=beam->particle[l].wgt;
      l++;
    }
    wgt[i]=tmp;
  }

  beam->drive_data->rf_kick=(double*)malloc(sizeof(double)*beam->bunches
					    *beam->slices_per_bunch);
  //EA SINGLE MODE ONLY (only first mode used) - NO
  r_over_q=drive_data.r_over_q[0];
  l_cav=drive_data.l_cav*1e6;
  //EA SINGLE MODE ONLY (only first mode used)
  beta_g=drive_data.beta_group_l[0];
  n=beam->slices/beam->macroparticles;
  k=2.0*PI/lambda;
  dist_max=l_cav*(1.0-beta_g)/fabs(beta_g);
  //EA SINGLE MODE ONLY (only first mode used)
  scale=1e-9*(charge*1.602e-19)*C/drive_data.lambda_longitudinal[0]
    *2.0*PI/l_cav*r_over_q*pow(a,-(norder+1))*lambda/(2.0*PI);
  scale/=(1.0-beta_g);
printf("scale is %g\n",scale);
  for (j=0;j<n;j++){
    loss=0.0*l_cav*wgt[j];
    for (i=j-1;i>=0;i--){
      dist=beam->z_position[j]-beam->z_position[i];
      if (dist>dist_max) break;
      l_vis=l_cav-beta_g/(1.0-beta_g)*dist;
      loss+=l_vis*wgt[i]*sin(dist*k);
    }
    beam->drive_data->rf_kick[j]=-loss*scale;
    //    placet_printf(INFO,"kick %g %g\n",loss,scale);
  }
}

double rf_kick4(double grad,double L,double r2,double rp,double s1,double s2)
{
  double dr,r1,eps=1e-6;
  L=s2-s1;
  dr=rp*L;
  r1=r2-dr;
  if (dr*dr<eps){
    return grad*L*4.0*r2*r2*r2;
  }
  return grad*L*(r2*r2*r2*r2-r1*r1*r1*r1)/dr;
}

double rf_kick8(double grad,double L,double r2,double rp,double s1,double s2)
{
  double dr,r1,eps=1e-6;
  L=s2-s1;
  dr=rp*L;
  r1=r2-dr;
  if (dr*dr<eps){
    r1=r2*r2;
    return grad*L*8.0*r1*r1*r1*r2;
  }
  r2*=r2;
  r2*=r2;
  r1*=r1;
  r1*=r1;
  return grad*L*(r2*r2-r1*r1)/dr;
}

/*
  calculates the longitudinal field in dependence of the radius
*/

double rf_long(double /*de0*/,double /*L*/,double r2,double /*rp*/)
{
  return 1.0+drive_data.rf_size[0]*pow(r2/drive_data.rf_a0,drive_data.rf_order);
}

double rf_longxxx(double /*de0*/,double L,double r2,double rp)
{
  double dr;
  dr=L*rp;
  return 1.0+drive_data.rf_size[0]
    *(pow(r2/drive_data.rf_a0,drive_data.rf_order+1)
      -pow((r2-dr),drive_data.rf_order+1))/(dr*(drive_data.rf_order+1));
}

void rf_kick2(int order,double a0_i,double grad,double L,double phi0,double x,
	      double xp,double y,double yp,double s1,double s2,double *kx,
	      double *ky)
{
  double r,c,s,phi,k;
  L=s2-s1;
  x-=0.5*L*xp;
  y-=0.5*L*yp;
  phi=phi0+atan2(x,y);
  s=sin(order*phi);
  c=cos(order*phi);
  r=sqrt(x*x+y*y)*a0_i;
  /* check for size */
  //k=grad*L*(double)order*pow(r,(double)(order-2))*a0_i*a0_i;
  k=grad*L*(double)order*pow_i(r,order-2)*a0_i*a0_i;
  // printf("%d %g %g %g\n",order,grad,x,y);
  /* check for sign of sin() */
  *kx=k*(c*x-s*y);
  *ky=k*(s*x+c*y);
  //printf("%g %g\n",*kx,*ky);
}

double rf_kick48(double a0_i,double grad,double grad2,double L,double r2,
		 double rp,double s1,double s2)
{
  double dr,r1,eps=1e-6;
  L=s2-s1;
  dr=rp*L;
  r1=r2-dr;
  r1*=a0_i;
  r2*=a0_i;
  if (dr*dr<eps){
    r1=r2*r2;
    return L*(8.0*grad*r1*r1+grad2*4.0)*r1*r2;
  }
  r2*=r2;
  r2*=r2;
  r1*=r1;
  r1*=r1;
  return L*((r2*r2-r1*r1)*grad+(r2-r1)*grad2)/dr;
}

double rf_kickxx(double grad,double L,double r2,double rp,double /*d1*/,double /*d2*/)
{
  int norder;
  double r1p,r2p,r21,r1,eps=1e-10;

  norder=drive_data.rf_order-1;
  r21=rp*L;
  if (rp*rp<eps){
    return grad*L*(norder+1)*pow(r2,(double)norder);
  }
  r1=r2-r21;
  r1p=pow(r1,norder);
  r2p=pow(r2,norder);
  return 2.0*grad/(L*(norder+2))
    *(pow(r2,norder)*((norder+1)*L*L+(norder)*r1*L/rp)
      -(r1/rp)*(r1/rp)*(r2p-r1p));
}

double rf_kicknothere(double grad,double L,double r2,double rp,double s1,double s2)
{
  int norder;
  double r1p,r2p,rf,eps=1e-10;

  norder=drive_data.rf_order;
  if (rp*rp<eps){
    return grad*(s2-s1)*(norder)*pow(r2,(double)(norder-1));
  }

  r1p=pow(r2-rp*(s2-s1),norder-1);
  r2p=pow(r2,norder-1);

  rf=(r2-rp*s2)/rp;
  return 2.0*grad/((double)(norder+1)*L)
    *(r2p*(rf*((norder-1)*s2-rf)+norder*s2*s2)
      -r1p*(rf*((norder-1)*s1-rf)+norder*s1*s1));
}

/* creates the beam for the drivebeam including the RF-kick */

void set_zero_z_position(BEAM *beam)
{
  int i,n;

  n=beam->slices;
  for (i=0;i<n;i++){
    beam->z_position[i]=0.0;
  }
}
