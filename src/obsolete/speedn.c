#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../fftw/fftw-2.1.3/fftw/fftw.h"
#include "rndm.d"

#define TWOPI 6.283185307179586

#define MTOTAL 1324
#define N nfound
#define M 100
//#define STEP 10
#define STEP 1
#define START 1
#define NFILE 5000
#define SINGLE 0

main(int argc,char *argv[])
{
  int i,j,m,mdo,mdone,nfound;
  fftw_complex *in,*norm,tmp;
  fftw_plan p,p2;
  double r;
  FILE *file;
  char buffer[1000],*point;
  double d[M],d0[M];
  float f[M];


  file=fopen("freq.dat","r");
  point=fgets(buffer,1000,file);
  N=strtol(point,&point,10);

  in=(fftw_complex*)malloc(sizeof(fftw_complex)*N*M);
  norm=(fftw_complex*)malloc(sizeof(fftw_complex)*N);
  rndmst8(strtol(argv[1],NULL,10));

  p = fftw_create_plan_specific(N, FFTW_FORWARD, FFTW_ESTIMATE | FFTW_IN_PLACE,
				norm,1,norm,1);
  p2 = fftw_create_plan_specific(N, FFTW_BACKWARD,
				 FFTW_ESTIMATE | FFTW_IN_PLACE,
				in,1,in,1);
  for (i=0;i<N;i++){
    point=fgets(buffer,1000,file);
//    strtod(point,&point);
    norm[i].re=strtod(point,&point);
    norm[i].im=0.0;
  }
  fclose(file);

  m=0;
  mdo=M;
  if (mdo>MTOTAL) mdo=MTOTAL;

  for (i=0;i<mdo;i++){
      for (j=0;j<N;j++){
	  r=TWOPI*rndm8();
	  tmp.re=cos(r);
	  tmp.im=sin(r);
	  in[m].re=tmp.re*norm[j].re-tmp.im*norm[j].im;
	  in[m].im=tmp.re*norm[j].im+tmp.im*norm[j].re;
	  m++;
      }
  }
  fftw(p2,mdo,in,1,N,NULL,1,N);

  m=START*STEP;
  m=m-(int)(m/N)*N;
  for (i=0;i<mdo;i++){
    d0[i]=in[m+i*N].re;
  }
  for (j=0;j<NFILE;j++){
      snprintf(buffer,1000,"res.%d\0",j);
      file=fopen(buffer,"w");
      m=(START+j)*STEP;
      m=m-(int)(m/N)*N;

      if (SINGLE) {
	  for (i=0;i<mdo;i++){
	    //	      f[i]=in[m+i*N].re;
	      f[i]=in[m+i*N].re-d0[i];
	      d0[i]=in[m+i*N].re;
	  }
	  fwrite(f,sizeof(float)*mdo,1,file);
      }
      else {
	  for (i=0;i<mdo;i++){
	    //	      d[i]=in[m+i*N].re;
	      d[i]=in[m+i*N].re-d0[i];
	      d0[i]=in[m+i*N].re;
//	      fwrite(&in[m+i*N].re,sizeof(double),1,file);
	  }
	  fwrite(d,sizeof(double)*mdo,1,file);
      }
      fclose(file);
  }
  
  mdone=mdo;
  while (mdone<MTOTAL) {

    printf("mdone is %d and MTOTAL %d\n",mdone,MTOTAL);

      mdo=MTOTAL-mdone;
      if (mdo>M) mdo=M;
      m=0;
      for (i=0;i<mdo;i++){
	  for (j=0;j<N;j++){
	      r=TWOPI*rndm8();
	      tmp.re=cos(r);
	      tmp.im=sin(r);
	      in[m].re=tmp.re*norm[j].re-tmp.im*norm[j].im;
	      in[m].im=tmp.re*norm[j].im+tmp.im*norm[j].re;
	      m++;
	  }
      }
      fftw(p2,mdo,in,1,N,NULL,1,N);

      m=START*STEP;
      m=m-(int)(m/N)*N;
      for (i=0;i<mdo;i++){
	d0[i]=in[m+i*N].re;
      }
      for (j=0;j<NFILE;j++){
	  snprintf(buffer,1000,"res.%d\0",j);
	  file=fopen(buffer,"a");
	  m=(START+j)*STEP;
	  m=m-(int)(m/N)*N;
	  if (SINGLE) {
	      for (i=0;i<mdo;i++){
		//		  f[i]=in[m+i*N].re;
		  f[i]=in[m+i*N].re-d0[i];
		  d0[i]=in[m+i*N].re;
	      }
	      fwrite(f,sizeof(float)*mdo,1,file);
	  }
	  else {
	      for (i=0;i<mdo;i++){
		//		  d[i]=in[m+i*N].re;
		  d[i]=in[m+i*N].re-d0[i];
		  d0[i]=in[m+i*N].re;
//		  fwrite(&in[m+i*N],sizeof(double),1,file);
	      }
	      fwrite(d,sizeof(double)*mdo,1,file);
	  }
	  fclose(file);
      }
      mdone+=mdo;
  }
  fftw_destroy_plan(p);
  fftw_destroy_plan(p2);
  exit(0);
}

