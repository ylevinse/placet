#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <ctime>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"
#include "girder.h"
#include "placeti3.h"
#include "quadrupole.h"
#include "cavity.h"
#include "beamline.h"
#include "dipole.h"
#include "bpm.h"

/* Function prototypes */

#include "transport.h"

struct{
  double wavelength,*phase;
} transport_data;

void
solenoid_list_transport(FILE *file,ELEMENT *element)
{
  double f;
  f=element->v1*10.0;
  fprintf(file," 19.0  %12E;\n",element->length,f);
}

void
solcav_list_transport(FILE *file,ELEMENT *element)
{
  double f,g;
  f=element->v2*10.0;
  g=element->v1*element->length;
  fprintf(file," 19.0  %12E %12E %12E %12E %12E;\n",element->length,f,g,
	  transport_data.phase[element->field],transport_data.wavelength);
}

void
quadcav_list_transport(FILE *file,ELEMENT *element)
{
  double f,g;
  f=element->v2*10.0/0.3/element->length;
  g=element->v1*element->length;
  fprintf(file,"  5.0  % 12E % 12E % 12E % 12E % 12E;\n",element->length,f,g,
	  transport_data.phase[element->field],transport_data.wavelength);
}

void
quadrupole_list_transport(FILE *file,ELEMENT *element)
{
  double f,aperture=0.02;
  char name [9];
  static int i=1;
  sprintf(name,"Q%07d\0",i);
  f=element->v1*10.0/0.3*aperture/element->length;
  fprintf(file,"  5.0  % 12E % 12E % 12E                 \"%s\" ;\n",element->length,f,aperture*100.0);
  i++;
}

void
drift_list_transport(FILE *file,ELEMENT *element)
{
  char name [9];
  static int i=1;
  sprintf(name,"D%07d\0",i);
  fprintf(file,"  3.0  % 12E                                             \"%s\" ;\n",element->length,name);
  i++;
}

void
bpm_list_transport(FILE *file,ELEMENT *element)
{
  char name [9];
  static int i=1;
  sprintf(name,"B%07d\0",i);
  fprintf(file," 13.0  % 12E                                             \"%s\" ;\n",element->length,name);
  i++;
}

void
dipole_list_transport(FILE *file,ELEMENT *element)
{
  char name [9];
  static int i=1;
  sprintf(name,"D%07d\0",i);
  fprintf(file,"  7.0  % 12E;\n",element->length);
}

void
cavity_list_transport(FILE *file,ELEMENT *element,double gradient)
{
  char name [9];
  static int i=1;
  sprintf(name,"C%07d\0",i);
  fprintf(file," 11.0  % 12E % 12E % 12E % 12E   \"%s\" ;\n",element->length,
	  element->v1*element->length*gradient,element->v5*360.0/TWOPI,
	  transport_data.wavelength*100.0,name);
  i++;
}

void
beamline_list_transport(FILE *file,BEAMLINE *beamline,double gradient)
{
  GIRDER *girder;
  ELEMENT *element;

  girder=beamline->first;
  transport_data.wavelength=0.01;
  transport_data.phase=(double*)malloc(sizeof(double)*10);
  transport_data.phase[0]=-2.0;
  transport_data.phase[1]=6.0;
  transport_data.phase[2]=30.0;
  fprintf(file," 15.  1. 'M';\n");
  fprintf(file," 15.  3. 'M';\n");
  fprintf(file," 15.  5. 'CM';\n");               
  fprintf(file," 15.  8. 'M';\n");
  fprintf(file," 15.  9. 'KG' ;\n");
  fprintf(file," 15.  11.  'GEV';\n");
  while (girder) {
    element=girder->element();
    while (element) {
      switch (element->type) {
      case QUAD:
      case QUADBPM:
	quadrupole_list_transport(file,element);
	break;
      case BPM:
	bpm_list_transport(file,element);
	break;
      case _DIPOLE:
	dipole_list_transport(file,element);
	break;
      case CAV:
        cavity_list_transport(file,element,gradient);
	break;
      case DRIFT:
	drift_list_transport(file,element);
	break;
      case SOLENOID:
	solenoid_list_transport(file,element);
	break;
      case SOLCAV:
	solcav_list_transport(file,element);
	break;
      case QUADCAV:
	quadcav_list_transport(file,element);
	break;
      }
      element=element->next;
    }
    girder=girder->next();
  }
}

int
transport_element_read(FILE *f,BEAMLINE *beamline,GIRDER **girder)
{
  char *p,buffer[1024];
  int t;
  double length,aperture,gradient,phase;
  ELEMENT *cavity;
  
  p=fgets(buffer,1024,f);
  if (p==NULL) {
    return 0;
  }
  *girder=new GIRDER(0.0);
  girder_add(beamline,*girder,0.0);
  t=(int)strtod(p,&p);
  switch (t) {
  case 1:
    break;
  case 3:
    *girder=new GIRDER(0.0);
    girder_add(beamline,*girder,0.0);
    length=(double)strtod(p,&p);
    *girder->add_element(drift_make(length,-1));
    break;
  case 5:
    *girder=new GIRDER(0.0);
    girder_add(beamline,*girder,0.0);
    length=(double)strtod(p,&p);
    gradient=(double)strtod(p,&p);
    aperture=(double)strtod(p,&p);
    *girder->add_element(quadrupole_make(length,100.0*0.03*gradient/aperture
					 *length,-1));
    break;
  case 7:
    *girder=new GIRDER(0.0);
    girder_add(beamline,*girder,0.0);
    length=(double)strtod(p,&p);
    *girder->add_element(new DIPOLE(length,0.0,0.0,-1));
    break;
  case 11:
    *girder=new GIRDER(0.0);
    girder_add(beamline,*girder,0.0);
    length=(double)strtod(p,&p);
    gradient=(double)strtod(p,&p)/length;
    phase=(double)strtod(p,&p)*180.0/acos(-1.0);
    cavity=cavity_make(length,0,phase);
    *girder->add_element(cavity);
    cavity->v1=gradient;
    break;
  case 13:
    *girder=new GIRDER(0.0);
    girder_add(beamline,*girder,0.0);
    length=(double)strtod(p,&p);
    *girder->add_element(new BPM(length));
    break;
  case 19:
    printf("add solenoid\n");
    break;
  }
  return 1;
}

void
transport_read_file (FILE *file,BEAMLINE *beamline,GIRDER **girder)
{
  while(transport_element_read(file,beamline,girder)) {
  }
}
