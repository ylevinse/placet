#ifndef quadcav_h
#define quadcav_h

void
quadcav_coefficients(double gradient,double k,double length,double energy0,
		     R_MATRIX *r1,R_MATRIX *r2);
void
quadcav_coefficients2(double gradient,double k,double length,double energy0,
		      R_MATRIX *r1,R_MATRIX *r2,int n);
void
quadcav_sigma_step_end(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		       double energy,double d_g);

void
quadcav_sigma_step(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy,
		      double energy,double length,double gradient,double k);
void
quadcav_step(ELEMENT *element,BEAM *beam);
void
quadcav_step_4d_0(ELEMENT *element,BEAM *beam);
void
quadcav_step_twiss(ELEMENT *element,BEAM *beam,FILE *file,double step0,int j,
		   double s,void (*callback)(FILE*,BEAM*,int,double));
ELEMENT* quadcav_make(double length,double gradient,double k,double phase);

#endif // quadcav_h
