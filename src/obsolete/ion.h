#ifndef ion_h
#define ion_h

void drift_step_half(ELEMENT *,BEAM *);
void drift_step_half_0(ELEMENT *,BEAM *);
void drift_step_quarter(ELEMENT *,BEAM *);
void cavity_step_half1(ELEMENT *,BEAM *);
void cavity_step_half2(ELEMENT *,BEAM *);
void ion_plot(FILE *,BEAM *,int ,double);
void twiss_plot2(BEAMLINE *,BEAM *);
double ion_sum(BEAMLINE *,BEAM *,double);
int tk_IonPlot(ClientData ,Tcl_Interp *,int ,char **);
int tk_IonSum(ClientData ,Tcl_Interp *,int ,char **);

#endif // ion_h
