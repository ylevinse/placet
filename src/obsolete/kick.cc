#define PLACET
#include "kick.h"
#include "structures_def.h"

void
KICK::step_4d_0(BEAM *beam)
{
  for(int i=0;i<beam->slices;++i) {
    beam->particle[i].xp+=kickx/beam->particle[i].energy;
    beam->particle[i].yp+=kicky/beam->particle[i].energy;
  }
}

void
KICK::step_4d(BEAM *beam)
{
  this->step0(beam);
}

