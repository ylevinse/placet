#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifndef PI
#define PI 3.141592653589793
#endif

#ifndef max
#define max(a,b) (((a)<(b))?(b):(a))
#endif

#ifndef min
#define min(a,b) (((a)>(b))?(b):(a))
#endif


#include "old/gp.pub/memory.d"
#include "old/gp.pub/hist.d"
#include "old/gp.pub/rndm.d"

double frequency;

compress(double r56,double z0[],double e0[],double ch[],int n)
{
  int i;
  double *z,*e,zsum=0.0,chsum=0.0;
  
  z=z0; e=e0;
  for (i=0;i<n;i++){
    z0[i] += r56 * e0[i];
    zsum+=z0[i]*ch[i];
//    chsum+=ch[i];
  }
//  zsum/=chsum;
//  for (i=0;i<n;i++){
//    z0[i]-=zsum;
//  }
}

accelerate(double delta_e,double lambda,double phase,double z0[],
	   double e0[],int n)
{
  int i;
  double omega,*z,*e;

  omega=2.0*PI/lambda;
  phase *= PI/180.0;
  z=z0; e=e0;
  for (i=0;i<n;i++){
    *e++ += delta_e*cos(phase + *z++ * omega);
  }
}

double scale_a=0.64;

double wl(double z)
{
  const double scale=250.0*(frequency/2.998)*(frequency/2.998)*(1.6e-19*1e12)
    *1e-6;
  return scale_a*scale*exp(-0.85*sqrt(z*(frequency/2.998)*1e3));
}

wakefield(double l,double z[],double e[],double ch[],int n)
{
  int i,j;
  double dist;

  for (j=1;j<n;j++){
    for (i=0;i<j;i++){
      dist=z[j]-z[i];
      if (dist<0){
	e[j]-=wl(-dist)*l*ch[i];
      }
      else{
	e[i]-=wl(dist)*l*ch[j];
      }
    }
  }
}

init(double charge,double e0,double sigma_e,double sigma_z,double z[],
     double e[],double ch[],int n,double cut_e)
{
  int i;
  double a=-3.0,b=3.0,dz,zpos,scale,tmp;

  rndmst();

  scale=charge/sqrt(2.0*PI)*(b-a)/(double)n;
  for (i=0;i<n;i++){
    zpos=a+(b-a)*((double)i+0.5)/(double)n;
    z[i]=zpos*sigma_z;
    tmp=gasdev();
    while(fabs(tmp)>cut_e) tmp=gasdev();
    e[i]=e0+tmp*sigma_e;
    ch[i]=scale*exp(-0.5*zpos*zpos);
  }
}

init_rndm(double charge,double e0,double sigma_e,double sigma_z,double z[],
	  double e[],double ch[],int n,double cut_e)
{
  int i;
  double a=-3.0,b=3.0,dz,zpos,scale,tmp;

  rndmst();

  scale=charge/(double)n;
  for (i=0;i<n;i++){
    zpos=gasdev();
    while((zpos<a)||(zpos>b)) zpos=gasdev();
    z[i]=zpos*sigma_z;
    tmp=gasdev();
    while(fabs(tmp)>cut_e) tmp=gasdev();
    e[i]=e0+sigma_e*tmp;
    ch[i]=scale;
  }
}

cut_energy(double e[],double ch[],double z[],int *n0,double e_min,double e_max)
{
  int i,j=0,n;
  n=*n0;
  for (i=0;i<n;i++){
    if ((e[i]>e_min)&&(e[i]<e_max)){
      e[j]=e[i];
      ch[j]=ch[i];
      z[j]=z[i];
      j++;
    }
  }
  *n0=j;
}

print_bunch(char *name,double z[],double e[],double ch[],int n)
{
  int i;
  double charge=0.0;
  FILE *file;
  double zsum=0.0,zsum2=0.0,esum=0.0,esum2=0.0,chsum=0.0;

  file=fopen(name,"w");
  for (i=0;i<n;i++){
    fprintf(file,"%d %g %g %g\n",i,z[i]*1e6,e[i],ch[i]);
    zsum+=ch[i]*z[i];
    zsum2+=ch[i]*z[i]*z[i];
    esum+=ch[i]*e[i];
    esum2+=ch[i]*e[i]*e[i];
    chsum+=ch[i];
  }
  fclose(file);
  zsum/=chsum;
  zsum2/=chsum;
  esum/=chsum;
  esum2/=chsum;
  printf("sigmaz %g sigmae %g zmean %g emean %g charge %g\n",
	 sqrt(zsum2-zsum*zsum)*1e6,
	 sqrt(esum2-esum*esum),zsum*1e6,esum,chsum);
}

plot_bunch(char *name_z,char *name_e,double z[],double e[],double ch[],int n)
{
  HISTOGRAM hist_z,hist_e;
  int i;
  FILE *file;
  double emin,emax,zmin,zmax;

  emin=1e300; emax=-1e300; zmin=1e300; zmax=-1e300;
  for (i=0;i<n;i++){
    zmin=min(zmin,z[i]);
    zmax=max(zmax,z[i]);
    emin=min(emin,e[i]);
    emax=max(emax,e[i]);
  }
  make_histogram(&hist_z,1,zmin*1e6,zmax*1e6,50,"tmp_z");
  make_histogram(&hist_e,1,emin,emax,50,"tmp_e");
  for (i=0;i<n;i++){
    add_histogram(&hist_z,z[i]*1e6,ch[i]);
    add_histogram(&hist_e,e[i],ch[i]);
  }
  file=fopen(name_z,"w");
  fprint_histogram(file,&hist_z);
  fclose(file);
  file=fopen(name_e,"w");
  fprint_histogram(file,&hist_e);
  fclose(file);
}

step_rf(double delta_e,double gradient,double eff,double phase,
	double z[],double e[],double ch[],int n)
{
  accelerate(delta_e*eff,0.48/frequency*0.625,phase,z,e,n);
  wakefield(delta_e/gradient,z,e,ch,n);
}

adjust_z(double z[],int n,double z0)
{
  int i;
  for (i=0;i<n;i++){
    z[i]-=z0;
  }
}

double power(double z[],double w[],int n)
{
  double s=0.0,c=0.0,ws=0.0;
  int i;
  for (i=0;i<n;i++){
    s+=sin(2.0*PI*z[i]/0.01)*w[i];
    c+=cos(2.0*PI*z[i]/0.01)*w[i];
    ws+=w[i];
  }
  return (s*s+c*c)/(ws*ws);
}

#define NMAX 10000
main(int argc,char *argv[])
{
  double z[NMAX],e[NMAX],ch[NMAX];
  double gradient,eff,phase,z0,delta_e,r56,sigma_z,sigma_e,e0,charge,pw,
    cut_e,e_min,e_max;
  int n=2000,i=0,do_rndm;
  char *point,line[1000];
  char name1[100],name2[100];

  printf("strt\n");
  point=fgets(line,1000,stdin);
  frequency=strtod(point,&point);
  charge=strtod(point,&point);
  sigma_e=strtod(point,&point);
  cut_e=strtod(point,&point);
  sigma_z=strtod(point,&point);
  e0=strtod(point,&point);
  n=strtol(point,&point,10);
  do_rndm=strtol(point,&point,10);

  if (do_rndm){
    init_rndm(charge,e0,sigma_e,sigma_z,z,e,ch,n,cut_e);
  }
  else{
    init(charge,e0,sigma_e,sigma_z,z,e,ch,n,cut_e);
  }

  printf("%g %g %g\n",e0,sigma_e,sigma_z);

  while(point=fgets(line,1000,stdin)){
    if (line[0]!='#'){
      delta_e=strtod(line,&point);
      gradient=strtod(point,&point);
      eff=strtod(point,&point);
      phase=strtod(point,&point);
      r56=strtod(point,&point);
      z0=strtod(point,&point);
      e_min=strtod(point,&point);
      e_max=strtod(point,&point);
      
      //printf("%g %g %g %g %g %g\n",delta_e,gradient,eff,phase,r56,z0);    
      step_rf(delta_e,gradient,eff,phase,z,e,ch,n);
      cut_energy(e,ch,z,&n,e_min,e_max);
      compress(r56,z,e,ch,n);
      adjust_z(z,n,z0);
      sprintf(name1,"tmp.%d\0",i);
      print_bunch(name1,z,e,ch,n);
      sprintf(name1,"hist_z.%d\0",i);
      sprintf(name2,"hist_e.%d\0",i);
      plot_bunch(name1,name2,z,e,ch,n);
      i++;
    }
  }
  pw=power(z,ch,n);
  printf("power: %g %g\n",pw,sqrt(pw));
  exit(0);
}
