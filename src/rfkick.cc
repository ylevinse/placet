#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <limits>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "beam.h"
#include "structures_def.h"

/* Function prototypes */

#include "function.h"
#include "placeti3.h"
#include "rfkick.h"
#include "rmatrix.h"

extern double zero_point;

extern PetsWake drive_data;

void rf_kick2p(int order,double grad,double L,double /*phi0*/,double x,
	       double xp,double y,double yp,double c0,double s0,
	       double *kx,double *ky)
{
  const double eps=1e-30;
  double r2,c,s,k;
  x-=0.5*L*xp;
  y-=0.5*L*yp;
  r2=x*x+y*y;
  if (r2<eps) {
    *kx=0.0;
    *ky=0.0;
    return;
  }
  c=y;
  s=x;
  turn_angle(c0,s0,c,s);
  pow_angle(c,s,order);
  k=grad*L*(double)order/r2;
  *kx=k*(c*x-s*y);
  *ky=k*(s*x+c*y);
}

void rf_long2p(int order,double grad,double L,double /*phi0*/,double x,
	       double xp,double y,double yp,double c0,double s0,
	       double *k)
{
  const double eps=1e-30;
  double r2,c,s;
  x+=0.5*L*xp;
  y+=0.5*L*yp;
  r2=x*x+y*y;
  if (r2<eps) {
    *k=0.0;
    return;
  }
  c=y;
  s=x;
  turn_angle(c0,s0,c,s);
  pow_angle(c,s,order);
  *k=grad*c;
  return;
}

BEAM* make_multi_bunch_drive_rf(BEAMLINE * /*beamline*/,DRIVE_BEAM_PARAM *param,
				PetsWake *drive_data, int n_max)
{
  //  FILE *file;
  BEAM *bunch;
  int i,n,k,nmacro,j,j1,nbunches,i_m;
  double theta,wgtsum,fact,sigmay,sigmayp,sigmax,sigmaxp;
  float tmp1,tmp2,tmp3;
  double betax,alphax,epsx;
  double betay,alphay,epsy;
  double charge,e0;
  double attenuation;
  double c0,s0;

  s0=sin(param->phi0);
  c0=cos(param->phi0);
  nbunches=param->n_bunch;
  e0=param->e0;
  nmacro=param->n_macro;
  charge=param->charge;

  epsx=param->emittx;
  epsy=param->emitty;
  alphax=param->alphax;
  alphay=param->alphay;
  betax=param->betax;
  betay=param->betay;

  epsx*=EMASS/e0*1e12*EMITT_UNIT;
  epsy*=EMASS/e0*1e12*EMITT_UNIT;

  n=param->n_slice;

  /* scd to update for number of phases */ 

  bunch=(BEAM*)bunch_make(nbunches,n,nmacro,1,n_max);
  bunch->drive_data->param.drive=(DRIVE_BEAM_PARAM*)
    malloc(sizeof(DRIVE_BEAM_PARAM));
  *(bunch->drive_data->param.drive)=*param;

  wgtsum=0.0;
  
  sigmay=param->envelope*sqrt(epsy*betay);
  sigmayp=param->envelope*sqrt(epsy/betay);

  sigmax=param->envelope*sqrt(epsx*betax);
  sigmaxp=param->envelope*sqrt(epsx/betax);

  for (i=0;i<n;i++) {
    tmp1=bunch->drive_data->param.drive->slice_z_pos[i];
    tmp2=0.0;
    tmp3=bunch->drive_data->param.drive->slice_wgt[i];
//    fscanf(file,"%g %g %g",&tmp1,&tmp2,&tmp3);
    for (k=0;k<nmacro;k++) {
      if (nmacro>1) {
	if (k/param->n_energy==0){
	  fact=(1.0-param->envelope_wgt)*param->eslice_wgt[k];
	  bunch_set_slice_y(bunch,0,i,k,zero_point);
	  bunch_set_slice_yp(bunch,0,i,k,0.0);
#ifdef TWODIM
	  bunch_set_slice_x(bunch,0,i,k,0.0);
	  bunch_set_slice_xp(bunch,0,i,k,0.0);
#endif
	}
	else{
	  theta=TWOPI*(double)k/(double)(nmacro-param->n_energy);
	  fact=param->envelope_wgt
	    *param->eslice_wgt[k%param->n_energy]
	    /(double)(nmacro/param->n_energy-1);
	  bunch_set_slice_y(bunch,0,i,k,zero_point+sigmay*sin(theta)*c0);
	  bunch_set_slice_yp(bunch,0,i,k,sigmayp*cos(theta)*c0);
#ifdef TWODIM
	  bunch_set_slice_x(bunch,0,i,k,sigmax*sin(theta)*s0);
	  bunch_set_slice_xp(bunch,0,i,k,sigmaxp*cos(theta)*s0);
#endif
	}
      }
      else{
	fact=1.0;
	bunch_set_slice_y(bunch,0,i,k,zero_point);
	bunch_set_slice_yp(bunch,0,i,k,0.0);
#ifdef TWODIM
	bunch_set_slice_x(bunch,0,i,k,0.0);
	bunch_set_slice_xp(bunch,0,i,k,0.0);
#endif
      }
      // if no slice energy list is given, use uniform energy  (+ eventual spread)
      if( param->slice_init_energy == NULL ) {
	bunch_set_slice_energy(bunch,0,i,k,
			       e0+param->d_energy[k%param->n_energy]);
      // if slice energy list is given, set each slice to the given energy (+ eventual spread)
      } else {
	//	bunch_set_slice_energy(bunch,0,i,k,
	//	       e0+param->d_energy[k%param->n_energy]);
	//	placet_printf(INFO,"EA2: slices_energy_list: %i %g %g %g\n", i, e0, param->slice_init_energy[0]/1e9,param->slice_init_energy[50]/1e9 );  
		bunch_set_slice_energy(bunch,0,i,k,
		       param->slice_init_energy[i] + param->d_energy[k%param->n_energy]);
      } 
      bunch_set_slice_wgt(bunch,0,i,k,(double)tmp3*fact);
      wgtsum+=tmp3*fact;
#ifdef TWODIM
      bunch_set_sigma_xx(bunch,0,i,k,betax,alphax,epsx);
      bunch_set_sigma_xy(bunch,0,i,k);
#endif
      bunch_set_sigma_yy(bunch,0,i,k,betay,alphay,epsy);
    }
    //    bunch->acc_field[0][i]=0.0;
    bunch->field->de[i]=tmp2*1e-3;
    bunch->z_position[i]=tmp1;
  }

/*
  should be possible to be left out
  for (i=0;i<n*nmacro;i++){
    bunch->particle[i].wgt/=wgtsum;
  }
  */

  // copy information from first bunch to the rest of the train
  k=n*nmacro;
  for (j=1;j<nbunches;j++){
    for (i=0;i<n*nmacro;i++){
      bunch->particle[k]=bunch->particle[i];
      bunch->sigma[k]=bunch->sigma[i];
#ifdef TWODIM
      bunch->sigma_xx[k]=bunch->sigma_xx[i];
      bunch->sigma_xy[k]=bunch->sigma_xy[i];
#endif
      k++;
    }
  }

  // EA: set last_wgt parameter for drive beam
  bunch->last_wgt = param->last_wgt;

  k=n;
  bunch->drive_data->bunch_z=(double*)malloc(sizeof(double)*bunch->bunches);
  // EA: added explicit mem. init for j=0  (before: could lead to arbitrary values)
  bunch->drive_data->bunch_z[0]=0;
  for (j=1;j<nbunches;j++){
    if (j<param->n_z0) {
      //      bunch->drive_data->bunch_z[j]=j*param->distance*1e6+param->z0[j];
      bunch->drive_data->bunch_z[j]=param->bucket[j]*param->distance*1e6
	+param->z0[j];
      for (i=0;i<n;i++){
	bunch->z_position[k]=bunch->z_position[i]
	  +bunch->drive_data->bunch_z[j];
	k++;
      }
    }
    else {
      bunch->drive_data->bunch_z[j]=j*param->distance*1e6;
      for (i=0;i<n;i++){
	bunch->z_position[k]=bunch->z_position[i]+j*param->distance*1e6;
	k++;
      }
    }
  }

  if (param->n_z0) {
    bunch->drive_data->bunch_z[0]=param->bucket[0]*param->distance*1e6
	+param->z0[0];
    for (i=0;i<n;i++){
      bunch->z_position[i]+=param->z0[0];
      //      placet_printf(INFO,"zpos[%d]= %g\n",i,tmp1);
    }
  }

  if (param->bucket) {
    j1=0;
    for (j=0;j<nbunches;j++) {
      for (i=0;i<n*nmacro;i++){
	// if weight is put to 0 particles are removed -> put "missing" particles weight to a insignifcant value instead
	if( param->ch[j] > 0.0 ) {
	  bunch->particle[j1].wgt*=param->ch[j];
	} else {
	  bunch->particle[j1].wgt = 2*std::numeric_limits<double>::epsilon();
	}
	j1++;
      }
    }
  }
  else {
    j1=std::min(param->n_ramp,nbunches);
    k=0;
    fact=param->s_ramp;
    for(j=0;j<j1;j++){
      for (i=0;i<n*nmacro;i++){
	bunch->particle[k].wgt*=fact;
	k++;
      }
      if (((j+1)%param->ramp_step)==0){
	fact+=(1.0-param->s_ramp)*(double)param->ramp_step/(double)param->n_ramp;
      }
    }
  }

  k=n;
  // for (j=1;j<nbunches;j++){
  //   for (i=0;i<n;i++){
  //     bunch->acc_field[0][k]=0.0;
  //     k++;
  //   }
  // }

  /* not needed in the moment */

//  drive_longitudinal_2(bunch,drive_data->lambda_longitudinal*1e6,charge);
  /*
  rf_kick_fill(bunch,drive_data.lambda_longitudinal*1e6,charge);
  */
  bunch->drive_data->longrange_max=1;

  /* scd new 26.1.98*/
  if(param->resist){ 
    for (i=1;i<nbunches;i++){
      bunch->drive_data->blong[i]=resist_wall_transv((double)i*param->distance)
	*param->charge*ECHARGE*1e-9;
    }
  }

  if (param->resist){
    bunch->which_field=14; // no method in wake_kick
  }
  else{
    bunch->which_field=12;
  }

  bunch->drive_data->empty_buckets=0;
  bunch->drive_data->bunches_per_train=bunch->bunches;

  //  longrange_fill_new(bunch,drive_data->lambda_transverse[0]);
  /*
  bunch->drive_data->al=(double*)malloc(sizeof(double)*bunch->bunches*
					bunch->slices_per_bunch);
  bunch->drive_data->bl=(double*)malloc(sizeof(double)*bunch->bunches*
					bunch->slices_per_bunch);
  */
  bunch->drive_data->along=(double*)realloc(bunch->drive_data->along,
					    sizeof(double)*drive_data->n_mode
					    *bunch->bunches);
  for (i_m=0;i_m<drive_data->n_mode;++i_m) {
    bunch->drive_data->along[i_m*bunch->bunches]=drive_data->a0[i_m]
      *(charge*ECHARGE*1e12)*1e-6*1e-3;

    attenuation=exp(-param->distance/drive_data->lambda_transverse[i_m]
		    *PI/drive_data->q_value[i_m]
		    *1.0/(1.0-drive_data->beta_group_t[i_m]));
    placet_printf(INFO,"Attenuation %g\n",attenuation);
    for (i=1;i<nbunches;i++){
      bunch->drive_data->along[i+i_m*bunch->bunches]=
	exp(-(bunch->drive_data->bunch_z[i]-bunch->drive_data->bunch_z[i-1])
	    *1e-6/
	    drive_data->lambda_transverse[i_m]*PI/drive_data->q_value[i_m]
	    *1.0/(1.0-drive_data->beta_group_t[i_m]));
    }
  }
  bunch->drive_data->longrange_max=nbunches;



  // longitudinal wake amplitude and kick (for the moment: no damping foreseen TODO: INCLUDE DAMPING FROM HERE!)
  bunch->drive_data->factor_long=(double*)realloc(bunch->drive_data->factor_long,
					    sizeof(double)*drive_data->n_mode_long*bunch->bunches);
  bunch->drive_data->factor_kick=(double*)realloc(bunch->drive_data->factor_kick,
					    sizeof(double)*drive_data->n_mode_long*bunch->bunches);
 for (i_m=0;i_m<drive_data->n_mode_long;++i_m){
   bunch->drive_data->factor_long[i_m]=-TWOPI*1e-9*(charge*ECHARGE)*C_LIGHT
	     /drive_data->lambda_longitudinal[i_m]*drive_data->r_over_q[i_m];

   bunch->drive_data->factor_long[i_m]/=(1.0-drive_data->beta_group_l[i_m]);

   bunch->drive_data->factor_kick[i_m]=-TWOPI*1e-9*(charge*ECHARGE)*C_LIGHT
     /drive_data->lambda_longitudinal[i_m]*drive_data->r_over_q[i_m]
     *drive_data->lambda_longitudinal[i_m]/(TWOPI)*1e6*1e6;

   bunch->drive_data->factor_kick[i_m]/=(1.0-drive_data->beta_group_l[i_m]);

   placet_printf(INFO,"kick: %g\n",bunch->drive_data->factor_kick[i_m]);

   attenuation=exp(-param->distance/drive_data->lambda_longitudinal[i_m]
		    *PI/drive_data->q_value_long[i_m]
		    *1.0/(1.0-drive_data->beta_group_l[i_m]));
    placet_printf(INFO,"Attenuation long %g\n",attenuation);
    for (i=1;i<nbunches;i++){
      bunch->drive_data->factor_long[i+i_m*bunch->bunches]=
	exp(-(bunch->drive_data->bunch_z[i]-bunch->drive_data->bunch_z[i-1])
	    *1e-6/
	    drive_data->lambda_longitudinal[i_m]*PI/drive_data->q_value_long[i_m]
	    *1.0/(1.0-drive_data->beta_group_l[i_m]));
    }

 }
  
  //  longrange_fill_long_new(bunch,drive_data->lambda_longitudinal);
  //  set_zero_z_position(bunch);

  return bunch;
}
