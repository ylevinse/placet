#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include <tcl.h>
#include <tk.h>

#include "minimise_tk.h"
#include "powell.h"

namespace {

struct FuncObj {
  int n;
  Tcl_Obj **arg,**arg2;
  Tcl_CmdInfo info;
  Tcl_Interp *interp;
};

double func(double *x);
double func2(double *x);
FuncObj* function_eval_init(Tcl_Interp *interp,char *f,int n);
FuncObj* function_eval_init2(Tcl_Interp *interp,char *f,int n);
int function_eval2(FuncObj *p,double *x,double *res);
int function_eval(FuncObj *p,double *x,double *res);

struct FUNCTION_COMMON {
  Tcl_Interp *interp;
  char *f;
  int n;
  FuncObj *p;
} function_common;


double func(double *x)
{
  double res;
  //  int i;
  /*  
      for (i=0;i<function_common.n;i++){
      placet_printf(INFO,"%d %g\n",i,x[i]);
      }
  */
  if (function_eval(function_common.p,x,&res)!=TCL_OK) {
  }
  //  placet_printf(INFO,"result %g\n",res);
  return res;
}


double func2(double *x)
{
  double res;

  if (function_eval2(function_common.p,x,&res)!=TCL_OK){
  }
  return res;
}

FuncObj*
function_eval_init(Tcl_Interp *interp,char *f,int n)
{
  int i;
  FuncObj *p;
  
  p=(FuncObj*)malloc(sizeof(FuncObj));
  p->arg=(Tcl_Obj**)malloc(n*sizeof(Tcl_Obj*));
  p->arg2=(Tcl_Obj**)malloc(3*sizeof(Tcl_Obj*));
  p->n=n;
  Tcl_GetCommandInfo(interp,f,&(p->info));
  p->interp=interp;
  for (i=0;i<n;i++){
    p->arg[i]=Tcl_NewDoubleObj(0.0);
  }
  p->arg2[0]=Tcl_NewStringObj(f,strlen(f));
  p->arg2[1]=Tcl_NewListObj(n,p->arg);
  p->arg2[2]=(Tcl_Obj*)NULL;
  return p;
}

FuncObj*
function_eval_init2(Tcl_Interp *interp,char *f,int n)
{
  int i;
  FuncObj *p;
  
  p=(FuncObj*)malloc(sizeof(FuncObj));
  p->arg=(Tcl_Obj**)malloc((n+2)*sizeof(Tcl_Obj*));
  p->arg2=(Tcl_Obj**)NULL;
  p->n=n;
  Tcl_GetCommandInfo(interp,f,&(p->info));
  p->interp=interp;
  p->arg[0]=Tcl_NewStringObj(f,strlen(f));
  for (i=0;i<n;i++){
    p->arg[i+1]=Tcl_NewDoubleObj(0.0);
  }
  p->arg[n+1]=(Tcl_Obj*)NULL;
  return p;
}

int
function_eval2(FuncObj *p,double *x,double *res)
{
  int error=TCL_OK;
  int i;

  for (i=0;i<p->n;i++){
    p->arg[i+1]=Tcl_NewDoubleObj(x[i]);
  }
  (*(p->info).objProc)((p->info.objClientData),p->interp,p->n+1,p->arg);
  Tcl_GetDoubleFromObj(p->interp,Tcl_GetObjResult(p->interp),res);
  return error;
}

int
function_eval(FuncObj *p,double *x,double *res)
{
  int error=TCL_OK;
  int i;

  for (i=0;i<p->n;i++){
    Tcl_SetDoubleObj(p->arg[i],x[i]);
    //    placet_printf(INFO,"%d %g\n",i,x[i]);
//    p->arg[i]=Tcl_NewDoubleObj(x[i]);
  }
  p->arg2[1]=Tcl_NewListObj(p->n,p->arg);
//  Tcl_SetListObj(p->arg2[1],p->n,p->arg);
//  Tcl_ListObjReplace(p->interp,p->arg2[1],0,p->n,p->n,p->arg);
  (*(p->info).objProc)((p->info.objClientData),p->interp,2,p->arg2);
  Tcl_GetDoubleFromObj(p->interp,Tcl_GetObjResult(p->interp),res);
  //  placet_printf(INFO,"result %g\n",*res);
  return error;
}
} // end anonymous namespace

int
tk_MinimiseFunction(ClientData /*clientdata*/,Tcl_Interp *interp,int argc,
		    char *argv[])
{
  int error;
  char *f=NULL,*s=NULL,*d=NULL,**v;
  double *x,**x1,*xbase,fres,tol=1e-12;
  int n,i,j,nd,iter;
  int list=0;
  char buffer[100];
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_HELP,(char*)NULL,
     (char*)NULL,
     (char*)"Usage: MatchList options"},
    {(char*)"-function",TK_ARGV_STRING,(char*)NULL,
     (char*)&f,
     (char*)"Function to minimise"},
    {(char*)"-start",TK_ARGV_STRING,(char*)NULL,
     (char*)&s,
     (char*)"Starting point"},
    {(char*)"-delta",TK_ARGV_STRING,(char*)NULL,
     (char*)&d,
     (char*)"Differences from starting point"},
    {(char*)"-tolerance",TK_ARGV_FLOAT,(char*)NULL,
     (char*)&tol,
     (char*)"Tolerance for stopping"},
    {(char*)"-list",TK_ARGV_INT,(char*)NULL,
     (char*)&list,
     (char*)"If not 0 function takes a list as argument"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  /* parse arguments */

  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))
      !=TCL_OK){
    return error;
  }
  if (argc!=1)
  {
    Tcl_SetResult(interp,(char*)"Too many arguments to MinimiseFunction",
                  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if (!f) {
    Tcl_SetResult(interp,"You need to provide a function for the minimisation",
		  TCL_VOLATILE);
    return TCL_ERROR;
  }
  if(error=Tcl_SplitList(interp,s,&n,&v)) {
    return error;
  }
  x=(double*)alloca(sizeof(double)*n);
  x1=(double**)alloca(sizeof(double*)*(n+1));
  xbase=(double*)alloca(sizeof(double)*(n+1)*n);
  for (i=0;i<n;i++){
    x[i]=strtod(v[i],NULL);
  }
  for (i=0;i<n+1;i++){
    x1[i]=xbase+n*i;
    for (j=0;j<n;j++){
      x1[i][j]=x[j];
    }
  }
  if (d) {
    free(v);
    if(error=Tcl_SplitList(interp,d,&nd,&v)) {
      return error;
    }
    if (nd!=n) {
      Tcl_AppendResult(interp,"mist",NULL);
      return TCL_ERROR;
    }
    if (0) {
      for (i=0;i<n;i++){
	for (j=0;j<n;j++){
	  x1[i+1][j]=0.0;
	}
	x1[i+1][i]=strtod(v[i],NULL);
      }
    }
    else {
      for (i=0;i<n;i++){
	x1[i+1][i]+=strtod(v[i],NULL);
      }
    }
  }
  else {
    if (0) {
      for (i=0;i<n;i++){
	for (j=0;j<n;j++){
	  x1[i+1][j]=0.0;
	}
	x1[i+1][i]=1.0;
      }
    }
    else {
      for (i=0;i<n;i++){
	x1[i+1][i]+=1.0;
      }
    }
  }
  double (*fun)(double *);
  if (list) {
    function_common.p=function_eval_init(interp,f,n);
    fun=&func;
  }
  else {
    function_common.p=function_eval_init2(interp,f,n);
    fun=&func2;
  }
  function_common.n=n;
  function_common.f=f;
  function_common.interp=interp;
  POWELL powe(fun);
  powe.powell(xbase,xbase+n,n,n,tol,&iter,&fres);
  Tcl_ResetResult(interp);
  for(i=0;i<n;i++){
    snprintf(buffer,100,"%g",x1[0][i]);
    //    placet_printf(INFO,"%g\n",x1[0][i]);
    fflush(stdout);
    Tcl_AppendElement(interp,buffer);
  }
  free(v);
  return TCL_OK;
}

int
Minimise_Init(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp,"MinimiseFunction",tk_MinimiseFunction,
		    (ClientData)NULL,(Tcl_CmdDeleteProc*)NULL);
  Tcl_PkgProvide(interp,"Minimise","0.1");
  return TCL_OK;
}
