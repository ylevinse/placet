#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <fstream>
#include <tcl.h>

#define PLACET
#include "placet.h"
#include "placet_prefix.hh"

#include "collimatortable.h"
#include "girder.h"
#include "lattice.h"
#include "structures_def.h"
#include "spline.h"

/* Function prototypes */

#include "collimator.h"

extern Tcl_Interp* beamline_survey_hook_interp;
extern INTER_DATA_STRUCT inter_data;

COLLIMATOR::COLLIMATOR(int &argc, char **argv ) : ELEMENT()
{
  // initialise daniel members:
  nmax=0, dz=0., dzi=0., zmin=0., zmax=0., wtx=0, wty=0, rhox=0, rhoy=0, spx=0, spy=0; 
  wtx=NULL;
  wty=NULL;
  rhox=NULL;
  rhoy=NULL;
  nmax=0, zmin=0., zmax=0.;
  dz=0., dzi=0.,

  // initialise giovanni members
  in_height=0.,fin_height=0.,width=0.,taper_length=0.,flat_length=0.;
  sigma=0.,tau=0.;
  Nb=0.0;
  vertical=true;
  wake_meth=false;
  Ctable=NULL;

  // temporary variables
  std::string output_filename;
  std::string spline_name_x,spline_name_y;
  bool horizontal=false;

  attributes.add("outputfile","name of the output file with kick data [STRING]",OPT_STRING,&output_filename);

  // Daniel's implementation parameters

  attributes.add("x_wakefield","name of the spline that contains the transverse wakefield in x [STRING]",OPT_STRING,&spline_name_x);
  attributes.add("y_wakefield","name of the spline that contains the transverse wakefield in y [STRING]",OPT_STRING,&spline_name_y);
  attributes.add("n","Number of bins in z direction",OPT_INT,&nmax);
  attributes.add("zmin","Lowest position in z direction",OPT_DOUBLE,&zmin);
  attributes.add("zmax","Largest position in z direction",OPT_DOUBLE,&zmax);

  // Giovanni's implementation parameters
  
  attributes.add("in_height","initial height of the collimator [m]",OPT_DOUBLE,&in_height);
  attributes.add("fin_height","final height of the collimator [m]",OPT_DOUBLE,&fin_height);
  attributes.add("width","Width of the collimator [m]",OPT_DOUBLE,&width);
  attributes.add("taper_length","Length of the taper [m]",OPT_DOUBLE,&taper_length);
  attributes.add("flat_length","Length of the flat part of the collimator [m]",OPT_DOUBLE,&flat_length);
  attributes.add("sigma","Electrical conductivity of the collimator material [(Ohm m)^-1] (i.e. graphite=6.e4)",OPT_DOUBLE,&sigma);
  attributes.add("tau","Relaxation time of the collimator material [s]",OPT_DOUBLE,&tau);
  attributes.add("charge","Bunch charge [e]",OPT_DOUBLE,&Nb);
  attributes.add("horizontal","Horizontal collimator",OPT_BOOL,&horizontal);
  attributes.add("vertical","Vertical collimator (default)",OPT_BOOL,&vertical);
  attributes.add("correct_offset","If not zero take into account the offset of the collimator",OPT_BOOL,&correct_offset);

  // Adina's implementation parameters

  attributes.add("wake_method","If not zero calculates the resistive transversal wakefields kicks using RJB & AT method",OPT_BOOL,&wake_meth);
  attributes.add("wake_table","If wake_method not zero file name which contains the resistive transversal wakefields",OPT_STRING,&wake_tname);

  set_attributes(argc, argv);
  if(horizontal) vertical=false; // collimator wake field added only in one direction

  /// constants
  s0_in=pow((2*pow(in_height,2)/Z0/sigma),1./3.);
  s0_fin=pow((2*pow(fin_height,2)/Z0/sigma),1./3.);
  cap_gam=C_LIGHT*tau/s0_in;
  lambda=1/Z0/sigma;
  angle=atan((in_height-fin_height)/taper_length);
  imax = 10;

  placet_printf(VERBOSE,"\n s0_fin = %lg  s0_in = %lg Z0 = %lg \n", s0_fin,s0_in,Z0);

  if (!spline_name_x.empty() && !spline_name_y.empty()) {
    // Daniel implementation
    giovanni=false;

    spx = get_spline(spline_name_x.c_str());
    spy = get_spline(spline_name_y.c_str());
    // set bins
    dz=(zmax-zmin)/nmax; 
    dzi=1.0/dz;

    correct_offset=0; // is this really what we want? - JS

  } else {
    // Giovanni / Adina implementation
    giovanni=true;

    if(wake_meth) {
      // if _wake_table is not given, take default table.
      if(!wake_tname.empty()) {
	wake_tname = get_placet_sharepath("table_m1.txt");
      }

      double Gamma=C_LIGHT*tau/s0_fin;
      double xi=s0_fin*s0_fin/fin_height/fin_height;
      placet_printf(VERBOSE,"\n Gamma = %lg  xi = %lg \n", Gamma, xi);

      //  collimatortable *Ctable=new collimatortable("table_m1.txt",Gamma,xi);
      Ctable=new collimatortable(wake_tname.c_str(),Gamma,xi);
      std::ofstream f("adinatest.txt");
      for(float x=0; Ctable->inrange(x);x+=0.1)
	//placet_cout<<DEBUG<<x<<" "<<Ctable->interpolate(x)<<endmsg;
	f <<x<<" "<<Ctable->interpolate(x)<<std::endl;
      f.close();
    }

    // open output file (only for Giovanni / Adina):  
    if (!output_filename.empty()) {
      output_file.open(output_filename.c_str());
      if (!output_file.is_open()) {
	placet_cout << ERROR << "Unable to open file " << output_filename << endmsg;
	exit(1);
      } else {
	// add header
	output_file << "# z geometric resistive resistive-flat resistive-flat2 resistive-taper resistive-taper2" << std::endl;
      }
    }
  }
}

COLLIMATOR::~COLLIMATOR() {
  delete Ctable;
  // close output file
  if (output_file.is_open()) output_file.close();
}

namespace { // helper functions
void
beam_bin(BEAM *beam,double hx[],double hy[])
{
  int i,j,k=0;

  for (i=0;i<beam->slices_per_bunch;++i) {
    hx[i]=0.0;
    hy[i]=0.0;
    for (j=0;j<beam->particle_number[i];++j){
      hx[i]+=beam->particle[k].wgt*beam->particle[k].x;
      hy[i]+=beam->particle[k].wgt*beam->particle[k].y;
      k++;
    }
    fflush(stdout);
  }
}

void
fold_kick(double rx[],double ry[],double x[],double y[],int n)
{
  int i,j;
  for (i=n-1;i>=0;--i){
    x[i]*=rx[0];
    y[i]*=ry[0];
    for (j=0;j<i;++j) {
      x[i]+=x[j]*rx[i-j];
      y[i]+=y[j]*ry[i-j];
    }
    x[i]*=ECHARGE; /* *1e9*1e-9 */
  }
}

void
beam_kick(BEAM *beam,double hx[],double hy[])
{
  int i,j,k=0;
  for (i=0;i<beam->slices_per_bunch;++i){
    for (j=0;j<beam->particle_number[i];++j){
      beam->particle[k].xp+=hx[i]/beam->particle[k].energy;
      beam->particle[k].yp+=hy[i]/beam->particle[k].energy;
      k++;
    }
  }
}
}

void
COLLIMATOR::daniel_step_4d_0(BEAM *beam)
{
  // The first command may not be necessary (needs to be checked)

  inter_data.bunch=beam;
  /*
    Actual collimator kick
   */

  rhox=(double*)alloca(sizeof(double)*beam->slices_per_bunch); // allocate memory on the stack, freed automagically
  rhoy=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  wtx=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  wty=(double*)alloca(sizeof(double)*beam->slices_per_bunch);
  spx->get_table(0.0,
		 beam->z_position[1]-beam->z_position[0],wtx,
		 beam->slices_per_bunch);
  spy->get_table(0.0,
		 beam->z_position[1]-beam->z_position[0],wty,
		 beam->slices_per_bunch);
  beam_bin(beam,rhox,rhoy);
  fold_kick(wtx,wty,rhox,rhoy,beam->slices_per_bunch);
  beam_kick(beam,rhox,rhoy);
}

void
COLLIMATOR::daniel_step(BEAM *beam)
{
  daniel_step_4d_0(beam);
}

int tk_Collimator(ClientData /*clientdata*/, Tcl_Interp *interp, int argc, char **argv )
{
  if (element_help_message<COLLIMATOR>(argc,argv, "This command places a collimator in the current girder.\n\nCollimator:", interp)) return TCL_OK;
  if (check_beamline_changeable(interp,argv[0],TCL_OK)) return TCL_ERROR;
  if (check_girder_existence(interp,argv[0],TCL_OK)) return TCL_ERROR;
  COLLIMATOR *coll = new COLLIMATOR(argc, argv);
  inter_data.girder->add_element(coll);
  return TCL_OK;
}
