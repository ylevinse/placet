#include <valarray>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <limits>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "placet_tk.h"
#include "structures_def.h"
#include "short_range.h"

/* Function prototypes */

#include "cavity.h"
#include "matrix.h"
#include "conversion.h"
#include "sort_index.hh"

#ifdef EARTH_FIELD
extern EARTH_FIELD_STRUCT earth_field;
#endif

int data_nstep;
PetsWake drive_data;
WAKEFIELD_DATA_STRUCT wakefield_data;

/* wakefield_data contains the switches for the wakefields
 * transv==1: use transverse wakefields
 * transv==2: use transverse wakefields with several particles per slice
 */

/* r_over_q is given in circuit ohms per meter :-) */

// check for CAV type only

void wakefield_init(int longit,int transv)
{
  wakefield_data.transv=transv;
  wakefield_data.longit=longit;
}

OStream &operator<<(OStream &stream, const CAVITY &cav )
{
  return stream << static_cast<const ELEMENT &>(cav)
		<< cav.gradient   // was v1
		<< cav.phase // v5
		<< injector_data.lambda[0];
}

void CAVITY::init_attributes() 
{
  attributes.add("gradient", "Accelerating gradient [GV/m]", OPT_DOUBLE, &gradient);
  attributes.add("phase", "Relative phase [deg]", OPT_DOUBLE, &phase, deg2rad, rad2deg);
  attributes.add("type", "Type of the cavity [INT]", OPT_INT, &field);
  attributes.add("lambda", "Wavelength [m]", OPT_DOUBLE, &lambda);
  attributes.add("frequency", "Operating frequency [GHz]", OPT_DOUBLE, &lambda, freq2lambda, lambda2freq);
  attributes.add("bookshelf_x", "Bookshelf angle in x [rad]", OPT_DOUBLE, &book.x);
  attributes.add("bookshelf_y", "Bookshelf angle in y [rad]", OPT_DOUBLE, &book.y);
  attributes.add("bookshelf_phase", "Bookshelf relative beam phase [rad]", OPT_DOUBLE, &book.phase);
  attributes.add("bpm_offset_x", "Horizontal structure position error after realignment [um]", OPT_DOUBLE, &bpm.offset.x);
  attributes.add("bpm_offset_y", "Vertical structure position error after realignment [um]", OPT_DOUBLE, &bpm.offset.y);
  attributes.add("bpm_reading_x", "Horizontal wakefield monitor reading [um]", OPT_DOUBLE, &bpm.pos.x);
  attributes.add("bpm_reading_y", "Vertical wakefield monitor reading [um]", OPT_DOUBLE, &bpm.pos.y);
  attributes.add("dipole_kick_x", "Horizontal dipole kick [urad*GeV=keV]", OPT_DOUBLE, &_dipole_kick.x);
  attributes.add("dipole_kick_y", "Vertical dipole kick [urad*GeV=keV]", OPT_DOUBLE, &_dipole_kick.y);
  attributes.add("pi_mode", "Enable Rosenzweig-Serafini focusing [BOOL]", OPT_BOOL, &pi_mode);
}

CAVITY::CAVITY(int &argc, char **argv ) : ELEMENT(), _dipole_kick(0.0, 0.0), field(0), gradient(0.0), phase(0.0), lambda(0.0), wake_data(NULL)
{
  init_attributes();
  set_bookshelf(0.0,0.0);
  set_book_phase(-8.5*PI/180.0);
  v_tesla=1.0;
  pi_mode=false;
  set_attributes(argc, argv);
}

CAVITY::CAVITY(double length, int _field,double _gradient, double phase_deg) : ELEMENT(length), _dipole_kick(0.0, 0.0), field(_field), gradient(_gradient), lambda(0.0), wake_data(NULL)
{
  init_attributes();
  set_bookshelf(0.0,0.0);
  set_book_phase(-8.5*PI/180.0);
  phase=deg2rad(phase_deg);
  v_tesla=1.0;
  pi_mode=false;
  //  xz=NULL;
}

CAVITY::CAVITY(double length,int _field, double phase_deg) : ELEMENT(length), _dipole_kick(0.0, 0.0), field(_field), gradient(0.0), lambda(0.0), wake_data(NULL)
{
  init_attributes();
  set_bookshelf(0.0,0.0);
  set_book_phase(-8.5*PI/180.0);
  phase=deg2rad(phase_deg);
  v_tesla=1.0;
  pi_mode=false;
  // xz=NULL;
}

void CAVITY::fill_z_mode_xy(double *x, double *y, int n)
{
  if (yz.size()!=0) {
    for (int i=0;i<n;i++) {
      x[i]=0.0;
      y[i]=yz[i];
    }
  } else {
    for (int i=0;i<n;i++) {
      x[i]=0.0;
      y[i]=0.0;
    }
  }
}

void CAVITY::bookshelf(BEAM *beam )
{
  double c_book=cos(phase+book.phase)*gradient*geometry.length*1e6;
  double s_book=sin(phase+book.phase)*gradient*geometry.length*1e6;
  double *s_beam=beam->s_long[field];
  double *c_beam=beam->c_long[field];
  int n=beam->slices/beam->macroparticles;
  for (int i=0,k=0;i<n;i++) {
    double k_x=(c_beam[i]*c_book+s_beam[i]*s_book)*book.x;
    double k_y=(c_beam[i]*c_book+s_beam[i]*s_book)*book.y;
    //    k_x+=beam->field[0].de[i]*cavity->length*1e6*book.x;
    //    k_y+=beam->field[0].de[i]*cavity->length*1e6*book.y;
    for (int j=0;j<beam->macroparticles;++j){
      beam->particle[k].xp+=k_x/beam->particle[k].energy;
      beam->particle[k].yp+=k_y/beam->particle[k].energy;
      k++;
    }
  }
}

void CAVITY::structure_banana(double /*bowx*/, double bowy, double /*offsetx*/,double offsety, int noreset )
{
  int n=injector_data.steps;
  double dz=geometry.length/n;
  double z=-0.5*(geometry.length-dz);
  if ((int)yz.size()!=n) {
    add_yz(n);
  }
  if (!noreset) {
    for (int i=0;i<n;i++) {
      yz[i]=0.0;
    }
  }
  double y0;
  if (fabs(z)>std::numeric_limits<double>::epsilon()) {
    bowy/=z*z;
    y0=bowy*(z*z+0.5*(n-1)*(2*z*dz+dz*dz*(2*n-1)/3.0));
  } else {
    bowy=0.0;
    y0=0.0;
  }
  for (int i=0;i<n;i++) {
    yz[i]+=bowy*z*z-y0+offsety;
    // ysum+=cavity->yz[i];
    z+=dz;
  }
}

void M_print(R_MATRIX *r)
{
  placet_printf(INFO,"%g %g\n%g %g\n",r->r11,r->r12,r->r21,r->r22);
}


void CAVITY::track_rf_0(BEAM *beam )
{
  step_in(beam);
  step_rf_0(beam);
  step_out(beam);
}

/*
  The routine steps through a cavity using second order transport
*/

void CAVITY::sigma_step_x(R_MATRIX *sigma,R_MATRIX *sigma_xx,R_MATRIX *sigma_xy, double delta )
{
  double lndelta,lndelta_delta;
  /* include the rotation due to the endfields */
#ifdef END_FIELDS
#ifdef CAV_PRECISE
  if (delta>0.01) {
    lndelta=0.5*log1p(delta);
    lndelta_delta=lndelta/delta;
  } else {
    lndelta_delta=0.5*(1.0-delta*(0.5+delta*0.3308345316809));
    lndelta=delta*lndelta_delta;
  }
#else
  lndelta=0.5*delta;
  ln_delta_delta=0.5;
#endif
  /* Transfer matrix with endfield kicks */
  R_MATRIX r1,r2;
  r1.r11=1.0-lndelta;
  r1.r12=2.0*geometry.length*lndelta_delta;
  r1.r21=-0.5*delta/(geometry.length*(1.0+delta))*lndelta;
  r1.r22=(1.0+lndelta)/(1.0+delta);
  r2.r11=r1.r11;
  r2.r12=r1.r21;
  r2.r21=r1.r12;
  r2.r22=r1.r22;
#else
  r1.r11=1.0;
  r2.r11=1.0;
  r1.r21=0.0;
  r2.r12=0.0;
#ifdef CAV_PRECISE
  if (delta>0.01) {
    r1.r12=log1p(delta)/delta*geometry.length;
  } else {
    r1.r12=geometry.length*(1.0-delta*(0.5+delta*0.3308345316809));
  }
#else
  r1.r12=geometry.length;
#endif
  r2.r21=r1.r12;
  r1.r22=1.0/(1.0+delta);
  r2.r22=r1.r22;
#endif
  mult_M_M(&r1,sigma,sigma);
  mult_M_M(sigma,&r2,sigma);
  mult_M_M(&r1,sigma_xx,sigma_xx);
  mult_M_M(sigma_xx,&r2,sigma_xx);
  mult_M_M(&r1,sigma_xy,sigma_xy);
  mult_M_M(sigma_xy,&r2,sigma_xy);
}


#include "cavity_many.c"

/*
  Load the cavity routine for cavity adjustment and without for first and
  second order transport
*/

#define RFCAV
#define SECOND_ORDER
#include "cavity0.cc"
#undef SECOND_ORDER
#include "cavity0.cc"
#undef RFCAV

#define SECOND_ORDER
#include "cavity0.cc"
#undef SECOND_ORDER

#include "cavity0.cc"

void CAVITY::step_twiss(BEAM *beam,FILE *file,double step0,int iel,double s0,int n1,int n2,void (*callback0)(FILE*,BEAM*,int,double,int,int))
{
  int nstep=(int)(geometry.length/step0)+1;
  double nlength=geometry.length/(nstep);
  double half_length=0.5*nlength;
  // double length_i=1.0/nlength;
  // double *de=beam->acc_field[field];
  double *s_beam=beam->s_long[field];
  double *c_beam=beam->c_long[field];
  double s_long,c_long;
  sincos(phase,&s_long,&c_long);

#ifdef END_FIELDS
  for (int i=0;i<beam->slices;i++){
    /*
      de0=gradient*de[i/beam->macroparticles]
      +factor*beam->field[0].de[i/beam->macroparticles];****/
    double de0=(c_beam[i/beam->macroparticles]*c_long
		+s_beam[i/beam->macroparticles]*s_long)*gradient
      +beam->factor*beam->field[0].de[i/beam->macroparticles];
    double delta=half_length*de0/beam->particle[i].energy;

    /*
      Transfer matrix for endfields
    */
    R_MATRIX r1;
    r1.r11=1.0;
    r1.r12=0.0;
    r1.r21=-delta/nlength;
    r1.r22=1.0;

    R_MATRIX r2;
    r2.r11=r1.r11;
    r2.r12=r1.r21;
    r2.r21=r1.r12;
    r2.r22=r1.r22;

    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
    mult_M_M(beam->sigma+i,&r2,beam->sigma+i);
    mult_M_M(&r1,beam->sigma_xx+i,beam->sigma_xx+i);
    mult_M_M(beam->sigma_xx+i,&r2,beam->sigma_xx+i);
    mult_M_M(&r1,beam->sigma_xy+i,beam->sigma_xy+i);
    mult_M_M(beam->sigma_xy+i,&r2,beam->sigma_xy+i);
  }
#endif
  callback0(file,beam,iel,s0,n1,n2);
  for (int istep=0;istep<nstep;istep++){
    s0+=step0;
    for (int i=0;i<beam->slices;i++){
      /*
	de0=gradient*de[i/beam->macroparticles]
	+factor*beam->field[0].de[i/beam->macroparticles];
      */
      double de0=(c_beam[i/beam->macroparticles]*c_long
		  +s_beam[i/beam->macroparticles]*s_long)*gradient
	+beam->factor*beam->field[0].de[i/beam->macroparticles];
      double delta=half_length*de0/beam->particle[i].energy;

#ifdef CAV_PRECISE
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      }
      else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
#else
      lndelta=1.0;
#endif
      
      /*
	Transfer matrix without endfield kicks
      */
      
      R_MATRIX r1;
      r1.r11=1.0;
      r1.r12=nlength*lndelta;
      r1.r21=0.0;
      r1.r22=1.0/(1.0+2.0*delta);
      
      R_MATRIX r2;
      r2.r11=r1.r11;
      r2.r12=r1.r21;
      r2.r21=r1.r12;
      r2.r22=r1.r22;
      
      mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
      mult_M_M(beam->sigma+i,&r2,beam->sigma+i);
#ifdef TWODIM
      mult_M_M(&r1,beam->sigma_xx+i,beam->sigma_xx+i);
      mult_M_M(beam->sigma_xx+i,&r2,beam->sigma_xx+i);
      mult_M_M(&r1,beam->sigma_xy+i,beam->sigma_xy+i);
      mult_M_M(beam->sigma_xy+i,&r2,beam->sigma_xy+i);
#endif
      beam->particle[i].energy+=nlength*de0;
    }
    
    callback0(file,beam,iel,s0,n1,n2);
  }

#ifdef END_FIELDS
  /* include the rotation due to the endfields */
  for (int i=0;i<beam->slices;i++){
    /*
      de0=de[i/beam->macroparticles]*gradient
      +factor*beam->field[0].de[i/beam->macroparticles];*/
    double de0=(c_beam[i/beam->macroparticles]*c_long
		+s_beam[i/beam->macroparticles]*s_long)*gradient
      +beam->factor*beam->field[0].de[i/beam->macroparticles];
    double delta=half_length*de0/beam->particle[i].energy;
    /*
      Transfer matrix for endfields
    */
    R_MATRIX r1;
    r1.r11=1.0;
    r1.r12=0.0;
    r1.r21=delta/nlength;
    r1.r22=1.0;

    R_MATRIX r2;
    r2.r11=r1.r11;
    r2.r12=r1.r21;
    r2.r21=r1.r12;
    r2.r22=r1.r22;

    mult_M_M(&r1,beam->sigma+i,beam->sigma+i);
    mult_M_M(beam->sigma+i,&r2,beam->sigma+i);
    mult_M_M(&r1,beam->sigma_xx+i,beam->sigma_xx+i);
    mult_M_M(beam->sigma_xx+i,&r2,beam->sigma_xx+i);
    mult_M_M(&r1,beam->sigma_xy+i,beam->sigma_xy+i);
    mult_M_M(beam->sigma_xy+i,&r2,beam->sigma_xy+i);
  }
#endif
}

void CAVITY::step_twiss_0(BEAM *beam,FILE * file,double /*step0*/,int iel,double s0,int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int))
{
  placet_cout << VERBOSE << " CAVITY:: step_twiss_0:  twiss computed at the entrance and at the exit of the cavity " << endmsg;
  callback(file,beam,iel,s0,n1,n2);
  track_0(beam);
  s0+=geometry.length;
  callback(file,beam,iel,s0,n1,n2);
}

void CAVITY::step_partial(BEAM* beam, double l, void (ELEMENT::*step_function)(BEAM*))
{
  placet_cout << ERROR << "Partial stepping not implemented for cavities" << endmsg;
}

void CAVITY::step_6d_0(BEAM *beam )
{
#define SQRT_8 2.82842712474619
  const double eps=std::numeric_limits<double>::epsilon();
  if (short_range.wake || !short_range.name.empty()) {
    if (!short_range.wake) {
      short_range.wake = get_short_range(short_range.name.c_str());
    }
  }
  if (fabs(gradient)>eps) {
    double k0 = 2*M_PI/lambda*1e-6;
    double half_length = geometry.length/2;
    if (short_range.wake) {
      std::vector<int> index_; // array of valid particles
      for (size_t i=0; i<beam->slices; i++) {
	if (!beam->particle[i].is_lost())
	  index_.push_back(i);
      }

      // checks if the particles are ordered by 'z', and find min and max
      const std::vector<int> index = sort_index(beam->particle, index_);

      // number of slices for wakefield computation (must be a power of two)
      const size_t Nslices = 64;
      std::pair<std::valarray<double>, std::valarray<double> > data = short_range.wake->wake_z_convolve(beam, index, geometry.length, Nslices);
      const std::valarray<double> z = data.first; 
      const std::valarray<double> data_Wz = data.second;
      const double dz = z[1] - z[0];
      if(pi_mode) {
	//based on J. Rosenzweig and L. Serafini, PRE 49,2 (1994) 1599-1602
	// first half of the structure
	for (int i=0, slice=0; i<index.size(); i++) {
	  PARTICLE &particle = beam->particle[index[i]];
	  while (slice<(Nslices-1) && particle.z>z[slice+1]) {
	    slice++;
	  }
	  double a = (particle.z - z[slice]) / dz;
	  double dE = (slice<Nslices-1) ? (1.0 - a) * data_Wz[slice] + a * data_Wz[slice+1] : data_Wz[slice];
	  // compute effective gradient including energy loss from longitudinal wakefield
	  double cosph=cos(phase-k0*particle.z);
	  double effective_gradient = cosph*gradient - dE / geometry.length;
	  // first half of the acceleration
	  double energy_gain = effective_gradient*half_length;
	  double delta = energy_gain/particle.energy; 
	  double onepd = 1.0 + delta;
	  double alpha, sinal, cosal;
	  double xo=particle.x, yo=particle.y;
	  
	  alpha=(fabs(energy_gain) > std::numeric_limits<double>::epsilon()) ? gradient*log1p(delta)/(SQRT_8*effective_gradient) : gradient*half_length/particle.energy/SQRT_8;
	  sincos(alpha, &sinal, &cosal);
	  
	  particle.xp += -0.5*effective_gradient/particle.energy*xo;
	  particle.yp += -0.5*effective_gradient/particle.energy*yo;
	  
	  particle.x = cosal*xo+particle.energy/gradient*SQRT_8*sinal*particle.xp;
	  particle.y = cosal*yo+particle.energy/gradient*SQRT_8*sinal*particle.yp;
	  particle.xp = -gradient/particle.energy/SQRT_8*sinal/onepd*xo+cosal/onepd*particle.xp;
	  particle.yp = -gradient/particle.energy/SQRT_8*sinal/onepd*yo+cosal/onepd*particle.yp;
	  
	  particle.energy += energy_gain;
	}

	{
	  double x_mean=0.0;
	  double y_mean=0.0;
	  double wgt=0.0;
	  for (int i=0; i<beam->slices; i++) {
	    PARTICLE &particle = beam->particle[i];
	    x_mean += particle.x*fabs(particle.wgt);
	    y_mean += particle.y*fabs(particle.wgt);
	    wgt += fabs(particle.wgt);
	  }
	  bpm.set_x_position(x_mean/wgt);
	  bpm.set_y_position(y_mean/wgt);
	}

	// compute transverse wakefield kicks
	data = short_range.wake->wake_x_convolve(beam, index, geometry.length, Nslices); const std::valarray<double> data_Wx = data.second;
	data = short_range.wake->wake_y_convolve(beam, index, geometry.length, Nslices); const std::valarray<double> data_Wy = data.second;
	
	// apply trasnverse wakefield kicks and user-defined _dipole_kick
	for (int i=0, slice=0; i<index.size(); i++) {
	  PARTICLE &particle = beam->particle[index[i]];
	  while (slice<(Nslices-1) && particle.z>z[slice+1]) {
	    slice++;
	  }
	  double Kx, Ky;
	  if (slice<Nslices-1) {
	    double a = (particle.z - z[slice]) / dz;
	    Kx = (1.0 - a) * data_Wx[slice] + a * data_Wx[slice+1];
	    Ky = (1.0 - a) * data_Wy[slice] + a * data_Wy[slice+1];
	  } else {
	    Kx = data_Wx[slice];
	    Ky = data_Wy[slice];
	  }
	  particle.xp += (Kx + _dipole_kick.x) / particle.energy;
	  particle.yp += (Ky + _dipole_kick.y) / particle.energy;
	}
	
	// second half of the structure
	for (int i=0, slice=0; i<index.size(); i++) {
	  PARTICLE &particle = beam->particle[index[i]];
	  while (slice<(Nslices-1) && particle.z>z[slice+1]) {
	    slice++;
	  }
	  double a = (particle.z - z[slice]) / dz;
	  double dE = (slice<Nslices-1) ? (1.0 - a) * data_Wz[slice] + a * data_Wz[slice+1] : data_Wz[slice];
	  double cosph = cos(phase-k0*particle.z);
	  // second half of the cavity
	  double effective_gradient = cosph*gradient - dE / geometry.length;
	  double energy_gain = effective_gradient*half_length;
	  double delta = energy_gain/particle.energy;
	  double onepd = 1.0+delta;
	  double alpha, sinal, cosal;
	  double xo=particle.x, yo=particle.y;

	  alpha=(fabs(energy_gain) > std::numeric_limits<double>::epsilon()) ? gradient*log1p(delta)/(SQRT_8*effective_gradient) : gradient*half_length/particle.energy/SQRT_8;
	  sincos(alpha, &sinal, &cosal);
	  
	  particle.x = cosal*xo+particle.energy/gradient*SQRT_8*sinal*particle.xp;
	  particle.y = cosal*yo+particle.energy/gradient*SQRT_8*sinal*particle.yp;
	  particle.xp = -gradient/particle.energy/SQRT_8*sinal/onepd*xo+cosal/onepd*particle.xp;
	  particle.yp = -gradient/particle.energy/SQRT_8*sinal/onepd*yo+cosal/onepd*particle.yp;
	  
	  particle.xp += 0.5*effective_gradient/particle.energy/onepd*particle.x;
	  particle.yp += 0.5*effective_gradient/particle.energy/onepd*particle.y;

	  particle.energy += energy_gain;
	}
      } else {
	// first half of the structure
	for (int i=0, slice=0; i<index.size(); i++) {
	  PARTICLE &particle = beam->particle[index[i]];
	  while (slice<(Nslices-1) && particle.z>z[slice+1]) {
	    slice++;
	  }
	  double a = (particle.z - z[slice]) / dz;
	  double dE = (slice<Nslices-1) ? (1.0 - a) * data_Wz[slice] + a * data_Wz[slice+1] : data_Wz[slice];
	  // compute effective gradient including energy loss from longitudinal wakefield
	  double effective_gradient = cos(phase-k0*particle.z)*gradient - dE / geometry.length;
	  // entrance end field (Daniel's Lectures, Main Linac A1)
	  particle.xp -= 0.5*effective_gradient*particle.x/particle.energy;
	  particle.yp -= 0.5*effective_gradient*particle.y/particle.energy;
	  // first half of the acceleration
	  double energy_gain = effective_gradient*half_length;
	  double delta = energy_gain/particle.energy;
	  double log1p_delta = log1p(delta)/delta;
	  particle.x += particle.xp * log1p_delta * half_length;
	  particle.y += particle.yp * log1p_delta * half_length;
	  particle.xp /= (1+delta);
	  particle.yp /= (1+delta);
	  particle.energy += energy_gain;
	}
	
	{
	  double x_mean=0.0;
	  double y_mean=0.0;
	  double wgt=0.0;
	  for (int i=0; i<beam->slices; i++) {
	    PARTICLE &particle = beam->particle[i];
	    x_mean += particle.x*fabs(particle.wgt);
	    y_mean += particle.y*fabs(particle.wgt);
	    wgt += fabs(particle.wgt);
	  }
	  bpm.set_x_position(x_mean/wgt);
	  bpm.set_y_position(y_mean/wgt);
	}

	// compute transverse wakefield kicks
	data = short_range.wake->wake_x_convolve(beam, index, geometry.length, Nslices); const std::valarray<double> data_Wx = data.second;
	data = short_range.wake->wake_y_convolve(beam, index, geometry.length, Nslices); const std::valarray<double> data_Wy = data.second;
	
	// apply trasnverse wakefield kicks and user-defined _dipole_kick
	for (int i=0, slice=0; i<index.size(); i++) {
	  PARTICLE &particle = beam->particle[index[i]];
	  while (slice<(Nslices-1) && particle.z>z[slice+1]) {
	    slice++;
	  }
	  double Kx, Ky;
	  if (slice<Nslices-1) {
	    double a = (particle.z - z[slice]) / dz;
	    Kx = (1.0 - a) * data_Wx[slice] + a * data_Wx[slice+1];
	    Ky = (1.0 - a) * data_Wy[slice] + a * data_Wy[slice+1];
	  } else {
	    Kx = data_Wx[slice];
	    Ky = data_Wy[slice];
	  }
	  particle.xp += (Kx + _dipole_kick.x) / particle.energy;
	  particle.yp += (Ky + _dipole_kick.y) / particle.energy;
	}
	
	// second half of the structure
	for (int i=0, slice=0; i<index.size(); i++) {
	  PARTICLE &particle = beam->particle[index[i]];
	  while (slice<(Nslices-1) && particle.z>z[slice+1]) {
	    slice++;
	  }
	  double a = (particle.z - z[slice]) / dz;
	  double dE = (slice<Nslices-1) ? (1.0 - a) * data_Wz[slice] + a * data_Wz[slice+1] : data_Wz[slice];
	  // second half of the cavity
	  double effective_gradient = cos(phase-k0*particle.z)*gradient - dE / geometry.length;
	  double energy_gain = effective_gradient*half_length;
	  double delta = energy_gain/particle.energy;
	  double log1p_delta = log1p(delta)/delta;
	  // second half of the acceleration
	  particle.x += particle.xp * log1p_delta * half_length;
	  particle.y += particle.yp * log1p_delta * half_length;
	  particle.xp /= (1+delta);
	  particle.yp /= (1+delta);
	  // longitudinal phase space
	  // particle.z += half_length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
	  particle.energy += energy_gain;
	  // exit end field
	  particle.xp += 0.5*effective_gradient*particle.x/particle.energy;
	  particle.yp += 0.5*effective_gradient*particle.y/particle.energy;
	}
      }
    } else {
      double x_mean = 0.0;
      double y_mean = 0.0;
      double wgt = 0.0;
      if(pi_mode){
	for (int i=0;i<beam->slices;i++) {
	  PARTICLE &particle = beam->particle[i];
	  double cosph=cos(phase-k0*particle.z);
	  if(cosph==0.0){
	    particle.x += geometry.length*particle.xp;
	    particle.y += geometry.length*particle.yp;
	  } else{
	    // First half of the cavity
	    double effective_gradient = cosph*gradient;
	    double energy_gain = effective_gradient*half_length;
	    double delta = energy_gain/particle.energy;
	    double onepd = 1.0 + delta;
	    double xo=particle.x, yo=particle.y;
	    double alpha, sinal, cosal;

	    alpha=(fabs(cosph) > std::numeric_limits<double>::epsilon()) ? log1p(delta)/(SQRT_8*cosph) : gradient*half_length/particle.energy/SQRT_8;
	    sincos(alpha, &sinal, &cosal);

	    particle.xp += -0.5*gradient/particle.energy*cosph*xo;
	    particle.yp += -0.5*gradient/particle.energy*cosph*yo;

	    particle.x = cosal*xo+particle.energy/gradient*SQRT_8*sinal*particle.xp;
	    particle.y = cosal*yo+particle.energy/gradient*SQRT_8*sinal*particle.yp;
	    particle.xp = -gradient/particle.energy/SQRT_8*sinal/onepd*xo+cosal/onepd*particle.xp;
	    particle.yp = -gradient/particle.energy/SQRT_8*sinal/onepd*yo+cosal/onepd*particle.yp;

	    particle.energy += energy_gain;

	    // now we are in the middle of the cavity
	    x_mean+=particle.x*fabs(particle.wgt);
	    y_mean+=particle.y*fabs(particle.wgt);
	    wgt += fabs(particle.wgt);
	    
	    // dipole kick
	    particle.xp += _dipole_kick.x/particle.energy;
	    particle.yp += _dipole_kick.y/particle.energy;
	    // now apply the wakefield kick
	    /////// we assume the following /commented/ quantities don't change

	    delta=energy_gain/particle.energy;
	    onepd= 1.0 + delta;
	    // second half of the cavity
	    xo=particle.x;
	    yo=particle.y;

	    alpha=(fabs(cosph) > std::numeric_limits<double>::epsilon()) ? log1p(delta)/(SQRT_8*cosph) : gradient*half_length/particle.energy/SQRT_8;	    
	    sincos(alpha, &sinal, &cosal);
	    
	    particle.x = cosal*xo+particle.energy/gradient*SQRT_8*sinal*particle.xp;
	    particle.y = cosal*yo+particle.energy/gradient*SQRT_8*sinal*particle.yp;
	    particle.xp = -gradient/particle.energy/SQRT_8*sinal/onepd*xo+cosal/onepd*particle.xp;
	    particle.yp = -gradient/particle.energy/SQRT_8*sinal/onepd*yo+cosal/onepd*particle.yp;
	    
	    particle.xp += 0.5*gradient/particle.energy/onepd*cosph*particle.x;
	    particle.yp += 0.5*gradient/particle.energy/onepd*cosph*particle.y;
	    
	    particle.energy += energy_gain;
	  }
	}
      } else {
	for (int i=0;i<beam->slices;i++) {
	  PARTICLE &particle = beam->particle[i];
	  // entrance end field (Daniel's Lectures, Main Linac A1)
	  double effective_gradient = cos(phase-k0*particle.z)*gradient;
	  particle.xp -= 0.5*effective_gradient*particle.x/particle.energy;
	  particle.yp -= 0.5*effective_gradient*particle.y/particle.energy;
	  // first half of the acceleration
	  double energy_gain = effective_gradient*half_length;
	  double delta = energy_gain/particle.energy;
	  double log1p_delta = log1p(delta)/delta;
	  particle.x += particle.xp * log1p_delta * half_length;
	  particle.y += particle.yp * log1p_delta * half_length;
	  particle.xp /= (1+delta);
	  particle.yp /= (1+delta);
	  // longitudinal phase space
	  // particle.z += half_length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
	  particle.energy += energy_gain;
	  // now we are in the middle of the cavity
	  x_mean+=particle.x*fabs(particle.wgt);
	  y_mean+=particle.y*fabs(particle.wgt);
	  wgt += fabs(particle.wgt);
	  // dipole kick
	  particle.xp += _dipole_kick.x/particle.energy;
	  particle.yp += _dipole_kick.y/particle.energy;
	  // now apply the wakefield kick
	  /////// we assume the following /commented/ quantities don't change
	  //double effective_gradient = cos(phase-k0*particle.z)*gradient;
	  //double energy_gain = effective_gradient*half_length;
	  delta = energy_gain/particle.energy;
	  log1p_delta = log1p(delta)/delta;
	  // second half of the acceleration
	  particle.x += particle.xp * log1p_delta * half_length;
	  particle.y += particle.yp * log1p_delta * half_length;
	  particle.xp /= (1+delta);
	  particle.yp /= (1+delta);
	  // longitudinal phase space
	  // particle.z += half_length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
	  particle.energy += energy_gain;
	  // exit end field
	  particle.xp += 0.5*effective_gradient*particle.x/particle.energy;
	  particle.yp += 0.5*effective_gradient*particle.y/particle.energy;
	}
      }
      bpm.set_x_position(x_mean/wgt);
      bpm.set_y_position(y_mean/wgt);
    }
  } else {
    if (geometry.length < std::numeric_limits<double>::epsilon()) return;
    if (short_range.wake) {
      std::vector<int> index_; // array of valid particles
      for (size_t i=0; i<beam->slices; i++) {
	if (!beam->particle[i].is_lost())
	  index_.push_back(i);
      }

      // checks if the particles are ordered by 'z', and find min and max
      const std::vector<int> index = sort_index(beam->particle, index_);

      // number of slices for wakefield computation (must be a power of two)
      const size_t Nslices = 64;
      std::pair<std::valarray<double>, std::valarray<double> > data = short_range.wake->wake_z_convolve(beam, index, geometry.length, Nslices);
      const std::valarray<double> z = data.first; 
      const std::valarray<double> data_Wz = data.second;
      const double dz = z[1] - z[0];

      for (int i=0; i<beam->slices; i++) {
	PARTICLE &particle = beam->particle[i];
	particle.x += geometry.length*particle.xp/2;
	particle.y += geometry.length*particle.yp/2;
	particle.z += geometry.length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2()/2;
      }

      {
	double x_mean=0.0;
	double y_mean=0.0;
	double wgt=0.0;
	for (int i=0; i<beam->slices; i++) {
	  PARTICLE &particle = beam->particle[i];
	  x_mean += particle.x*fabs(particle.wgt);
	  y_mean += particle.y*fabs(particle.wgt);
	  wgt += fabs(particle.wgt);
	}
	bpm.set_x_position(x_mean/wgt);
	bpm.set_y_position(y_mean/wgt);
      }

      // compute transverse wakefield kicks
      data = short_range.wake->wake_x_convolve(beam, index, geometry.length, Nslices); const std::valarray<double> data_Wx = data.second;
      data = short_range.wake->wake_y_convolve(beam, index, geometry.length, Nslices); const std::valarray<double> data_Wy = data.second;
	
      // apply trasnverse wakefield kicks and user-defined _dipole_kick
      for (int i=0, slice=0; i<index.size(); i++) {
	PARTICLE &particle = beam->particle[index[i]];
	while (slice<(Nslices-1) && particle.z>z[slice+1]) {
	  slice++;
	}
	double Kx, Ky;
	if (slice<Nslices-1) {
	  double a = (particle.z - z[slice]) / dz;
	  Kx = (1.0 - a) * data_Wx[slice] + a * data_Wx[slice+1];
	  Ky = (1.0 - a) * data_Wy[slice] + a * data_Wy[slice+1];
	} else {
	  Kx = data_Wx[slice];
	  Ky = data_Wy[slice];
	}
	particle.xp += (Kx + _dipole_kick.x) / particle.energy;
	particle.yp += (Ky + _dipole_kick.y) / particle.energy;
      }
      for (int i=0; i<beam->slices; i++) {
	PARTICLE &particle = beam->particle[i];
	particle.x += geometry.length*particle.xp/2;
	particle.y += geometry.length*particle.yp/2;
	particle.z += geometry.length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2()/2;
      }
    } else {

      double x1_mean = 0.0;
      double y1_mean = 0.0;
      double wgt = 0.0;
      for (int i=0; i<beam->slices; i++) {
	PARTICLE &particle = beam->particle[i];
	x1_mean += particle.x*fabs(particle.wgt);
	y1_mean += particle.y*fabs(particle.wgt);
	wgt += fabs(particle.wgt);
      }
      
      for (int i=0; i<beam->slices; i++) {
	PARTICLE &particle = beam->particle[i];
	particle.x += geometry.length*particle.xp;
	particle.y += geometry.length*particle.yp;
	particle.z += geometry.length*1e6*particle.momentum_deviation(ref_energy)/ref_gamma2();
      }

      double x2_mean = 0.0;
      double y2_mean = 0.0;
      for (int i=0; i<beam->slices; i++) {
	PARTICLE &particle = beam->particle[i];
	x2_mean += particle.x*fabs(particle.wgt);
	y2_mean += particle.y*fabs(particle.wgt);
      }
      
      bpm.set_x_position(0.5*(x1_mean+x2_mean)/wgt);
      bpm.set_y_position(0.5*(y1_mean+y2_mean)/wgt);
    }
  }
#undef SQRT_8
}

Matrix<6,6> CAVITY::get_transfer_matrix_6d(double _energy ) const
{
  double energy_gain = cos(phase)*gradient*geometry.length;
  double e0 = (_energy == -1.0) ? ref_energy : _energy;
  double delta = energy_gain / e0;
  double sqrt1p_delta = sqrt(1. + delta);
  Matrix<6,6> A = Identity<6,6>();
  A[0][0] = A[2][2] = sqrt1p_delta;
  A[0][1] = A[2][3] = geometry.length * log1p(delta) * sqrt1p_delta / delta;
  A[1][1] = A[3][3] = 1. / sqrt1p_delta;
  double k0 = 2*M_PI/lambda;
  A[5][4] = k0*sin(phase)*gradient*geometry.length / (e0 + energy_gain);
  Matrix<6,6> E1 = Identity<6,6>();
  E1[1][0] = E1[3][2] = -delta / 2 / geometry.length;
  Matrix<6,6> E2 = Identity<6,6>();
  E2[1][0] = E2[3][2] = +delta / 2 / geometry.length / (1. + delta);
  return E2 * A * E1;
}

std::string CAVITY::aml_string(std::string new_name ) const 
{
  using namespace std;
  //  double energy=ref_energy;
  std::ostringstream result;
  int old_precision=result.precision(15);
  result<<"<element";
  std::string name;
  if (new_name!="") 
    name=new_name;
  else
    name=get_attribute_string("name");
  if (name!="")
    result<<" name = \""<<name<<"\"";
  result<<">"<<endl;   
  double length = get_attribute_double("length");
  std::string type;
  result<<"  <rf_cavity>\n";
  result<<"    <rf_freq design = \"" << lambda2freq(get_lambda()) * 1e9 << "\" />\n";
  result<<"    <phase0 design = \"" << get_phase() << "\" />\n";
  result<<"    <gradient design = \"" << get_gradient() * 1e9 << "\" />\n";
  result<<"  </rf_cavity>\n";
  double x=get_attribute_double("x");
  double y=get_attribute_double("y");
  double xp=get_attribute_double("xp");
  double yp=get_attribute_double("yp");
  double roll=get_attribute_double("roll");
  if (x!=0 || y!=0 || xp!=0 || yp!=0 || roll!=0) {
    result<<"  <orientation>\n";
    if (x!=0)
      result<<"    <x_offset design = \""<<x*1e-6<<"\" />\n";
    if (y!=0)
      result<<"    <y_offset design = \""<<y*1e-6<<"\" />\n";
    if (xp!=0)
      result<<"    <x_pitch design = \""<<xp*1e-6<<"\" />\n";
    if (yp!=0)
      result<<"    <y_pitch design = \""<<yp*1e-6<<"\" />\n";
    if (roll!=0)
      result<<"    <roll design = \""<<roll*1e-6<<"\" />\n";
    result<<"  </orientation>\n";
  }
  if (length!=0)
    result<<"  <length design = \""<<length<<"\" />\n";
  double aperture_x=get_attribute_double("aperture_x");
  double aperture_y=get_attribute_double("aperture_y");
  std::string aperture_shape=get_attribute_string("aperture_shape");
  if ((aperture_x!=0 || aperture_y!=0)&&(aperture_x<1e6 || aperture_y<1e6)) {
    if (aperture_shape!="" || aperture_x!=0 || aperture_y!=0) {
      if (aperture_shape!="") {
	if (aperture_shape == "oval" || aperture_shape == "elliptic") {
	  aperture_shape = "ELLIPTICAL";
	} else if (aperture_shape == "circular") {
	  aperture_shape = "CIRCULAR";
	} else {
	  aperture_shape = "RECTANGULAR";
	}
	result<<"  <aperture shape = \""<<aperture_shape<<"\">\n";
      } else 
	result<<"  <aperture>\n";
      if (aperture_x!=0)
	result<<"    <x_limit design = \""<<aperture_x<<"\" />\n";
      if (aperture_y!=0)
	result<<"    <y_limit design = \""<<aperture_y<<"\" />\n";
      result<<"  </aperture>\n";
    }
  }
  result<<"</element>"<<endl;
  result.precision(old_precision);
  return result.str();  
}
