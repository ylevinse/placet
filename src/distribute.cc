void distribute_cav(double S_r[],double C_r[],double sum_S[],double sum_C[],
		    int n,double factor,double z[],int k0)
{
  double w,*Sk,*Ck;
  int i,j,k;

  for(i=0;i<n;i++){
    sum_S[i]=0.0;
    sum_C[i]=0.0;
  }
  // loop over trailing bunches
  for (k=k0-1;k>=0;k--){
    w=factor*(z[k0]-z[k]); // catch-up distance from bunch k0 to bunch k 
    j=(int)w;
    w-=j;
    // break if bunches are no longer visible
    if (j>=n) break;
    i=0;
    Sk=S_r+k*(n+1);
    Ck=C_r+k*(n+1);
    // compute wake in each cavity section, using linear interpolation
    // 0'th section (only second part applicable)
    sum_S[j]+=(1.0-w)*(Sk[i]+0.5*(1.0-w)*(Sk[i+1]-Sk[i]));
    sum_C[j]+=(1.0-w)*(Ck[i]+0.5*(1.0-w)*(Ck[i+1]-Ck[i]));
    j++;
    while (j<n){
      // first part of j'th section
      sum_S[j]+=w*(Sk[i]+(1.0-0.5*w)*(Sk[i+1]-Sk[i]));
      sum_C[j]+=w*(Ck[i]+(1.0-0.5*w)*(Ck[i+1]-Ck[i]));
      i++;
      // second part of j'th section
      sum_S[j]+=(1.0-w)*(Sk[i]+0.5*(1.0-w)*(Sk[i+1]-Sk[i]));
      sum_C[j]+=(1.0-w)*(Ck[i]+0.5*(1.0-w)*(Ck[i+1]-Ck[i]));
      j++;
    }
  }
}

void distribute_cav_Q(double S_r[],double C_r[],double sum_S[],double sum_C[],
		 int n,double factor,double z[],double dec[],int k0)
{
  double w,*Sk,*Ck,dectot=1.0;
  int i,j,k;

  for(i=0;i<n;i++){
    sum_S[i]=0.0;
    sum_C[i]=0.0;
  }
  for (k=k0-1;k>=0;k--){
    dectot*=dec[k+1];
    w=factor*(z[k0]-z[k]);
    j=(int)w;
    w-=j;
    if (j>=n) break;
    i=0;
    Sk=S_r+k*(n+1);
    Ck=C_r+k*(n+1);
    sum_S[j]+=dectot*(1.0-w)*(Sk[i]+0.5*(1.0-w)*(Sk[i+1]-Sk[i]));
    sum_C[j]+=dectot*(1.0-w)*(Ck[i]+0.5*(1.0-w)*(Ck[i+1]-Ck[i]));
    j++;
    while (j<n){
      sum_S[j]+=dectot*w*(Sk[i]+(1.0-0.5*w)*(Sk[i+1]-Sk[i]));
      sum_C[j]+=dectot*w*(Ck[i]+(1.0-0.5*w)*(Ck[i+1]-Ck[i]));
      i++;
      sum_S[j]+=dectot*(1.0-w)*(Sk[i]+0.5*(1.0-w)*(Sk[i+1]-Sk[i]));
      sum_C[j]+=dectot*(1.0-w)*(Ck[i]+0.5*(1.0-w)*(Ck[i+1]-Ck[i]));
      j++;
    }
  }
}
