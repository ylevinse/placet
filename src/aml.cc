#include <cstdio>
#include <cmath>
#include <list>
#include <map>
#include <string>
#include <sstream>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "beamline.h"
#include "structures_def.h"
//#include "solenoid.h"
//#include "quadrupole.h"
//#include "quadbpm.h"
//#include "multipole.h"
//#include "dipole.h"
//#include "cavity.h"
//#include "rmatrix.h"
//#include "photon.h"

/* Function prototypes */

#include "element.h"
#include "placet_tk.h"
#include "aml.h"

std::string ELEMENT::aml_string(std::string aml_name ) const 
{
  double energy=ref_energy;
  std::ostringstream result;
  int old_precision = result.precision(15);
  result<<"<element";
  std::string name;
  if (aml_name!="") 
    name=aml_name;
  else
    name=get_attribute_string("name");
  if (name!="")
    result<<" name = \""<<name<<"\"";
  result<<">"<<std::endl;
  double length = get_attribute_double("length");
  std::string type;
  if (is_multipole()) {
    int order = get_attribute_int("type");
    if (order==3) type="sextupole";
    else if (order==4) type="octupole";
  }
  if (is_quad()) {
    result<<"  <quadrupole>\n";
    double strength = get_attribute_double("strength");
    if (strength != 0)
      result<<"    <k design = \""<<strength/energy/geometry.length<<"\" />\n";
    result<<"  </quadrupole>\n";
  } else if (type=="sextupole") {
    result<<"  <sextupole>\n";
    double strength = get_attribute_double("strength");
    if (strength != 0)
      result<<"    <k n = \"2\" design = \""<<strength/energy/geometry.length<<"\" />\n";
    result<<"  </sextupole>\n";    
  } else if (type=="octupole") {
    result<<"  <octupole>\n";
    double strength = get_attribute_double("strength");
    if (strength != 0)
      result<<"    <k n = \"3\" design = \""<<strength/energy/geometry.length<<"\" />\n";
    result<<"  </octupole>\n";    
  } else if (is_multipole()) {
    result<<"  <multipole>\n";
    int order = get_attribute_int("type");
    double strength = get_attribute_double("strength");
    if (strength != 0 && order >0)
      result<<"    <k n = \""<<order-1<<"\" design = \""<<strength/energy/geometry.length<<"\">\n";
    result<<"  </multipole>\n";    
  } else if (is_sbend()) {
    result<<"  <bend>\n";
    double angle = get_attribute_double("angle");
    double e1 = get_attribute_double("E1");
    double e2 = get_attribute_double("E2");
    double k = get_attribute_double("K");
    double k2 = get_attribute_double("K2");
    if (angle != 0 && length >0)
      result<<"    <g design = \""<<angle/length<<"\" />\n";
    if (e1 != 0)
      result<<"    <e1 design = \""<<e1<<"\" />\n";
    if (e2 != 0)
      result<<"    <e2 design = \""<<e2<<"\" />\n";
    result<<"  </bend>\n";
    if (k!=0 || k2!=0) {
      result<<"  <thick_multipole>\n";
      if (k!=0)
        result<<"    <k n = \"1\" design = \""<<k/energy<<"\" />\n";
      if (k2!=0)
        result<<"    <k n = \"2\" design = \""<<k2/energy<<"\" />\n";
      result<<"  </thick_multipole>\n";
    }  
  } else if (is_dipole()) {
    result<<"  <kicker />\n";
  } else if (is_cavity()) {
    // actually this is implemented directly in CAVITY::aml_string();
    result<<"  <rf_cavity />\n";
    placet_cout<<ERROR<< " : 'rf_cavity' not implemented, element : "<<name<<endmsg;
  } else if (is_bpm()) {
    result<<"  <instrument type = \"bpm\" />\n";
  } else if (is_drift()) {
    if (geometry.length==0.)
      result<<"  <marker />\n";
  } else {
    result<<"  <custom type = \""<<typeid(*this).name()<<"\" />\n";
    placet_cout<<ERROR<<" : "<<typeid(*this).name()<<" not implemented, element : "<<name<<endmsg;
  }
  double x=get_attribute_double("x");
  double y=get_attribute_double("y");
  double xp=get_attribute_double("xp");
  double yp=get_attribute_double("yp");
  double roll=get_attribute_double("roll");
  double tilt=0.;
  if (is_quad() || is_multipole() || is_sbend()) {
    tilt=get_attribute_double("tilt");
  }
  if (tilt!=0 || x!=0 || y!=0 || xp!=0 || yp!=0 || roll!=0) {
    result<<"  <orientation>\n";
    if (x!=0)
      result<<"    <x_offset design = \""<<x*1e-6<<"\" />\n";
    if (y!=0)
      result<<"    <y_offset design = \""<<y*1e-6<<"\" />\n";
    if (xp!=0)
      result<<"    <x_pitch design = \""<<xp*1e-6<<"\" />\n";
    if (yp!=0)
      result<<"    <y_pitch design = \""<<yp*1e-6<<"\" />\n";
    if (roll!=0)
      result<<"    <roll design = \""<<roll*1e-6<<"\" />\n";
    if (tilt!=0)
      result<<"    <tilt design = \""<<tilt<<"\" />\n";
    result<<"  </orientation>\n";
  }
  if (length!=0)
    result<<"  <length design = \""<<length<<"\" />\n";
  std::string aperture_shape=get_attribute_string("aperture_shape");
  if (aperture_shape!="none") {
    double aperture_x=get_attribute_double("aperture_x");
    double aperture_y=get_attribute_double("aperture_y");
    if ((aperture_x!=0 || aperture_y!=0)&&(aperture_x<1e6 || aperture_y<1e6)) {
      if (aperture_shape!="" || aperture_x!=0 || aperture_y!=0) {
	if (aperture_shape!="") {
	  if (aperture_shape == "oval" || aperture_shape == "elliptic") {
	    aperture_shape = "ELLIPTICAL";
	  } else if (aperture_shape == "circular") {
	    aperture_shape = "CIRCULAR";
	  } else {
	    aperture_shape = "RECTANGULAR";
	  }
	  result<<"  <aperture shape = \""<<aperture_shape<<"\">\n";
	} else 
	  result<<"  <aperture>\n";
	if (aperture_x!=0)
	  result<<"    <x_limit design = \""<<aperture_x<<"\" />\n";
	if (aperture_y!=0)
	  result<<"    <y_limit design = \""<<aperture_y<<"\" />\n";
	result<<"  </aperture>\n";
      }
    }
  }
  result<<"</element>"<<std::endl;
  result.precision(old_precision);
  return result.str();  
}

int tk_BeamlineSaveAML(ClientData /*clientdata*/,Tcl_Interp *interp,int argc, char *argv[])
{
  char *filename=NULL;
  char *beamlinename=NULL;
  int gzip=0;
  Tk_ArgvInfo table[]={
    {(char*)"-beamline",TK_ARGV_STRING,(char*)NULL,(char*)&beamlinename,
     (char*)"Name of the beamline to be saved"},
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&filename,
     (char*)"Name of the output file"},
    {"-gzip", TK_ARGV_CONSTANT, (char *) 1, (char *) &gzip, "Write the file in GZIP compressed format"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }

  BEAMLINE *beamline = get_beamline(beamlinename);
  if (!beamline) beamline = inter_data.beamline;

  FILE *file = NULL;
  if (filename) {
    if (gzip) {
      std::ostringstream command_str;
      command_str << "gzip -c > " << filename << ".gz";
      file = popen(command_str.str().c_str(), "w");
    } else { 
      file = fopen(filename, "w");
    }
  } else {
    file = gzip ? popen("gzip -c", "w") : stdout;
  }

  if (file) {
    fputs("<laboratory aml_version = \"1\" >\n", file);
    std::list<std::string> names;
    {
      std::map<std::string,int> name_repetitions;
      ELEMENT **element=beamline->element;
      for (int i=0;i<beamline->n_elements;i++) {
	std::string name = element[i]->get_name_as_string();
	if (name=="")
	  name="unnamed_element";
	if (int counts = name_repetitions[name]++) {
	  std::ostringstream aml_name;
	  aml_name << name << '.' << counts;
	  name = aml_name.str();
	}
	names.push_back(name);
	std::string aml_string = element[i]->aml_string(name);
	fputs(aml_string.c_str(), file);
      }
    }
    fprintf(file, "<sector name = \"%s\" >\n", beamlinename ? beamlinename : "unnamed_beamline");
    for (std::list<std::string>::iterator itr=names.begin();itr!=names.end();++itr) {
      fprintf(file, "  <element ref = \"%s\" />\n", (*itr).c_str());
    }
    fputs("</sector>\n</laboratory>\n", file);
    if (gzip) 
      pclose(file);
    else if (filename)
      fclose(file);
  } else {
    if (filename)
      Tcl_AppendResult(interp,"Error: cannot open file '", filename, "'\n",NULL);
    return TCL_ERROR;
  }
  return TCL_OK;  
}
