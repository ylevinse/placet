#include <cmath>
#include <typeinfo>

#include <tcl.h>
#include <tk.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_integration.h>

#include "beamline.h"
#include "collimator.h"
#include "structures_def.h"

#include "collimatortable.h"

// formulas based on (G. Rumolo, A. Latina, D. Schulte)
// http://www.eurotev.org/reports__presentations/eurotev_reports/2006/e669/EUROTeV-Report-2006-026.pdf
// Wake lookup table (wake_meth true) by A. M. Toader, R. Barlow
// Some additional info (A. M. Toader, R. Barlow)
// http://accelconf.web.cern.ch/accelconf/e08/papers/wepp166.pdf
// original caluclations (G. V. Stupakov)
// http://slac.stanford.edu/cgi-wrap/getdoc/slac-pub-9375.pdf

using namespace std;

// helper functions
namespace {
  const int WORKSPACE_LIMIT = 50000;
  static double alfat, kt, s0_loc;       /* needed global, they appear in functions */
  
  static gsl_integration_workspace * w1, *w2;
  struct _dummy {
    _dummy() { w1 = gsl_integration_workspace_alloc (WORKSPACE_LIMIT); w2 = gsl_integration_workspace_alloc (WORKSPACE_LIMIT); }
    ~_dummy() { gsl_integration_workspace_free (w1); gsl_integration_workspace_free (w2); }
  } __dummy; 

  static inline double res_lr_dc2(double z, double zz)
  {
    return 1/sqrt(z-zz);
  }
  
  static inline double res_sr_ac2(double z, double zz)
  {
    return -s0_loc/3/(kt*kt+alfat*alfat)*
      (exp(-alfat*(z-zz)/s0_loc)*(kt*sin(kt*(z-zz)/s0_loc)-alfat*cos(kt*(z-zz)/s0_loc))+alfat);
  }
  
  static double fint(double x, void * params)
  {
    double alpha = *(double *) params;
    double xx=x*x;
    return xx*exp(-xx*alpha)/(xx*xx*xx+8);
  }
  
  static double intmed(double zp, void * params)
  {
    double alpha,result,error;
    double alpha1 = *(double *) params;
    
    static gsl_function F;
    
    //  gsl_integration_workspace * w 
    //    = gsl_integration_workspace_alloc (WORKSPACE_LIMIT);  
    
    //alpha1=s0_loc
    
    placet_printf(DEBUG,"\n upper bound = %lg \n",2.*sqrt(alpha1/2./fabs(zp)));
    alpha=fabs(zp)/alpha1;
    F.function = &fint;
    F.params = &alpha;
    gsl_integration_qag (&F, 1e-20, 2.*sqrt(alpha1/2./fabs(zp)), 1e-8, 1e-5, WORKSPACE_LIMIT, 6, w1, &result, &error);
    
    // http://www.gnu.org/software/gsl/manual/html_node/QAG-adaptive-integration.html
    /* Function: int gsl_integration_qag (const gsl_function *f, double a, double b, double epsabs, double epsrel, size_t limit, int key, gsl_integration_workspace * workspace, double * result, double * abserr)
       
       This function applies an integration rule adaptively until an estimate of the integral of f over (a,b) is achieved within the desired absolute and relative error limits, epsabs and epsrel. The function returns the final approximation, result, and an estimate of the absolute error, abserr. The integration rule is determined by the value of key, which should be chosen from the following symbolic names,
       
       GSL_INTEG_GAUSS15  (key = 1)
       GSL_INTEG_GAUSS21  (key = 2)
       GSL_INTEG_GAUSS31  (key = 3)
       GSL_INTEG_GAUSS41  (key = 4)
       GSL_INTEG_GAUSS51  (key = 5)
       GSL_INTEG_GAUSS61  (key = 6)
       
       corresponding to the 15, 21, 31, 41, 51 and 61 point Gauss-Kronrod rules. The higher-order rules give better accuracy for smooth functions, while lower-order rules save time when the function contains local difficulties, such as discontinuities.
       
       On each iteration the adaptive integration strategy bisects the interval with the largest error estimate. The subintervals and their results are stored in the memory provided by workspace. The maximum number of subintervals is given by limit, which may not exceed the allocated size of the workspace. 
    */	
    // source code: http://gsl.sourcearchive.com/documentation/1.6/qag_8c-source.html
	
    // should a test if the integration reached the right error be done?
    return result;

    //  intmed=result;
    //  gsl_integration_workspace_free (w);
    //return intmed;
  }

  static double intmed22(double z, double zz, double blength)
  { 
    //double intmed2;
    double alpha1,result,error;
    static gsl_function F;
    
    //    gsl_integration_workspace * w 
    //      = gsl_integration_workspace_alloc (WORKSPACE_LIMIT);  
  
    //set2[0]=s0_loc
    //set2[1]=zp
    //set2[2]=sz0
  
    //  placet_printf(DEBUG,"\n lower bound = %lg  upper bound = %lg \n", 1.e-5*set2[2],fabs(z));
    alpha1=s0_loc;
    F.function = &intmed; // using the static function intmed
    F.params = &alpha1;
    /* gsl_integration_qag (&F, 1.e-5*blength, blength, 0, 1e-5, WORKSPACE_LIMIT, 5, w2, &result, &error);
       inttemp=result;
       gsl_integration_qag (&F, fabs(z-zz), blength, 0, 1e-5, WORKSPACE_LIMIT, 5, w2, &result, &error);
       inttemp-=result;
    */
  
    gsl_integration_qag (&F, 1.e-5*blength, fabs(z-zz), 1e-8, 1e-5, WORKSPACE_LIMIT, 6, w2, &result, &error);
    // see intmed() for explanation of this function

    return result;

    //  inttemp=result;
    //intmed2=inttemp;
    //    gsl_integration_workspace_free (w);
    //return intmed2;

  }

  static inline void findpoles(double cg)
  {  
    if (cg<0.2) {
      alfat=1.;
      kt=SQRT3;
    }
    else if (cg>=0.2 && cg<0.4) {
      alfat=0.74;
      kt=1.8;
    }
    else if (cg>=0.4 && cg<0.7) {
      alfat=0.52;
      kt=1.87;
    }
    else if (cg>=0.7) {
      alfat=1./4./cg;
      kt=pow(8./cg,0.25);
    }
  }
}

void COLLIMATOR::resistive_intermediate(int slice,double zp,double diffz,double blength,double* Np,double* deltayarray,
					double &kickyrwt_a,double &kickyrwf_a,double &kickyrwt2_a,double &kickyrwf2_a,double &kickyrwt_b,double &kickyrwf_b,double &kickyrwt2_b,double &kickyrwf2_b)const
{
  kickyrwt_a=0.;
  kickyrwf_a=0.;
  kickyrwt2_a=0.;
  kickyrwf2_a=0.;
  kickyrwt_b=0.;
  kickyrwf_b=0.;
  kickyrwt2_b=0.;
  kickyrwf2_b=0.;
  for(int imain=0;imain<=imax;imain++) { 
    double bb_loc=in_height-(in_height-fin_height)/imax*imain;
    double bbbb_loc=bb_loc*bb_loc;
    s0_loc=pow((2*bbbb_loc/Z0/sigma),1./3.);
    double cap_gam_loc=C_LIGHT*tau/s0_loc;
    findpoles(cap_gam_loc);  // why is this done? alfat and kt are not used here? // JS
    double coeffrwt=32*RE*(taper_length/imax)/(bbbb_loc*bbbb_loc);
    double coeffrwt2=32*RE*(taper_length/imax)*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
    /* kmain=0 is the head of the bunch */
    for(int kmain=0;kmain<slice;kmain++) {
      double _res_sr_ac2=res_sr_ac2(blength/2-zp, kmain*diffz);
      kickyrwt_a+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain])*_res_sr_ac2;
      double _intmed22=intmed22(blength/2-zp, kmain*diffz, blength);
      kickyrwt2_a+=Np[kmain]*coeffrwt2*(0.822*deltayarray[kmain])*_intmed22;
      kickyrwt_b+=Np[kmain]*coeffrwt*(0.411)*_res_sr_ac2;
      kickyrwt2_b+=Np[kmain]*coeffrwt2*(0.411)*_intmed22;
    }
  }
  if(flat_length>0.) {
    double bbbb_loc = std::pow(fin_height,4);
    double coeffrwf=32*RE*flat_length/(bbbb_loc*bbbb_loc);
    double coeffrwf2=32*RE*flat_length*M_SQRT2/M_PI/(bbbb_loc*bbbb_loc);
    for(int kmain=0;kmain<slice;kmain++) {
      double _res_sr_ac2=res_sr_ac2(blength/2-zp, kmain*diffz);
      kickyrwf_a+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain])*_res_sr_ac2;
      double _intmed22=intmed22(blength/2-zp, kmain*diffz, blength);
      kickyrwf2_a+=Np[kmain]*coeffrwf2*(0.822*deltayarray[kmain])*_intmed22;
      kickyrwf_b+=Np[kmain]*coeffrwf*(0.411)*_res_sr_ac2;
      kickyrwf2_b+=Np[kmain]*coeffrwf2*(0.411)*_intmed22;
    }   
  }
}

void COLLIMATOR::resistive_longrange(double yp,double zp,double diffz,double blength,double gam,double*Np,double* deltayarray,
				     double &kickyrwt,double &kickyrwf)const 
{
  kickyrwt=0.;
  kickyrwf=0.;
  double ffh=fin_height*fin_height;
  double coeffrwt=2*RE*sqrt(lambda/M_PI)/gam*(in_height+fin_height)*taper_length/ffh/in_height/in_height;
  double coeffrwf=2*RE*sqrt(lambda/M_PI)/gam*flat_length/(fin_height*ffh);
  int ind_slice=int(floor((blength/2-zp)/diffz));
	      
  /* kmain=0 is the head of the bunch */
	      
  for(int kmain=0;kmain<ind_slice;kmain++) {
    kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*res_lr_dc2(blength/2-zp, kmain*diffz);
    if(flat_length>0.)
      kickyrwf+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain]+0.411*yp)*res_lr_dc2(blength/2-zp, kmain*diffz);
  }
}

void COLLIMATOR::resistive_shortrange(double yp,double zp,double diffz,double blength,double gam,double*Np,double* deltayarray,
				     double &kickyrwt,double &kickyrwf)const 
{
  int ind_slice=int(floor((blength/2-zp)/diffz));
  kickyrwt=0.;
  kickyrwf=0.;
  for(int imain=0;imain<imax;imain++) {
    double bb_loc=in_height-(in_height-fin_height)/imax*imain;
    double bbbb_loc=bb_loc*bb_loc;
    s0_loc=pow((2*bbbb_loc/Z0/sigma),1./3.);
    double cap_gam_loc=C_LIGHT*tau/s0_loc;
    findpoles(cap_gam_loc);
    double coeffrwt=32*RE*(taper_length/imax)/gam/(bbbb_loc*bbbb_loc);
    /* kmain=0 is the head of the bunch */
    for(int kmain=0;kmain<ind_slice;kmain++)
      kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*res_sr_ac2(blength/2-zp, kmain*diffz);
  }
  if(flat_length>0.) {
    double bbbb_loc = std::pow(fin_height,4);
    double coeffrwf=32*RE*flat_length/gam/(bbbb_loc*bbbb_loc);
    for(int kmain=0;kmain<ind_slice;kmain++)
      kickyrwf+=Np[kmain]*coeffrwf*(0.822*deltayarray[kmain]+0.411*yp)*res_sr_ac2(blength/2-zp, kmain*diffz);
  }
}

void COLLIMATOR::resistive_table(double yp,double zp,double diffz,double blength,double gam,double*Np,double* deltayarray, double &kickyrwt,double &kickyrwf)const
{
  kickyrwt=0.;
  kickyrwf=0.;
	    
  double E;
  int ind_slice=int(floor((blength/2-zp)/diffz));
  double ffh=fin_height*fin_height;
  double coeffrwt=4*RE*sqrt(lambda/M_PI)/gam*(in_height+fin_height)*taper_length/ffh/in_height/in_height;
  double coeffrwf=RE/gam*flat_length/(ffh*ffh)*s0_fin*sqrt(s0_fin/2/M_PI);
  placet_printf(DEBUG,"\n coefff= %lg \n", coeffrwf);
	  
  for(int kmain=0;kmain<ind_slice;kmain++) {
	      
    double z=blength/2-zp-kmain*diffz;
    double absz= abs(z);
    double s=z/s0_fin;
    double Chao=-2*coeffrwf*sqrt(1/absz);
	    
    if (Ctable->inrange(s)){
      E=Ctable->interpolate(s);
      E=E*s0_fin*RE/gam*flat_length/(ffh*ffh);
    }else{
      E=Chao;
    }
	    
    /* kmain=0 is the head of the bunch */
    kickyrwt+=Np[kmain]*coeffrwt*(0.822*deltayarray[kmain]+0.411*yp)*E;
    if(flat_length>0.)
      kickyrwf+=Np[kmain]*(0.822*deltayarray[kmain]+0.411*yp)*E;
  }
}

double COLLIMATOR::geometric_inductive(double yp,double Np,double deltay,double diffz,double gam)const
{
  // eq. 15 of http://accelconf.web.cern.ch/accelconf/e08/papers/wepp166.pdf
  const double diff_height_squared = (in_height - fin_height)*(in_height - fin_height);
  double kicky=RE/gam*
    ((M_PI*width*diff_height_squared*
      (in_height+fin_height)/taper_length/in_height/in_height/
      fin_height/fin_height-2*diff_height_squared/
      in_height/fin_height/taper_length)*deltay
     +2*diff_height_squared/
     in_height/fin_height/taper_length*yp)*Np/diffz;
  //	    kicky_old=2*RE/gam/(in_height-fin_height)*(in_height-fin_height)/
  //	      in_height/fin_height/taper_length*(0.822*deltay+0.411*yp)*Np/diffz;
  return kicky;
}

double COLLIMATOR::geometric_intermediate(double yp,double Np,double deltay,double diffz,double gam,double sz0)const
{
  // eq.4 of http://www.eurotev.org/reports__presentations/eurotev_reports/2006/e669/EUROTeV-Report-2006-026.pdf 
  // eq.20 of http://accelconf.web.cern.ch/accelconf/e08/papers/wepp166.pdf
  // Yokoya dipole factor = pi^2 / 12 = 0.822
  // Yokoya quadrupole factor = pi^2 / 24 = 0.411

  double kicky=RE*2.7*sqrt(2*angle)/gam*sqrt(2*M_PI*sz0)/sqrt(fin_height*fin_height*fin_height)*(0.822*deltay+0.411*yp)*Np/diffz;
  return kicky;
}

double COLLIMATOR::geometric_diffractive(double yp,double Np,double deltay,double diffz,double gam,double sz0)const
{
  // eq. 3 of http://www.eurotev.org/reports__presentations/eurotev_reports/2006/e669/EUROTeV-Report-2006-026.pdf
  // eq. 19 of http://accelconf.web.cern.ch/accelconf/e08/papers/wepp166.pdf
  double fih=fin_height/in_height;
  fih*=fih;
  double kicky=2*RE*sz0*SQRT_PI*(1-(fih*fih))/fin_height/fin_height/gam
    *(0.822*deltay+0.411*yp)*Np/diffz;
  return kicky;
}

void COLLIMATOR::giovanni_step_4d_0(BEAM *beam )
{
  drift_step_4d_0(beam, 0.5); // track beam as drift the first half of the collimator
  int k=0;
  for (int ibunch=0;ibunch<beam->bunches;ibunch++) {
    const int NSL=beam->slices_per_bunch;
    double deltayarray[NSL]; // average position per slice
    double Np[NSL]; // number of particles per slice
    // direction
    double PARTICLE::* direction;
    if (vertical) direction = &PARTICLE::y;
    else direction = &PARTICLE::x;
    // calculate average offset per slice
    if (beam->particle_beam) {
      for (int i=0;i<beam->slices_per_bunch; i++) {
	double deltay=0.0;
	double wgtsum=0.0;
	for (int j=0;j<beam->particle_number[i]; j++) {
	  PARTICLE &particle=beam->particle[k];
	  double wgt=particle.wgt;
	  deltay += particle.*direction * wgt;
	  wgtsum += wgt;
	  k++;
	}
	if(wgtsum>std::numeric_limits<double>::epsilon()) {
	  deltayarray[i]=deltay/wgtsum*1e-6; 
	} else {deltayarray[i]=0;}
	Np[i]=wgtsum*Nb;
      }
    } else {
      for (int i=0;i<beam->slices_per_bunch; i++) {
	double deltay=0.0;
	double wgtsum=0.0;
	for (int j=0;j<beam->macroparticles; j++) {
	  PARTICLE &particle=beam->particle[ibunch*beam->slices_per_bunch*beam->macroparticles+i*beam->macroparticles+j];
	  double wgt=particle.wgt;
	  deltay += particle.*direction * wgt;
	  wgtsum += wgt;
	}
	if(wgtsum>std::numeric_limits<double>::epsilon()) {
	  deltayarray[i]=deltay/wgtsum*1e-6; 
	} else {deltayarray[i]=0;}
	Np[i]=wgtsum*Nb;
      }
    }
    
    double sz0=bunch_get_length(beam, ibunch)*1e-6;
    double diffz=(beam->z_position[1+ibunch*beam->slices_per_bunch]-beam->z_position[0+ibunch*beam->slices_per_bunch])*1e-6;
    double blength=diffz*beam->slices_per_bunch;

      
    // Regimes clasification: eq. 18  http://inspirehep.net/record/582296/files/slac-pub-9375.pdf 
      
    int regime_geometric=0;
    if (angle<9.6*fin_height*sz0/width/width) {               /* inductive regime */
      regime_geometric=1;
      placet_printf(VERBOSE,"\n geometric regime is inductive\n");
    } else if (angle>0.14*sz0/fin_height) {        /* diffractive regime */
      regime_geometric=2;
      placet_printf(VERBOSE,"\n geometric regime is diffractive\n");
    } else {                                                  /* intermediate regime */
      placet_printf(VERBOSE,"\n geometric regime is intermediate\n");
    }
      
    int regime_resistive=0;                                   /* intermediate regime */
    if (wake_meth) {                                          /* lookup table */
      regime_resistive=3;
    } else if (sz0>10.*s0_in) {                               /* long range regime */
      regime_resistive=1;
      placet_printf(VERBOSE,"\n resistive regime is long-range\n");
    } else if (sz0<0.1*s0_fin) {                              /* short range regime */
      regime_resistive=2;
      placet_printf(VERBOSE,"\n resistive regime is short-range\n");
    } else {                                                  /* intermediate regime */
      placet_printf(VERBOSE,"\n resistive regime is intermediate\n");
    }
      
    findpoles(cap_gam);
    placet_printf(DEBUG,"\n alfat = %lg  kt = %lg \n", alfat, kt);

    // kick factors for resistive wake
    double kickyrwt=0.;
    double kickyrwt2=0.;
    double kickyrwf=0.;
    double kickyrwf2=0.;
    double kickyrwt_a=0.;
    double kickyrwt2_a=0.;
    double kickyrwf_a=0.;
    double kickyrwf2_a=0.;
    double kickyrwt_b=0.;
    double kickyrwt2_b=0.;
    double kickyrwf_b=0.;
    double kickyrwf2_b=0.;

    k=0; // if there is more than 1 bunch??
    
    if (beam->particle_beam) {
      for (int i=0;i<beam->slices_per_bunch;i++) {
	// negative z
	double zp=-beam->z_position[ibunch*beam->slices_per_bunch+i]*1e-6;
	// intermediate resistive regime can be calculated per slice
	if (regime_resistive == 0) {
	  resistive_intermediate(i,zp,diffz,blength,Np,deltayarray,
				 kickyrwt_a,kickyrwf_a,kickyrwt2_a,kickyrwf2_a,kickyrwt_b,kickyrwf_b,kickyrwt2_b,kickyrwf2_b);
	}
	for (int jj=0; jj<beam->particle_number[i];jj++) {
	  PARTICLE &particle=beam->particle[k++];
	  double yp;
	  if (vertical) {
	    yp=particle.y*1e-6; /* coordinates of the particle that feels the kick */
	  } else {
	    yp=particle.x*1e-6; /* coordinates of the particle that feels the kick */
	  }
	  double gam=particle.energy/EMASS;
	  double kicky;
	  
	  switch(regime_geometric) {

	  case 1: // inductive
	    kicky = geometric_inductive(yp,Np[i],deltayarray[i],diffz,gam);
	    break;
	    
	  case 0: // intermediate
	    kicky = geometric_intermediate(yp,Np[i],deltayarray[i],diffz,gam,sz0);
	    break;
	    
	  case 2: // diffractive
	    kicky = geometric_diffractive(yp,Np[i],deltayarray[i],diffz,gam,sz0);
	    break;    
	  }
	  
	  switch(regime_resistive) {

	  case 1:
	    resistive_longrange(yp,zp,diffz,blength,gam,Np,deltayarray,kickyrwt,kickyrwf);
	    break;
	    
	  case 2:
	    resistive_shortrange(yp,zp,diffz,blength,gam,Np,deltayarray,kickyrwt,kickyrwf);
	    break;
	    
	  case 0:
	    kickyrwt=(kickyrwt_a+kickyrwt_b*yp)/gam;
	    kickyrwt2=(kickyrwt2_a+kickyrwt2_b*yp)/gam;
	    if(flat_length>0.) {
	      kickyrwf=(kickyrwf_a+kickyrwf_b*yp)/gam;
	      kickyrwf2=(kickyrwf2_a+kickyrwf2_b*yp)/gam;
	    }
	    break;	 

	  case 3:
	    resistive_table(yp,zp,diffz,blength,gam,Np,deltayarray,kickyrwt,kickyrwf);
	    break;
	  }
	  
	  if (vertical) 
	    particle.yp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  else
	    particle.xp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  
	  placet_cout << DEBUG << "kick, z position " << zp*1e6 << " geometrical kick " << kicky*1e6 << " total resisitive " << (kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6 << " flat part1 " << kickyrwf*1e6 << " flat part2 " << kickyrwf2*1e6 << " tapered part1 " << kickyrwt*1e6 << " tapered part2 " << kickyrwt2*1e6 << endmsg;

	  if (output_file.is_open()) {
	    output_file << zp*1e6 << " " << kicky*1e6 << " " << (kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6 << " " << kickyrwf*1e6 << " " << kickyrwf2*1e6 << " " << kickyrwt*1e6 << " " << kickyrwt2*1e6 << std::endl;
	  }
	} 
      }
    } else { // sliced beam
      for (int slice=0;slice<beam->slices_per_bunch;slice++) {
	// negative z
	double zp=-beam->z_position[ibunch*beam->slices_per_bunch + slice]*1e-6;
	// intermediate resistive regime can be calculated per slice
	if (regime_resistive == 0) {
	  resistive_intermediate(slice,zp,diffz,blength,Np,deltayarray,
				 kickyrwt_a,kickyrwf_a,kickyrwt2_a,kickyrwf2_a,kickyrwt_b,kickyrwf_b,kickyrwt2_b,kickyrwf2_b);
	}

	for (int jj=0;jj<beam->macroparticles;jj++) {
	  PARTICLE &particle=beam->particle[(ibunch*beam->slices_per_bunch+slice)*beam->macroparticles+jj];
	  double yp;
	  if (vertical) {
	    yp=particle.y*1e-6; /* coordinates of the particle that feels the kick */
	  } else {
	    yp=particle.x*1e-6; /* coordinates of the particle that feels the kick */
	  }
	  double gam=particle.energy/EMASS;
	  double kicky;
	  
	  switch(regime_geometric) {
	  case 1:
	    kicky = geometric_inductive(yp,Np[slice],deltayarray[slice],diffz,gam);
	    break;
	    
	  case 0:
	    kicky = geometric_intermediate(yp,Np[slice],deltayarray[slice],diffz,gam,sz0);
	    break;
	    
	  case 2: 
	    kicky = geometric_diffractive(yp,Np[slice],deltayarray[slice],diffz,gam,sz0);
            break;    
	  }
	  
	  switch(regime_resistive) {
	  case 1:	  
	    resistive_longrange(yp,zp,diffz,blength,gam,Np,deltayarray,kickyrwt,kickyrwf);
	    break;
	    
	  case 2:
	    resistive_shortrange(yp,zp,diffz,blength,gam,Np,deltayarray,kickyrwt,kickyrwf);
	    break;
	    
	  case 0:
	    kickyrwt=(kickyrwt_a+kickyrwt_b*yp)/gam;
	    kickyrwt2=(kickyrwt2_a+kickyrwt2_b*yp)/gam;
	    if(flat_length>0.) {
	      kickyrwf=(kickyrwf_a+kickyrwf_b*yp)/gam;
	      kickyrwf2=(kickyrwf2_a+kickyrwf2_b*yp)/gam;
	    }
	    break;
	  case 3:
	    resistive_table(yp,zp,diffz,blength,gam,Np,deltayarray,kickyrwt,kickyrwf);
	    break;
	  }
	  
	  if (vertical) 
	    particle.yp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  else
	    particle.xp += (kicky+kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6;
	  
	  placet_cout << DEBUG << "kick, z position " << zp*1e6 << " geometrical kick " << kicky*1e6 << " total resistive " << (kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6 << " flat part1 " << kickyrwf*1e6 << " flat part2 " << kickyrwf2*1e6 << " tapered part1 " << kickyrwt*1e6 << " tapered part2 " << kickyrwt2*1e6 << endmsg;
	  if (output_file.is_open()) {
	    output_file << zp*1e6 << " " << kicky*1e6 << " " << (kickyrwt+kickyrwf+kickyrwt2+kickyrwf2)*1e6 << " " << kickyrwf*1e6 << " " << kickyrwf2*1e6 << " " << kickyrwt*1e6 << " " << kickyrwt2*1e6 << std::endl;
	  }
	}
      }
    }
  }
  drift_step_4d_0(beam, 0.5); // track last half of collimator as drift
}

void COLLIMATOR::giovanni_step(BEAM *beam )
{
  giovanni_step_4d_0(beam);
}

int tk_CollimatorNumberList(ClientData /*clientdata*/, Tcl_Interp *interp, int argc, char **argv )
{
  Tk_ArgvInfo table[]={
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };

  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK) {
    return error;
  }
  
  ELEMENT **element=inter_data.beamline->element;
  for (int i=0;i<inter_data.beamline->n_elements;i++){
    if (typeid(*element[i])==typeid(COLLIMATOR)) {
      char buffer[100];
      snprintf(buffer,100,"%d",i);
      Tcl_AppendElement(interp,buffer);
    }
  }
  return TCL_OK;
}
