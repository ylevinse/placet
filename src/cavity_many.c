// included by cavity.cc

#include "quadwake.h"

/*
  The routine steps through a cavity using first order transport
*/

void CAVITY::step_many_0_x(BEAM *beam )
{
  double factor_transv=beam->transv_factor*geometry.length*beam->factor;
  double half_length=0.5*geometry.length;
  double length_i=1.0/geometry.length;
/*changed to allow different structures ***/
  double *s_beam=beam->s_long[field];
  double *c_beam=beam->c_long[field];
  double s_long,c_long;
  sincos(phase,&s_long,&c_long);
  double *rho_y=beam->rho_y[0];
  double *rho_x=beam->rho_x[0];
  double *p=beam->field[0].kick;
#ifdef HTGEN
  int imacro3=0; //haloparticles
#endif
  for (int i=0,i_s=0;i_s<beam->slices_per_bunch;i_s++) {
    double sumx=0.0;      
    double sumy=0.0;
    double pos_x=0.0;
    double pos_y=0.0;
    // kick caused by all previous slices
    for (int j=0;j<i_s;j++) {
      sumy+=rho_y[j]* p[0];
      sumx+=rho_x[j]* p[0];
      p++;
    }
    double de0=(c_beam[i_s]*c_long+s_beam[i_s]*s_long)*gradient+beam->factor*beam->field[0].de[i_s];
    // apply kick to particles in this slice
    // beam particles
    for (int i_m=0;i_m<beam->particle_number[i_s];i_m++) {
      PARTICLE &particle=beam->particle[i];
#ifdef RFCAV
      wgt+=particle.wgt;
      y1+=particle.y*particle.wgt;
      x1+=particle.x*particle.wgt;
#endif
      double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
      double gammap=delta*length_i;
      particle.yp-=gammap*particle.y;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      gammap=delta*length_i;
#ifdef END_FIELDS
      particle.yp-=gammap*particle.y;
#endif
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
      particle.yp*=tmp;
      
#ifdef END_FIELDS
      particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
      particle.xp*=tmp;
      particle.energy+=half_length*de0;
      
      pos_x+=particle.x*particle.wgt;
      pos_y+=particle.y*particle.wgt;
      particle.xp+=sumx*factor_transv/particle.energy;
      particle.yp+=sumy*factor_transv/particle.energy;
#ifdef CAV_PRECISE
      delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
      gammap=delta/geometry.length;
#endif
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      tmp=1.0/(1.0+delta);
#else
      //    gammap=0.5*de0/(particle.energy*geometry.length);
      gammap=half_length*de0/(particle.energy*length);
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
      particle.yp+=gammap*particle.y;
#endif
      particle.yp*=tmp;
      
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
      particle.xp+=gammap*particle.x;
#endif
      particle.xp*=tmp;
      
      particle.energy+=half_length*de0;
      
#ifdef EARTH_FIELD
      particle.xp+=earth_field.x*geometry.length/particle.energy;
      particle.yp+=earth_field.y*geometry.length/particle.energy;
#endif
      
#ifdef RFCAV
      y2+=particle.y*particle.wgt;
      x2+=particle.x*particle.wgt;
#endif
      i++;
    }
#ifdef HTGEN
    // haloparticles
    for (int i_h=0;i_h<beam->particle_number_sec[i_s];i_h++) {
      PARTICLE &particle=beam->particle_sec[imacro3];
      double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
      double gammap=delta*length_i;
      particle.yp-=gammap*particle.y;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      gammap=delta*length_i;
#ifdef END_FIELDS
      particle.yp-=gammap*particle.y;
#endif
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
      particle.yp*=tmp;
      
#ifdef END_FIELDS
      particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
      particle.xp*=tmp;
      particle.energy+=half_length*de0;
      particle.xp+=sumx*factor_transv/particle.energy;
      particle.yp+=sumy*factor_transv/particle.energy;
#ifdef CAV_PRECISE
      delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
      gammap=delta/geometry.length;
#endif
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      tmp=1.0/(1.0+delta);
#else
      gammap=half_length*de0/(particle.energy*length);
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
      particle.yp+=gammap*particle.y;
#endif
      particle.yp*=tmp;
      
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
      particle.xp+=gammap*particle.x;
#endif
      particle.xp*=tmp;
      
      particle.energy+=half_length*de0;
      
#ifdef EARTH_FIELD
      particle.xp+=earth_field.x*geometry.length/particle.energy;
      particle.yp+=earth_field.y*geometry.length/particle.energy;
#endif
      
      imacro3++;
    }
#endif
    rho_y[i_s]=pos_y;
    rho_x[i_s]=pos_x;
  }
#ifdef RFCAV
  y1/=wgt;
  y2/=wgt;
  x1/=wgt;
  x2/=wgt;
#endif
}

#define CAVITY_2

void CAVITY::step_many_x(BEAM *beam)
{
  double factor_transv=beam->transv_factor*geometry.length*beam->factor;
  double half_length=0.5*geometry.length;
  double length_i=1.0/geometry.length;
  /*changed to allow different structures */
  double *s_beam=beam->s_long[field];
  double *c_beam=beam->c_long[field];
  double s_long,c_long;
  sincos(phase,&s_long,&c_long);
  double *rho_y=beam->rho_y[0];
  double *rho_x=beam->rho_x[0];
  double *p=beam->field[0].kick;
  /*
    Move beam to the centre of the structure
  */
#ifdef HTGEN
  int imacro3=0; // haloparticles
#endif
  for (int i=0,i_s=0;i_s<beam->slices_per_bunch;i_s++) {
    double sumx=0.0;
    double sumy=0.0;
    double pos_x=0.0;
    double pos_y=0.0;
    for (int j=0;j<=i_s;j++) {
      sumy+=rho_y[j]* p[0];
      sumx+=rho_x[j]* p[0];
      p++;
    }
    for (int i_m=0;i_m<beam->particle_number[i_s];i_m++) {
      PARTICLE &particle=beam->particle[i];
#ifdef RFCAV
      wgt+=particle.wgt;
      y1+=particle.y*particle.wgt;
      x1+=particle.x*particle.wgt;
#endif
      double de0=(c_beam[i_s]*c_long
		  +s_beam[i_s]*s_long)*gradient
	+beam->factor*beam->field[0].de[i_s];
      double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
      double gammap=delta*length_i;
      particle.yp-=gammap*particle.y;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      double gammap=delta*length_i;
#ifdef END_FIELDS
      particle.yp-=gammap*particle.y;
#endif
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
      particle.yp*=tmp;
      
#ifdef END_FIELDS
      particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
      particle.xp*=tmp;
      particle.energy+=half_length*de0;
      
#ifdef CAVITY_2
      sigma_step_x(beam->sigma+i,beam->sigma_xx+i,
		   beam->sigma_xy+i,2.0*delta);
#endif
      
      pos_y+=particle.y*particle.wgt;
      particle.yp+=
	sumy*factor_transv/particle.energy;
      pos_x+=particle.x*particle.wgt;
      particle.xp+=
	sumx*factor_transv/particle.energy;
#ifdef CAV_PRECISE
      delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
      gammap=delta/geometry.length;
#endif
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      tmp=1.0/(1.0+delta);
#else
      //    gammap=0.5*de0/(particle.energy*geometry.length);
      gammap=half_length*de0/(particle.energy*length);
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
      particle.yp+=gammap*particle.y;
#endif
      particle.yp*=tmp;
      
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
      particle.xp+=gammap*particle.x;
#endif
      particle.xp*=tmp;
      
      particle.energy+=half_length*de0;
      
#ifdef EARTH_FIELD
      particle.xp+=earth_field.x*geometry.length/particle.energy;
      particle.yp+=earth_field.y*geometry.length/particle.energy;
#endif
      
#ifdef RFCAV
      y2+=particle.y*particle.wgt;
      x2+=particle.x*particle.wgt;
#endif
      i++;
    }
    rho_y[i_s]=pos_y;
    rho_x[i_s]=pos_x;
#ifdef HTGEN
    // haloparticles
    for (int i_h=0;i_h<beam->particle_number_sec[i_s];i_h++) {
      PARTICLE &particle=beam->particle_sec[imacro3];
      double de0=(c_beam[i_s]*c_long+s_beam[i_s]*s_long)*gradient+beam->factor*beam->field[0].de[i_s];
      double delta=half_length*de0/particle.energy;
#ifdef CAV_PRECISE
#ifdef END_FIELDS
      double gammap=delta*length_i;
      particle.yp-=gammap*particle.y;
#endif
      double lndelta;
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      double tmp=1.0/(1.0+delta);
#else
      double gammap=delta*length_i;
#ifdef END_FIELDS
      particle.yp-=gammap*particle.y;
#endif
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
      particle.yp*=tmp;
      
#ifdef END_FIELDS
      particle.xp-=gammap*particle.x;
#endif
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
      particle.xp*=tmp;
      particle.energy+=half_length*de0;
      
      particle.yp+=sumy*factor_transv/particle.energy;
      particle.xp+=sumx*factor_transv/particle.energy;
#ifdef CAV_PRECISE
      delta=half_length*de0/particle.energy;
#ifdef END_FIELDS
      gammap=delta/geometry.length;
#endif
      if (delta>0.01) {
	lndelta=log1p(delta)/delta;
      } else {
	lndelta=1.0-delta*(0.5+delta*0.3308345316809);
      }
      tmp=1.0/(1.0+delta);
#else
      gammap=half_length*de0/(particle.energy*length);
      tmp=(1.0-gammap*length);
#endif
      
#ifdef CAV_PRECISE
      particle.y+=half_length*particle.yp*lndelta;
#else
      particle.y+=half_length*particle.yp;
#endif
#ifdef END_FIELDS
      particle.yp+=gammap*particle.y;
#endif
      particle.yp*=tmp;
      
#ifdef CAV_PRECISE
      particle.x+=half_length*particle.xp*lndelta;
#else
      particle.x+=half_length*particle.xp;
#endif
#ifdef END_FIELDS
      particle.xp+=gammap*particle.x;
#endif
      particle.xp*=tmp;
      
      particle.energy+=half_length*de0;
      
#ifdef EARTH_FIELD
      particle.xp+=earth_field.x*geometry.length/particle.energy;
      particle.yp+=earth_field.y*geometry.length/particle.energy;
#endif   
      imacro3++;
    }
#endif
  }
#ifdef RFCAV
  y1/=wgt;
  y2/=wgt;
  x1/=wgt;
  x2/=wgt;
#endif
}

#undef CAVITY_2
