#include "structures_def.h"
#include "drift.h"

DRIFT::DRIFT() {}

void DRIFT::step_out(BEAM *beam )
{
  ELEMENT::step_out(beam);
  //placet_printf(INFO,"EA: DRIFT::step_out updating csr distance_from_sbend\n");
  beam->csrwake->distance_from_sbend += geometry.length;
}
