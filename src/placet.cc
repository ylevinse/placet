#include <cstdlib>
#include <cstring>
#include <errno.h>
#include <signal.h>

#include <tk.h>
#include "placet.h"
#include "placet_tk.h"

#include "malloc.h"

namespace {
  int do_wish;
}

static void placet_termination_handler(int signum )
{
  exit(0);
}

int Tcl_AppInit(Tcl_Interp *interp)
{
  /* initialisation of Tcl part */

  if (Tcl_Init(interp)){
    placet_printf(INFO,"%s\n",Tcl_GetStringResult(interp));
  }

  if (do_wish){
    if (Tk_Init(interp)){
      placet_printf(INFO,"%s\n",Tcl_GetStringResult(interp));
    }
  }

  Placet_Init(interp);
  return TCL_OK;
}
/**
 * Reduce argc/argv by one.
 */
int reduce_argc(int &argc, char *argv[])
{
    for (int i=1;i<argc-1;i++){
      argv[i]=argv[i+1];
    }
    argc-=1;
    return 0;
}

int main(int argc,char *argv[])
{
  // set up a termination action
  struct sigaction new_action;
  new_action.sa_handler = placet_termination_handler;
  sigemptyset (&new_action.sa_mask);
  new_action.sa_flags = 0;
  sigaction (SIGINT, &new_action, NULL);
  // include end message at exit of placet
  atexit(PLACET_COUT::end_message);
  // here we go
  do_wish=0;
  set_verbosity(INFO);
  print_data.exit_on_error=false;
  xmalloc_init();
  // checking input arguments...
  bool match = true;
  while (argc>1 && match){
    if (!strcmp(argv[1],"-w")){do_wish=1;}
    else if(!strcmp(argv[1],"-s"))   set_verbosity(ERROR);
    else if(!strcmp(argv[1],"-v"))   set_verbosity(VERBOSE);
    else if(!strcmp(argv[1],"-vv"))  set_verbosity(VERYVERBOSE);
    else if(!strcmp(argv[1],"-dbg")) set_verbosity(DEBUG);
    else if(!strcmp(argv[1],"-error")) print_data.exit_on_error=true;
    else if(!strcmp(argv[1],"-h"))     return print_help(argv[0]);
    else if(!strcmp(argv[1],"--help")) return print_help(argv[0]);    
    else {match=false;}
    if (match) {reduce_argc(argc,argv);}
  }

  print_copy();
  if (do_wish) Tk_Main(argc,argv,&Tcl_AppInit);
  Tcl_Main(argc,argv,&Tcl_AppInit);
  exit(0);
}
