#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

#include <tcl.h>
#include <tk.h>

#define PLACET
#include "placet.h"
#include "structures_def.h"

/* Function prototypes */

#include "beam.h"
#include "particle.h"
#include "placeti3.h"
#include "placet_tk.h"

#ifdef HTGEN
namespace BDSIM {
  static double *particle_t=NULL;
}
#endif

BEAM*
beam_make_dispersion(BEAM *beam0,double e[],int n,int i_ref)
{
  BEAM *beam;
  int i;
  double zpos;
  beam=bunch_make(1,n,1,beam0->n_field,0);
  beam->drive_data=beam0->drive_data;
  zpos=beam0->z_position[i_ref];
  for(i=0;i<n;i++){
    beam->particle[i]=beam0->particle[i_ref];
    bunch_set_slice_energy(beam,0,i,0,beam0->particle[i_ref].energy*e[i]);
    bunch_set_slice_wgt(beam,0,i,0,1.0/(double)n);
    beam->z_position[i]=zpos;
    beam->sigma=beam0->sigma;
#ifdef TWODIM
    beam->sigma_xx=beam0->sigma_xx;
    beam->sigma_xy=beam0->sigma_xy;
#endif
    beam->field->de[i]=beam0->field->de[i_ref];
    placet_printf(INFO,"scd %g\n",beam->particle[i].y);
  }
  for(i=0;i<n*(n+1)/2;i++){
    field_set_kick(beam->field,i,0.0);
  }
  beam->factor=beam0->factor;
  beam->which_field=1;
  return beam;
}

OStream &operator<<(OStream &stream, const BEAM &beam )
{
  stream << std::string("placet::beam") << size_t(beam.slices);
  if (beam.particle_beam==true) {
    stream.write((const char*)beam.particle, beam.slices * sizeof *beam.particle);
    // for (int i=0;i<beam.slices;i++) {
    //   stream << beam.particle[i].x
    // 	     << beam.particle[i].xp
    // 	     << beam.particle[i].y
    // 	     << beam.particle[i].yp
    // 	     << beam.particle[i].z
    // 	     << beam.particle[i].energy
    // 	     << beam.particle[i].wgt;
    // }
  } else {
    int i=0;
    for (int l=0;l<beam.bunches;l++) {
      for (int k=0;k<beam.slices_per_bunch;k++) {
	for (int j=0;j<beam.particle_number[k+l*beam.slices_per_bunch];j++) {
	  stream << beam.particle[i].x
		 << beam.particle[i].xp
		 << beam.particle[i].y
		 << beam.particle[i].yp
		 << beam.z_position[k+l*beam.slices_per_bunch]
		 << beam.particle[i].energy
		 << beam.particle[i].wgt;
	  i++;
	}
      }
    }
  }
  
  return stream;
}

IStream &operator>>(IStream &stream, BEAM &beam )
{
  double wgt0 = 0.0;
  for (int i=0; i<beam.slices; i++)
    wgt0 += beam.particle[i].wgt;
  std::string beam_name;
  size_t beam_size;
  stream >> beam_name;
  stream >> beam_size;
  stream.read((char*)beam.particle, beam.slices * sizeof *beam.particle);
  std::sort(beam.particle, beam.particle+beam.slices);
  /* check whether this is necessary */
  double z_max = 0.5*(beam.z_position[1]+beam.z_position[0]);
  for (int i=0; i<beam.slices_per_bunch*beam.bunches; i++)
    beam.particle_number[i] = 0;
  for (int i=0, j=0; i<beam.slices; i++) {
    const PARTICLE &particle = beam.particle[i];
    while (particle.z>z_max) {
      j++;
      if (j==beam.slices_per_bunch*beam.bunches) {
	placet_printf(WARNING,"too large z %g\n",particle.z);
	j=beam.slices_per_bunch*beam.bunches-1;
	break;
      }
      if (j<beam.slices_per_bunch*beam.bunches-1) {
	z_max=0.5*(beam.z_position[j+1]+beam.z_position[j]);
      } else {
	z_max=beam.z_position[j]+0.5*(beam.z_position[j]
				      -beam.z_position[j-1]);
      }
    }
    beam.particle[i].wgt = wgt0/beam.slices;
    beam.particle_number[j]++;
  }
  beam.particle_beam=true;
  return stream;
}

int BEAM::get_slice_number_generic(int index)const {
  int i;
  if (particle_beam) {
    int n=0;
    for (i=0; i<slices_per_bunch*bunches; i++) {
      n+=particle_number[i];
      if (index<=n) break;
    }
  } else {
    i = get_slice_number(index);
  }
  return i;
}

#ifdef HTGEN
bool BEAM::read_halo(const char *filename )
{
  nhalo=-1;
  FILE *file = read_file(filename);
  char buffer[1024];
  if (fgets(buffer,sizeof(buffer)/sizeof(char),file)) {
    if (const char *str1=strstr(buffer,"#")) {
      if (const char *str2=strstr(str1+strlen("#"),"nparticles")) {
	if (const char *str3=strstr(str2+strlen("nparticles"),"=")) {
	  nhalo=atol(str3+strlen("="));
	}
      }
    }
    if (nhalo!=-1) {
      particle_sec=(PARTICLE*)realloc(particle_sec,sizeof(PARTICLE)*nhalo);
      BDSIM::particle_t=(double*)realloc(BDSIM::particle_t,sizeof(double)*nhalo);
      if (nhalo==0||(particle_sec&&BDSIM::particle_t)) {
	for (int i=0;i<nhalo;i++) {
	  if (char *ptr=fgets(buffer,sizeof(buffer)/sizeof(char),file)) {
	    PARTICLE &particle1=particle_sec[i];
	    particle1.energy=strtod(ptr,&ptr);
	    particle1.x=strtod(ptr,&ptr);
	    particle1.y=strtod(ptr,&ptr);
	    particle1.z=strtod(ptr,&ptr);
	    particle1.xp=strtod(ptr,&ptr);
	    particle1.yp=strtod(ptr,&ptr);
	    BDSIM::particle_t[i]=strtod(ptr,&ptr);
	  } else {
	    placet_cout << ERROR << "error while reading the halo : file is too short" << endmsg;
	    nhalo=-1;
	    break;
	  }
	}
      } else {
	placet_cout << ERROR << "error while reading the halo : not enough memory" << endmsg;
	nhalo=-1;
      }
    } else {
      placet_cout << ERROR << "error while reading the halo : unknown number of particles in file " << filename << endmsg;
    }
  } else {
    placet_cout << ERROR << "error while reading the halo : file is empty" << endmsg;
  }
  close_file(file);

  if (nhalo>=0) {
    //  qsort(particle_sec,nhalo,sizeof(PARTICLE),cmp_func_t(&particle_cmp_p));
    return true;
  }
  particle_sec=(PARTICLE*)realloc(particle_sec,0);
  BDSIM::particle_t=(double*)realloc(BDSIM::particle_t,0);
  nhalo=0;
  return false;
}

void BEAM::write_halo(const char *filename )
{
  FILE *file = open_file(filename);
  fprintf(file,"# nparticles = %d\n",nhalo);
  for (int i=0;i<nhalo;i++) {
    PARTICLE &particle0=particle_sec[i];
    fprintf(file," %.15g %.15g %.15g %.15g %.15g %.15g %.15g\n",
	    particle0.energy,
	    particle0.x,
	    particle0.y,
	    particle0.z,
	    particle0.xp,
	    particle0.yp,
	    BDSIM::particle_t[i]);
  }
  close_file(file);
}

int tk_BeamReadHalo(ClientData /*clientdata*/,Tcl_Interp *interp,int argc, char *argv[])
{
  char *fname=NULL,*bname=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"Name of the file from where to read the particles"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&bname,
     (char*)"Name of the beam into which to read the particles"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_AppendResult(interp,"Usage: ",argv[0],"options\n",NULL);
    return TCL_ERROR;
  }
  BEAM *beam;
  if (bname) {
    beam=get_beam(bname);
  } else {
    beam=inter_data.bunch;
  }
  if (fname) {
    if (beam->read_halo(fname)) return TCL_OK;
  }
  return TCL_ERROR;
}
    
int tk_BeamWriteHalo(ClientData /*clientdata*/,Tcl_Interp *interp,int argc, char *argv[])
{
  char *fname=NULL,*bname=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-file",TK_ARGV_STRING,(char*)NULL,(char*)&fname,
     (char*)"Name of the file where to write the particles to"},
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&bname,
     (char*)"Name of the beam from which to read the particles"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_AppendResult(interp,"Usage: ",argv[0],"options\n",NULL);
    return TCL_ERROR;
  }
  BEAM *beam;
  if (bname) {
    beam=get_beam(bname);
  } else {
    beam=inter_data.bunch;
  }
  if (fname) {
    beam->write_halo(fname);
  }
  return TCL_ERROR;
}

int tk_BeamClearHalo(ClientData /*clientdata*/,Tcl_Interp *interp,int argc, char *argv[])
{
  char *bname=NULL;
  Tk_ArgvInfo table[]={
    {(char*)"-beam",TK_ARGV_STRING,(char*)NULL,(char*)&bname,
     (char*)"Name of the beam to be modified"},
    {(char*)NULL,TK_ARGV_END,(char*)NULL,(char*)NULL,(char*)NULL}
  };
  int error;
  if ((error=Tk_ParseArgv(interp,NULL,&argc,argv,table,0))!=TCL_OK){
    return error;
  }
  if (argc!=1) {
    Tcl_AppendResult(interp,"Usage: ",argv[0],"options\n",NULL);
    return TCL_ERROR;
  }
  BEAM *beam;
  if (bname) {
    beam=get_beam(bname);
  } else {
    beam=inter_data.bunch;
  }
  beam->particle_sec=(PARTICLE*)realloc(beam->particle_sec,0);
  beam->nhalo=0;
  BDSIM::particle_t=NULL;
  return TCL_OK;
}
#endif // HTGEN
