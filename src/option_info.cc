#include "option_info.hh"
#include <cfloat>

option_info::AccessMap option_info::option_access_map;

option_info::option_info(const char *_name, option_type _type, void *_dest, double_unary_function func, double_unary_function ifunc ):var_ptr(_dest),access(NULL)
{
  name = _name;
  if (_type==OPT_BOOL)  has_arg = 1; //2 for optional argument
  else                  has_arg = 1;
  flag = NULL;
  val = 0;
  if (name==NULL) return;

  // build option_access:
  option_access dummy;
  dummy.name = _name;
  dummy.description = NULL;
  dummy.type = _type;
  dummy.variable_ptr = true;
  if (func || ifunc) {
    dummy.var_access = new option_access::variable_access();
    dummy.var_access->set = func;
    dummy.var_access->get = ifunc ? ifunc : func;
  } else {dummy.var_access = NULL;}
  // add to map
  std::pair<AccessMapIt,bool> inserter= add(dummy);
  access = &((*inserter.first).first);
  if (inserter.second==false) delete dummy.var_access;
}

option_info::option_info(const char *_name, option_type _type, option_helper *_object, Set _set, Get _get ):obj_ptr(_object),access(NULL)
{
  name = _name;
  if (_type==OPT_BOOL)  has_arg = 1; //2 for optional argument
  else                  has_arg = 1;
  flag = NULL;
  val = 0;
  if (name==NULL) return;

  // build option_access:
  option_access dummy;
  dummy.name = _name;
  dummy.description = NULL;
  dummy.type = _type;
  dummy.variable_ptr = false;
  if (_set || _get) {
    dummy.obj_access = new option_access::object_access();
    dummy.obj_access->set = _set;
    dummy.obj_access->get = _get;
  } else {dummy.obj_access = NULL;}
  // add to map
  std::pair<AccessMapIt,bool> inserter= add(dummy);
  access = &((*inserter.first).first);
  if (inserter.second==false) delete dummy.obj_access;
}

option_info::option_info(const char *_name, const char *_description, option_type _type, void *_dest, double_unary_function func, double_unary_function ifunc ):var_ptr(_dest),access(NULL)
{
  name = _name;
  if (_type==OPT_BOOL)  has_arg = 1; //2 for optional argument
  else                  has_arg = _dest!=NULL ? 1 : 0;
  flag = NULL;
  val = 0;
  if (name==NULL) return;

  // build option_access:
  option_access dummy;
  dummy.name = _name;
  dummy.description = _description;
  dummy.type = _type;
  dummy.variable_ptr = true;
  if (func || ifunc) {
    dummy.var_access = new option_access::variable_access();
    dummy.var_access->set = func;
    dummy.var_access->get = ifunc ? ifunc : func;
  } else {dummy.var_access = NULL;}
  // add to map
  std::pair<AccessMapIt,bool> inserter= add(dummy);
  access = &((*inserter.first).first);
  if (inserter.second==false) delete dummy.var_access;
}

option_info::option_info(const char *_name, const char *_description, option_type _type, option_helper *_object, Set _set, Get _get ):obj_ptr(_object),access(NULL)
{
  name = _name;
  if (_type==OPT_BOOL)  has_arg = 1; //2 for optional argument
  else                  has_arg = 1;
  flag = NULL;
  val = 0;
  if (name==NULL) return;

  // build option_access:
  option_access dummy;
  dummy.name = _name;
  dummy.description = _description;
  dummy.type = _type;
  dummy.variable_ptr = false;
  if (_set || _get) {
    dummy.obj_access = new option_access::object_access();
    dummy.obj_access->set = _set;
    dummy.obj_access->get = _get;
  } else {dummy.obj_access = NULL;}
  // add to map
  std::pair<AccessMapIt,bool> inserter = add(dummy);
  access = &((*inserter.first).first);
  if (inserter.second==false) delete dummy.obj_access;
}

option_info::option_info(const option_info& other){
  name = other.name;
  has_arg = other.has_arg;
  flag = other.flag;
  val = other.val;
  if (other.access->variable_ptr) {
    var_ptr = other.var_ptr;
  } else {
    obj_ptr = other.obj_ptr;
  }
  access = other.access;
  if (access) {add(*access);}
}

option_info& option_info::operator=(const option_info& other){
  if (this != &other) { // gracefully handle self assignment
    // add new access from map
    if (other.access) {add(*other.access);}
    // remove old access from map
    if (access) {remove(*access);}

    name = other.name;
    has_arg = other.has_arg;
    flag = other.flag;
    val = other.val;
    if (other.access) {
      if (other.access->variable_ptr) {
	var_ptr = other.var_ptr;
      } else {
	obj_ptr = other.obj_ptr;
      }
    }
    access = other.access;
  }
  return *this;
}

std::pair<option_info::AccessMapIt,bool> option_info::add(const option_access& dummy)const {
  // try to insert dummy into map with counter 1
  std::pair<AccessMapIt,bool> inserter = option_access_map.insert(std::make_pair(dummy,1));
  // take access from map
  if (inserter.second==false) {
    // option_info was already defined
    (*(inserter.first)).second++; // increase counter
  }
  return inserter;
}

void option_info::remove(const option_access& dummy)const {
  AccessMapIt it = option_access_map.find(dummy);
  if (it != option_access_map.end()) {
    int &count = (*it).second;
    if (count > 1) {
      count--;
    } else {
      option_access_map.erase(it);
    }
  }
}

int option_info::get_int() const
{
  const option_access* acc = access;
  if (acc->variable_ptr) {
    return *reinterpret_cast<int*>(var_ptr);
  } else if (obj_ptr && acc->obj_access && acc->obj_access->get) {
    Get_int get = Get_int(acc->obj_access->get);
    return (obj_ptr->*get)();
  }
  return 0;
}

size_t option_info::get_uint() const
{
  const option_access* acc = access;
  if (acc->variable_ptr) {
    return *reinterpret_cast<size_t*>(var_ptr);
  } else if (obj_ptr && acc->obj_access && acc->obj_access->get) {
    Get_uint get = Get_uint(acc->obj_access->get);
    return (obj_ptr->*get)();
  }
  return 0;
}

bool option_info::get_bool() const
{
  const option_access* acc = access;
  if (acc->variable_ptr) {
    return *reinterpret_cast<bool*>(var_ptr);
  } else if (obj_ptr && acc->obj_access && acc->obj_access->get) {
    Get_bool get = Get_bool(acc->obj_access->get);
    return (obj_ptr->*get)();
  }
  return false;
}

double option_info::get_double() const
{
  const option_access* acc = access;
  if (acc->variable_ptr) {
    if (acc->var_access && acc->var_access->get) {
      return acc->var_access->get(*reinterpret_cast<double*>(var_ptr));
    } else {
      return *reinterpret_cast<double*>(var_ptr);
    }
  } else if (obj_ptr && acc->obj_access && acc->obj_access->get) {
    Get_double get = Get_double(acc->obj_access->get);
    return (obj_ptr->*get)();
  }
  return 0.0;
}

MatrixNd option_info::get_matrix() const
{
  const option_access* acc = access;
  if (acc->variable_ptr) {
    return *reinterpret_cast<MatrixNd*>(var_ptr);
  } else if (obj_ptr && acc->obj_access && acc->obj_access->get) {
    Get_matrix get = Get_matrix(acc->obj_access->get);
    return (obj_ptr->*get)();
  }
  return MatrixNd();
}

std::string option_info::get_string() const
{
  const option_access* acc = access;
  if (acc->variable_ptr) {
    return *reinterpret_cast<std::string*>(var_ptr);
  } else if (obj_ptr && acc->obj_access && acc->obj_access->get) {
    Get_string get = Get_string(acc->obj_access->get);
    return (obj_ptr->*get)();
  }
  return "";
}

const char *option_info::get_char_ptr() const
{
  const option_access* acc = access;
  if (acc->variable_ptr) {
    return reinterpret_cast<const char *>(var_ptr);
  } else if (obj_ptr && acc->obj_access && acc->obj_access->get) {
    Get_char_ptr get = Get_char_ptr(acc->obj_access->get);
    return (obj_ptr->*get)();
  }
  return "";
}

std::complex<double> option_info::get_complex() const
{
  const option_access* acc = access;
  if (acc->variable_ptr) {
    return *reinterpret_cast<std::complex<double>*>(var_ptr);
  } else if (obj_ptr && acc->obj_access && acc->obj_access->get) {
    Get_complex get = Get_complex(acc->obj_access->get);
    return (obj_ptr->*get)();
  }
  return std::complex<double>(0.,0.);
}

void option_info::set(int value )
{
  if (description()!=NULL&&strstr(description(), "READ-ONLY")) return;
  const option_access* acc = access;
  if (acc->variable_ptr) {
    (*reinterpret_cast<int*>(var_ptr)) = value;
  } else if (obj_ptr && acc->obj_access && acc->obj_access->set) {
    Set_int set = Set_int(acc->obj_access->set);
    (obj_ptr->*set)(value);
  }
}

void option_info::set(size_t value )
{
  if (description()!=NULL&&strstr(description(), "READ-ONLY")) return;
  const option_access* acc = access;
  if (acc->variable_ptr) {
    (*reinterpret_cast<size_t*>(var_ptr)) = value;
  } else if (obj_ptr && acc->obj_access && acc->obj_access->set) {
    Set_uint set = Set_uint(acc->obj_access->set);
    (obj_ptr->*set)(value);
  }
}

void option_info::set(bool value )
{
  if (description()!=NULL&&strstr(description(), "READ-ONLY")) return;
  const option_access* acc = access;
  if (acc->variable_ptr) {
    (*reinterpret_cast<bool*>(var_ptr)) = value;
  } else if (obj_ptr && acc->obj_access && acc->obj_access->set) {
    Set_bool set = Set_bool(acc->obj_access->set);
    (obj_ptr->*set)(value);
  }
}

void option_info::set(double value )
{
  if (description()!=NULL&&strstr(description(), "READ-ONLY")) return;
  const option_access* acc = access;
  if (acc->variable_ptr) {
    if (acc->var_access && acc->var_access->set) {
      (*reinterpret_cast<double*>(var_ptr)) = acc->var_access->set(value);
    } else {
      (*reinterpret_cast<double*>(var_ptr)) = value;
    }
  } else if (obj_ptr && acc->obj_access && acc->obj_access->set) {
    Set_double set = Set_double(acc->obj_access->set);
    (obj_ptr->*set)(value);
  }
}

void option_info::set(const MatrixNd &value )
{
  if (description()!=NULL&&strstr(description(), "READ-ONLY")) return;
  const option_access* acc = access;
  if (acc->variable_ptr) {
    (*reinterpret_cast<MatrixNd*>(var_ptr)) = value;
  } else if (obj_ptr && acc->obj_access && acc->obj_access->set) {
    Set_matrix set = Set_matrix(acc->obj_access->set);
    (obj_ptr->*set)(value);
  }
}

void option_info::set(const char *value )
{
  if (description()!=NULL&&strstr(description(), "READ-ONLY")) return;
  const option_access* acc = access;
  if (acc->type==OPT_STRING) {
    if (acc->variable_ptr) {
      (*reinterpret_cast<std::string*>(var_ptr)) = value;
    } else if (obj_ptr && acc->obj_access && acc->obj_access->set) {
      Set_string set = Set_string(acc->obj_access->set);
      (obj_ptr->*set)(value);
    }
  } else if (acc->type==OPT_CHAR_PTR) {
    if (acc->variable_ptr) {
      strcpy((reinterpret_cast<char*>(var_ptr)), value);
    } else if (obj_ptr && acc->obj_access && acc->obj_access->set) {
      Set_string set = Set_string(acc->obj_access->set);
      (obj_ptr->*set)(value);
    }
  }
}

void option_info::set(const std::string &str )
{
  if (description()!=NULL&&strstr(description(), "READ-ONLY")) return;
  set(str.c_str());
}

void option_info::set(const std::complex<double> &value )
{
  if (description()!=NULL&&strstr(description(), "READ-ONLY")) return;
  const option_access* acc = access;
  if (acc->variable_ptr) {
    (*reinterpret_cast<std::complex<double>*>(var_ptr)) = value;
  } else if (obj_ptr && acc->obj_access && acc->obj_access->set) {
    Set_complex set = Set_complex(acc->obj_access->set);
    (obj_ptr->*set)(value);
  }
}

std::string option_info::get_value_as_string() const
{
  std::ostringstream str;
  switch(type()) {
  case OPT_DOUBLE:
    str.precision(DBL_DIG);
    str << get_double();
    break;
  case OPT_INT:
    str << get_int();
    break;
  case OPT_UINT:
    str << get_uint();
    break;
  case OPT_BOOL:
    str << get_bool();
    break;
  case OPT_MATRIX:
    str << get_matrix();
    break;
  case OPT_STRING:
    str << get_string();
    break;
  case OPT_CHAR_PTR:
    str << get_char_ptr();
    break;
  case OPT_COMPLEX:
    // str << get_complex();
    {
      str.precision(DBL_DIG);
      std::complex<double> _value=get_complex();
      if (fabs(std::imag(_value))<std::numeric_limits<double>::epsilon()) {
	str << std::real(_value);
      } else {
	str << '(' << std::real(_value) << ',' << std::imag(_value) << ')';
      }
    }
    break;
  case OPT_NONE:
    // print error warning? JS
    break;
  }
  return str.str();
}

void option_info::set_value_from_string(const std::string &value )
{
  std::istringstream str(value);
  switch(type()) {
  case OPT_DOUBLE: {
    double dummy;
    str >> dummy;
    set(dummy);
  }
    break;
  case OPT_INT: {
    int dummy;
    str >> dummy;
    set(dummy);
  }
    break;
  case OPT_UINT: {
    size_t dummy;
    str >> dummy;
    set(dummy);
  }
    break;
  case OPT_BOOL: {
    bool dummy;
    str >> dummy;
    set(dummy);
  }
    break;
  case OPT_MATRIX: {
    MatrixNd dummy;
    str >> dummy;
    set(dummy);
  }
    break;
  case OPT_STRING:
  case OPT_CHAR_PTR:
    set(value);
    break;
  case OPT_COMPLEX: {
    std::complex<double> dummy;
    str >> dummy;
    set(dummy);
  } break;
  case OPT_NONE:
    // print error warning? JS
    break;
  }
}
