#include <cstdio>

#define PLACET

/* Function prototypes */

#include "matrix.h"

void matrix_mult(double a[],int na,int ma,double b[],int /*nb*/,int mb,double c[])
{
  int i,j,k;
  double sum,*p1,*p2;
  for(i=0;i<na;i++){
    for (j=0;j<mb;j++){
      sum=0.0;
      p1=a+ma*i;
      p2=b+j;
      for (k=0;k<ma;k++){
	sum+= (*p1++)*(*p2);
	p2+=mb;
      }
      *c++=sum;
    }
  }
}

void M_mult_4(R_MATRIX *r_xx,R_MATRIX *r_xy,R_MATRIX *r_yx,R_MATRIX *r_yy,
              R_MATRIX *sigma,R_MATRIX *sigma_xy,R_MATRIX *sigma_xx)
{
  double m1[16],m2[16],m[16];

  m2[0]=sigma_xx->r11;
  m2[1]=sigma_xx->r12;
  m2[4]=sigma_xx->r21;
  m2[5]=sigma_xx->r22;

  m2[2]=sigma_xy->r11;
  m2[3]=sigma_xy->r12;
  m2[6]=sigma_xy->r21;
  m2[7]=sigma_xy->r22;

  m2[8]=sigma_xy->r11;
  m2[9]=sigma_xy->r21;
  m2[12]=sigma_xy->r12;
  m2[13]=sigma_xy->r22;

  m2[10]=sigma->r11;
  m2[11]=sigma->r12;
  m2[14]=sigma->r21;
  m2[15]=sigma->r22;

  m1[0]=r_xx->r11;
  m1[1]=r_xx->r21;
  m1[4]=r_xx->r12;
  m1[5]=r_xx->r22;

  m1[2]=r_yx->r11;
  m1[3]=r_yx->r21;
  m1[6]=r_yx->r12;
  m1[7]=r_yx->r22;

  m1[8]=r_xy->r11;
  m1[9]=r_xy->r21;
  m1[12]=r_xy->r12;
  m1[13]=r_xy->r22;

  m1[10]=r_yy->r11;
  m1[11]=r_yy->r21;
  m1[14]=r_yy->r12;
  m1[15]=r_yy->r22;

  matrix_mult(m2,4,4,m1,4,4,m);

  matrix_copy(m,m2,4*4);

  m1[0]=r_xx->r11;
  m1[1]=r_xx->r12;
  m1[4]=r_xx->r21;
  m1[5]=r_xx->r22;

  m1[2]=r_xy->r11;
  m1[3]=r_xy->r12;
  m1[6]=r_xy->r21;
  m1[7]=r_xy->r22;

  m1[8]=r_yx->r11;
  m1[9]=r_yx->r12;
  m1[12]=r_yx->r21;
  m1[13]=r_yx->r22;

  m1[10]=r_yy->r11;
  m1[11]=r_yy->r12;
  m1[14]=r_yy->r21;
  m1[15]=r_yy->r22;

  matrix_mult(m1,4,4,m2,4,4,m);

  sigma->r11=m[10];
  sigma->r12=m[11];
  sigma->r21=m[14];
  sigma->r22=m[15];

  sigma_xx->r11=m[0];
  sigma_xx->r12=m[1];
  sigma_xx->r21=m[4];
  sigma_xx->r22=m[5];

  sigma_xy->r11=m[2];
  sigma_xy->r12=m[3];
  sigma_xy->r21=m[6];
  sigma_xy->r22=m[7];
}

void matrix_copy(double a[],double b[],int n)
{
  int i;
  for (i=0;i<n;i++){
    *b++=*a++;
  }
}

