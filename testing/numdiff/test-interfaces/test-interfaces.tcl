set filename "myfile.txt"

proc create_file {} {
    global filename
    set fp [open $filename w]
    puts $fp "Hello Mr Bug!"
    close $fp
    set fexist [file exists $filename]
    puts "File exist in tcl: $fexist"
}


file delete $filename

Python {
import os
placet.Tcl_Eval('create_file')
print "File exist in python:",os.path.isfile("$filename")
}

file delete $filename

Octave {
    Tcl_Eval('create_file');
    fexist=exist("$filename");
    printf("File exist in octave: %i",fexist);
}
