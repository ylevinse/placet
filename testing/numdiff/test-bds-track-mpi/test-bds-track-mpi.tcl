#################################################################################################################################
#                                                                                                                                
# main.tcl
#
#################################################################################################################################

set e_initial 1500
set e0 $e_initial
set base_dir .
set script_dir ../../script_dir/

set bpmres 0.010

# synchrotron radiation on
set sr 1

set synrad $sr
set quad_synrad $sr 
set mult_synrad $sr 
set sbend_synrad $sr

# six dimensional step functions:
set six_dim 1

set six_dim_sbend $six_dim
# this is not implemented yet
set six_dim_quad 0

SetReferenceEnergy $e0
Girder
MPI_Begin
source $script_dir/lattices/bds.match.linac4b_v_10_10_11
MPI_End
TclCall -script { 
  Octave {
    B = placet_get_beam();
    printf("Beam distr: %.6e %.6e %.6e %.6e %.6e %.6e\n",mean(B))
    printf("Beam std: %.6e %.6e %.6e %.6e %.6e %.6e\n",std(B))
  }
}
BeamlineSet -name test

source $script_dir/make_bds_beam.tcl

FirstOrder 1

puts "\nMPI tracking:"
Octave tic
MPI_TestNoCorrection -beam beam0 -survey None -emitt /dev/null -mpirun "mpiexec -np 8"
Octave toc
BpmReadings -file mpi_reading.dat

puts "\nSingle-thread tracking:"
Octave tic
TestNoCorrection -beam beam0 -survey None -emitt /dev/null
Octave toc
BpmReadings -file single_reading.dat
