Girder
Quadrupole -length 0.05 -strength 0.25
for {set i 0} {$i<9}  {incr i} {
    Girder
    Drift -length 2.9
    Quadrupole -length 0.1 -strength -0.5
    Drift -length 2.9
    Quadrupole -length 0.1 -strength 0.5
}
Drift -length 2.9
Quadrupole -length 0.1 -strength -0.5
Drift -length 2.9
Quadrupole -length 0.05 -strength 0.25

# fixes the beamline, call once and only once
BeamlineSet
