#!/bin/bash

# simple trick to run placet and write output to file:
$1 -s < $2 | tee $3
exit ${PIPESTATUS[0]}
