# Need to define this policy to allow for if(true)
cmake_policy(SET CMP0012 NEW)

# first we set up the macros:
if($ENV{PLACET})
   set(PLACET $ENV{PLACET})
else()
   set(PLACET @PWD@/src/placet)
endif()
if(NOT DEFINED ENV{PLACETDIR})
   set(ENV{PLACETDIR} @PWD@/pre_install/)
endif()
find_program(NUMDIFF numdiff)

# testname should be the name of the folder
# such that testname.tcl exists
# testlabel can be one of either "SHORT", "LONG", or "VERYLONG"
# depending on how slow the test is
macro(placet_test testname testlabel)
   # list output files and delete output from previous run:
   file(GLOB TEST_OUTPUT "*.ref.gz" "*.ref")
   string(REGEX REPLACE ".ref(.gz)?" ".out" OLD_OUTPUT "${TEST_OUTPUT}")
   string(REGEX REPLACE ".ref(.gz)?" "" TEST_OUTPUT "${TEST_OUTPUT}")
   file(REMOVE ${OLD_OUTPUT} ${TEST_OUTPUT})

   # run placet:
   add_test(${testname} "@PWD@/testing/run_placet_output2file.sh" ${PLACET} ${testname}.tcl ${testname}.out )
   set_tests_properties(${testname} PROPERTIES
      LABELS ${testlabel})

   # if we have numdiff, check output:
   if(NUMDIFF)
      file(GLOB TEST_OUTPUT "*.ref.gz" "*.ref")
      string(REGEX REPLACE ".ref(.gz)?" "" TEST_OUTPUT "${TEST_OUTPUT}")
      # add numdiff test:
      add_test(${testname}-numdiff ${NUMDIFF} -b -l -t ${testname} ${TEST_OUTPUT})
      # the numdiff tests depends on the test itself
      # numdiff returns 0 if failure, so need to look for regexp:
      set_tests_properties(${testname}-numdiff PROPERTIES
         DEPENDS ${testname}
         PASS_REGULAR_EXPRESSION ".*${testname}.*PASS"
         LABELS ${testlabel})
   endif()
endmacro()

if(@USE_OCTAVE@)
   subdirs(numdiff/test-bds-track)
   subdirs(numdiff/test-bds-track-4d)
   # single element tests
   subdirs(numdiff/test-dipole)
   subdirs(numdiff/test-quadrupole)
   subdirs(numdiff/test-sextupole)
   subdirs(numdiff/test-octupole)
   subdirs(numdiff/test-decapole)
   subdirs(numdiff/test-sbend)
   subdirs(numdiff/test-crab)
   subdirs(numdiff/test-collimator1)
   subdirs(numdiff/test-collimator2)
   subdirs(numdiff/test-collimator3)

   if(@USE_MPI@)
      subdirs(numdiff/test-bds-track-mpi)
   endif()
   subdirs(numdiff/test-bds-track-nosynrad)
endif()
if(@USE_PYTHON@)
      subdirs(numdiff/test-bds-track-bpmtransmission)
endif()
subdirs(numdiff/test-clic-1)
subdirs(numdiff/test-clic-2)
subdirs(numdiff/test-clic-3)
subdirs(numdiff/test-clic-4)
subdirs(numdiff/test-createbeamline)
subdirs(numdiff/test-csr_shielding)
subdirs(numdiff/test-forloop)
subdirs(numdiff/test-groundmotion)
if(@USE_OCTAVE@ AND @USE_PYTHON@)
   subdirs(numdiff/test-interfaces)
endif()
subdirs(numdiff/test-irtracking)
subdirs(numdiff/test-matchfodo)
