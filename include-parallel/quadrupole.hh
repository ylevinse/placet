#ifndef quadrupole_hh
#define quadrupole_hh

#include "element.hh"

class Quadrupole : public Element {

  double k0;	// GeV / m
  double tilt;
  
public:

  Quadrupole(const std::string &name, double length, double strength ) : Element(name, length), k0(strength) {}
  Quadrupole(double length, double strength ) : Element(length), k0(strength) {}
  Quadrupole() {}

  int get_id() const { return ELEMENT_ID::QUADRUPOLE; }

  void transport(std::vector<Particle> &beam ) const;
  void transport_synrad(std::vector<Particle> &beam ) const;

  friend OStream &operator<<(OStream &stream, const Quadrupole &q )	{ return stream << static_cast<const Element&>(q) << q.tilt << q.k0; }
  friend IStream &operator>>(IStream &stream, Quadrupole &q )		{ return stream >> static_cast<Element&>(q) >> q.tilt >> q.k0; }

};

#endif /* quadrupole_hh */
