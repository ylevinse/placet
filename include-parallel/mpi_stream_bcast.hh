#ifndef mpi_stream_bcast_hh
#define mpi_stream_bcast_hh

#ifndef mpi_buffered_stream_bcast_hh
#warning "Why using `mpi_stream_bcast.hh' when `mpi_buffered_stream_bcast.hh' is much faster?"
#endif

#include <mpi.h>

#include "fifo_stream.hh"
#include "stream.hh"

class MPI_OStream_Bcast : public OStream {

  int root;
  MPI_Comm comm;
	
protected:

  bool writable() const { return true; }

public:

  MPI_OStream_Bcast(MPI_Comm _comm = MPI_COMM_WORLD ) : comm(_comm) { MPI_Comm_rank(comm, &root); }
	
  // writing methods
#define WRITE(TYPE, MPI_TYPE)						\
  inline size_t write(const TYPE *ptr, size_t n ) { MPI_Bcast(const_cast<TYPE*>(ptr),  n, MPI_TYPE, root, comm); return n; } \
  inline size_t write(const TYPE &ref )           { MPI_Bcast(const_cast<TYPE*>(&ref), 1, MPI_TYPE, root, comm); return 1; }
  
  WRITE(char,		   MPI_CHAR)
  WRITE(bool,		   MPI_INT)
  WRITE(signed short,	   MPI_SHORT)
  WRITE(signed int,	   MPI_INT)
  WRITE(signed long int,   MPI_LONG)
  WRITE(unsigned short,	   MPI_UNSIGNED_SHORT)
  WRITE(unsigned int,	   MPI_UNSIGNED)
  WRITE(unsigned long int, MPI_UNSIGNED_LONG)
  WRITE(float,		   MPI_FLOAT)
  WRITE(double,		   MPI_DOUBLE)
  WRITE(long double,	   MPI_LONG_DOUBLE)
#undef WRITE

};

class MPI_IStream_Bcast : public IStream {
	
  int root;
  MPI_Comm comm;
	
  MPI_IStream_Bcast();

protected:
	
  bool readable()	const { return true; }
	
public:
	
  explicit MPI_IStream_Bcast(int _root, MPI_Comm _comm = MPI_COMM_WORLD ) :root(_root), comm(_comm) {}
	
  // reading methods
#define READ(TYPE, MPI_TYPE)						\
  inline size_t read(TYPE *ptr, size_t n )        { MPI_Bcast(const_cast<TYPE*>(ptr),  n, MPI_TYPE, root, comm); return n; } \
  inline size_t read(TYPE &ref )                  { MPI_Bcast(const_cast<TYPE*>(&ref), 1, MPI_TYPE, root, comm); return 1; }
  
  READ(char,              MPI_CHAR)
  READ(bool,              MPI_INT)
  READ(signed short,      MPI_SHORT)
  READ(signed int,        MPI_INT)
  READ(signed long int,	  MPI_LONG)
  READ(unsigned short,    MPI_UNSIGNED_SHORT)
  READ(unsigned int,      MPI_UNSIGNED)
  READ(unsigned long int, MPI_UNSIGNED_LONG)
  READ(float,             MPI_FLOAT)
  READ(double,            MPI_DOUBLE)
  READ(long double,       MPI_LONG_DOUBLE)
#undef READ
  
};
#endif /* mpi_stream_bcast_hh */
