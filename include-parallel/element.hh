#ifndef element_hh
#define element_hh

#include <typeinfo>

#include <gsl/gsl_rng.h>

#include "photon_spectrum.hh"
#include "particle.hh"
#include "matrix.hh"
#include "object.hh"

#include "stream.hh"
#include "error.hh"

#include "placet.h"
#include "element_id.hh"

class Element : public Object {
protected:

  double ref_energy;

  inline double ref_gamma() const { return ref_energy / INV_EMASS; }
  inline double ref_gamma2() const { double __tmp = ref_energy / INV_EMASS; return __tmp*__tmp; }

public:
	
  static PhotonSpectrum syngen;
  static gsl_rng *rng;

  static void insert_element_ptr(OStream &stream, const Element *element_ptr );
  static Element *extract_element_ptr(IStream &stream );

  static Element *duplicate(const Element *e );
	
public:

  explicit Element(const std::string &name, double _length=0.0, double _ref_energy=0.0 ) : Object(name, _length), ref_energy(_ref_energy) {}
  explicit Element(double _length=0.0, double _ref_energy=0.0 ) : Object(_length), ref_energy(_ref_energy) {}
  Element(const std::string &name, int id ) : Object(name, id) {}
	
  virtual ~Element() {}

  virtual int get_id() const = 0;

  inline void 	set_ref_energy(double e ) { ref_energy=e; }
  inline double	get_ref_energy() const { return ref_energy; }

  virtual void transport_in(std::vector<Particle> &beam ) const;
  virtual void transport_out(std::vector<Particle> &beam ) const;

  virtual void transport(std::vector<Particle> &beam ) const = 0;		
  virtual void transport_synrad(std::vector<Particle> &beam ) const = 0;

  friend OStream &operator<<(OStream &stream, const Element &e ) { return stream << static_cast<const Object &>(e) << e.ref_energy; }
  friend IStream &operator>>(IStream &stream, Element &e ) { return stream >> static_cast<Object &>(e) >> e.ref_energy; }

};

#endif /* element_hh */
