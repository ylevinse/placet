#ifndef constants_hh
#define constants_hh

/** physical constants from http://physics.nist.gov/cuu/Constants/index.html */

#define C 299792458 /** velocity of light [m/s] */
#define EMASS 0.000510998928 /** electron mass [GeV] */
#define INV_EMASS 1956.9514
#define RE 2.8179403267e-15 /** classical electron radius [m] */
#define ECHARGE 1.602176565e-19 /** electron charge [C] */
#define Z0 (C*4e-7*M_PI) /** characteristic impedance [Ohm] */
#define ALPHA_EM (1.0/137.035999074) /** fine structure constant */
#define HBAR 6.58211928e-25 /** reduced Planck constant [GeV s] */
#define ENERGY_LOSS 1.4079285720036e-05 /// e * e / 6 / pi / epsilon0 / m / ((electronmass * c * c / GeV)**4)

#endif  /*  constants_hh  */
