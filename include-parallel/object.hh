#ifndef object_hh
#define object_hh

#include <string>

#include "matrix.hh"
#include "stream.hh"

class Object {
protected:

  std::string name;
  int id;
		
  double x, xp, y, yp, roll;
  double length;
	
public:

  explicit Object(const std::string &_name, double _length=0. ) : name(_name), x(0.), xp(0.), y(0.), yp(0.), roll(0.), length(_length) {}
  explicit Object(double _length=0. ) : x(0.), xp(0.), y(0.), yp(0.), roll(0.), length(_length) {}
  Object(const std::string &_name, int _id ) : name(_name), id(_id) {}
	
  const std::string &get_name() const { return name; }
  void set_name(const std::string &str ) { name = str; }
	
  int get_id() const { return id; }
  void set_id(int i ) { id = i; }

  double get_length() const { return length; }
  void set_length(double l ) { length=l; }
	
  friend OStream &operator<<(OStream &stream, const Object &o )	{ return stream << o.name << o.id << o.x << o.xp << o.y << o.yp << o.length; }
  friend IStream &operator>>(IStream &stream, Object &o )	{ return stream >> o.name >> o.id >> o.x >> o.xp >> o.y >> o.yp >> o.length; }
	
};

#endif /* object_hh */
