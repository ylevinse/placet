#ifndef array_hh
#define array_hh

#include "stream.hh"

template <class T> class Array {

  size_t _size;
  T *_data;
	
public:

  explicit Array(size_t _s ) : _size(_s) { if (_size) _data=new T[_size];}
  Array(const Array &a ) : _size(a._size) { if (_size) { _data=new T[_size]; for (size_t i=0; i<_size; i++) _data[i]=a._data[i]; }}
  Array() : _size(0) {}

  ~Array() { if (_size) delete []_data; }
		
  size_t size() const { return _size; }

  const T *c_ptr() const { return _data; }
  T *ptr() { return _data; }
	
  void resize(size_t _s )
  {
    if (_size!=_s) {
      if (_size)
	delete []_data;
      _size=_s;
      if (_size)
	_data=new T[_size];
    }
  }
  
  void free() { if (_size) { delete []_data; _size=0; } }
  
  const T &operator[](size_t i ) const { return _data[i]; }
  T &operator[](size_t i ) { return _data[i]; }
  
  operator const T *() const { return _data; }
  operator T *() { return _data; }
  
  const Array &operator=(const Array &a )
  {
    if (this!=&a) {
      if (_size!=a._size) {
	if (_size)
	  delete []_data;
	_size=a._size;
	if (_size) {
	  _data = new T[_size];
	  for (size_t i=0; i<_size; i++)
	    _data[i]=a._data[i];
	}
      }
    }
    return *this;
  }
	
  friend OStream &operator<<(OStream &stream, const Array &array )
  {
    stream << array._size;
    for (size_t i=0; i<array._size; i++)
      stream << array._data[i];
    return stream;
  }
	
  friend IStream &operator>>(IStream &stream, Array &array)
  {
    size_t size;
    stream >> size;
    array.resize(size);
    for (size_t i=0; i<array._size; i++)
      stream >> array._data[i];
    return stream;
  }

};

#endif /* array_hh */
