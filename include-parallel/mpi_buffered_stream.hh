#ifndef mpi_buffered_stream_hh
#define mpi_buffered_stream_hh

#include "fifo_stream.hh"
#include "mpi_stream.hh"

#define BUFFER_SIZE (1L<<18)

class MPI_Buffered_OStream : public OStream {

  MPI_OStream stream;
  FIFO_Stream buffer;
				
protected:

  bool writable() const { return stream && buffer.free() > 0; }

public:

  MPI_Buffered_OStream(int _dest=-1, int _tag=0, MPI_Comm _comm=MPI_COMM_WORLD, size_t _size=BUFFER_SIZE ) : stream(_dest, _tag, _comm), buffer(_size) {}
  MPI_Buffered_OStream(int _dest, int _tag, size_t _size ) : stream(_dest, _tag), buffer(_size) {}
	
  ~MPI_Buffered_OStream() { flush(); }
  
  int get_dest() const { return stream.get_dest(); }
  int get_tag() const { return stream.get_tag(); }
  MPI_Comm get_comm() const { return stream.get_comm(); }
	
  void set_dest(int d ) { stream.set_dest(d); }
  void set_tag(int t ) { stream.set_tag(t); }
  void set_comm(MPI_Comm c ) { stream.set_comm(c); }

  void flush()	{ send_buffer(); }

  // write methods
#define WRITE(TYPE)				\
  size_t write(const TYPE *ptr, size_t n )	\
  {						\
    size_t __t = 0;				\
    while(__t<n) {				\
      __t += buffer.write(ptr+__t,n-__t);	\
      if (__t < n) {				\
	send_buffer();				\
      }						\
    }						\
    return __t;					\
  }						\
  size_t write(const TYPE &ref )		\
  {						\
    for (;;) {					\
      if (buffer.write(ref))			\
	break;					\
      send_buffer();				\
    }						\
    return 1;					\
  }
  
  WRITE(char)
  WRITE(bool)
  WRITE(float)
  WRITE(double)
  WRITE(long double)
  WRITE(signed short)
  WRITE(signed int)
  WRITE(signed long int)
  WRITE(unsigned short)
  WRITE(unsigned int)
  WRITE(unsigned long int)
#undef WRITE

  void send_buffer()
  {
    size_t size=buffer.size();
    if (size>0) {
      stream.write(size);
      stream.write(buffer.c_ptr(),size);
      buffer.clear();
    }
  }	

};

class MPI_Buffered_IStream : public IStream {

  MPI_IStream stream;
  FIFO_Stream buffer;
		
protected:

  bool readable() const { return stream && buffer.size() > 0; }

public:

  MPI_Buffered_IStream(int _src=0, int _tag=0, MPI_Comm _comm=MPI_COMM_WORLD, size_t _size=BUFFER_SIZE ) : stream(_src, _tag, _comm), buffer(_size) {}
  ~MPI_Buffered_IStream() {}
  
  int get_src() const { return stream.get_src(); }
  int get_tag() const { return stream.get_tag(); }
  MPI_Comm get_comm() const { return stream.get_comm(); }
	
  void set_src(int d ) { stream.set_src(d); }
  void set_tag(int t ) { stream.set_tag(t); }
  void set_comm(MPI_Comm c ) { stream.set_comm(c); }

  // read methods
#define READ(TYPE)				\
  size_t read(TYPE *ptr, size_t n )		\
  {						\
    size_t __t = 0;				\
    while(__t<n) {				\
      __t += buffer.read(ptr+__t,n-__t);	\
      if (__t < n) {				\
	receive_buffer();			\
      }						\
    }						\
    return __t;					\
  }						\
  size_t read(TYPE &ref )			\
  {						\
    for (;;) {					\
      if (buffer.read(ref))			\
	break;					\
      receive_buffer();				\
    }						\
    return 1;					\
  }

  READ(char)
  READ(bool)
  READ(float)
  READ(double)
  READ(long double)
  READ(signed short)
  READ(signed int)
  READ(signed long int)
  READ(unsigned short)
  READ(unsigned int)
  READ(unsigned long int)
#undef READ

  void receive_buffer()
  {
    size_t size;
    buffer.clear();
    stream.read(size);
    stream.read(buffer.c_ptr(),size);
    buffer.in(size);
  }

};

#endif /* mpi_buffered_stream_hh */
