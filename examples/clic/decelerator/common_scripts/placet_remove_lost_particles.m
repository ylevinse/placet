function B_lost = placet_remove_lost_particles(B)

[r,c] = size(B);

B_lost = zeros(r,c);
myeps = 1.0e-9;
myeps_w = 1.0e-20;

n=1;
if(c==6)
for i=1:r,
  if( B(i,1) > myeps  )
    B_lost(n,:) = B(i,:);
    n=n+1;
  end% if
end% for
end% if
n=1;
if(c==17)
 for i=1:r,
  if( (B(i,3) > myeps) && (B(i,2) > myeps_w ) )
    B_lost(n,:) = B(i,:);
    n=n+1;
  end% if
 end% for
end% if

B_lost = B_lost(1:n-1,:);

return;
