% writes values to virtual.currents file
function  writevmdevices(devdescr, values)

vmndevs = length(devdescr);

for j=1:vmndevs
  vmname = [devdescr(j).prefix '.' devdescr(j).name];
  writevmdevice(vmname, values(j) );
end

return;
