set pa_suffix OCTALL

set load_save_R $do_multisim_load_save_R


Octave {

    disp('Starting calc disp many');

nm=$n_machines;

Dy_temp_NC = 0;
Dy_tot2 = 0;
y0_tot2 = 0;
y1_tot2 = 0;
Dy_tot2_SC = 0;
y0_tot2_SC = 0;
y1_tot2_SC = 0;
Dy_tot2_DFS = 0;
y0_tot2_DFS = 0;
y1_tot2_DFS = 0;
env2_NC = 0;
env2_SC = 0;
env2_DFS = 0;
emitty_NC = 0;
emitty_SC = 0;
emitty_DFS = 0;
losses_NC = 0;
losses_SC = 0;
losses_DFS = 0;
BEAM_NC = 0;    
BEAM_SC = 0;    
BEAM_DFS = 0;    
    
Q = placet_get_number_list("$mysimname", "quadrupole");
Qstrength_nominal = placet_element_get_attribute("$mysimname", Q, "strength");
C = Q;
#placet_element_get_attribute("$mysimname", Q(56:58), "e0")
#placet_element_set_attribute("$mysimname", Q(2), "y", 20);
#placet_element_get_attribute("$mysimname", Q(57), "s")
B = placet_get_number_list_real_bpm("$mysimname");
Bs = placet_element_get_attribute("$mysimname", B, "s");
  
envmax_NC = zeros(length(Q), 1);
envmax_SC = zeros(length(Q), 1);
envmax_DFS = zeros(length(Q), 1);

if( $track_1to1_correction || $track_DFS_correction ),    
        if( exist("R0") )
      disp('Using already calculated R0');
    else
    if ( $load_save_R )
      if (exist("$deceleratorrootpath/precalc/responsematrices/R0_${mysimname}_${Rsuffix}.dat", "file")) 
        disp('Loading previously calculated R0...');
        load $deceleratorrootpath/precalc/responsematrices/R0_${mysimname}_${Rsuffix}.dat;
      else
        disp('Recalculating R0...');
        R0 = placet_get_response_matrix("$mysimname", "$beamname1", B, C, "Zero");
        %save -text R0_${mysimname}_${Rsuffix}.dat R0
      end% if
    else
      disp('Recalculating R0...');
      R0 = placet_get_response_matrix("$mysimname", "$beamname1", B, C, "Zero");
    end% if
  end% if
end% if
if( $track_DFS_correction ),    
    if( exist("R1") )
      disp('Using already calculated R1');
    else
    if ( $load_save_R )
      if (exist("$deceleratorrootpath/precalc/responsematrices/R1_${mysimname}_${Rsuffix}.dat", "file")) 
        disp('Loading previously calculated R1...');
        load $deceleratorrootpath/precalc/responsematrices/R1_${mysimname}_${Rsuffix}.dat;
      else
        disp('Recalculating R1...');
        R1 = placet_get_response_matrix("$mysimname", "$testbeam1", B, C, "Zero") - R0;
        %save -text R1_${mysimname}_${Rsuffix}.dat R1
      end% if
    else
      disp('Recalculating R1...');
      R1 = placet_get_response_matrix("$mysimname", "$testbeam1", B, C, "Zero") - R0;
    end% if
    end% if
end% if
    
    placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);


%rand("seed", 42);


%
%
%  MAIN LOOP
%

    for i=1:nm,

%  SET QUAD FAILURE
n_failure_quad = str2num(Tcl_GetVar("n_failure_quad"));
Q_failed = sort(genrandlist(length(Q), n_failure_quad))
if( ($quadfail_in_series == 2) && (length(Q_failed) > 0) )
  Q_failed = [Q_failed Q_failed+2] % somewhat sloppy for serie fault
end% if

%  SET QUAD STRENGTH JITTER
Q = placet_get_number_list("$mysimname", "quadrupole");
scale_Q = 1+randn(1,length(Q))*$quad_strength_jitter;


% SET PETS INHIBITION
P = placet_get_number_list("$mysimname", "cavity_pets");
if( length(P) > 0 )
n_failure_PETS = str2num(Tcl_GetVar("n_failure_PETS"));
P_failed = sort(genrandlist(length(Q), n_failure_PETS))
PETS_nominal = placet_element_get_attribute("$mysimname", P, "e0"); % we (ab)use the parameter "e0" to induce PETS failure
end% if



 Tcl_Eval("my_survey");

    #
    # DO NC
    #
    
    if( $track_no_correction )
if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P(P_failed), "e0", -1.0); end; %  drive wedges PETS
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal.*scale_Q); % strength jitter
placet_element_set_attribute("$mysimname", Q(Q_failed), "strength", 0);
placet_element_set_attribute("$mysimname", B, "resolution", 0);
Tcl_Eval("FirstOrder 0"), [E0, BEAM] = placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, B(end)); Tcl_Eval("FirstOrder 1");
y0 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
E1 = placet_test_no_correction("$mysimname", "$testbeamE2", "None", 1, 0, B(end));
y1 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal ); % restore quads
if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P, "e0", PETS_nominal); end; %  restore PETS

dp_p = ($energyfraction2-100) / 100;

Dy = (y0-y1) / dp_p;
  
    Dy_temp_NC += Dy;
    Dy_tot2 += Dy.^2;
    y0_tot2 += y0.^2;
    y1_tot2 += y1.^2;
    env2_NC += (E0(1:(end-1),8)'.^2);
    envmax_NC(:,i) =  E0(1:(end-1),8);
    emitty_NC += (E0(1:(end-1),6)');
    losses_NC += placet_element_get_attribute("$mysimname", Q, "aperture_losses");
    BEAM_NC += BEAM;
    end% if


    #
    # DO SIMPLE CORRECTION
    #
    if( $track_1to1_correction )
    disp('Starting OCT SC current machine routine');
      
    tB = B;
    tC = C;
    tR=R0;
    [U,S,V]=svd(tR);
    tR_pinv = V*pinv(S)*U';
    placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, tB(end));
    tb0 = placet_element_get_attribute("$mysimname", tB, "reading_y")';
    tb = [tb0];
    tc = -tR_pinv * tb;
    tc = [zeros(size(tc)),tc];
    placet_vary_corrector("$mysimname", tC, tc);
    disp('Finishing OCTSC current machine routine');
# WHY DOES THIS NOT WORK???
%source "/afs/cern.ch/user/e/eadli/work/myPlacet/12ghz/def_SC_cm.m"

if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P(P_failed), "e0", -1.0); end; %  drive wedges PETS
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal.*scale_Q); % strength jitter
placet_element_set_attribute("$mysimname", Q(Q_failed), "strength", 0);
placet_element_set_attribute("$mysimname", B, "resolution", 0);
Tcl_Eval("FirstOrder 0"), [E0, BEAM] = placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, B(end)); Tcl_Eval("FirstOrder 1");
y0 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
E1 = placet_test_no_correction("$mysimname", "$testbeamE2", "None", 1, 0, B(end));
y1 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal );
if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P, "e0", PETS_nominal); end; %  restore PETS

dp_p = ($energyfraction2-100) / 100;
Dy = (y0-y1) / dp_p;
    Dy_tot2_SC += Dy.^2;
    y0_tot2_SC += y0.^2;
    y1_tot2_SC += y1.^2;
    env2_SC += (E0(1:(end-1),8)'.^2);
    envmax_SC(:,i) =  E0(1:(end-1),8);
    emitty_SC += (E0(1:(end-1),6)');
    losses_SC += placet_element_get_attribute("$mysimname", Q, "aperture_losses");
    BEAM_SC += BEAM;
end



    #
    # DO DFS
    #
    if( $track_DFS_correction )
    disp('Starting OCT DFS current machine routine');
    placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
    tB = B;
    tC = C;
    tR=R0;
    [U,S,V]=svd(tR);
    tR_pinv = -V*pinv(S)*U';
    placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, tB(end));
    tb0 = placet_element_get_attribute("$mysimname", tB, "reading_y")';
    placet_test_no_correction("$mysimname", "$testbeam1", "None", 1, 0, tB(end));
    tb1 = placet_element_get_attribute("$mysimname", tB, "reading_y")' - tb0;
    dfs.Wgt = [1, sqrt($DFS_w1_w0)];
   tb = [dfs.Wgt(1)*tb0 ; dfs.Wgt(2)*tb1];
    tR=[dfs.Wgt(1)*R0 ; dfs.Wgt(2)*R1];
    [U,S,V]=svd(tR);
    tR_pinv = V*pinv(S)*U';
    tc = -tR_pinv * tb;
    tc = [zeros(size(tc)),tc];
    placet_vary_corrector("$mysimname", tC, tc)
    disp('Finishing DFS current machine routine');
# WHY DOES THIS NOT WORK???
%source "/afs/cern.ch/user/e/eadli/work/myPlacet/12ghz/def_SC_cm.m"

if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P(P_failed), "e0", -1.0); end; %  drive wedges PETS
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal.*scale_Q); % strength jitter
placet_element_set_attribute("$mysimname", Q(Q_failed), "strength", 0);
placet_element_set_attribute("$mysimname", B, "resolution", 0);
Tcl_Eval("FirstOrder 0"), [E0, BEAM] = placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, B(end)); Tcl_Eval("FirstOrder 1");
y0 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
E1 = placet_test_no_correction("$mysimname", "$testbeamE2", "None", 1, 0, B(end));
y1 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal );
if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P, "e0", PETS_nominal); end; %  restore PETS

dp_p = ($energyfraction2-100) / 100;
Dy = (y0-y1) / dp_p;
    Dy_tot2_DFS += Dy.^2;
    y0_tot2_DFS += y0.^2;
    y1_tot2_DFS += y1.^2;
    env2_DFS += (E0(1:(end-1),8)'.^2);
    envmax_DFS(:,i) =  E0(1:(end-1),8);
    emitty_DFS += (E0(1:(end-1),6)');
    losses_DFS += placet_element_get_attribute("$mysimname", Q, "aperture_losses");
    BEAM_DFS += BEAM;
end


    
end


%
% POST PROCESSING
%
    Dy_tot_NC = sqrt(Dy_tot2 / nm);
    y0_tot_NC = sqrt(y0_tot2 / nm);
    y1_tot_NC = sqrt(y1_tot2 / nm);
    Dy_tot_SC = sqrt(Dy_tot2_SC / nm);
    y0_tot_SC = sqrt(y0_tot2_SC / nm);
    y1_tot_SC = sqrt(y1_tot2_SC / nm);
    Dy_tot_DFS = sqrt(Dy_tot2_DFS / nm);
    y0_tot_DFS = sqrt(y0_tot2_DFS / nm);
    y1_tot_DFS = sqrt(y1_tot2_DFS / nm);
    env_NC = sqrt(env2_NC / nm);
    env_SC = sqrt(env2_SC / nm);
    env_DFS = sqrt(env2_DFS / nm);
    emitty_NC = emitty_NC / nm;
    emitty_SC = emitty_SC / nm;
    emitty_DFS = emitty_DFS / nm;
    losses_NC /= nm;
    losses_SC /= nm;
    losses_DFS /= nm;
    Dy_temp_NC /= nm;
    BEAM_NC ./= nm;
    BEAM_SC ./= nm;
    BEAM_DFS ./= nm;
    envmax_NC_sort = sort(envmax_NC')';
    envmax_NC = envmax_NC_sort(:, round(size(envmax_NC_sort, 2)*$beam_envelope_percentile));
    envmax_SC_sort = sort(envmax_SC')';
    envmax_SC = envmax_SC_sort(:, round(size(envmax_SC_sort, 2)*$beam_envelope_percentile));
    envmax_DFS_sort = sort(envmax_DFS')';
    envmax_DFS = envmax_DFS_sort(:, round(size(envmax_DFS_sort, 2)*$beam_envelope_percentile));

    if( $savebeam_active )
save -text disp${pa_suffix}.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run.dat Dy_tot_NC  y0_tot_NC  y1_tot_NC  Dy_tot_SC Dy_tot_DFS y0_tot_SC y0_tot_DFS  y1_tot_SC y1_tot_DFS  dp_p  env_NC  env_SC  env_DFS envmax_NC  envmax_SC  envmax_DFS  emitty_NC  emitty_SC  emitty_DFS   losses_NC  losses_SC  losses_DFS  Dy_temp_NC BEAM_NC  BEAM_SC BEAM_DFS
    else
save -text disp${pa_suffix}.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run.dat Dy_tot_NC  y0_tot_NC  y1_tot_NC  Dy_tot_SC Dy_tot_DFS y0_tot_SC y0_tot_DFS  y1_tot_SC y1_tot_DFS  dp_p  env_NC  env_SC  env_DFS envmax_NC  envmax_SC  envmax_DFS  emitty_NC  emitty_SC  emitty_DFS   losses_NC  losses_SC  losses_DFS  Dy_temp_NC 
end% if
disp('Finished calc disp');
}

