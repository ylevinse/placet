if { [lindex $PETSlist [expr $n_pets_slot]] } {
    DecCavity -name "PETS" -length $cavitylength	
    Drift -name "CB.DCELL4" -length [expr 0.91865 - $cavitylength]
    incr n_pets_installed
} else {
    Drift -name "CB.DCELL4" -length 0.91865
}
incr n_pets_slot
