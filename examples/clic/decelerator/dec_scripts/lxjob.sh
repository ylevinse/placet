#!/bin/bash

# example usage: sh lxjobs.sh 1nd 1

bsub -q $1 -m "lxscd01 lxscd02 lxscd03 lxscd04 lxscd05 lxscd06 lxscd07 lxscd08" -oo /dev/null << EOF
/afs/cern.ch/eng/sl/clic-code/software/i686/bin/placet-octave $PWD/../dec_scripts/loop_main.tcl $PWD/.. >placet_output.txt
cp * $PWD
EOF

