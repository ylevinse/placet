n=1:N;

do_plot = 1;

E_min = E0*(1-S_grad); % S_grad CLIC nominal equals 90%
E = E0-(E0-E_min)*n/N;
delta_E = E(end-1)-E(end);
delta_g = delta_E; % we use same units g and E

% padding the E-vector in order to calc last power supply in same framework
N_pad = 3000;
E_pad = [((N_pad:-1:1)*delta_g+E(1)) E];
E_ideal = E;
E = E_pad;

% enforce correct gradien starting from last power-supply (bin)
n_P = 1; % calculation PS 1
c_T = E((end-N_T+1):end);
I_P(n_P) = 1;
i_start_P(n_P) = length(E)-N_T+1;


% calculate PS currents and PS start points
while( i_start_P(n_P) > (N_T+1) )
  n_P += 1;
  I_P(n_P) = ( c_T(1)*I_P(n_P-1) + delta_g*(1+(c_T(1)/c_T(N_T))^(n_P-2)*5e-1)) / c_T(N_T) ;
  %I_P(n_P) = ( c_T(1)*I_P(n_P-1) + delta_g*(exp(n_P*1e-1))) / c_T(N_T) ;
  i_start_P(n_P) = i_start_P(n_P-1)-N_T;
  while( (E(i_start_P(n_P)) < I_P(n_P)*c_T(1)) && (i_start_P(n_P) > 1) )
    i_start_P(n_P) -= 1;
  end% while
end% while
N_P = n_P % number of PS required (including padding)

% calculate number of REAL PS (without padding)
N_P_real = 1;
while( i_start_P(N_P_real) > (length(E_pad)-length(E_ideal)+1) )
  N_P_real += 1;
end% while


% distribute magnet types
i_type_change = zeros(N_P, N_T); % where in a string a magnet type
                                 % is changed
i_type_change(1,:) = 1:N_T;
for n_P=2:N_P,
  delta_i = ( i_start_P(n_P-1)-i_start_P(n_P) ) / N_T; % avg slot interval
  i_type_change(n_P,1) = 1; % first type also defined as a change
  for n_T=2:N_T;
    i_type_change(n_P, n_T) = 1+round(delta_i*(n_T-1));
  end% for
end


% calculate resulting effective gradient (E_focus)
for n_P=1:N_P,
  i_slot = 0;
  for n_T=1:N_T-1,
    % fill up E-table until last change
    do
        E_focus( i_start_P(n_P) + i_slot ) = c_T(n_T)*I_P(n_P);
        i_slot += 1;
    until ( i_slot >= i_type_change(n_P, n_T+1)-1 )
    % fill up for last type (until next PS)
end% for
n_T = N_T;
if( n_P > 1)
  while (  (i_start_P(n_P) + i_slot ) < (i_start_P(n_P-1)) )
        E_focus( i_start_P(n_P) + i_slot ) = c_T(n_T)*I_P(n_P);
        i_slot += 1;
    end% while
else% if
        E_focus( i_start_P(n_P) + i_slot) = c_T(n_T)*I_P(n_P);
end% if
end% for


%
% crop back gradient vector to real size (first set of magnets
% partially used)
%
E_focus = E_focus((length(E_focus)-length(E_ideal)+1):end);


% special case individual power supply (not handled correctly by
% main algorithm)
if  (N_T == 1)
  E_focus = E_ideal;
end% if
%stop



%plot(E, 'rx');
%hold on;
%plot(E_focus, 'bx');
%hold off;
%grid on;

%stop;



%
%
% new, simpler variant : # of power supplies, same type, step gradient
%
%

% this variant string N and N magnets, k/k0 will increase
if ( str2num(Tcl_GetVar("use_stepped_gradient_strings")) )
  N_quad_per_string = str2num(Tcl_GetVar("quad_per_string"));
  N = length(E_ideal);
  N_P = ceil(N / N_quad_per_string); 
  E_focus = E_ideal;
  n_start = floor(N_quad_per_string/2);
  if(n_start == 0)
    n_start = 1;
  end;
  for n=1:N_P*str2num(Tcl_GetVar("lattice_string_cut")),
    for m=1:N_quad_per_string;
      if((((n-1)*N_quad_per_string+m) <= length(E_focus)) && (((n-1)*N_quad_per_string+n_start) <= length(E_focus))),
        E_focus((n-1)*N_quad_per_string+m) = E_ideal((n-1)*N_quad_per_string + n_start);
      end% if
    end% for
  end% for
%
%
%

% this variant keeps worst k/k0 constant, for the same number of strings
% (power supplies)

%
% HOPELESS, FOR any large N does not make sense in our disrete case
%
if( 0 )
N_P = 10;
N = length(E_ideal);
norm_sum = 0;
for n=1:N_P,
  if( n<3 ),
    norm_sum += n; % a_n = n for n < 3
  else
    norm_sum += 3*2^(n-3); % a_n for n >= 3
  end
end% for
N / norm_sum
scale = ceil( N / norm_sum) 

m = 1;
s_a_n_1 = 0; % sigma of prev a_n
for n=N_P:-1:1,
  if(n<3),
    a_n = n;
  else
    a_n = 3*2^(n-3);
  end% for
  k = 1;
   while( (k < a_n*scale) && (m < length(E_ideal)) ),
    E_focus(m) = E0 - delta_E*a_n*scale/2 - delta_E*s_a_n_1*scale;
    k = k + 1;
    m = m + 1;
   end% while
   s_a_n_1 += a_n;
end% for
end% if
end% if(0)


%
%
%  PLOT
%
%


E_focus = E_focus(1:length(E_ideal)); 
k_dev = E_focus ./ E_ideal;
max_dev(N_T) = max(abs(1-k_dev));
N_P_array(N_T) = N_P_real;

if( exist('do_plot') ) 
% plot gradient versus energy
if( 1 )
plot(E_ideal, '-r;ideal;', E_focus, '-b;real;');
hold on;
%plot(E_focus, '-b;real;')
hold off;
grid on;
axis([1 length(E_focus) 0 max(E_ideal)])
xlabel('PETS number [-]'); ylabel('Ideal and real gradient [GeV]');
%legend('ideal gradient', 'real gradient');
% same in T/M
%plot(E_ideal*81.2/max(E_ideal), '-r');
%hold on;
%plot(E_focus*81.2/max(E_ideal), '-b')
%hold off;
%grid on;
%axis([1 length(E_focus) 0 max(E_ideal)])
%xlabel('PETS number [-]'); ylabel('Ideal and stepped gradient [T/m]');
%legend('ideal gradient', 'stepped gradient');
% axis for zoom
%axis([1400 1492 0.2*81.2/max(E_ideal) 0.4*81.2/max(E_ideal)])
pause;
end% if

% plot k-values
%plot(1,1*100);
%hold on;
plot(k_dev*100, '-xb')
hold off;
grid on;
xlabel('PETS number [-]'); ylabel('k/k_0 [%]');
%axis([0 1492 99 101]);
end% if


% pad (for use with placet)
E_focus = [E_focus(1) E_focus];


