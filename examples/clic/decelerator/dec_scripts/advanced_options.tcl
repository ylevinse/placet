# advanced options for DECELERATOR SIMULATONS
# (change this file only if you know what you are doing)

# sigma cut for beam envelope calcs   (for decelerator: 3 sigmas,  for TBL, 1 sigma)
set envelope_sigma_cut 3.0
if { $mysimname == "clic12" } { set envelope_sigma_cut 3.0 }

# TBL length scaling (must be integer), including current scaling for same final energy
set tbl_length_scaling 1.0

# "clicnote" or new parameters
set clicnote_parameters 0
if { $mysimname == "tbl12" } { set clicnote_parameters 0 }

# do charge / phase ramp (changes beam charge distribution to imported "charge_ramp.dat")
#   THIS IS OBSOLETE: use only do_charge_ramp flag instead
set do_phase_ramp 0

# modify bunch z position (changes bunch z-position according to imported "zz_init.dat")
set do_modify_initial_z 0
# start from this index in zz_init.dat (continue n_bunches)
set modify_initial_z_start_n 1100

# mini-beam parameters (for doing quick tests, NB: responses will be off)
set minibeam 0

# in principle should be able to choose dipoles or movers, but for the moment only movers implemented for CLIC
set dipole_correctors 0

# 1: use realistic stepped gradients (as opposed to ideal gradient)
# "complex option" with N_T types of magnets
set use_stepped_gradient 0
set stepped_gradient_n_types [expr 50]

# "simple options" with N magnet per PS string (in multisim)
set use_stepped_gradient_strings 0
set quad_per_string 2
# stop stringing this far into the lattice
set lattice_string_cut [expr 5.0/10.0]

# 1: loss tracking: NB: macro-particles only
set track_with_macro_losses 0
# loss aperture in fraction of full aperture [-]
set loss_aperture_frac 0.5

# 1: loss test with spread in macros in y (simulate p-beam): 1) macroparticles initial scattered in y, 2 NB: no reset of offsets in loop, so SHOULD NOT BE USED with other offsets, ONLY Y MOTION
#   requires track_with_macro_losses to observe losses
set macroparticle_init_y 0
if { $macroparticle_init_y } {
    set track_with_macro_losses 1
}

# 1: also do tracking in x, as well as y (set emittance,  misalign and correct) 
set do_x_alignment 0
if { $macroparticle_init_y } { set do_x_alignment 0 }
# TBL: always track both planes (decelerator correction: usually only one plane)
if { $mysimname == "tbl12" } { set do_x_alignment 1 }

if { $do_x_alignment} {
    set sigma_quad_roll 1.0e3
    set sigma_bpm_roll 1.0e3
    set sigma_quad_x 20
    set sigma_quad_xp 1.0e3
    set sigma_bpm_x 20
    set sigma_bpm_xp 1.0e3
    set sigma_pets_x 100
    set sigma_pets_xp 1.0e3
} else {
    set sigma_quad_x 0
    set sigma_quad_xp 0
    set sigma_bpm_x 0
    set sigma_bpm_xp 0
    set sigma_pets_x 0
    set sigma_pets_xp 0
}   

# 1: set emitt_x to 0 (study only y)
if { $do_x_alignment } {
    set set_emitt_x_0 0
} else {
    puts "EA: x set to 1"
    set set_emitt_x_0 1
}

# 1: put in multipole component 
#   NB: restriced use:  only works on centroid particles
#                       (set dist to 0 and enlarge beam)
#                        i.e. in user_options: set set_zero_macroparticle_emittance 1
#                       and can one put one component at the time
#   NB: still to do: misalign multipoles with quads
set do_multipole_tracking 0
if { $do_multipole_tracking } {
    set set_zero_macroparticle_emittance 1
    # also: set these beam parameters as example of how to compensate
    set jitter_mainbeam_offset_total 5.0
    set jitter_all_mode_frequencies 1
}
# define multipole components
    # type [-] (order follows European standard, 2: quadrupole, 3: sextupole etc.)
set mpoltype 6
# int strength wrt. quad int strength... [-]
set mpolrelstrength [expr 1.0e-5*$loop_param2]
# ...at distance [m]
set mpolrelstrength_d 11.0e-3


#
# use placet power procedures to perform scans (script saves a power scan file, then exits)
#
set do_power_scans 0

# 1: activate envelelope scaling (compensation when running simulations in one plane only)  
# if 1: scale simlulated envelope by factor r0 + (y*scale_ad - r0) * sqrt(2) / scale_ad
set activate_envelope_scaling 0
if { $do_x_alignment } { set activate_envelope_scaling 0 }
if { $mysimname == "tbl12" } { set activate_envelope_scaling 0 }

# use main placet-octave simulation scripts (should always be 1)
set do_multisim 1

# which correction scheme to run w/o octave (from before "multisim.m", kept to keep a minimum of no-octave functionality)
set track_no_correction_wo_octave 1
set track_1to1_correction_wo_octave 0
set track_DFS_correction_wo_octave 0

#if { ($B_BBA_interval != "1") || ($Q_BBA_interval != "1") || ($QB_BBA_interval != "1") } { set do_multisim_load_save_R 0; set do_multisim_load_mach 0; set do_multisim_save_mach 0;    }
set do_calc_disp 0

# scaling factors for RQ, Q and w.  All modes scaled simultainously. Scalong factor of 1: nominal PETS parameters
# for vg scaling
#set RQ_vg_scaling_factor [expr 2.8e-0]
#set RQ_scaling_factor [expr 1.0334e-0]

# number of PETS steps
set n_PETS_steps 5

# set quadrupole y jitter
#set quad_y_jitter $loop_param2
set quad_y_jitter 0.0

# number of PETS failed (is this working?)
set n_failure_PETS 0

# 1: Kick beam for each failed PETS, 0: drift for each failed PETS (is this working?)
set PETS_breakdown_kick 0

# force values for loss study beam
if { $macroparticle_init_y } { set set_zero_macroparticle_emittance 1 }
if { $macroparticle_init_y } { set minibeam 0 }
if { $macroparticle_init_y } { set sigma_quad_roll 0.0 }

# how many percent of machines  max envelope will cover [-]
set beam_envelope_percentile 1.00

# 1: simulate 100ns/140ns trains (instead of only up to ~steady-state) - especially important for high Q studies
set long_trains 0

# 1: track and save only pure particle beam - DOESN'T WORK WITH PETS :(
set is_pure_particle_beam 0

# 1: replace first quad with drift (e.g. get beam straight onto PETS)
set deactivate_first_quad 0

# 1: instrumentation section (full quad) or envelope studies/BBA (1/2 quad + last corrector BPM)
set instrumentation_section_present 0
if { $clicnote_parameters } { set instrumentation_section_present 0 }   

# 1: use low current, missing-bunch main beam
set main_beam_missing_bunches 0

# 1: Ignore wall losses in power calculations
set ignore_wall_losses 0

# 1: compact drive lattice, put PETS in all slots (# of PETS same, but shorter decelerator total length than real lattice), if 0: read from file
set compactdrivelattice 0
if { $clicnote_parameters } { set compactdrivelattice 1 }   

# 1: Quadrupole thin_lens approximation (for comparision with analytic calculations)
set quad_thin_lens 0

# current scaling
#set I_scaling_factor [expr 1.0*(0.80/0.90)]
#set de0_scaling [expr 1.0*((80.0+($loop_param1-1))/90.0)]
#set I_scaling_factor [expr 1.0/24.0]
#set de0_scaling [expr 1.0/24.0*$loop_param1]
set I_scaling_factor [expr 1.0]
set de0_scaling [expr 1.0]
# if we do I scaling, should we scale de0 in lattice as well?  (0: do not scale lattice, typically used when I_scaling represent unknown energy)
set I_scaling_scale_lattice 0

# initial energy scaling
#set E_scaling_factor [expr 1.0+($loop_param1-11.0)/200.0]
#set E_scaling_factor [expr 1.0*1.21061715831336]
set E_scaling_factor [expr 1.0]
# if we do E scaling, should we scale e0 in lattice as well?  (0: do not scale lattice, typically used when E_scaling represent unknown energy)
set E_scaling_scale_lattice 0

# mistmatch: vary initial twiss parameters for beam  (0: perfect match)
  # loops forward to next random numbers
set beta_y_offset  0.0
set alpha_y_offset 0.0
#set beta_y_offset  [expr 3.335*0.01*($loop_param1-31)]
#set alpha_y_offset [expr 0.005*($loop_param1-31)]
#  for {set i 0} {$i < [expr ($loop_param1_n-1)*23.0+$loop_param2_n]} {incr i } {
#      set beta_y_offset  [expr ([rand_gauss]*[expr $loop_param2])/10.0]
#      set alpha_y_offset [expr ([rand_gauss]*[expr $loop_param2])/10.0]
#  }
puts "beta_y_offset: $beta_y_offset"
puts "alpha_y_offset: $alpha_y_offset"

# set DFS_weight w1/w0
set DFS_w1_w0 100.0

# set correction SVD cuts
set svd_cut_DFS 0.6
#set svd_cut_SC 0.9
set svd_cut_SC 0.9

# correction binning ( a large number, e.g. > 10000 yields a single bin) 
Octave {
    corr_per_bin_oct = 8*2^($loop_param1);
    Tcl_SetVar("corr_per_bin_oct", corr_per_bin_oct);
}
#set corr_per_bin  [expr $corr_per_bin_oct]
set corr_per_bin  11000
#set bin_overlap [expr $corr_per_bin/2] 
set bin_overlap 1
 
#DFS one-bin (in order to compare directly with SC), 0: many bins  [OBSOLETE WITH MULTISIM]
set DFS_one_bin 1


# set dynamic qaudrupole motion [um]
set dyn_qpole_motion 0.0

# scaling of FODO phase-advance ( k_scaling_factor=1 corresponds to 90.0 deg phase advance, variable_k_active means set back to 90 deg after E=(1-ksf*E0) )
set k_scaling_factor 1.0
set variable_k_active 0


# BEAM LONGITDUINAL DISTTRIBUTION
set longdisttype gaussian
#set longdisttype uniform
#set longdisttype import


# MAIN BEAM x offsets (typically used in spectrometer simulations)
set static_mainbeam_offset_x 0.0
set jitter_mainbeam_offset_total_x 0.0


# TEST BEAMS:

# static beam offset and jitter beam offset (total to be partioned over freqs) in quantities of sigma_y  - NOT IMPLEMENTED IN TEST MEASURED CORRECION !!!
set static_testbeamnom_offset 0.0
set jitter_testbeamnom_offset_total 0.0

# static beam offset and jitter beam offset (total to be partioned over freqs) in quantities of sigma_y  - NOT IMPLEMENTED IN TEST MEASURED CORRECION !!!
set static_testbeam_offset 0.0
set jitter_testbeam_offset_total 0.0

# DFS jitter offset (in quantities of sigma_y)
set jitter_DFS 0.0

# save beams

# save final beam included in main data dump
#   (disable for e.g. large beams and loops)
set savebeam_multisim_active 1
# NB: cannot save beams with multiple machines if losses, not same amount of particles
if { $track_with_macro_losses && $n_machines != "1" } {
    set savebeam_multisim_active 0
}
set savebeam_particle_active 0
if { $macroparticle_init_y } { 
    set savebeam_active 0 
}
set savebeam_1PETS_active 0
# save particle beam at end of lattice
# e.g. calc of sigma matrix after each quadrupole
set savebeam_intermediate_active 0


# # of bunches
if {$deactivate_PETS} { 
    # without PETS everything is steady-state
    set n_bunches 3;
} else {
    if { $mysimname == "clic12" } {
	if { $long_trains } { 
	    set n_bunches 1200
	} else {
	# n_steady_state = 12
	    set n_bunches $n_bunches_sim
	    # set last weight to simulate a longer train  (240 ns)
	    set last_bunch_weight [expr 2901-$n_bunches]
	}
    } elseif { $mysimname == "tbl12" } {
	if { $long_trains } { 
	    set n_bunches 1700
	} else {
	    # n_steady_state = 39 
	    set n_bunches $n_bunches_sim
	    # set last weight to simulate a longer train (140 ns)
	    set last_bunch_weight [expr 1701-$n_bunches]
	}
    } elseif { $mysimname == "tbts" } {
	    # n_steady_state = 47 
	    set n_bunches $n_bunches_sim
	    # set last weight to simulate a longer train (140 ns)
	    set last_bunch_weight [expr 1701-$n_bunches]
    }   
}


# # of slices per bunch  (e.g. 51 for power/energy/stability studies, 21 for DFS and loss measurement, 510 for energy distribution)
if { $n_bunches_sim == "1680" } {
    # special for TBL full beam studies, use only 21
    set n_slices 21
} else {
    # default
    set n_slices 51
}
if { $macroparticle_init_y } { 
    set n_slices 21 
}



# some SIM "modes"
set sim_BBA 0
# current responces matrices are created with the sim_BBA beam
if { $track_1to1_correction || $track_DFS_correction && $mysimname == "clic12" } {
    set sim_BBA 1
}
set sim_wake_instab 0
if { $sim_BBA } {
    set n_slices 51
    set n_bunches 15
    set n_PETS_steps 1
}
if { $sim_wake_instab } {
    set n_slices 51
    set n_bunches 50
    set n_PETS_steps 5
}
if { $minibeam } {
    set n_slices 5
    set n_bunches 15
    set n_PETS_steps 1
}




# set last weight to simulate full bunch train
set last_bunch_weight 1.0

if { $mysimname == "clic12" } {
    set last_bunch_weight [expr 2901-$n_bunches]
} elseif { $mysimname == "tbl12" } {
   set last_bunch_weight [expr 1701-$n_bunches]
}   


# internal parameter scan lists (not used for the moment)
set param1_list {100}
set param2_list {0}

# number of runs for each parameter setting
set n_runs 1


puts "\nMAJOR PARAMETER AND ERROR SETTINGS: "
puts "\nmachine simulated: $mysimname"
puts "n_machines: $n_machines"
puts "sigma_quad: $sigma_quad"
puts "sigma_bpm: $sigma_bpm"
puts "sigma_pets: $sigma_pets"
puts "\nset_zero_macroparticle_emittance: $set_zero_macroparticle_emittance"
puts "I_scaling_factor: $I_scaling_factor"
puts "Q_t_scaling_factor: $Q_t_scaling_factor"
puts "w_t_scaling_factor: $w_t_scaling_factor"
puts "Longitudinal beam profile: $longdisttype"
puts "k_scaling_factor: $k_scaling_factor"
puts "variable_k_active: $variable_k_active"
puts "static_mainbeam_offset: $static_mainbeam_offset"
puts "jitter_mainbeam_offset_total $jitter_mainbeam_offset_total"
puts "jitter_DFS: $jitter_DFS"
puts "bpm_resolution: $bpm_resolution"
puts "savebeam_active: $savebeam_active"
puts "savebeam_intermediate_active: $savebeam_intermediate_active"
puts "param1_list: $param1_list"
puts "param2_list: $param2_list"
puts "ext_param1: $ext_param1"
puts "n_runs: $n_runs"
puts "n_bunches: $n_bunches"
puts "n_slices: $n_slices"
puts "dyn_qpole_motion \[um\]: $dyn_qpole_motion"
puts "  track_no_correction: $track_no_correction"
puts "  track_1to1_correction: $track_1to1_correction"
puts "  track_DFS_correction: $track_DFS_correction"
puts "\n"


