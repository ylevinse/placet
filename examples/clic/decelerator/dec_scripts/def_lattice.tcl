# Define geometric parameters (one unit - half a FODO - consists of n cavities, one bpm and one qpole. The qpole strength is reduced as the beam is decelerated)
#set n_PETS DEFINED IN CAVITY FILE
#set dx_detuning   DEFINED IN CAVITY FILE
#set cavitylength [expr $cavitylength + $dx_detuning]   DEFINED IN CAVITY FILE
if { $mysimname == "tbts" } {
    set do_tbts 1
    SetReferenceEnergy $e0

    Girder
    if { $savebeam_initial_active } {
	TclCall -name "tcl dump" -script {BeamSaveAll -file beam_initial.dat}
    }
    # Initial observer
    Quadrupole -length 0 -strength 0
    Drift -length 1.0
    Bpm -name "bpm" -length 0  
    DecCavity -name "PETS" -length $cavitylength	
    Drift -length 1.0
    # Final observer
    Bpm -name "bpm" -length 0  
    Quadrupole -length 0 -strength 0

    if { $savebeam_initial_active } {
	TclCall -name "tcl dump" -script {BeamSaveAll -file beam_final.dat}
    }

    # Dummy value for MatchFODO
    set n_PETSperFODO 2
    set unitlength 1.4
    set quadrupolelength 0.15
    set bpmlength [expr 0.15 - $dx_detuning*$n_PETSperFODO/2]
    set k_qpole [expr 7*$k_scaling_factor*1]
} else {
# common part CLIC and TBL

# inititalising loading list of real gradients (only applicable if "use_stepped_gradient" flag is used)
if { $use_stepped_gradient } {
    octave {
	N = $n_PETS;
	E0 = $e0;
	N_T = $stepped_gradient_n_types;
	S_grad = $requested_E_spread;
	run "$deceleratorrootpath/dec_scripts/calc_quad_gradients.m"
    }
}

if { $mysimname == "clic12" } {
    if { $compactdrivelattice } {
	# simplified lattice, all slots filled with PETS
	set PETSlist {}
	for {set j 0} {$j < $n_PETS} {incr j } {
	    lappend PETSlist 1
	    lappend PETS_slot_mapping $j
	}	    
        set n_slots [llength $PETSlist]
	set n_PETSperFODO 4
    } else {
	# real CLIC PETS lists		  
	#   which decelerator station to simulate? [from 1-24]
	set n_decelerator_station 1
	source "$deceleratorrootpath/dec_scripts/read_lattice.tcl"
    }

    set unitlength 1.005
    set quadrupolelength 0.15
    set bpmlength [expr 0.08825 - $dx_detuning*$n_PETSperFODO/2]
    set coupler_drift 0.088
    set qpole_drift 0.025
    # Normalized quadrupole strength; gives phase advance of ~ 90 deg 
    set k_qpole [expr 10.1*$k_scaling_factor]
    # for 90.00 deg phase-advance
    #set k_qpole [expr 9.8884*$k_scaling_factor]  
    #puts "EA: warning, 90.00 deg phase-advance"
} elseif { $mysimname == "tbl12" } {
    # TBL PETS lists - MANUAL (to set TBL PETS on / off)
    #
	# simplified lattice, all slots filled with PETS
    # set PETSlist "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 "
    # set PETSlist "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1"
    #  set n_slots [llength $PETSlist]
    set PETSlist {}
    for {set j 0} {$j < [expr 16*$tbl_length_scaling]} {incr j } {
	lappend PETSlist 1
	lappend PETS_slot_mapping $j
    }	    
    set PETS_slot_mapping "0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15"
    set PETSlist $tbl_PETS_list
    set n_slots [llength $PETSlist]

    set n_PETSperFODO 2
    set unitlength 1.4
    set quadrupolelength 0.15
    set bpmlength [expr 0.15 - $dx_detuning*$n_PETSperFODO/2]
    set coupler_drift 0.075
    set qpole_drift 0.05
    # Normalized quadrupole strength; gives phase advance of ~ 90 deg 
    set k_qpole [expr 6.99*$k_scaling_factor*1]
    #set k_qpole [expr (55.0/150.0)*7*$k_scaling_factor*1]
}   

set extra_drift [expr ( $unitlength - $n_PETSperFODO/2*($cavitylength + $coupler_drift) - $bpmlength - $quadrupolelength - 2*$qpole_drift ) / 2 ]
set girderlength [expr $n_PETSperFODO/2*($cavitylength + $coupler_drift) + $quadrupolelength + $bpmlength + 2*$qpole_drift + 2*$extra_drift ]
puts "   TEST: Girderlength (unitlength): $girderlength"
# insert non-drift coupler element
if {[info exists couplerlength]} {        set coupler_drift [expr $coupler_drift - $couplerlength]       }

#   RANDOM UPDATE
# update random counter for looped simulations
# loop_param1: which random run
# loop_param2: how many quads fail
  for {set i 0} {$i < [expr ($loop_param1-1)*$loop_param2]} {incr i } {
      set dummy [expr ([rand_linear]*1)]
  }


# Generate list of failed QUADS   [ NB: NORMALLY NOT USED ANYMORE ]
# 0.0 : quad failure implies 0 strength
set n_failure_quad_lattice 0
set quad_fail_scaling 0.0
set quadlist_failed {}
# how many quads [of one type] in series?  (if one F fails, then the second F fails etc.)
set n_quads [expr [llength $PETSlist]/$n_PETSperFODO*2 - 2]
while {$n_failure_quad_lattice > 0} {
    set fail_unit [expr {int([rand_linear]*[expr $n_quads])}]
    if { [lsearch $quadlist_failed $fail_unit] < 0 } {
	lappend quadlist_failed [expr $fail_unit]
	incr n_failure_quad_lattice -1
    }
}

puts "quadlist_failed: $quadlist_failed"



# Generate list of failed PETS units    [ NB: NORMALLY NOT USED ANYMORE ]
#set n_failure_PETS [expr ($n_PETS/4) / 5]
set n_failure_PETS_lattice 0
set PETSlist_failed {}
while {$n_failure_PETS_lattice > 0} {
    set fail_unit [expr {int([rand_linear]*[expr $n_PETS])}]
    if { [lsearch $PETSlist_failed $fail_unit] < 0 } {
	lappend PETSlist_failed [expr $fail_unit]
	incr n_failure_PETS_lattice -1
    }
}

# set manually failed PETS
# NB: PETSlist_failed list is given in PETS number [starting from 0], not in SLOT number.  The PETS SLOT mapping is then used to fail the correct PETS
#set PETSlist_failed { 0 }
#set PETSlist_failed [list [expr ($loop_param1-1)*8]]
#set PETSlist_failed {}
#lappend PETS_fail_unit 1
puts "PETSlist_failed: $PETSlist_failed"

# put PETS fail information onto the PETSlist  (0: no PETS, 1: working PETS, 2 failed PETS)
for {set j 0} {$j < [llength $PETSlist_failed]} {incr j } {
    set PETS_failed_index [lindex $PETS_slot_mapping [lindex $PETSlist_failed $j]]
    #puts $PETS_failed_index
    set PETSlist [lreplace $PETSlist $PETS_failed_index $PETS_failed_index "2"]  
    #puts $PETSlist
}


puts "\nProducing lattice..."
puts "   Unitlength (girderlength, FODO 1/2-length): $unitlength m"
puts "   Number of PETS: $n_PETS"
puts "   Number of PETS per quad: [expr $n_PETSperFODO/2]"
puts "   Number of PETS slots: $n_slots"
puts "   -> resulting number of units: [expr [llength $PETSlist]/$n_PETSperFODO*2],  and number of FODO cells: [expr [llength $PETSlist]/$n_PETSperFODO]"
puts "   -> resulting Stationlength: [expr [llength $PETSlist]/$n_PETSperFODO*2*$unitlength] m"
puts "   PETS length: $cavitylength"
puts "   PETS coupler drift length: $coupler_drift"
puts "   Qpole active length: $quadrupolelength"
puts "   Qpole driftlength (before and after qpole): $qpole_drift"
puts "   Qpolestrength, normalized: $k_qpole \[m^-2]"
puts "   -> resulting focal length: [expr 1/($k_qpole*$quadrupolelength)] \[m]"
puts "   -> resulting qpole gradient: [expr ($k_qpole*$e0)/(0.2998)] \[T\/m]"
puts "   BPMlength: $bpmlength"
if {[info exists couplerlength]} {      puts "   Cavity coupler active length: $couplerlength"         }
puts "   Extra drift length (spare space, at start of first cavity and end of second cavity): $extra_drift"
# adjust de0 (slope of quadrupole gradient), e.g. for I_scaling with adjusted lattice
set de0 [expr $de0*$de0_scaling] 
if {$deactivate_PETS} { 
    set de0 0 
    puts "NB: LATTICE WILL HAVE ALL PETS DEACTIVATED (and no de0)\n"
}
puts "   Max. deceleration per PETS, de0: [expr $de0*1000] \[MeV\]"
puts ""

#
# Define the lattice: N identical FODO cells, starting in the middle of an F qpole, and with focusing strength adjusted so that the lowest energy particle has constant phase-advance
#

# if e0-scaling AND "lattice doesn't know" this, reverse effect for optics, if not, lattice scales with e0-scaling
if { $E_scaling_scale_lattice } {
    set e $e0
} else {
    set e [expr $e0/$E_scaling_factor]
}
set e_ideal $e
#if { $sigma_E_upon_E > 0 } {
#    puts "e0 in lattice set to e0_min: $e0_min"
#    set e [expr $e0_min]
#}



# write various quantities along the lattice
proc myUserProcedure {save_count} {
	Octave {
	    #beam = placet_get_beam();
	    #beam = placet_remove_lost_particles(beam);
	    #env = placet_get_envelope(beam)
	    #mu_e =  ( sum( beam(:,2) .* beam(:,3) ) ) / sum(beam(:,2))

	    #sigma = placet_get_sigma_matrix(beam)
	    #twiss = placet_get_twiss_matrix(beam)
	    #emitt_norm = placet_get_emittance(beam)
	    #emitt_geo = [sqrt(det(sigma(1:2, 1:2))) sqrt(det(sigma(3:4, 3:4)))]
	    #y2 =    ( sum( beam(:,2) .* beam(:,3).^2 ) ) / sum(beam(:,2));
	    #y = sqrt(y2)						    
													     

	    # plot y- phase-space REAL-TIME :)))
# 	    sigma_w = sum(beam(:,2));
# 	    mu_y = beam(:,2)'*beam(:,6) / sigma_w;
# 	    mu_yp = beam(:,2)'*beam(:,7) / sigma_w;
# 	    M1 = [mu_y; mu_yp];
# 	    M2 = sigma(3:4, 3:4);
# 	    phi = 2 * pi * (0:0.01:1);
# 	    crcle = [cos(phi); sin(phi)];
# 	    ell = sqrtm(M2) * crcle + repmat(M1, 1, size(crcle, 2));
# 	    axis([-1000 1000 -500 500]);
# 	    plot(ell(1,:), ell(2,:), 'r');

	# visualize particle beam phase space in y
	#plot(beam(:,3)/1e3, beam(:,6)/1e3, 'x');
	#xlabel('y [mm]'); ylabel('yp [mrad]');
	#pause;
	}
}
 
proc my_save_beam {} {
    global savebeam_intermediate_active
    global save_count
    if { $savebeam_intermediate_active } {
	TclCall -name "tcl dump" -script "myUserProcedure $save_count"
    }
    incr save_count
}

proc mySaveParticleBeamProcedure {save_particle_count} {
	 global loop_param1_n loop_param2_n param1_n param2_n run
    #if { $save_particle_count == "1" } {
	Octave {
	    beam = placet_get_beam();
	    #sigma_matrix_tracked = placet_get_sigma_matrix(beam)
	    #twiss_matrix_tracked = placet_get_twiss_matrix(beam)
	    #emitt_norm_tracked = placet_get_emittance(beam)
	    #emitt_geo_tracked = [sqrt(det(sigma_matrix_tracked(1:2, 1:2))) sqrt(det(sigma_matrix_tracked(3:4, 3:4)))]
	    name = ["beam_particles.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run.dump"];
	    save(name, "beam");
	}
    #}
}    


set beam_initial_modif_performed 0
proc my_beam_modif_initial {} {
    global beam_initial_modif_performed
    TclCall -name "beam_modif_initial" -script {
	if { $do_charge_ramp } {
	    puts "EA: MODIFYING INTIAL BEAM CHARGE ACCORDING TO BEAM LOADING RAMP (imported)"
	    Octave {
		BEAM=placet_get_beam();
		for n=1:length(ramp_up)
		#   add minimum charge  if you want to "see" the zero bunches as well
		#   (result is the same)
		# if(ramp_up(n) == 0)
		#   ramp_up(n) = 2*eps;
		# end% if
		  BEAM( ((n-1)*($n_slices*$n_macros)+1):(n*($n_slices*$n_macros)), 2) *= ramp_up(n);
		end% for
		placet_set_beam(BEAM);
	    }
	}
	if { $do_modify_initial_z } {
	    puts "EA: MODIFYING INTIAL BEAM Z ACCORDING TO FILE (imported)"
	    Octave {
		if(0),
		# FIRST APPROACH: displace particles, BLOODY HELL, DOES NOT WORK! z-pos is NOT taken from beam
		BEAM=placet_get_beam();
		# retrieve slicing for first bunch (we assume slicing for all bunches will be the same)
		delta_z_slices = BEAM(1:($n_slices*$n_macros), 1);
		# adjust bunches, starting from the zz_n [ns] index in the file
		for n=1:$n_bunches,
		BEAM( ((n-1)*($n_slices*$n_macros)+1):(n*($n_slices*$n_macros)), 1) = zz_init($modify_initial_z_start_n+n)*3e5 + delta_z_slices;
		end% for
		placet_set_beam(BEAM);
		end% (0)

		# SECOND APPROACH: empty buckets, ONLY WORKS IF list in zz_init has 12 GHz as base frequency
		BEAM=placet_get_beam();
		filled_buckets = round(sort(zz_init) / (1/12)); 
		n_bucket_start = filled_buckets($modify_initial_z_start_n);
		all_buckets = zeros(1, $n_bunches);
		m=1;
		for(n=1:$n_bunches),
		if( (n_bucket_start + n) == filled_buckets($modify_initial_z_start_n+m) ),
		  BEAM( ((n-1)*($n_slices*$n_macros)+1):(n*($n_slices*$n_macros)), 2) *= 0; # empty bucket
		    else
		  BEAM( ((n-1)*($n_slices*$n_macros)+1):(n*($n_slices*$n_macros)), 2) *= 1; # filled bucket
		m += 1;
		    end% if
		end% for
		placet_set_beam(BEAM);
	    }
	}
	    # .....the beam CHARGE is reset when doing a second tracking, but NOT the beam z-positions....
	if { $beam_initial_modif_performed=="0" } {
      set beam_initial_modif_performed 1
	if { $do_phase_ramp } {
      puts "EA: MODIFYING INTIAL BEAM Z-POS!"
	    Octave {
	BEAM=placet_get_beam();
	# example: creating phase-shift of first half-train
	n_bunch_shift_end = length(ramp_up) / 2;
	#n_train_shift_end = 2;
	shift_angle = 30; # [deg]
	shift_z = (shift_angle / 360) * $lambda_l;
	for n=1:(n_bunch_shift_end-1)
		BEAM( ((n-1)*($n_slices*$n_macros)+1):(n*($n_slices*$n_macros)), 1) += shift_z*1e6;
	end% for
      placet_set_beam(BEAM);
	    }
	}
    }
    }
 }



proc my_beam_modif_initial_generic {} {
    puts "EA: INTRODUCING MODIFICATIONS IN INITIAL BEAM"
    TclCall -name "beam_modif_BNS" -script {
	    Octave {
	BEAM=placet_get_beam();
#		BEAM(2,6) = -BEAM(2,6);
#		BEAM(2,7) = BEAM(2,6)/$twiss_match(beta_y)
#		BEAM(2,6) = 0
#		BEAM(2,6) = 1
#		BEAM(2,7) = 1
      placet_set_beam(BEAM);
	}
    }
}

proc my_save_tbl_beam {} {
	TclCall -name "tcl dump" -script "mySaveParticleBeamProcedure 1"
}


proc my_save_particle_beam {} {
    global savebeam_particle_active
    global save_particle_count
    if { $savebeam_particle_active } {
	TclCall -name "tcl dump" -script "mySaveParticleBeamProcedure $save_particle_count"
    }
    incr save_particle_count
}

set save_count 1
set save_particle_count 1

#EATEST ISR TEST
#     Girder
#     Drift -length 10
#     Multipole -type 2 -length 10 -strength 100 -apx 1e6 -apy 1e6 -synrad 1
#     Drift -length 10
#     set is_first_order 1
#EATESTEND

set first 1

# 1: normal (matched),  -1: inverted focusing
set quad_change_sign 1

# Reference energy needed for e.g. GetMatrix functions 
SetReferenceEnergy $e

# counter of PETS installed [not included failed pets]
set total_PETS_installed 0

# for TBL: lattice is in a user-beamline file
# (TBL beamline is converted from master MADX-file)
if { $mysimname == "tbl12" } {
    source "$deceleratorrootpath/dec_scripts/def_lattice_tbl.tcl"
} else    {
# generic arbitrary length lattice


# calculate multipole components, if applicable
if { $do_multipole_tracking } {
    Octave {
	mpoltype_fac = factorial($mpoltype-1)
	Tcl_SetVar("mpoltype_fac", mpoltype_fac);
	mpol_distnorm = (1/$mpolrelstrength_d)^($mpoltype-2)
	Tcl_SetVar("mpol_distnorm", mpol_distnorm);
    }
}


for {set i 0} {$i < [expr $n_slots / $n_PETSperFODO]} {incr i } {
    # NB: PLACET SIGN CONVENTION OF QUADS SEEMS OPPOSITE OF NORMAL: quad strength (and k) > 0 implies focusing in Y instead of x!
    set k_qpole_eff $k_qpole

    if { $variable_k_active } {
	# set varying phase advance in order to keep gradient constant until phase=90 deg
	if { $i >= [expr (1/0.9)*(1 - $k_scaling_factor)*($n_PETS / 4 )] } {
	    set k_qpole_eff [expr $k_qpole / $k_scaling_factor]
	} else {
	    set k_qpole_eff [expr $k_qpole * $e0 / $e]
	}
	puts "EA variable k: i: $i,   k_qpole_eff: $k_qpole_eff,   g': [expr $k_qpole_eff * $quadrupolelength * $e]"
    }

    Girder
    if {$first == "1"} {
	    # NB: CANNOT TRACK PARTICLE BEAM THROUGH PETS !!!
	    #SlicesToParticles -seed 1504
	    #ParticlesToSlices -e0 0.12

	# CHARGE RAMP  MODIF 
	if {$do_charge_ramp  || $do_phase_ramp || $do_modify_initial_z } {
	   my_beam_modif_initial
	}

	#my_beam_modif_initial_generic
	    
	# FIRST PASS (to start at a symmetry point in lattice)
	if { $is_pure_particle_beam } {
	    # NB: CANNOT TRACK PARTICLE BEAM THROUGH PETS !!!
	    #SlicesToParticles -seed 1504
	}
	
	# Save initial beam
	if { $savebeam_initial_active } {
	    TclCall -name "tcl dump" -script {BeamSaveAll -file beam_initial.dat}
	    #SlicesToParticles -seed 1504
	    #my_save_particle_beam
	}
	# TEMP BPM JUST AFTER SAVING INIT
	#Bpm -name "bpm" -length 0  -store_bunches 1
	# 1/2 D quadrupole  (focusing in y-plane)
	put_bpm
	# EA VERY TEMP
	#Bpm -length $bpmlength
	#Drift -length $qpole_drift
	# EA - END VERY TEMP
	if {$quad_thin_lens} {
	    # intial kick in middle of f-quad 
	    #  		set kick_size 1e-0
	    #  		set length 0.0
	    #  		set tilt_angle [expr $SI_pi/2]
	    #  		Multipole -name "MP" -type 1 -length $length -strength [expr $kick_size*$e0] -synrad 0 -tilt $tilt_angle
	    if { $deactivate_first_quad } {
		Quadrupole -thin_lens $quad_thin_lens -length [expr 0.0000001] \
		    -strength [expr -0.0 * $quad_change_sign * $k_qpole_eff * $quadrupolelength * $e]
	    } else {
		Quadrupole -thin_lens $quad_thin_lens -length [expr 0.0000001] \
		    -strength [expr -0.5 * $quad_change_sign * $k_qpole_eff * $quadrupolelength * $e]
	    }
	    Drift -length [expr 0.4999999*$quadrupolelength]
	} else {
	    if { $deactivate_first_quad } {
		Quadrupole -thin_lens $quad_thin_lens -length [expr 0.5*$quadrupolelength] \
		    -strength [expr -0.0 * $k_qpole_eff * $quadrupolelength * $e]
	    } else {
		Quadrupole -thin_lens $quad_thin_lens -length [expr 0.5*$quadrupolelength] \
		    -strength [expr -0.5 * $k_qpole_eff * $quadrupolelength * $e]
	    }
	}
	Drift -length $qpole_drift
	set first 0
    } else {
	# Bpm element: acts as drift space during tracking
	Bpm -length $bpmlength
	Drift -length $qpole_drift
	
	# default: quad is OK (strength scaling is 1.0)
	set quad_fail 1.0
	# check if quad is failed
	for {set j 0} {$j < $quadfail_in_series} {incr j } {
	    if { [lsearch $quadlist_failed [expr ($i-$j)*2]] >= 0 } {
		set quad_fail $quad_fail_scaling
		puts "EA: quad [expr $i*2] failed"
	    }
	}

# 	if { [lsearch $quadlist_failed [expr $i*2]] < 0 } {
# 	    # quadrupole ok
# 	    set quad_fail 1.0
# 	} else {
# 	    # quadrupole fail
# 	    set quad_fail $quad_fail_scaling
# 		puts "EA: quad [expr $i*2] failed"
# 	}
	# D quadrupole  (focusing in y-plane)
	Quadrupole -thin_lens $quad_thin_lens -length [expr 1*$quadrupolelength] \
	    -strength [expr -$quad_change_sign * $quad_fail * $k_qpole_eff * $quadrupolelength * $e]  \
	    -aperture_x $aperture_x -aperture_y $aperture_y -aperture_shape $aperture_shape
	if { $do_multipole_tracking } {
	    Multipole -name "MP" -type $mpoltype -length 0 -strength [expr [rand_gauss]*$mpolrelstrength*$mpol_distnorm*$mpoltype_fac * $quad_change_sign * $quad_fail * $k_qpole_eff * $quadrupolelength * $e] -tilt 0.0  
	}

	# User routine that is called when the beam goes through
	my_save_beam
	put_bpm
	Drift -length $qpole_drift
    }

    Drift -name "drift" -length $extra_drift
    # N PETS
    set pets_relative_counter 0
    source "$deceleratorrootpath/dec_scripts/insert_n_pets.tcl"
    Drift -name "drift" -length $extra_drift

	if { $savebeam_1PETS_active && $i=="0" } {
	    #SlicesToParticles -seed 1504
	    #my_save_particle_beam
	    TclCall -name "tcl dump" -script {BeamSaveAll -file beam_1PETS.dat}
	}

    # Focusing adjustment
    set e [expr $e - $n_pets_installed*$de0]
    set e_ideal [expr $e_ideal - $n_pets_installed*$de0]
    set total_PETS_installed [expr $total_PETS_installed + $n_pets_installed]
    if { $use_stepped_gradient } {
	Octave {
	    Tcl_SetVar("e_focus", E_focus($total_PETS_installed+1));
	}
        set e [expr $e_focus]
        #puts "[expr $e_focus/$e_ideal]     [expr $e_focus]    [expr $e_ideal]  [expr $total_PETS_installed]"
    }	
    SetReferenceEnergy $e_ideal

    Bpm -length $bpmlength
    Drift -length $qpole_drift
    # F quadrupole  (defocusing in y-plane)

	# default: quad is OK (strength scaling is 1.0)
	set quad_fail 1.0
	# check if quad is failed
	for {set j 0} {$j < $quadfail_in_series} {incr j } {
	    if { [lsearch $quadlist_failed [expr ($i-$j)*2+1]] >= 0 } {
		set quad_fail $quad_fail_scaling
		puts "EA: quad [expr $i*2+1] failed"
	    }
	}

# 	if { [lsearch $quadlist_failed [expr $i*2+1]] < 0 } {
# 	    # quadrupole ok
# 	    set quad_fail 1.0
# 	} else {
# 	    # quadrupole fail
# 	    set quad_fail $quad_fail_scaling
# 	    puts "EA: quad [expr $i*2+1] failed"
# 	}
    Quadrupole -thin_lens $quad_thin_lens -length [expr 1*$quadrupolelength] \
	-strength [expr +$quad_change_sign * $quad_fail * $k_qpole_eff * $quadrupolelength * $e] \
        -aperture_x $aperture_x -aperture_y $aperture_y -aperture_shape $aperture_shape
    if { $do_multipole_tracking } {
	Multipole -name "MP" -type $mpoltype -length 0 -strength [expr [rand_gauss]*$mpolrelstrength*$mpol_distnorm*$mpoltype_fac * $quad_change_sign * $quad_fail * $k_qpole_eff * $quadrupolelength * $e] -tilt 0.0  
    }
    my_save_beam
    put_bpm
    Drift -length $qpole_drift

    Drift -name "drift" -length $extra_drift
    # N PETS
    set pets_relative_counter $n_PETSperFODO/2
    source "$deceleratorrootpath/dec_scripts/insert_n_pets.tcl"
    Drift -name "drift" -length $extra_drift

    # Focusing adjustment
    set e [expr $e - $n_pets_installed*$de0]
    set e_ideal [expr $e_ideal - $n_pets_installed*$de0]
    set total_PETS_installed [expr $total_PETS_installed + $n_pets_installed]
    if { $use_stepped_gradient } {
	Octave {
	    Tcl_SetVar("e_focus", E_focus($total_PETS_installed+1));
	}
        set e [expr $e_focus]
    }	
    #puts "[expr $n_pets_installed]"
    #puts "[expr $e_ideal]"
    SetReferenceEnergy $e_ideal
}

Girder
 Bpm -name "bpm" -length $bpmlength

Drift -name "drift" -length $qpole_drift

#
# EA TEMP
#
TclCall -script {
#     Octave {
# 	beam = placet_get_beam();
# 	disp('EA: min beam energy after tracking:');
# 	min_E = min(beam(:,3))
	#filename = ["minE." num2str($loop_param1_n)];
	#save filname min_E 
#    }
}


# for instrumentation studies we want a full last quadrupole and some drift to simulate the real beam, e.g. for spectrometer measurement  [obselte, now "user_tblbeamline.tcl" instead"]
if { $instrumentation_section_present } {
    # D quadrupole  (focusing in y-plane)
    Quadrupole -thin_lens $quad_thin_lens -length [expr 1*$quadrupolelength] \
	-strength [expr -$quad_change_sign * $k_qpole_eff * $quadrupolelength * $e * 1.0]
    put_bpm
    
    # TEMP BPM JUST AFTER SAVING FINAL
    #Bpm -name "bpm" -length 0  -store_bunches 1
    Drift -name "drift" -length $qpole_drift
    Drift -name "drift" -length $extra_drift
    Bpm -name "bpm" -length $bpmlength
    
    # SOME EXTRA DRIFT FOR QUAD SCAN TESTS	
    Drift -name "drift" -length 0.3
    
    # e.g. dipole spectrometer goes here
    
    if { $savebeam_active } {
	TclCall -name "tcl dump" -script {BeamSaveAll -file beam_final.dat}
	#Quadrupole -thin_lens $quad_thin_lens -name "quadrupole" -length [expr 0.0*$quadrupolelength] -strength [expr -0.0 * $k_qpole_eff * $quadrupolelength * $e]
	#TclCall -name "tcl dump" -script {array set beam_measure [BeamMeasure -bunch -1]}
	#TclCall -script {StoreParticles -file_name bunch_particles_final.dat  -bunch 49  -particles 5000}
	
	# NB: must hardcode in rfkick.cc to make SlicesToParticles work -  bunch=(BEAM*)bunch_make(nbunches,n,nmacro,1,50000);
	# Final beam save
    }
    if { $savebeam_particle_active } {
	SlicesToParticles -seed 1504
	my_save_particle_beam
    }
    # Spectrometer dipole would go here
      
} else {
    # END AFTER HALF-QUAD
    #   (known CS-params for action calcs of final beam)
    # For BBA we want to have a last corrector
     #  (we put one of 0 length straight after quad)
    # 

    # 1/2 D quadrupole (focusing in y)
    if {$quad_thin_lens} {
	Drift -name "drift" -length [expr 0.4999999*$quadrupolelength]
	Quadrupole -thin_lens $quad_thin_lens -name "quadrupole" -length [expr 0.0000001] \
        -strength [expr -0.5 * $quad_change_sign * $k_qpole_eff * $quadrupolelength * $e]
    } else {
	Quadrupole -thin_lens $quad_thin_lens -name "quadrupole" -length [expr 0.5*$quadrupolelength] \
	    -strength [expr -$quad_change_sign * 0.5 * $k_qpole_eff * $quadrupolelength * $e]
    }
    put_bpm
    Bpm -name "lastbpm" -length 0

  # Various final beam save routines (just after last qpole)
    if { $savebeam_active } {
	TclCall -name "tcl dump" -script {BeamSaveAll -file beam_final.dat}
	TclCall -name "tcl dump" -script {array set beam_measure [BeamMeasure -bunch -1]
	}
	#TclCall -script {StoreParticles -file_name bunch_particles_final.dat  -bunch 49  -particles 5000}
	# Final beam save - particle beam
    }

if { $savebeam_particle_active } {
    SlicesToParticles -seed 1504
    my_save_particle_beam
}

}

# HERE ENDS CLIC12 PART (if tbl...)
}


}