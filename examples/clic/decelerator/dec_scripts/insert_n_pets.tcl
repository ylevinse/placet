    set n_pets_installed 0
    for {set j 0} {$j < $n_PETSperFODO/2} {incr j } {
	# is there a PETS at this slot?  (PETS_slot_status   0: no pets, 1: PETS, 2: failed PETS)
	set PETS_slot_status [lindex $PETSlist [expr $i*$n_PETSperFODO+$j+$pets_relative_counter]]
        if {$deactivate_PETS || ($PETS_slot_status == "0") } { 
	    Drift -name "PETS_open_slot" -length $cavitylength	
	    if {[info exists couplerlength]} {  Drift -length $couplerlength }
	    Drift -name "drift" -length $coupler_drift
	} else {
	    incr n_pets_installed
	    # PETS break down? (0: no pets, 1: PETS, 2: failed PETS)
	    if { $PETS_slot_status == "1" } {
		#puts "GOOD PETS in FODO cell $i\n"
		DecCavity -name "PETS" -length $cavitylength	
		if {[info exists couplerlength]} {  DecCavityCoupler -length $couplerlength }
		Drift -name "drift" -length $coupler_drift
	    } else {
		puts "INACTIVE/FAILED PETS in FODO cell $i\n"
		if { $PETS_breakdown_kick } {
		    set length 0.0
		    #set kick_size 1e-4
		    # kick size in [GV]
		    set kick_size_voltage 100e-6
		    # kick in +y
		    #set break_kick_angle [expr $SI_pi/2]
		    # kick in aribtrary angle
		    set break_kick_angle [expr [rand_linear]*($SI_pi/2)]
		    #		    set tilt_angle 0
		    #kick normalized to HIGHEST energy particles
		    #Multipole -name "MP" -type 1 -length $length -strength [expr $kick_size*$e0] -synrad 0 -tilt $break_kick_angle
		    #kick at a specified voltage
		    Multipole -name "MP" -type 1 -length $length -strength [expr $kick_size_voltage] -synrad 0 -tilt $break_kick_angle
		    #Dipole -length $length -strength_y [expr $kick_size*1e6*$e0] 
		    puts "PETS break down kick of $kick_size_voltage \[kV\] at angle $break_kick_angle \[rad\] applied for PETS in FODO cell $i\n"
		}
		#Drift -name "PETS_breakdown" -length $cavitylength	
		DecCavityWedge -name "PETS" -length $cavitylength	
		if {[info exists couplerlength]} {  Drift -length $couplerlength }
		Drift -name "drift" -length $coupler_drift
	    } 
	}
    }
