proc make_beam_many {name nslice n} {
    global charge e_initial match n_total
    set ch {1.0}
    calc beam.dat $charge -3 3 $match(sigma_z) $nslice
    InjectorBeam $name -bunches 1 \
	    -macroparticles [expr $n] \
	    -particles $n_total \
	    -energyspread [expr 0.01*$match(e_spread)*$e_initial] \
	    -ecut 3.0 \
	    -e0 $e_initial \
	    -file beam.dat \
	    -chargelist $ch \
	    -charge 1.0 \
	    -phase 0.0 \
	    -overlapp [expr -390*0.3/1.3] \
	    -distance [expr 0.3/1.3] \
	    -alpha_y $match(alpha_y) \
	    -beta_y $match(beta_y) \
	    -emitt_y $match(emitt_y) \
	    -alpha_x $match(alpha_x) \
	    -beta_x $match(beta_x) \
	    -emitt_x $match(emitt_x)
}

