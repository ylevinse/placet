B = placet_get_number_list("default", "bpm");
C = placet_get_number_list("default", "quadrupole");

R0 = placet_get_response_matrix("default", "beam0", B, C);
R1 = placet_get_response_matrix("default", "beam1", B, C) - R0;
R2 = placet_get_response_matrix("default", "beam2", B, C) - R0;

save -text R0.dat R0
save -text R1.dat R1
save -text R2.dat R2
