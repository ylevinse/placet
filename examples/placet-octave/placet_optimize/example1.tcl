set script_dir .

source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl

set e_initial 9.0
set e0 $e_initial

BeamlineNew
source $script_dir/clic_linac.tcl
BeamlineSet -name linac

array set match {beta_x 6.6868 beta_y 2.7269 alpha_x -1.7211 alpha_y 0.7851}

set match(emitt_x) 6.8
set match(emitt_y) 0.1
set match(e_spread) 2.0
set match(charge) 2.56e9
set charge $match(charge)
set match(sigma_z) 30.8

set n_slice 31
set n 9
set n_total 10000

#
# Create the beam
#

make_beam_slice beam0 $n_slice $n

Octave {

  #
  # here we select a sub-set of correctors
  #
  
  Q=placet_get_number_list("beamline", "quadrupole");
  Q=Q(1:100:end)

  #
  # here we scatter the quadrupoles
  #
  
  placet_element_set_attribute("beamline", Q, "y", 10 * randn(size(Q)));

  #
  # here we define the figure of merit
  #

  function E=final_emittance(beamline)
    emitt=placet_test_no_correction(beamline, "beam0", "None");
    E=emitt(end,6);
  end
  
  #
  # here we optimize using the correctors
  # 
  
  optimum=placet_optimize("beamline", "final_emittance", Q, "y", ones(size(Q)))
}
