B = placet_get_number_list("default", "bpm");
C = placet_get_number_list("default", "quadrupole");

#R0 = placet_get_response_matrix("default", "beam0", B, C);
#R1 = placet_get_response_matrix("default", "beam1", B, C) - R0;
#R2 = placet_get_response_matrix("default", "beam2", B, C) - R0;

#save -text R0.dat R0
#save -text R1.dat R1
#save -text R2.dat R2

load R0.dat
load R1.dat
load R2.dat

w = [1, 100, 100];

R0 *= w(1);
R1 *= w(2);
R2 *= w(3);

R = cat(1, R0, R1, R2);

E = 0;

for i=1:20

  placet_test_no_correction("default", "beam0", "my_survey");
  b0 = placet_get_bpm_readings("default", B);

  placet_test_no_correction("default", "beam1", "None");
  b1 = placet_get_bpm_readings("default", B) - b0;

  placet_test_no_correction("default", "beam2", "None");
  b2 = placet_get_bpm_readings("default", B) - b0;

  b0 *= w(1);
  b1 *= w(2);
  b2 *= w(3);

  b = cat(1, b0, b1, b2);
  b = b(:,2);

  c = -R \ b;

  c = [zeros(size(c)),c];

  placet_vary_corrector("default", C, c);

  E += placet_test_no_correction("default", "beam0", "None"); 

endfor

E /= 20;

save -text measured.dat E
