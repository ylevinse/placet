BeamlineNew
Girder
Quadrupole -length 123 -strength 321
Drift -length 10
Bpm -resolution 1.0
Quadrupole -length 23 -strength -321
Drift -length 12
BeamlineSet -name beamline1

BeamlineNew
Girder
Quadrupole
Drift
Bpm
Quadrupole
Drift
BeamlineSet -name beamline2

Octave {
    BL1 = placet_element_get_attributes("beamline1");
    save -text status1.dat BL1
    placet_element_set_attributes("beamline2", BL1);
    BL2 = placet_element_get_attributes("beamline2")  
}

Octave