% some functions to calculate statistics
% and some helper functions
%

%----------------------------------------
% calculates the mean of weighted values
% the weight could be, e.g., the charge of a particle
function m=mean(x,w)
sx=size(x);
sw=size(w);

if (sx(1)!=1)&&(sx(2)!=1)
  m=0.0;
  return;
end;

if (sx(1)<sx(2))
  x=x.';
end;

if (sw(1)>sw(2))
  w=w.';
end;

if ((sw(1)==1)&&(sw(2)==1))
  m=sum(x)/length(x);
else
  m=w*x/sum(w);
end;
endfunction;
%----------------------------------------

%----------------------------------------
%calculates the rms of weighted values
%the weight could be, e.g., the charge of a particle
function r=rms(x,w)
xm=mean(x,w);
sx=size(x);
sw=size(w);

if (sx(1)!=1)&&(sx(2)!=1)
  r=0.0;
  return;
end;

if (sx(1)<sx(2))
  x=x.';
end;

if (sw(1)>sw(2))
  w=w.';
end;

if ((sw(1)==1)&&(sw(2)==1))
  r=sqrt(sum((x-xm).^2)/length(x));
else
  r=sqrt(w*((x-xm).^2)/sum(w));
end;
endfunction;
%----------------------------------------

%----------------------------------------
% calculates the non-normalized projected emittance
function e=emitproj(x,xp,w)
sw=size(w);
sx=size(x);
sxp=size(xp);
if ((sx(1)!=sxp(1))&&(sx(2)!=sxp(2)))
  e=0;
  return;
end;
if (sx(1)>sx(2))
  x=x.';
  xp=xp.';
end;
if ((sw(1)==1)&&(sw(2)==1))
  w=w*ones(sx(2),sx(1));
end;
sw=size(w);
sx=size(x);
if (sw==sx)
  w=w.';
end;

mx=mean(x,w);
vxc=x-mean(x,w);
vyc=xp-mean(xp,w);

e=sqrt(((vxc.*vxc)*w/sum(w))*...
        ((vyc.*vyc)*w/sum(w))-...
        ((vxc.*vyc)*w/sum(w))^2);
endfunction;
%----------------------------------------

%----------------------------------------
function bn=units_std_to_placet(bo)
bn(:,1)=bo(:,1)/1e9;
bn(:,2)=bo(:,2)/1e-6;
bn(:,3)=bo(:,3)/1e-6;
bn(:,4)=bo(:,4)/1e-6;
bn(:,5)=bo(:,5)/1e-6;
bn(:,6)=bo(:,6)/1e-6;
endfunction;
%----------------------------------------

%----------------------------------------
function bn=units_placet_to_std(bo)
bn(:,1)=bo(:,1)*1e9;
bn(:,2)=bo(:,2)*1e-6;
bn(:,3)=bo(:,3)*1e-6;
bn(:,4)=bo(:,4)*1e-6;
bn(:,5)=bo(:,5)*1e-6;
bn(:,6)=bo(:,6)*1e-6;
endfunction;
%----------------------------------------

%----------------------------------------
function print_mean_rms(beam)
dee=beam(:,1)/mean(beam(:,1),1)-1;
gamma=mean(beam(:,1),1)/(0.5109989*1e6);

printf("mean de=%3.15e\n",mean(beam(:,1),1));
printf("mean x =%3.15e\n",mean(beam(:,2),1));
printf("mean y =%3.15e\n",mean(beam(:,3),1));
printf("mean z =%3.15e\n",mean(beam(:,4),1));
printf("mean xp=%3.15e\n",mean(beam(:,5),1));
printf("mean yp=%3.15e\n",mean(beam(:,6),1));
printf("rms de=%3.15e\n",rms(beam(:,1),1));
printf("rms de/mean de=%3.15e\n",rms(beam(:,1),1)/mean(beam(:,1),1));
printf("rms x =%3.15e\n",rms(beam(:,2),1));
printf("rms y =%3.15e\n",rms(beam(:,3),1));
printf("rms z =%3.15e\n",rms(beam(:,4),1));
printf("rms xp=%3.15e\n",rms(beam(:,5),1));
printf("rms yp=%3.15e\n",rms(beam(:,6),1));
printf("emit x=%3.15e\n",gamma*emitproj(beam(:,2),beam(:,5),1));
printf("emit y=%3.15e\n",gamma*emitproj(beam(:,3),beam(:,6),1));

endfunction;
%----------------------------------------

