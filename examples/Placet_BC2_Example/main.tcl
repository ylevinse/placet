# this is an example for particle tracking through a bunch compressor
#
# the example consists of four files:
# main.tcl (this file)
# lattice.tcl (setup of the beam line)
# beamsetup.tcl (creation of a particle distribution and
#                initialisation of Placet beam)
# octave_beam_statistics.m (some octave functions for statistics, etc.)
#
# no other files are required to run this example
#

#
#setup of lattice
#
BeamlineNew
source ./lattice.tcl
BeamlineSet -name rtml

#
#setup of the beam
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigma]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1]
# unit conversions are done later when needed

set beamparams(betax) 140.0
set beamparams(alphax) 3.2
set beamparams(emitnx) [expr 580.0*1e-9]
set beamparams(betay) 140.0
set beamparams(alphay) 3.2
set beamparams(emitny) [expr 6.0*1e-9]
set beamparams(sigmaz) [expr 175.0*1e-6]
set beamparams(charge) [expr 0.65*1e-9]
set beamparams(uncespread) [expr 3.16*1e-3]
set beamparams(echirp) 69.6
set beamparams(energy) [expr 9.0*1e9]
set beamparams(nslice) 100
set beamparams(nmacro) 100
set beamparams(nsigma) 4

source ./beamsetup.tcl
make_particle_beam beam1 beamparams

# store initial phase space distribution in file
BeamDump -beam beam1 -file bunch_initial.dat

#
# here we do everything, e.g. tracking, plotting, etc. in octave
#
Octave {
source("octave_beam_statistics.m");
% read in the initial Placet beam for usage in Octave
beam=placet_get_beam("beam1");
beam=units_placet_to_std(beam);
dee=beam(:,1)/mean(beam(:,1),1)-1;

print_mean_rms(beam);

% The old octave version does not seem to be able to
% print plots to files. Hence, I use a workaround
%gset terminal postscript color
%gset output "s-de_initial.ps"
%data=[-beam(:,4)*1e6,dee];
%gplot data u 1:2 w p

%plot(-beam(:,4),dee,".");
%sleep(3);


% track beam
[emitt,beam]=placet_test_no_correction("rtml","beam1","None");
beam=units_placet_to_std(beam);
dee=beam(:,1)/mean(beam(:,1),1)-1;

print_mean_rms(beam);

% The old octave version does not seem to be able to
% print plots to files. Hence, I use a workaround
%gset terminal postscript color
%gset output "s-de_final.ps"
%data=[-beam(:,4)*1e6,dee];
%gplot data u 1:2 w p

%figure();
%plot(-beam(:,4),dee,".");
%sleep(3);

r=placet_get_transfer_matrix("rtml",0,7)
}
# end of Octave part

# store final phase space distribution in file
BeamDump -beam beam1 -file bunch_final.dat
