proc nix { } { }

proc entry_line_2 { name label lwidth ewidth command var } {
    frame $name
    label $name.label -text $label -width $lwidth -anchor w
    eval {entry $name.entry -relief sunken} -textvariable $var -width $ewidth
    pack $name.label -side left
    pack $name.entry -side right -fill x -expand true
    bind $name.entry <Return> $command 
    return $name.entry
}

proc beamline_printit {x y} {
    global beamline_plot_data
#    set x [.c canvasx $x]
#    set y [.c canvasx $y]
    set obj [.beamline.c find closest $x $y]
    set l [.beamline.c coords $obj]
    set x0 [lindex $l 0]
    set y0 [lindex $l 1]
    set x1 [lindex $l 2]
    set y1 [lindex $l 3]
    set ymean [expr (-0.5*($y0+$y1)+$beamline_plot_data(y0))/ \
	    $beamline_plot_data(yscale)]
#    puts "$ymean"
    .beamline.frame3.posc configure -text $ymean
}

proc plot_beamline {no} {
    BeamlineShow plot .beamline.c -first $no
    .beamline.c bind all <Button-1> {beamline_printit %x %y}
    update
}

proc BeamlineView {} {
    global beamline_plot_data
    toplevel .beamline
    canvas .beamline.c
    array set tmp [BeamlineInfo]
    scale .beamline.s1 -from 0 -to [expr $tmp(n_element)-1] -length 650 \
	    -orient horizontal\
	    -tickinterval 2000 -command plot_beamline -label element
    .beamline.s1 configure -repeatinterval  10

    set beamline_plot_data(yscale) 1.0
    set beamline_plot_data(sscale) 30.0
    set beamline_plot_data(y0) 100.0
    set beamline_plot_data(quadrupoles) 1
    set beamline_plot_data(drifts) 0
    set beamline_plot_data(cavities) 1
    set beamline_plot_data(bpms) 1
    
    BeamlineShow configure .beamline.c -yscale $beamline_plot_data(yscale) \
	-sscale $beamline_plot_data(sscale) \
	-y0 $beamline_plot_data(y0) \
	-bpms $beamline_plot_data(bpms) \
	-quadrupoles $beamline_plot_data(quadrupoles) \
	-cavities $beamline_plot_data(cavities) \
	-drifts $beamline_plot_data(drifts)
    
    pack .beamline.c -fill both -expand 1
    pack .beamline.s1 -fill both

    set w .beamline.frame3
    frame $w
    pack $w

    label $w.pos -text position
    label $w.posc -text "" -width 10
    pack $w.pos $w.posc -side left

    set w .beamline.frame2
    frame $w
    pack $w
    
    entry_line_2 $w.e1 y0 6 8 \
	    {BeamlineShow plot .beamline.c -y0 $beamline_plot_data(y0)} \
	    beamline_plot_data(y0)
    entry_line_2 $w.e2 yscale 6 8 \
	    {BeamlineShow plot .beamline.c -yscale $beamline_plot_data(yscale)} \
	    beamline_plot_data(yscale)
    entry_line_2 $w.e3 sscale 6 8 \
	    {BeamlineShow plot .beamline.c -sscale $beamline_plot_data(sscale)} \
	    beamline_plot_data(sscale)
    pack $w.e1 -side left -padx 20
    pack $w.e2 -side left -padx 20
    pack $w.e3 -side left -padx 20

    set w .beamline.frame1
    frame $w
    pack $w

    foreach {i t} {quadrupoles quadrupoles bpms BPMs cavities cavities \
	    drifts drifts} {
	checkbutton $w.$i -text $t \
		-variable beamline_plot_data($i) \
		-command "{BeamlineShow} plot .beamline.c -$i \$beamline_plot_data($i)"
	pack $w.$i -side left
    }
    button .beamline.bend -text OK -command {
	destroy .beamline
	update
    }
    pack .beamline.bend
    button .beamline.print -text Print -command {
	.beamline.c postscript -file "tmp.ps"
	update
    }
    pack .beamline.bend
    update
    tkwait window .beamline
}

#set beam_film_data(stop) 1

proc beam_printit {x y} {
    global beam_film_data
#    set x [.c canvasx $x]
#    set y [.c canvasx $y]
    set obj [.beamxxx.c find closest $x $y]
    set l [.beamxxx.c coords $obj]
    set x0 [lindex $l 0]
    set y0 [lindex $l 1]
    set x1 [lindex $l 2]
    set y1 [lindex $l 3]
    set ymean [expr (-0.5*($y0+$y1)+$beam_film_data(y0))/ \
	    $beam_film_data(yscale)]
    set ysigma [expr 0.5*abs($y0-$y1)/$beam_film_data(yscale)]
#    puts "$ymean +/- $ysigma"
    .beamxxx.frame1.posc configure -text $ymean
    .beamxxx.frame1.sizec configure -text $ysigma
}

proc BeamFilmPlot {no} {
    global beam_film_data frank_file
    BeamShow plot .beamxxx.c -first $no
    set frank_file [open tmp.ps a]
    .beamxxx.c postscript -channel $frank_file
    close $frank_file
}

proc BeamFilmInit {} {
    global beam_film_data frank_file
# frank
    set frank_file [open tmp.ps w]
    close $frank_file

    toplevel .beamxxx
    canvas .beamxxx.c
    scale .beamxxx.s1 -from 0 -to 1300 -length 650 -orient horizontal\
	    -tickinterval 200 -command BeamFilmPlot -label bunch
    .beamxxx.s1 configure -repeatinterval  10

    set beam_film_data(yscale) 1.0
    set beam_film_data(slicelength) 10.0
    set beam_film_data(bunchdistance) 50.0
    set beam_film_data(y0) 100.0
    set beam_film_data(axis) 1
    
    BeamShow configure .beamxxx.c -yscale $beam_film_data(yscale) \
	-slicelength $beam_film_data(slicelength) \
	-bunchdistance $beam_film_data(bunchdistance) \
	-y0 $beam_film_data(y0) \
	-centre $beam_film_data(axis)

    pack .beamxxx.c -fill both -expand 1
    pack .beamxxx.s1 -fill both

    set w .beamxxx.frame2
    frame $w
    pack $w
    entry_line_2 $w.e1 y0 8 8 \
	    {BeamShow plot .beamxxx.c -y0 $beam_film_data(y0)} \
	    beam_film_data(y0)
    entry_line_2 $w.e2 yscale 8 8 \
	    {BeamShow plot .beamxxx.c -yscale $beam_film_data(yscale)} \
	    beam_film_data(yscale)
    entry_line_2 $w.e3 zscale 8 8 \
	    {BeamShow plot .beamxxx.c -slicelength $beam_film_data(slicelength)} \
	    beam_film_data(slicelength)
    entry_line_2 $w.e4 distance 8 8 \
	    {BeamShow plot .beamxxx.c \
	    -bunchdistance $beam_film_data(bunchdistance)} \
	    beam_film_data(bunchdistance)
    pack $w.e1 -side left -padx 20
    pack $w.e2 -side left -padx 20
    pack $w.e3 -side left -padx 20
    pack $w.e4 -side left -padx 20

    checkbutton $w.axis -text axis \
	-variable beam_film_data(axis) \
	-command "{BeamShow} plot .beamxxx.c -centre \$beam_film_data(axis)"
    pack $w.axis -side left
    
    set w .beamxxx.frame1
    frame $w
    pack $w

    label $w.pos -text position
    label $w.posc -text "" -width 10
    label $w.size -text size
    label $w.sizec -text "" -width 10
    pack $w.pos $w.posc $w.size $w.sizec -side left

    button .beamxxx.stop -text stop \
	    -command {tkwait variable beam_film_data(stop)}
    button .beamxxx.cont -text continue \
	    -command {set beam_film_data(stop) 0}
    button .beamxxx.step -text step \
	    -command {set beam_film_data(stop) 1}
    pack .beamxxx.stop -side left
    pack .beamxxx.cont -side left
    pack .beamxxx.step -side left

    .beamxxx.c bind all <Button-1> {beam_printit %x %y}
    set beam_film_data(stop) 1
    tkwait visibility .beamxxx
    update
    BeamFilmPlot 0
}

set beam_film_data(first) 1
set beam_film_data(count) 0

proc BeamViewFilm {} {
    global beam_film_data
#    puts $beam_film_data(count)
    incr beam_film_data(count)
    if {$beam_film_data(first)} {
	set beam_film_data(first) 0
	BeamFilmInit
    }
    BeamShow plot .beamxxx.c
    if {$beam_film_data(stop)==1} {
	tkwait variable beam_film_data(stop)
    }
    update
#    set frank_file [open tmp.ps a]
#    puts hier
#    .beamxxx.c postscript -channel $frank_file
#    close $frank_file
}
