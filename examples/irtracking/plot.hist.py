import pylab as p

#d1=p.loadtxt('particles.out.ffs')
#d2=p.loadtxt('electrons.before.ffs.out')

d1=p.loadtxt('particle.dist.no_solenoid.out')
d2=p.loadtxt('particle.dist.solenoid_and_synrad.out')

print d1.shape

def plot(c,title,fname):
    p.figure()
    p.title(title)
    n,bins,garbage=p.hist(d2[:,c],len(d2[:,c])/20,histtype='step',normed=True,align='mid',label='Sol&Synrad')
    p.hist(d1[:,c],bins,histtype='step',normed=True,align='mid',label='Ideal')
    p.legend()
    p.savefig(fname+'.pdf')

plot(1,'x','hist_x')
plot(2,'y','hist_y')
plot(3,'z','hist_z')
plot(4,'xp','hist_xp')
plot(5,'yp','hist_yp')

#p.show()
