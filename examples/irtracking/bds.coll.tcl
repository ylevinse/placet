Girder
Quadrupole -name "FQF" -synrad $quad_synrad -length 1 -strength [expr 0.0367475361*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "FDD" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "FQD" -synrad $quad_synrad -length 1 -strength [expr -0.0406008999*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "FDD" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "FQF2" -synrad $quad_synrad -length 1 -strength [expr -0.0390222116*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "FDD" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "FQD2" -synrad $quad_synrad -length 1 -strength [expr 0.00644991447*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF" -synrad $quad_synrad -length 1 -strength [expr 0.05749033421*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD" -synrad $quad_synrad -length 2 -strength [expr -0.05899319025*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF2A" -synrad $quad_synrad -length 1 -strength [expr 0.05168697685*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF2B" -synrad $quad_synrad -length 1 -strength [expr 0.05168697685*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD2A" -synrad $quad_synrad -length 1 -strength [expr -0.04249731134*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD2B" -synrad $quad_synrad -length 1 -strength [expr -0.04249731134*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF3A" -synrad $quad_synrad -length 1 -strength [expr 0.02830204031*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF3B" -synrad $quad_synrad -length 1 -strength [expr 0.02830204031*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD3A" -synrad $quad_synrad -length 1 -strength [expr -0.03962918848*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD3B" -synrad $quad_synrad -length 1 -strength [expr -0.03962918848*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD2" -length 2.048958333 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF4A" -synrad $quad_synrad -length 1 -strength [expr 0.0519502144*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF4B" -synrad $quad_synrad -length 1 -strength [expr 0.0519502144*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD4A" -synrad $quad_synrad -length 1 -strength [expr -0.04494646211*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD4B" -synrad $quad_synrad -length 1 -strength [expr -0.04494646211*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF5A" -synrad $quad_synrad -length 1 -strength [expr 0.02882318839*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF5B" -synrad $quad_synrad -length 1 -strength [expr 0.02882318839*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DD" -length 4.4375 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD5" -synrad $quad_synrad -length 2 -strength [expr -0.0553809776*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF6A" -synrad $quad_synrad -length 1 -strength [expr 0.01306276342*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF6B" -synrad $quad_synrad -length 1 -strength [expr 0.01306276342*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD6A" -synrad $quad_synrad -length 1 -strength [expr -0.01001168349*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD6B" -synrad $quad_synrad -length 1 -strength [expr -0.01001168349*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF7A" -synrad $quad_synrad -length 1 -strength [expr 0.00997737175*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF7B" -synrad $quad_synrad -length 1 -strength [expr 0.00997737175*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD7A" -synrad $quad_synrad -length 1 -strength [expr -0.00997737175*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD7B" -synrad $quad_synrad -length 1 -strength [expr -0.00997737175*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF8A" -synrad $quad_synrad -length 1 -strength [expr 0.01004313095*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF8B" -synrad $quad_synrad -length 1 -strength [expr 0.01004313095*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL" -length 9.088783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD8" -synrad $quad_synrad -length 2 -strength [expr -0.0519004528*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDL89" -length 7.588783547 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQF9" -synrad $quad_synrad -length 2 -strength [expr -0.0762238127*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DDLE" -length 4.040933635 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "TQD9" -synrad $quad_synrad -length 2 -strength [expr 0.0673954265*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
# WARNING: Multipole options not defined. Multipole type 0 with 0 strength added (tracked as a drift of length 0).
Girder
Multipole -name "SENTR" -synrad $mult_synrad -type 0 -length 0 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DUTIL" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 2.5 -strength [expr 0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.02432 -aperture_y 0.02432
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 2.5 -strength [expr 0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.02432 -aperture_y 0.02432
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "YGIRDER" -length 5 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.625 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM2" -length 0.625 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 2.5 -strength [expr -0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 2.5 -strength [expr -0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DUTIL" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "OCTEC3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-8.686414823*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DUTIL" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 2.5 -strength [expr -0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 2.5 -strength [expr -0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.625 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "ENGYSP" -length 0 -aperture_shape rectangular -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM2" -length 0.625 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr 0.002648407832*$e0] -aperture_shape elliptic -aperture_x 0.02412 -aperture_y 0.02412
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr 0.002648407832*$e0] -aperture_shape elliptic -aperture_x 0.02412 -aperture_y 0.02412
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr 0.002648407832*$e0] -aperture_shape elliptic -aperture_x 0.02412 -aperture_y 0.02412
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr 0.002648407832*$e0] -aperture_shape elliptic -aperture_x 0.02412 -aperture_y 0.02412
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 2.5 -strength [expr 0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.02432 -aperture_y 0.02432
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 2.5 -strength [expr 0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.02432 -aperture_y 0.02432
Girder
Drift -name "DUTIL" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "ENGYAB" -length 0 -aperture_shape rectangular -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DUTIL" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 2.5 -strength [expr 0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.02432 -aperture_y 0.02432
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 2.5 -strength [expr 0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.02432 -aperture_y 0.02432
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr 0.002648407832*$e0] -aperture_shape elliptic -aperture_x 0.02412 -aperture_y 0.02412
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr 0.002648407832*$e0] -aperture_shape elliptic -aperture_x 0.02412 -aperture_y 0.02412
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr 0.002648407832*$e0] -aperture_shape elliptic -aperture_x 0.02412 -aperture_y 0.02412
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr 0.002648407832*$e0] -aperture_shape elliptic -aperture_x 0.02412 -aperture_y 0.02412
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.625 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM2" -length 0.625 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.02321 -aperture_y 0.02321
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -2.798627536e-06 -E2 -2.798627536e-06 -aperture_shape elliptic -aperture_x 0.0238 -aperture_y 0.0238
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 2.5 -strength [expr -0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 2.5 -strength [expr -0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DUTIL" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "OCTEC4" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-0.1758651044*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DUTIL" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 2.5 -strength [expr -0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 2.5 -strength [expr -0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 1.25 -strength [expr -0.006897006729*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.625 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM2" -length 0.625 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 1.2606911845e-05 -E2 1.2606911845e-05 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DSXL" -length 5 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM" -length 1.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 2.5 -strength [expr 0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.02432 -aperture_y 0.02432
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 2.5 -strength [expr 0.001117829009*$e0] -aperture_shape elliptic -aperture_x 0.02432 -aperture_y 0.02432
Girder
Drift -name "DUTIL" -length 10 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BC" -length 0.2 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 1 -strength [expr 0.02582599304*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 1 -strength [expr 0.02582599304*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM0" -length 0.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 1 -strength [expr 0.02582599304*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 1 -strength [expr 0.02582599304*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D2BC" -length 0.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 1 -strength [expr -0.02103870081*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 1 -strength [expr -0.02103870081*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM0" -length 0.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 1 -strength [expr -0.02103870081*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 1 -strength [expr -0.02103870081*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM0" -length 0.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 1 -strength [expr -0.02103870081*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 1 -strength [expr -0.02103870081*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM0" -length 0.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 1 -strength [expr -0.02103870081*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 1 -strength [expr -0.02103870081*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BC" -length 23.26703 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 1 -strength [expr 0.006796697944*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 1 -strength [expr 0.006796697944*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "DMM0" -length 0.25 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 1 -strength [expr 0.006796697944*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 1 -strength [expr 0.006796697944*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D4BC" -length 0.385153 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Multipole -name "SEXIT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -20*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BCOL" -length 0.43201 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BCOL" -length 35.16752 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D2BCOL" -length 15.82195 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03394621952*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03394621952*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "YSP1" -length 0 -aperture_shape rectangular -aperture_x 0.01 -aperture_y 8e-05
Girder
Drift -name "D2BCOL" -length 15.82195 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "XSP1" -length 0 -aperture_shape rectangular -aperture_x 8e-05 -aperture_y 0.01
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BCOL" -length 35.16752 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BCOL" -length 0.43201 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BCOL" -length 0.43201 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BCOL" -length 35.16752 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "XAB1" -length 0 -aperture_shape elliptic -aperture_x 0.001 -aperture_y 0.001
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D2BCOL" -length 15.82195 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "YAB1" -length 0 -aperture_shape elliptic -aperture_x 0.001 -aperture_y 0.001
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03394621952*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03394621952*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "YSP2" -length 0 -aperture_shape rectangular -aperture_x 0.01 -aperture_y 8e-05
Girder
Drift -name "D2BCOL" -length 15.82195 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "XSP2" -length 0 -aperture_shape rectangular -aperture_x 8e-05 -aperture_y 0.01
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BCOL" -length 35.16752 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BCOL" -length 0.43201 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BCOL" -length 0.43201 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BCOL" -length 35.16752 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "XAB2" -length 0 -aperture_shape elliptic -aperture_x 0.001 -aperture_y 0.001
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D2BCOL" -length 15.82195 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "YAB2" -length 0 -aperture_shape elliptic -aperture_x 0.001 -aperture_y 0.001
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03394621952*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03394621952*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "YSP3" -length 0 -aperture_shape rectangular -aperture_x 0.01 -aperture_y 8e-05
Girder
Drift -name "D2BCOL" -length 15.82195 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "XSP3" -length 0 -aperture_shape rectangular -aperture_x 8e-05 -aperture_y 0.01
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BCOL" -length 35.16752 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BCOL" -length 0.43201 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BCOL" -length 0.43201 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BCOL" -length 35.16752 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "XAB3" -length 0 -aperture_shape elliptic -aperture_x 0.001 -aperture_y 0.001
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D2BCOL" -length 15.82195 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "YAB3" -length 0 -aperture_shape elliptic -aperture_x 0.001 -aperture_y 0.001
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03394621952*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03394621952*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "YSP4" -length 0 -aperture_shape rectangular -aperture_x 0.01 -aperture_y 8e-05
Girder
Drift -name "D2BCOL" -length 15.82195 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "XSP4" -length 0 -aperture_shape rectangular -aperture_x 8e-05 -aperture_y 0.01
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BCOL" -length 35.16752 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BCOL" -length 0.43201 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D3BCOL" -length 0.43201 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 1 -strength [expr -0.03033330069*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D1BCOL" -length 35.16752 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "XAB4" -length 0 -aperture_shape elliptic -aperture_x 0.001 -aperture_y 0.001
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 1 -strength [expr 0.02204757678*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "D2BCOL" -length 15.82195 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "YAB4" -length 0 -aperture_shape elliptic -aperture_x 0.001 -aperture_y 0.001
Girder
Drift -name "BTFD0" -length 0.0507269 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 5 -strength [expr -0.06049965*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "BTFD1" -length 8.712693 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 5 -strength [expr 0.01522023*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "BTFD2" -length 19.9951 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 5 -strength [expr 0.02524031*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "BTFD3" -length 19.97658 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 5 -strength [expr -0.03331435*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "BTFD4" -length 0.0525519 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "LMD4" -length 5.679712069 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QMD11" -synrad $quad_synrad -length 0.815 -strength [expr 0.04523032047*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QMD11" -synrad $quad_synrad -length 0.815 -strength [expr 0.04523032047*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "LMD5" -length 3.459567145 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QMD12" -synrad $quad_synrad -length 0.815 -strength [expr -0.07114828192*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QMD12" -synrad $quad_synrad -length 0.815 -strength [expr -0.07114828192*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "LMD6" -length 5.868 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QMD13" -synrad $quad_synrad -length 0.815 -strength [expr 0.05474534674*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QMD13" -synrad $quad_synrad -length 0.815 -strength [expr 0.05474534674*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "LMD7" -length 5.868 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QMD14" -synrad $quad_synrad -length 0.815 -strength [expr -0.02509775333*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QMD14" -synrad $quad_synrad -length 0.815 -strength [expr -0.02509775333*$e0] -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Drift -name "LMD8" -length 5.627249 -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008
Girder
Quadrupole -name "QF8" -synrad $quad_synrad -length 0.807339 -strength [expr -0.0001314726906*$e0] -aperture_shape elliptic -aperture_x 0.003 -aperture_y 0.003
