Two different field maps are given, ildantinobuck.txt and SiD+antiDID_2005.txt.

The format of the field maps is "x y z Bx By Bz". Units are metres and Tesla.

The main script is trackbds_ffsmy.tcl.

There are certain input arguments valid. Note that if you run with a total of 1 particles, 
the particle trajectory will also be written in singtrk files:

  placet trackbds_ffsmy.tcl n 1 n_slice 1
