\documentclass[11pt,a4paper,dvips]{article}
%\setlength{\topmargin}{1cm}

\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{graphicx}
\usepackage{subfig}
%\usepackage{lcd}
%\usepackage{lcd_title,ifthen}
\usepackage{mathptmx}
\usepackage{helvet}
\usepackage{amsmath}
\usepackage{color}
\usepackage{units}
\usepackage{url}
\usepackage{array,booktabs}
\usepackage{authblk}
%\usepackage{epstopdf}

\title{An implementation of 1D CSR shielding in PLACET.}
\date{April 30, 2015}

% Author(s) of the paper
\author[1]{J. Esberg}
\author[1]{A. Latina}
\author[2]{R. Apsimon}
\author[1]{D. Schulte}
% Affiliations
\affil[1]{CERN, Switzerland}
\affil[2]{Lancaster University, United Kingdom}

\begin{document}
\maketitle
%
% Give date: \today for drafts, a fixed date for final papers
\date{\today}

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main part
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

\section{CSR shielding in PLACET}
The implementation relies on \cite{mayes09} to calculate the modification of the wake due to shielding. An assumption of this model is that the bunch is a one dimensional rod of charge. This is valid if $\sigma_x \ll \sqrt{3}{l_b^2/\kappa}$. Furthermore, shielding is modeled by perfectly conducting parallel plates separated by a distance $H$ between which the beam travels in a circular path on a plane parallel to the plates. PLACET uses an ultra-relativistic approximation throughout, which means that the wake is simplified to
\begin{align}
\frac{dE_{shield}}{ds}(s)= &2N r_cmc^2\sum_{n=1}^Q (-1)^n \left[ \left.\frac{-\kappa \lambda(s_{\alpha,n})}{r_{\alpha,n}}\right|_{\alpha_a}^{\alpha_b} +\int_{\alpha_a}^{\alpha_b} \frac{\cos(\alpha)-1}{r_{\alpha,n}}\frac{d\lambda}{ds}(s_{\alpha,n})d\alpha\right].
\label{theor}
\end{align}
where $\frac{dE_{shield}}{ds}(s)$ is the energy loss per unit length, $r_c$ and $mc^2$ are the classical electron radius, and rest energy, respectively. $\lambda(s)$ is the longitudinal density of electrons normalized to 1. $r_{\alpha,n}=\sqrt{2-2\cos{\alpha}+(n\kappa H)^2}$, $\alpha_a=\kappa(s-L_{mag})$ and $\alpha_b=s$, $s_{\alpha,n}=s-(\alpha-r_{\alpha,n})/\kappa$.
This is the interaction between the bunch and $Q$ image charges in the parallel plates. Only a finite number of image charges can interact with the beam before the end of the magnet. The minimum number of image charges is set so $Q>\frac{R}{H}\sqrt{(\theta+l_b)^2-4\sin^2(\theta/2)}$ since an image charge at a distance larger than $QH$ from the bunch does not have time to interact with the bunch before it has traversed the entire magnet. 

The longitudinal density must be evaluated numerically at all points on the $s$-axis which opens up several options. In practice this is achieved by binning the particles longitudinally and smoothing the distribution using Savitzky-Golay filtering.

\subsection{Accounting for bunch compression}

Since the integration is parametrized in the retarded angle between emitting charge and the pickup charge, $\alpha$, it is straightforward to include the effect of bunch compression on the shielded wake. In PLACET this is done by storing the bunch shape at each slice of the magnet and using the distribution at the relevant retarded angle when evaluating both the integrals and the transient terms.
PLACET evaluates weather to do a 3$\sigma$ cut in teh longitudinal distribution, by doing a normality test of the sample of $z$ coordinates. The cut increases numerical stability. The determination is inspired by Spiegelhalter \cite{spiegel83}, but differs slightly in order to permit cuts on distributions that have got distributions that are only approximately Gaussian.

Spiegalhalter shows that the score statistic of a distribution with respect to a Gaussian null hypothesis is 

\begin{equation}
    W=\sum_{i=1}^N Z_i^2 \ln(Z_i^2)
\end{equation}
where $Z_i=(z_i-<z>)/\mbox{std}(z)$. W is normally distributed with width $\sigma_W=0.8969\sqrt{N}$ if the test distribution is normal. The mean value of W is $0.7301N$. $W$ is obtains its biggest deviations from the mean from contributions in the tail of the z sample, but also if the z distribution is heavily centered. Thus, we can choose metric for the Gaussian nature of the longitudinal distribution to be

\begin{equation}
    p_{pseudo}=1-\mbox{abs}\left|\mbox{erf}\left(\frac{W-0.7301N}{\sqrt{2}\cdot0.8969N}\right)\right|
\end{equation}

The shape is assumed Gaussian if $p_{pseudo}>0.9$. If this requirement is not fulfilled, PLACET throws a warning (see \ref{numstab})

\subsection{Accounting for chamber width}
The effect of the width of the chamber is incluced in a quite simple manner through a cut in the accepted integration limits of Eq. \eqref{theor}. The integration is stopped if the retarded angle is large enough that no line of sight exists between the emitting image charge and the test particle in the bunch. This is fulfilled if $\alpha > 2 \cos^{-1}(1-\kappa w/2)$ where $w$ is the full beam chamber width. The magnitude of the shielding wake is dominated by the smallest dimension of a rectangular beam chamber \cite{sagan09}. Since the PLACET implementation of the chamber width is quite simple, the program only accepts chamber widths that are twice as large as the chamber height.

\subsection{PLACET interface}
Some parameters are re-used, but take on an additional role in the shieldig case.
\begin{figure}
\begin{tabular}{l | >{\centering\arraybackslash}m{2cm}  >{\centering\arraybackslash}m{2cm}  >{\arraybackslash}m{5cm}}
%\begin{tabular}{l p{2cm} p{2cm} p{5cm}}
          Name & No shielding & Shielding & Description\\
          \hline
\texttt{csr} & x & x & Enable csr.\\
\hline
\texttt{csr\_charge} & x & x & Total bunch charge [C].\\
\hline
\texttt{csr\_nbins} & x & x & Number of bins used to evaluate longitudinal distribution and is derivative.\\
\hline
\texttt{csr\_filterorder} & x & x & Order of polynomial used for Savitzky-Golay filtering.\\
\hline
\texttt{csr\_nhalffilter} & x & x & Number of bins used on either side of a bin for Savitzky-Golay filtering.\\
\hline
\texttt{csr\_nsectors} & x & x & Number of sectors the magnet is split into ($\propto 1/\Delta s$).\\
\hline
\texttt{csr\_shielding} & & x & Switch on csr shielding.\\
\hline
\texttt{csr\_shielding\_n\_images} & & x & Minimum number of images charges (on one side of the plates) used by shielding. This is at the magnet entrance modified at the entrance to the magnet to \texttt{csr\_shielding\_n\_images}$>\frac{R}{H}\sqrt{(\theta+l_b)^2-4\sin^2(\theta/2)}$ with a warning.\\
\hline
\texttt{csr\_shielding\_height} & & x & Shielding height. [m]\\
\hline
\texttt{enable\_csr\_shielding\_width} & & x & Enable finite chamber width.\\
\hline
\texttt{csr\_shielding\_width} & & x & Width of rectangular chamber. [m]\\
\hline
\end{tabular}
\caption{PLACET input parameters}
\label{parameter_description}
\end{figure}
          
\subsection{Comments about numerical stability}
\label{numstab}
CSR and shielding are both effects that lead to physical beam instabilities. The number of integrals that need to be computed scales linearly with the number of shielding images, so numerical issues are amplified when the parallel plate distance is very small. Thorough tests were made to ensure that simulations converge in a wide range of the parameters described in Table \ref{parameter_description}. When the cut of $\pm$3 standard deviations is made on the longitudinal distrubution, the simulation generally converges. Around $10^5$ macro particles and approximately 1000 longitudinal bins are necessary to ensure accurate simulations. The choice of the number of magnet sectors depends on the parameters of the magnet, but a step size of $<3cm$ should be sufficient for most puposes. When placst warns about the bunch distribition being non-Gaussian, the user is encouraged to verify the stability if simulations by e.g. injecting bunch distributions with different random seeds.

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Style file to use with mcite.
% Use lcdstyle with just cite.
\bibliographystyle{LCD_bibliography/lcd}
\bibliography{LCD_bibliography/lcd}
\begin{thebibliography}{99}
\bibitem{saldin}
E.L. Saldin et \textit{al.}, "On the coherent radiation of an electron bunch moving in an arc of a circle", NIM-A \textbf{398} 373-394 (1997)
\bibitem{PLACET}
PLACET, https://savannah.cern.ch/projects/placet
\bibitem{mayes09} 
C. Mayes and G. Hoffstaetter, "Exact 1D model for coherent synchrotron radiation with shielding and bunch compression" PRST-AB" \textbf{12}, 024401 (2009)
\bibitem{sagan09}
D. Sagan, G. Hoffstaetter, C. Mayes, U. Sae-Ueng, "Extended one-dimensional method for coherent synchrotron radiation including shielding", PRST-AB" \textbf{12}, 040703 (2009)
\bibitem{spiegel83}
"Diagnostic tests of distributional shape", Biometrika, \textbf{70}, 2, pp 401-409 (1983)
\end{thebibliography}
\end{document}

