This command produces a wakefield set consisting of the wavelengths,
 the loss factors and the damping of each mode. It creates a new command with the name of the set to evaluate the wakefields at any given point
