#!/usr/bin/perl

# this script is able to processes a file saved in mad8 using the commands:

# select,optics,full
# optics, beta0=twss3, &
# columns=NAME, CLASS, BETX, BETY, ALFX, ALFY, &
# KEYWORD, TYPE, S, DP, L, RADLOSS, K0L, K1L, K2L,& 
# VOLT, LAG, FREQ, HARMON, TILT, KS, HKICK, VKICK,&
# E1, E2, H1, H2, EFIELD, file="facet_v28E_optics.out"

use constant PI => 4*atan2(1,1);

if (@ARGV < 1) { die "usage:\tmad2placet.pl FILE\n"; }

open(FILE, $ARGV[0]) or die "Could not open file $ARGV[0]\n";

@names = ();
@line = ();

sub search_index  # $i = search_index("ENERGY"); $energy = $line[$i];
{
  my $name = $_[0];
  my $i;
  
  for ($i = 0; $i < @names; $i++) {
    if ($names[$i] eq $name) {
      last;
    }
  }
  return $i;
}

sub search_value  # $energy = search_value("ENERGY");
{
  return $line[search_index($_[0])];
}

print "set sbend_synrad 0\n";
print "set quad_synrad 0\n";
print "set mult_synrad 0\n";
print "\n";

$first_element = 1;

$previous_s = 0.0;

while ($lines = <FILE>) {

  if ($lines =~ /^\*/) {

    @line = split(" ", $lines);

    for ($i=1;$i<@line;$i++) {
      push(@names, $line[$i]);
    }
    
  } elsif ($lines !~ /^[@\*\$]/) {

    @line = split(" ", $lines);

    if ($first_element == 1) {

      $first_element = 0;
   
      my $energy = search_value("ENERGY");

      print "set e0 $energy\n";
      print "set e_initial \$e0\n";
      print "\n";

      my $beta_x = search_value("BETX");
      my $beta_y = search_value("BETY");
      my $alpha_x = search_value("ALFX");
      my $alpha_y = search_value("ALFY");
 
      print "set match(beta_x) $beta_x\n";
      print "set match(beta_y) $beta_y\n";
      print "set match(alpha_x) $alpha_x\n";
      print "set match(alpha_y) $alpha_y\n";
      print "\n";
      print "SetReferenceEnergy \$e0\n";
    }
  
    my $keyword = search_value("KEYWORD");
    my $length = search_value("L");
    my $name = search_value("NAME");
    my $apx = 1.0; # beware, this is in meters
    my $apy = 1.0; # beware, this is in meters
    
    if ($length != search_value("S") - $previous_s) {
      print STDERR "Element $name (type $keyword ) = length mismatch\n";
      $length = search_value("S") - $previous_s;
    }
    
    if ($keyword =~ /DRIFT/) {
      print "Drift -name $name -length $length\n";
    } elsif ($keyword =~ /QUADRUPOLE/) {
      my $tilt = search_value("TILT");
      my $k1l = search_value("K1L");
      print "Quadrupole -name $name -synrad \$quad_synrad -length $length -strength \[expr $k1l*\$e0\]";
      if ($tilt != 0) {
        print " -tilt $tilt";
      }
      print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
    } elsif ($keyword =~ /SEXTUPOLE/) {
      my $tilt = -1.0 * search_value("TILT");
      my $k2l = search_value("K2L");
      print "Multipole -name $name -synrad \$mult_synrad -type 3 -length $length -strength \[expr $k2l*\$e0\]";
      if ($tilt != 0) {
        print " -tilt $tilt";
      }
      print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
    } elsif ($keyword =~ /OCTUPOLE/) {
      my $tilt = -1.0 * search_value("TILT");
      my $k3l = search_value("K3L");
      print "Multipole -name $name -synrad \$mult_synrad -type 4 -length $length -strength \[expr $k3l*\$e0\]";
      if ($tilt != 0) {
        print " -tilt $tilt";
      }
      print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
    } elsif ($keyword =~ /MULTIPOLE/) {
      my $tilt = -1.0 * search_value("TILT");
      my $k0l = search_value("K0L");
      my $k1l = search_value("K1L");
      my $k2l = search_value("K2L");
      my $k3l = search_value("K3L");
      if ($k0l != 0) {
        print "Dipole -name $name -synrad 0 -length $length -strength \[expr -1.0*$k0l*\$e0\]";
        if ($tilt != 0) {
          print " -tilt $tilt";
        }
        print "\n";
      } elsif ($k1l != 0) {
        print "Quadrupole -name $name -synrad 0 -length $length -strength \[expr -1.0*$k1l*\$e0\]";
        if ($tilt != 0)
        {
          print " -tilt $tilt";
        }
        print "\n";
     } elsif ($k2l != 0) {
        print "Multipole -name $name -synrad 0 -type 3 -length $length -strength \[expr -1.0*$k2l*\$e0\]";
        if ($tilt != 0)
        {
          print " -tilt $tilt";
        }
        print "\n";
     } elsif ($k3l != 0) {
        print "Multipole -name $name -synrad 0 -type 4 -length $length -strength \[expr -1.0*$k3l*\$e0\]";
        if ($tilt != 0)
        {
          print " -tilt $tilt";
        }
        print "\n";
      } else {
        print "# WARNING: Multipole options not defined";
        print "\n";
      } 
    } elsif ($keyword =~ /RBEND/) {
      my $k1l = search_value("K1L");
      my $angle = search_value("K0L");
      my $half_angle = $angle * 0.5;
      my $radius = $length / sin($half_angle) / 2;
      my $arc_length = $angle * $radius;
      my $e1 = search_value("E1") + $half_angle;
      my $e2 = search_value("E2") + $half_angle;
      print "# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2\n";
      print "# WARNING: original length was $length\n";
      print "Sbend -name $name -synrad \$sbend_synrad -length $arc_length -angle $angle -e0 \$e0 -E1 $e1 -E2 $e2";
      if ($k1l != 0.0) {
        print " -K \[expr $k1l*\$e0\]";
      }
      print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
    } elsif ($keyword =~ /SBEND/) {
      my $k1l = search_value("K1L");
      my $angle = search_value("K0L");
      my $e1 = search_value("E1");
      my $e2 = search_value("E2");
      print "Sbend -name $name -synrad \$sbend_synrad -length $length -angle $angle -e0 \$e0 -E1 $e1 -E2 $e2";
      if ($k1l != 0.0) {
        print " -K \[expr $k1l*\$e0\]";
      }
      print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
#      set a_old $length
#      set l_old $length
#      if {$comp_loss} {
# 	 print "#set e0 \[expr \$e0-14.1e-6*$length*$length/$length*\$e0*\$e0*\$e0*\$e0\]\n";
#      }
    } elsif ($keyword =~ /MATRIX/) {
#      if {$comp_loss==0} {
#        print "#set e0 \[expr \$e0-\$synrad*14.1e-6*$a_old*$a_old/$l_old*\$e0*\$e0*\$e0*\$e0\]\n";
#      }
    } elsif ($keyword =~ /RFCAVITY/ || $keyword =~ /LCAVITY/) {
      my $voltage = search_value("VOLT") * 1e-3;
      my $gradient = $voltage / $length;
      my $lag = search_value("LAG");
      my $freq = search_value("FREQ");
      my $phase = $lag * 360;
      my $phase_rad = $lag * 2 * PI;
      my $egain = $voltage * cos($phase_rad);
      print "AccCavity -name $name -length $length -gradient $gradient -phase $phase\n";
      print "set e0 \[expr \$e0 + \( $egain \) \]\nSetReferenceEnergy \$e0\n";
    } elsif ($keyword =~ /LCAVITY/) {
      print "AccCavity -name $name -length $length\n";
#      if {$comp_loss==0} {
#        print "#set e0 \[expr \$e0-\$synrad*14.1e-6*$a_old*$a_old/$l_old*\$e0*\$e0*\$e0*\$e0\]\n";
#      }
    } elsif ($keyword =~ /RCOLLIMATOR/) {
      print "# Collimator -name $name -length $length  -aperture_shape rectangular -aperture_x $apx -aperture_y $apy\n";
      if ($length != 0) {
        print "Drift -name $name -length $length\n";
      }
    } elsif ($keyword =~ /ECOLLIMATOR/) {
      print "# Collimator -name $name -length $length -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
      if ($length != 0) {
        print "Drift -name $name -length $length\n";
      }
    } elsif ($keyword =~ /HKICKER/) {
      print "HCORRECTOR -name $name -length $length\n";
      #if ($length != 0) {
      #  print "Drift -name $name -length $length\n";
      #}
    } elsif ($keyword =~ /VKICKER/) {
      print "VCORRECTOR -name $name -length $length\n";
      #if ($length != 0) {
      #  print "Drift -name $name -length $length\n";
      #}
    } elsif ($keyword =~ /MARKER/) {
      if ($length != 0) {
        print "Drift -name $name -length $length\n";
      }
    } elsif ($keyword =~ /MONITOR/ || $keyword =~ /INSTRUMENT/) {
      print "Bpm -name $name -length $length\n";
    } elsif ($keyword =~ /SOLENOID/) {
      my $ksi = search_value("KSI");
      my $ks = $ksi / $length;
      my $bz_e0 = $ks / 0.299792458;
      print "Solenoid -name $name -length $length -bz \[expr $bz_e0*\$e0\]\n";
    } elsif ($keyword =~ /LINE/) {
    } else {
        print "# UNKNOWN: @line\n";
        print "Drift -name $name -length $length\n"; 
    }
    $previous_s = search_value("S");
  } 
}

close(FILE);
