#ifndef libxsif_helper_h
#define libxsif_helper_h

#ifdef __cplusplus  /*  C++ compatibility  */
extern "C" {
#endif

extern signed int get_item_ (const signed int &index );
extern signed int get_ietyp_(const signed int &index );

extern signed int get_npos1_();
extern signed int get_npos2_();

extern signed int get_ietype_(const signed int &index );
extern double get_iedat_(const signed int &index, const signed int &parameter );
extern double get_kelabl_(const signed int &index, char *label, int len );

#ifdef __cplusplus  /*  C++ compatibility  */
}
#endif

#endif /* libxsif_helper_h */
