CC		= gcc
CXX		= g++
F90 		= gfortran

BINDIR  	= ../bin
LIBDIR  	= ../lib

TCL_STUB	= tclstub
TCL_VERSION	= tcl
TK_VERSION	= tk

TCL_LDFLAGS	= -l$(TCL_VERSION) 
TK_LDFLAGS	= -l$(TK_VERSION)

CFLAGS  	= -O3 -m32 -I../include\
		  -pipe -Wno-long-long\
		  -ffast-math -DUSE_NON_CONST \
		  -finline $(GSL_CFLAGS)# -fomit-frame-pointer 

LDFLAGS 	= -ldl
LDFLAGS_SO	= 
