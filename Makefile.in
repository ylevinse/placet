# Placet makefile

prefix = $(DESTDIR)@prefix@
TOP := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
pre_install = $(TOP)/pre_install
exec_prefix = @exec_prefix@
bindir = @bindir@
libdir = @libdir@

.NOTPARALLEL:
all: @target@

# Suppress display of executed commands.
$(VERBOSE).SILENT:

placet: @placet_deps@
	+$(MAKE) -C src placet

placet-htgen: 
	+$(MAKE) -C src placet-htgen

placet-lib:
	+$(MAKE) -C src libplacet.so

utils: 
	+$(MAKE) -C src utils

interfaces:
	+$(MAKE) -C src-interfaces @make_interfaces@

swig:
	+$(MAKE) -C src-swig

mpi:
	+$(MAKE) -C src-parallel

test: pre-install
	cd testing && @CTEST_PROGRAM@ -L SHORT

test-all: pre-install
	cd testing && @CTEST_PROGRAM@

pre-install: all
	@echo "Pre-installing Placet files ..."
	mkdir -p "$(pre_install)/share/placet/interfaces"
	mkdir -p "$(pre_install)/share/placet/modules"
	mkdir -p "$(pre_install)/share/placet/octave" 
	mkdir -p "$(pre_install)/share/placet/python" 
	mkdir -p "$(pre_install)/share/placet/tcl"
	mkdir -p "$(pre_install)/share/placet/doc/texi"
	mkdir -p "$(pre_install)/share/placet/doc/python"
ifeq (@OCTAVE_INTERFACE@,1)
	@echo "Installing Octave module ..."
	install -c src-interfaces/placet_octave.oct "$(pre_install)/share/placet/interfaces"
	install -c scripts/placet/octave/*.m "$(pre_install)/share/placet/octave"
endif
ifeq (@PYTHON_INTERFACE@,1)
	@echo "Installing Python module ..."
	install -c src-interfaces/_placet_python.so "$(pre_install)/share/placet/interfaces"
	install -c src-interfaces/placet_python.py "$(pre_install)/share/placet/interfaces"
	install -c scripts/placet/python/* "$(pre_install)/share/placet/python/"
endif
ifeq (@MPI_MODULE@,1)
	@echo "Installing MPI module ..."
	install -c src-parallel/mpi_6d-tracking-core "$(pre_install)/share/placet/modules"
endif
	install -c scripts/placet/tcl/*.tcl "$(pre_install)/share/placet/tcl"
	install -c doc/texi/*.texi "$(pre_install)/share/placet/doc/texi"
	cp -rf doc/python_scripts/_build/html/* "$(pre_install)/share/placet/doc/python/"
	install -m 644 -c doc/placet.pdf "$(pre_install)/share/placet/doc"

install: pre-install
	@echo "Installing Placet files ..."
	mkdir -p "$(bindir)"
	cd src && install -c $(subst utils, grid ground mad2gp, @make_all@ ) "$(bindir)" && cd ..
	install -m 755 -c doc/placet-doc "$(bindir)"
	install -m 755 -c doc/placet-pydoc "$(bindir)"
	cp -R $(pre_install)/share $(prefix)/.

install-lib: placet-lib
	test -d "$(libdir)" || mkdir "$(libdir)"
	install -c src/libplacet.so "$(libdir)"

clean:
	@echo "Cleaning Placet files ..."
	@+$(MAKE) -C src clean
	@+$(MAKE) -C src-parallel clean
	@+$(MAKE) -C src-interfaces clean

distclean: clean
	@rm -f config.log config.status Makefile src/Makefile.common src/config.h include/defaults.h src/stamp-h testing/run.sh
	@rm -rf $(pre_install)
