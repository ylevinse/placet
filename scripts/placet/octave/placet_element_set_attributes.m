## -*- texinfo -*-
## @deftypefn {Function File} {} placet_element_set_attributes (@var{beamline}, [@var{indexes}], @var{attributes})
## Sets the 'attributes' of elements in beamline 'beamline'. If 'indexes' is not specified, works with all elements.
## @end deftypefn

function placet_element_set_attributes(beamline, arg1, arg2)
  if (nargin==3 && ischar(beamline) && isvector(arg1) && (iscell(arg2) || isstruct(arg2))) || (nargin==2 && ischar(beamline) && (iscell(arg1) || isstruct(arg1)))
    if nargin==3
      indexes = arg1;
      if isstruct(arg2)
        attributes = cell(1);
        attributes{1} = arg2;
      else  
        attributes = arg2;
      end
    else
      indexes = placet_get_name_number_list(beamline, "*");
      if isstruct(arg1)
        attributes = cell(1);
        attributes{1} = arg1;
      else  
        attributes = arg1;
      end
    end
    if length(attributes) < length(indexes)
      error("too few elements in 'attributes'");
    end
    if length(attributes) > length(indexes)
      error("too many elements in 'attributes'");
    end
    skip_attributes = { "type_name"; "aperture_losses"; "hcorrector"; "vcorrector"; "reading_x"; "reading_y" };
    str_attributes = { "name"; "tclcall_entrance"; "tclcall_exit"; "strength_list"; "short_range_wake" };
    index_attributes = 1;
    for index_beamline = indexes
      type_name_beamline = placet_element_get_attribute(beamline, index_beamline, "type_name");
      type_name_attributes = attributes{index_attributes}.type_name;
      if !strcmp(type_name_attributes, type_name_beamline)
        error(sprintf("type mismatch for beamline element num '%d' ('%s' != '%s')\n", index_beamline, type_name_beamline, type_name_attributes));
      end
      attribute_list = fieldnames(attributes{index_attributes});
      for index_attribute = 1:length(attribute_list)
        if sum(strcmp(skip_attributes, attribute_list{index_attribute}))==0
          old_attribute = placet_element_get_attribute(beamline, index_beamline, attribute_list{index_attribute});
          new_attribute = attributes{index_attributes}.(attribute_list{index_attribute});
          if sum(strcmp(str_attributes, attribute_list{index_attribute}))>0
            replace = !strcmp(old_attribute, new_attribute);
            if replace && length(new_attribute)==0;
              new_attribute = "\0";
            end
          else
            replace = new_attribute != old_attribute;
          end
          if replace
            placet_element_set_attribute(beamline, index_beamline, attribute_list{index_attribute}, new_attribute);
          end
        end
      end
      index_attributes++;
    end
  else  
    help placet_element_set_attributes
  endif
end
