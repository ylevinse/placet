/*
**
** MPI tracking code for PLACET
**
*/

#include <mpi.h>
#include <fstream>
#include <sstream>
#include <cstring>

#include <unistd.h>
#include <getopt.h>
#include <libgen.h>

#include "file_buffered_stream.hh"
#include "mpi_buffered_stream_bcast.hh"
#include "mpi_buffered_stream.hh"
#include "mpi_stream_bcast.hh"
#include "mpi_stream.hh"
#include "stopwatch.hh"

#include "quadrupole.hh"
#include "multipole.hh"
#include "special.hh"
#include "sbend.hh"
#include "drift.hh"
#include "bpm.hh"

#include "emitt_data.hh"

#define DEBUG

static std::vector<std::pair<double,double> > MPI_Reduce_bpm_readings(std::vector<Element*> &beamline, int root, MPI_Comm comm=MPI_COMM_WORLD )
{
  std::vector<std::pair<double,double> > retval;
  std::vector<Bpm*> bpms_list;
  for (size_t i=0; i<beamline.size(); i++) {
    if (Bpm *bpm_ptr = dynamic_cast<Bpm*>(beamline[i])) 
      bpms_list.push_back(bpm_ptr);
  }
  if (bpms_list.size() > 0) {
    int rank;
    MPI_Comm_rank(comm, &rank);
    double bpms[3*bpms_list.size()];
    for (size_t i=0; i<bpms_list.size(); i++) {
      const Bpm &bpm = *bpms_list[i];
      bpms[3*i+0] = bpm.get_x_sum();
      bpms[3*i+1] = bpm.get_y_sum();
      bpms[3*i+2] = bpm.get_sum();
    }
    if (rank == root) {
      double bpms_sum[3*bpms_list.size()];
      MPI_Reduce(bpms, bpms_sum, 3*bpms_list.size(), MPI_DOUBLE, MPI_SUM, root, comm);
      for (size_t i=0; i<bpms_list.size(); i++) {
	std::pair<double,double> value;
	value.first  = bpms_sum[3*i+0] / bpms_sum[3*i+2];
	value.second = bpms_sum[3*i+1] / bpms_sum[3*i+2];
	retval.push_back(value);
      }
    } else {
      MPI_Reduce(bpms, NULL, 3*bpms_list.size(), MPI_DOUBLE, MPI_SUM, root, comm);
    }
  }
  return retval;
}

int main(int argc, char **argv )
{
  if (MPI_Init(&argc, &argv) == MPI_SUCCESS) {

    ////////////////////// all processes
    if (argc < 2) {
      MPI_Abort(MPI_COMM_WORLD, -1);
      fprintf(stderr, "error:\t`%s\' must be run from PLACET\n", basename(argv[0]));
      exit(1);
    }

    ////////////////////// all processes
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    ////////////////////// root variables
    File_Buffered_OStream streamout;
    File_IStream streamin;
    if (rank == 0) {
      const char *ififoname = argv[1];
      const char *ofifoname = argv[2];
      streamout.open(ofifoname);
      streamin.open(ififoname);
      if (! bool(streamin) || !  bool(streamout)) {
	fprintf(stderr, "error:\t`%s\' cannot establish a connection with PLACET\n", basename(argv[0]));
	MPI_Abort(MPI_COMM_WORLD, -1);
	exit(1);
      }
      /// Handshaking with PLACET
      streamout << std::string("Hello, PLACET!") << Stream::flush;
    }
    size_t beam_isizes[size];

#ifdef DEBUG
    ////////////////////// set up debug log file
    char debug_str[32];
    sprintf(debug_str, "mpi_%d.out", rank);
    std::ofstream debug(debug_str);
    char hostname[64];
    gethostname(hostname, 64);
    debug << "Running on node '" << hostname << "'\n";
#endif
    
    ////////////////////// initialize the random number generator with a different seed for each node
    for (int i=0; i < rank; i++) rand();
    gsl_rng_set(Element::rng, rand());

    ////////////////////// here we go
    bool loop = true;
    do {

      ////////////////////// all processes
      std::vector<Element*> beamline;
      std::vector<Particle> beam;
      std::string beam_name;
      size_t beam_size;
      
      MPI_Buffered_OStream ostream;
      MPI_Buffered_IStream istream;

#ifdef DEBUG
      Stopwatch watch;
#endif

      if (rank == 0) {

#ifdef DEBUG
	watch.start();
#endif
	////////////////////// receive and dispatch the beamline
	MPI_Buffered_OStream_Bcast broadcast;
	while (Element *element_ptr = Element::extract_element_ptr(streamin)) {
	  Element::insert_element_ptr(broadcast, element_ptr);
	  if (dynamic_cast<Special*>(element_ptr)) break;
	  beamline.push_back(element_ptr);
	}
	broadcast.flush();
	//////////////////////
#ifdef DEBUG
	watch.stop();
	debug << "time spent receiving and broadcasting the beamline\n" << watch << std::endl << std::endl;
#endif

#ifdef DEBUG
	watch.start();
#endif
	//////////// receive and dispatch the beam
	streamin >> beam_name >> beam_size;
	for (size_t i=0; i<size; i++) {
	  size_t beam_base = (i * beam_size) / size;
	  size_t beam_end = ((i+1) * beam_size) / size;
	  size_t beam_isize = beam_end - beam_base;
	  beam_isizes[i] = beam_isize;
	}
	for (size_t i=0; i<size-1; i++) {
	  size_t beam_isize = beam_isizes[i+1];
	  beam.resize(beam_isize);
	  streamin.read((char*)beam.data(), beam_isize * sizeof(Particle));
	  ostream.set_dest(i+1);
	  ostream.write(beam_isize);
	  ostream.write((const char*)beam.data(), beam_isize * sizeof(Particle));
	  ostream.flush();
	}
	size_t beam_isize = beam_isizes[0];
	beam.resize(beam_isize);
	streamin.read((char*)beam.data(), beam_isize * sizeof(Particle));
	//////////////////////
#ifdef DEBUG
	watch.stop();
	debug << "time spent receiving and distributing the beam\n" << watch << std::endl << std::endl;
#endif

      } else {

#ifdef DEBUG
	watch.start();
#endif
	////////////////////// receive the beamline
	MPI_Buffered_IStream_Bcast broadcast(0);
	while (Element *element_ptr = Element::extract_element_ptr(broadcast)) {
	  if (dynamic_cast<Special*>(element_ptr)) break;
	  beamline.push_back(element_ptr);
	}
	//////////////////////
#ifdef DEBUG
	watch.stop();
	debug << "time spent receiving the beamline\n" << watch << std::endl << std::endl;
#endif

#ifdef DEBUG
	watch.start();
#endif
	////////////////////// receive the beam
	size_t beam_isize;
	istream.set_src(0);
	istream >> beam_isize;
	beam.resize(beam_isize);
	istream.read((char*)beam.data(), beam_isize * sizeof(Particle));
	//////////////////////
#ifdef DEBUG
	watch.stop();
	debug << "time spent receiving the beam\n" << watch << std::endl << std::endl;
#endif
      }

#ifdef DEBUG
      watch.start();
#endif
      ////////////////////// all processes perform tracking
      // for (auto &i : beamline) {
      // Element *element_ptr = i;
      for (std::vector<Element*>::iterator i = beamline.begin(); i != beamline.end(); ++i) {
	Element *element_ptr = *i;
	element_ptr->transport_in(beam);
	element_ptr->transport_synrad(beam);
	element_ptr->transport_out(beam);
      }
      //////////////////////

#ifdef DEBUG
      watch.stop();
      debug << "time spent tracking \n" << watch << std::endl << std::endl;
#endif

#ifdef DEBUG
      watch.start();
#endif
      ////////////////////// take the average of the bpm readings
      std::vector<std::pair<double,double> > bpms = MPI_Reduce_bpm_readings(beamline, 0);
      //////////////////////
#ifdef DEBUG
      watch.stop();
      debug << "time spent reducing the bpm readings \n" << watch << std::endl << std::endl;
#endif

      ////////////////////// root process return the beam
      if (rank == 0) {

#ifdef DEBUG
	watch.start();
#endif
	streamout << beam_name << beam_size;
	streamout.write((const char*)beam.data(), beam.size() * sizeof(Particle));
	for(size_t i=0; i<size-1; i++) {
	  size_t beam_isize = beam_isizes[i+1];
	  beam.resize(beam_isize);
	  istream.set_src(i+1);
	  istream.read((char*)beam.data(), beam_isize * sizeof(Particle));
	  streamout.write((const char*)beam.data(), beam_isize * sizeof(Particle));
	}

#ifdef DEBUG
	watch.stop();
	debug << "time spent gathering and returning the beam \n" << watch << std::endl << std::endl;
#endif

#ifdef DEBUG
	watch.start();
#endif
	streamout << bpms;
#ifdef DEBUG
	watch.stop();
	debug << "time spent returning the bpm readings \n" << watch << std::endl << std::endl;
#endif
	streamout.flush();

      } else {

#ifdef DEBUG
	watch.start();
#endif
	ostream.set_dest(0);
	ostream.write((const char*)beam.data(), beam.size() * sizeof(Particle));
	ostream.flush();
#ifdef DEBUG
	watch.stop();
	debug << "time spent returning the beam \n" << watch << std::endl << std::endl;
#endif
      }
      //////////////////////

    } while(loop);
    MPI_Finalize();
  } else fputs("error:\tcould not initialize the MPI execution environment\n", stderr);
 
 return 0;
}
