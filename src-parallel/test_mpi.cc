/*
**
** MPI tracking code for PLACET
**
** 
*/

#include <fstream>
#include <cstring>
#include <iostream>

#include <getopt.h>
#include <libgen.h>

#include <mpi.h>

#include "mpi_buffered_stream_bcast.hh"
#include "mpi_buffered_stream.hh"
#include "file_buffered_stream.hh"
#include "mpi_stream_bcast.hh"
#include "mpi_stream.hh"

#include "stopwatch.hh"
#include "particle.hh"

int main(int argc, char **argv )
{
  if (MPI_Init(&argc, &argv) == MPI_SUCCESS) {

    ////////////////////// all processes
    int size, rank, namelen;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name,&namelen);

    printf("MPI: Process %d on %s out of %d\n",rank,processor_name,size);

    if (rank==0) {
      MPI_Buffered_OStream_Bcast stream;
      std::vector<Particle> beam;
      for (size_t i=0; i<10; i++) {
	Particle particle;
	particle.x = 2. - i;
	particle.xp = 3.;
	particle.y = 4.;
	particle.yp = 5.;
	particle.z = 5.5;
	particle.energy = 6. + i;
	particle.weight = 7.;
	beam.push_back(particle);
      }
      stream << beam;
    } else {
      MPI_Buffered_IStream_Bcast stream(0);
      std::vector<Particle> beam;
      stream >> beam;
      for (size_t i=0; i<beam.size(); i++)
	std::cout << beam[i] << std::endl;
    }
    MPI_Finalize();
  } else {
    std::cout << "MPI was not successfully initialised" << std::endl;
  }
  
  return 0;
}
  
