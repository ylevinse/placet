#ifndef particle_h
#define particle_h

#include <cmath>
#include <iostream>
#include <limits>

#include "stream.hh"
#include "placet.h"

typedef int (*cmp_func_t)(const void *, const void * );

/**
 * @brief a particle definition
 * 
 * 
 * This struct keeps information about a particle's parameters.
 * 
 * @author Daniel Schulte <Daniel.Schulte@cern.ch>
 */
struct PARTICLE {

#ifdef TWODIM
  /// horizontal position [\f$ \mu m \f$]
  double x;
  /// horizontal angle [\f$ \mu rad \f$]
  double xp;
#endif
  /// vertical position [\f$ \mu m \f$]
  double y;
  /// vertical angle [\f$ \mu rad \f$]
  double yp;
  /// longitudinal position [\f$ \mu m \f$]
  double z;
  /// particle energy [GeV]
  double energy;
  /// weight of particle, usually total weight at beam creation adds up to 1, or slightly less if a cut on the tails is made
  double wgt;
  
  /// Unique particle ID
  unsigned int id;
  
  inline double beta() const { double _t = energy * INV_EMASS; _t *= _t; return (_t - 0.5) / _t; }  // first order approximation
  inline double beta2() const { double _t = energy * INV_EMASS; _t *= _t; return (_t - 1) / _t; }
  
  inline double gamma() const { return energy * INV_EMASS; }
  inline double gamma2() const { double _t = energy * INV_EMASS; return _t*_t; }
  
  inline double energy_deviation(double energy0 ) const { return energy > 0. ? (energy-energy0)/energy0 : (-energy-energy0)/energy0; }
  inline double momentum_deviation(double energy0 ) const
  {
    double _t = energy0 * INV_EMASS; _t *= _t;
    double e0b0 = energy0 * (_t - 0.5) / _t;
    return (energy * beta() - e0b0) / e0b0;
  }
  /**
   * Returns zp = p_z/p in 1e-6
   * 
   * Formula used: \f$ x'^2 + y'^2 + z'^2 = 10^{12} \f$
   */
  double get_zp() const { return sqrt(1e+12 - xp*xp - yp*yp); }
  
  /// check if particle is lost 
  // (convention: wgt==0.0 or energy=0.0)
  inline bool is_lost()
  {
    return fabs(wgt)<std::numeric_limits<double>::epsilon() || 
      fabs(energy)<std::numeric_limits<double>::epsilon();
  }

  /// set particle to lost
  // (convention: wgt==0.0 and energy=0.0)
  inline void set_lost()
  {
    wgt=0.0;
    energy=0.0;
  }
  
  friend bool operator<(const PARTICLE &a, const PARTICLE &b ) { return a.z < b.z; }
  
  friend std::ostream &operator<<(std::ostream &stream, const PARTICLE &particle )
  {
    return stream << particle.energy << ' ' << particle.x <<  ' ' << particle.y << ' ' << particle.z << ' ' << particle.xp << ' ' << particle.yp;
  }

};

int particle_cmp(const PARTICLE *a, const PARTICLE *b );

#endif /* particle_h */
