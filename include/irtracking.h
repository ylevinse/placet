#ifndef IRTRACKING_H
#define IRTRACKING_H

#include <cmath>
#include <fstream>
#include <cctype>
#include <unistd.h>
#include "function.h"
#include "beam.h"
#include "beamline.h"
#include "detsolenoid.h"
#include "photon_spectrum.h"


class IRTracking {

 public:
  
  IRTracking(double step, double int_length, std::vector< double > bounds_list, DetectorSolenoid* point = NULL);
  ~IRTracking();
  
  /// Start tracking the beam through the beam-line
  void track(BEAM *beam, BEAMLINE *beamline);

  /**
   * @brief Change the step length
   *
   * @param st The new step length
   **/
  void SetStep(double st) { step=st; }
  /// Get the current step length
  double GetStep() { return step; }
  /// Set the integration length
  void SetIntLength(double leng) { int_length=leng; }
  /// Turn synchrotron radiation on/off
  void SetSynrad(bool synrad,bool write_synrad);
  /**
   * If this function is called,
   * the stream to write the full coordinates
   * for the first particle in the beam is 
   * set up.
   */
  int WriteFirstParticle();
  /// Track beam backwards through the lattice
  int SetBackwardsTracking(bool backward);

 protected:

  std::vector<double> boundlist;

 private:

  /// Integration length [m]
  double int_length;
  /// Distance integrated thus far [m]
  double current_int_length;
  std::ofstream * sing_trk_stream;
  std::ofstream * synrad_stream;
  /// method to decide if particle should be written to output stream
  bool ShouldWriteParticle(int particle_id);
  bool backwards_tracking;
  BEAM *beam;
  DetectorSolenoid *map;

  /// Set the list of boundary crossings
  void SetBoundList (std::vector<double> &bound);
  double checkboundary();
  /// Second Order Integrator
  void step_2nd_order(PARTICLE* particle, ELEMENT* element, double posz);
  /// 4th Order Integrator
  void step_4th_order(PARTICLE* particle, int particle_index, ELEMENT* element);
  /// moves the beam one step further along beamline with element index
  int step_particle(BEAMLINE* beamline, int index);
  /// sets array bfield to magnetic field of particle in element
  void SetBfield(PARTICLE* particle, ELEMENT* element, double* bfield);   
  void SetBoundCross(bool bcr) { boundcross=bcr; }
  bool GetBoundCross() { return boundcross; }
  /// The step we are currently using...
  double step;
  /// True if we are at the crossing of an element
  bool boundcross;
  /// Synchrotron radiation on/off
  bool syn; 
  /**
   * Kicks the beam a coefficient factor of the field strength in the
   * current step. Part of the step_4th_order() function.
   */
  int fourth_order_kicker(PARTICLE* particle, int particle_index, ELEMENT* element, double mom_div, double coefficient, double posz, double* vp, bool writeKick = false);
  /**
   * Moves the beam a coefficient factor of the current step. 
   * Part of the step_4th_order() function.
   */
  int fourth_order_mover(PARTICLE* particle, double coefficient, double& posz);
  int part_step_particle(int particle_index, ELEMENT* element);
  /**
   * Should be called at the beginning of any step function...
   */
  double get_posz(PARTICLE* particle);
  /**
   * Use this function to track through thin lens elements
   * in the correct manner...
   */
  void track_thin_elements(BEAM*, ELEMENT* element);
  /**
   * Needed for backwards tracking through thin elements..
   */
  void invert_beam_for_backward(BEAM* beam);
};
#endif //IRTRACKING_H
