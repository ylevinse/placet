#ifndef lattice_h
#define lattice_h

class BEAMLINE;

int check_beamline_changeable(Tcl_Interp *,char *,int);
int check_girder_existence(Tcl_Interp *,char *,int);
int tk_AccCavity(ClientData ,Tcl_Interp *,int ,char **);
int tk_AddBPMsToBeamline(ClientData ,Tcl_Interp *,int ,char **);
int tk_AddDipolesToBeamline(ClientData ,Tcl_Interp *,int ,char **);
int tk_AddDipoleGridToBeamline(ClientData ,Tcl_Interp *,int ,char **);
void add_bpms(BEAMLINE* beamline, bool before=true);
void add_dipoles(BEAMLINE* beamline, bool before=true);
void add_elements(BEAMLINE* beamline, bool before, bool bpm, bool dipole);
void add_dipole_grid(BEAMLINE* beamline, float spacing=1.);
int tk_Aperture(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamlinePrint(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamlinePrintSurvey(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamlineSet(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamlineUse(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamlineNew(ClientData ,Tcl_Interp *,int ,char **);
int tk_BeamlineDelete(ClientData ,Tcl_Interp *,int ,char **);
int tk_Bpm(ClientData ,Tcl_Interp *,int ,char **);
int tk_Cavity(ClientData ,Tcl_Interp *,int ,char **);
int tk_CavityBpm(ClientData ,Tcl_Interp *,int ,char **);
int tk_DecCavity(ClientData ,Tcl_Interp *,int ,char **);
int tk_Dipole(ClientData ,Tcl_Interp *,int,char **);
int tk_Drift(ClientData ,Tcl_Interp *,int,char **);
int tk_Girder(ClientData ,Tcl_Interp *,int,char **);
int tk_Multipole(ClientData ,Tcl_Interp *,int,char **);
int tk_RfMultipole(ClientData ,Tcl_Interp *,int,char **);
int tk_QuadBpm(ClientData ,Tcl_Interp *,int,char **);
int tk_Quadrupole(ClientData ,Tcl_Interp *,int,char **);
int tk_Rmatrix(ClientData ,Tcl_Interp *,int,char **);
int tk_SetupMainBeamline(ClientData ,Tcl_Interp *,int,char **);
int tk_SetupSmoothBeamline(ClientData ,Tcl_Interp *,int,char **);
int tk_Solenoid(ClientData ,Tcl_Interp *,int,char **);
int tk_TclCall(ClientData ,Tcl_Interp *,int,char **);
int tk_AddElement(ClientData ,Tcl_Interp *,int,char **);

#endif
