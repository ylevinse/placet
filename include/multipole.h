#ifndef multipole_h
#define multipole_h

#include <complex>
#include <vector>

#include "element.h"

class MULTIPOLE : public ELEMENT {

  std::complex<double> strength;
  std::vector<std::complex<double> > strength_list; // alternative implementation. when strength_list is not empty(), it applies multiple kicks
  int field;
  bool thick; // not used - JS

  void step_partial(BEAM* beam, double l, void (ELEMENT::*step_function)(BEAM*));

  void step_twiss(BEAM *beam,FILE *file,double step, int j,double s0, int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0, int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int));
  
  mutable std::complex<double> Kn;
  mutable std::vector<std::complex<double> > Kn_list;

  void init_kick()
  {
    if (strength_list.empty()) {
      std::complex<double> strength_n = strength * 1e6 / double(flags.thin_lens);
      for (int i=2;i<field;i++)
	strength_n/=i;
      Kn = strength_n;
    } else {
      Kn_list.clear();
      for (size_t i=0; i<strength_list.size(); i++) {
	std::complex<double> strength_n = strength_list[i] * 1e6 / double(flags.thin_lens);
	int _field=i+1;
	for (int j=2;j<_field;j++)
	  strength_n/=j;
	Kn_list.push_back(strength_n);
      }
    }
  }
  
  KICK get_kick(const PARTICLE &particle ) const
  {
    KICK kick;
    if (strength_list.empty()) {
      if (field==2) {
	if (std::real(strength)!=0.0||std::imag(strength)!=0.0) {
	  if (thick) {
	    std::complex<double> Kick = Kn / particle.energy * std::complex<double>(particle.x, particle.y) * 1e-6;
	    kick.xp = -real(Kick);
	    kick.yp = +imag(Kick);
	  } else {
	    std::complex<double> Kick = Kn / particle.energy * std::complex<double>(particle.x, particle.y) * 1e-6;
	    kick.xp = -real(Kick);
	    kick.yp = +imag(Kick);
	  }
	} else {
	  kick.xp = 0.0;
	  kick.yp = 0.0;
	}
      } else if (field>0) {
	const int n = field-1;
	std::complex<double> Kick = Kn  / particle.energy * std::pow(std::complex<double>(particle.x, particle.y) * 1e-6, n);
	kick.xp = -real(Kick);
	kick.yp = +imag(Kick);
      } else {
	kick.xp = 0.0;
	kick.yp = 0.0;
      }
    } else {
      kick.xp = 0.0;
      kick.yp = 0.0;
      const std::complex<double> &_K1 = Kn_list[0];
      if (std::real(_K1)!=0.0||std::imag(_K1)!=0.0) {
	std::complex<double> Kick = _K1 / particle.energy;
	kick.xp -= real(Kick);
	kick.yp += imag(Kick);
      }
      int Kn_length = int(Kn_list.size());
      if (Kn_length>1) {
	const std::complex<double> &_K2 = Kn_list[1];
	if (std::real(_K2)!=0.0||std::imag(_K2)!=0.0) {
	  if (thick) {
	    std::complex<double> Kick = _K2 / particle.energy * std::complex<double>(particle.x, particle.y) * 1e-6;
	    kick.xp -= real(Kick);
	    kick.yp += imag(Kick);
	  } else {
	    std::complex<double> Kick = _K2 / particle.energy * std::complex<double>(particle.x, particle.y) * 1e-6;
	    kick.xp -= real(Kick);
	    kick.yp += imag(Kick);
	  }
	}
	for (int n=2; n<Kn_length; n++) {
	  const std::complex<double> &_Kn = Kn_list[n];
	  if (std::real(_Kn)!=0.0||std::imag(_Kn)!=0.0) {
	    std::complex<double> _kick = _Kn / particle.energy * std::pow(std::complex<double>(particle.x, particle.y) * 1e-6, n);
	    kick.xp -= real(_kick);
	    kick.yp += imag(_kick);
	  }
	}
      }
    }
    return kick;
  }
  
 public:
  
  MULTIPOLE(int type, double strength );
  MULTIPOLE(int &argc, char **argv );

  bool is_multipole() const { return true; }
 
  MULTIPOLE *multipole_ptr() { return this; }
  const MULTIPOLE *multipole_ptr() const { return this; }

  void set_nstep(int n) { flags.thin_lens=n; }
  void set_strength(std::complex<double> s ) { strength=s; }

  int get_nstep() const { return flags.thin_lens; }
  int get_field() const { return field; }

  std::complex<double> get_strength() const { return strength; }
  
  std::string get_strength_list_str() const
  {
    std::ostringstream buf;
    if (strength_list.empty()) {
      for (int i=1; i<field; i++) {
      	buf << " 0.0";
      }
      buf << ' ' << strength;
    } else {
      for (size_t i=0; i<strength_list.size(); i++) {
	buf << ' ' << strength_list[i];
      }
    }
    return buf.str();
  }
  
  void set_strength_list_str(const char *strength_list_str )
  {
    strength_list.clear();
    {
      std::istringstream buf(strength_list_str);
      std::complex<double> k;
      while(buf>>k) {
	strength_list.push_back(k);
      }
    }
    size_t count=0;
    size_t pos=0;
    for (size_t i=0; i<strength_list.size(); i++) {
      double _real=strength_list[i].real();
      double _imag=strength_list[i].imag();
      if (_real*_real+_imag*_imag>std::numeric_limits<double>::epsilon()) {
	count++;
	pos=i;
      }
    }
    if (count==1) {
      strength=strength_list[pos];
      field=pos+1;
      strength_list.clear();
    } else {
      strength=0.0;
      field=0;
    }
  }

  // void list(FILE*) const;
  
  void GetMagField(PARTICLE *particle, double *bfield);

  friend OStream &operator<<(OStream &stream, const MULTIPOLE &multipole )
  {
    return stream << static_cast<const ELEMENT &>(multipole)
		  << multipole.field
		  << multipole.flags.thin_lens
		  << multipole.strength
		  << multipole.tilt;
  }

  friend IStream &operator>>(IStream &stream, MULTIPOLE &multipole )
  {
    return stream >> static_cast<ELEMENT &>(multipole)
                  >> multipole.field
		  >> multipole.flags.thin_lens
                  >> multipole.strength
                  >> multipole.tilt;
  }
  
  Matrix<6,6> get_transfer_matrix_6d(double _energy ) const;

};

int tk_MultipoleNumberList(ClientData clientdata,Tcl_Interp *interp,int argc,
			   char *argv[]);
int tk_MultipoleGetStrengthList(ClientData clientdata,Tcl_Interp *interp,
				int argc,char *argv[]);
int tk_MultipoleSetStrengthList(ClientData clientdata,Tcl_Interp *interp,
				int argc,char *argv[]);
#endif
