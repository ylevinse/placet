#ifndef cavity_pets_h
#define cavity_pets_h

#include "element.h"
#include "structures_def.h"

class BEAM;

class CAVITY_PETS : public ELEMENT {

//  double phi; // this should replace v1
  int field;
  double v1, v3,v4,v5;

  double rf_seed;
  int t;
  PetsWake *wake;
  
  void step_4d_0(BEAM*);
  void step_4d(BEAM*);
  void step_twiss(BEAM *beam,FILE *file,double step0,int iel, double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step0,int iel, double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));

  static struct _WAKE_TABLE {
    double *S_long,*C_long,*S_transv,*C_transv;
    double *S_b_long,*C_b_long,*S_b_transv,*C_b_transv;
    double *rho_S_long,*rho_C_long,*rho_S_transv,*rho_C_transv;
    int nmax,nmax_long,ntotal,ntotal_long,t_old;
    double *f_long;
    double *f_transv;
    double *sl,*st;
    double *S_b_transv_x,*C_b_transv_x;
    double *S_transv_x,*C_transv_x;
    double *rho_S_transv_x,*rho_C_transv_x;
    _WAKE_TABLE()
    {
      S_long=NULL; C_long=NULL; S_transv=NULL; C_transv=NULL;
      S_b_long=NULL; C_b_long=NULL; S_b_transv=NULL; C_b_transv=NULL;
      rho_S_long=NULL; rho_C_long=NULL; rho_S_transv=NULL; rho_C_transv=NULL;
      nmax=0; nmax_long=0; ntotal=0; ntotal_long=0; t_old=-1;
      f_long=NULL;
      f_transv=NULL;
      sl=NULL; st=NULL;
      S_b_transv_x=NULL; C_b_transv_x=NULL;
      S_transv_x=NULL; C_transv_x=NULL;
      rho_S_transv_x=NULL; rho_C_transv_x=NULL;
    }

    void init(PetsWake *drive_data0, double length, BEAM *bunch, int NMODE_LONG, int NMODE, int CAVSTEP )
    {
      // init drain term for long wake functions  ( z*b/(1-b) )
     if (nmax_long<NMODE_LONG) {
        f_long=(double*)realloc(f_transv,sizeof(double)*NMODE_LONG);
        sl=(double*)realloc(sl,sizeof(double)*NMODE_LONG*2);
     }
     for (int i=0;i<NMODE_LONG;++i) {
       f_long[i]=drive_data0->beta_group_l[i]/(1.0-drive_data0->beta_group_l[i])*1e-6*CAVSTEP/length;      
      }
       // init drain term for trans wake functions  ( z*b/(1-b) )
     if (nmax<NMODE) {
       f_transv=(double*)realloc(f_transv,sizeof(double)*NMODE);
       st=(double*)realloc(st,sizeof(double)*NMODE*2);
     }
     for (int i=0;i<NMODE;++i) {
       f_transv[i]=drive_data0->beta_group_t[i]/(1.0-drive_data0->beta_group_t[i])
	 *1e-6*CAVSTEP/length;
     }

     // init tables of long wakes for all bunches already calculated
     if (ntotal_long<bunch->bunches*NMODE_LONG*(CAVSTEP+1)) {
       ntotal_long=bunch->bunches*NMODE_LONG*(CAVSTEP+1);
       rho_S_long=(double*)realloc(rho_S_long,sizeof(double)*(CAVSTEP+1)*bunch->bunches*NMODE_LONG);
       rho_C_long=(double*)realloc(rho_C_long,sizeof(double)*(CAVSTEP+1)*bunch->bunches*NMODE_LONG);
     }
     
     // init tables of trans wakes for all bunches already calculated
     if (ntotal<bunch->bunches*NMODE*(CAVSTEP+1)) {
       ntotal=bunch->bunches*NMODE*(CAVSTEP+1);
       rho_S_transv=(double*)realloc(rho_S_transv,sizeof(double)*(CAVSTEP+1)*bunch->bunches*NMODE);
       rho_C_transv=(double*)realloc(rho_C_transv,sizeof(double)*(CAVSTEP+1)*bunch->bunches*NMODE);
#ifdef TWODIM
       rho_S_transv_x=(double*)realloc(rho_S_transv_x,sizeof(double)*(CAVSTEP+1)*bunch->bunches*NMODE);
       rho_C_transv_x=(double*)realloc(rho_C_transv_x,sizeof(double)*(CAVSTEP+1)*bunch->bunches*NMODE);
#endif
      }

    // init tables of long wakes for single bunch
    if (nmax_long<NMODE_LONG) {
        S_b_long=(double*)realloc(S_b_long,sizeof(double)*(CAVSTEP+1)*NMODE_LONG);
        C_b_long=(double*)realloc(C_b_long,sizeof(double)*(CAVSTEP+1)*NMODE_LONG);
        S_long=(double*)malloc(sizeof(double)*(CAVSTEP+1)*NMODE_LONG);
        C_long=(double*)malloc(sizeof(double)*(CAVSTEP+1)*NMODE_LONG);
        nmax_long=NMODE_LONG;
    }

    // init tables of trans wakes for single bunch
      if (nmax<NMODE) {
        S_b_transv=(double*)realloc(S_b_transv,sizeof(double)*(CAVSTEP+1)*NMODE);
        C_b_transv=(double*)realloc(C_b_transv,sizeof(double)*(CAVSTEP+1)*NMODE);
        S_transv=(double*)malloc(sizeof(double)*(CAVSTEP+1)*NMODE);
        C_transv=(double*)malloc(sizeof(double)*(CAVSTEP+1)*NMODE);
#ifdef TWODIM
        S_b_transv_x=(double*)malloc(sizeof(double)*(CAVSTEP+1)*NMODE);
        C_b_transv_x=(double*)malloc(sizeof(double)*(CAVSTEP+1)*NMODE);
        S_transv_x=(double*)malloc(sizeof(double)*(CAVSTEP+1)*NMODE);
        C_transv_x=(double*)malloc(sizeof(double)*(CAVSTEP+1)*NMODE);
#endif
        nmax=NMODE;
      }
    }   
  } wake_table;

public:
  
  CAVITY_PETS(double length,int field,double rotation);
  CAVITY_PETS();
  
  bool is_cavity_pets() const { return true; }

  CAVITY_PETS *cavity_pets_ptr() { return this; }
  const CAVITY_PETS *cavity_pets_ptr() const { return this; }

  void set_phi(double p ) { v1=p; }
  void set_wake(PetsWake *w ) { wake=w; }
  void set_rf_seed(double r ) { rf_seed=r; }

  double get_phi() const { return v1; }

};

#endif
