#ifndef rfkick_h
#define rfkick_h

void rf_kick2p(int ,double ,double ,double ,double ,double ,double ,
               double ,double ,double ,double *,double *);
void rf_long2p(int ,double ,double ,double ,double ,
	       double ,double ,double ,double ,double ,double *);
BEAM* make_multi_bunch_drive_rf(BEAMLINE *,DRIVE_BEAM_PARAM *,PetsWake *, int);

#endif //rfkick_h
