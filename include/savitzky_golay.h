#ifndef savitzky_golay_h
#define savitzky_golay_h

#include <vector>
#include <cmath>

#include "matrixnd.hh"

class SAVITZKY_GOLAY {
  
  std::vector<double> SG_coeffs;
  MatrixNd SG_full_coeffs;
  double SG_norm;
  int SG_order;
  int SG_nhalffilter;
  std::vector<double> lambda;
  std::vector<double> lambda_s;
  double binlength;

 public:
  double z_min;
  // 
  // calculate savitzky-golay coeffs ("optimal") for smoothing filter
  //
  void calculate_coeffs()
  {
    double* SG_A = (double*) alloca(sizeof(double) * SG_order * SG_coeffs.size());
    double* SG_AA = (double*) alloca(sizeof(double) * SG_order * SG_order);
    double* SG_inv_AA = (double*) alloca(sizeof(double) * SG_order * SG_order);
    double* SG_Ae = (double*) alloca(sizeof(double) * SG_order);
    double* SG_out = (double*) alloca(sizeof(double) * SG_order);
    //       placet_printf(INFO,"SG: order+1: %d\n", SG_order);
    //       placet_printf(INFO,"SG: halflength: %d\n", SG_coeffs.size());
    SG_full_coeffs=MatrixNd(SG_order,SG_coeffs.size());

    for(int j=0; j < SG_coeffs.size(); j++) {
      for(int m=0; m < SG_order; m++) {
	SG_A[m + j*SG_order] = std::pow((double) j-SG_nhalffilter, m);
      }
    }
 
    int s;

    gsl_matrix_view matrix_A = gsl_matrix_view_array(SG_A, SG_coeffs.size(), SG_order);
    gsl_matrix_view matrix_AA = gsl_matrix_view_array(SG_AA, SG_order, SG_order);
    gsl_matrix_view matrix_inv_AA = gsl_matrix_view_array(SG_inv_AA, SG_order, SG_order);
 
    // A'*A
    gsl_blas_dgemm (CblasTrans, CblasNoTrans,
		    1.0, &matrix_A.matrix, &matrix_A.matrix,
		    0.0, &matrix_AA.matrix);
    // inv(A'*A)
    gsl_permutation * p = gsl_permutation_alloc (SG_order);
    gsl_linalg_LU_decomp (&matrix_AA.matrix, p, &s);
    gsl_linalg_LU_invert (&matrix_AA.matrix, p, &matrix_inv_AA.matrix);    
 
    //placet_printf(INFO,"EA: matrix_inv_AA: \n");
    //gsl_matrix_fprintf(stdout, &matrix_inv_AA.matrix, "%g");
    
    for(int j=0; j < SG_coeffs.size(); j++) {
      gsl_vector_view vector_Ae = gsl_vector_view_array(SG_Ae, SG_order);
      gsl_vector_view vector_out = gsl_vector_view_array(SG_out , SG_order);
      gsl_matrix_get_row (&vector_Ae.vector, &matrix_A.matrix, j);
      // inv(A'*A)*(A'*e_n)
      gsl_blas_dgemv (CblasNoTrans, 1.0, &matrix_inv_AA.matrix, &vector_Ae.vector, 0.0, &vector_out.vector);
      // c_n = (inv(A'*A)*(A'*e_n))_0
      SG_coeffs[j] = SG_out[0];
      for(int i=0; i < SG_order; i++) {
	SG_full_coeffs[i][j]=SG_out[i];
      }
    }
    SG_norm = 1.0;

    //     FILE* file=open_file("SG_matrix");
    //     gsl_matrix_fprintf(stdout, &matrix_AA.matrix, "%g");
    //     close_file(file);

    // debug
    //    for(int j=0; j < SG_coeffs.size(); j++) {
    //      placet_printf(INFO,"EA: SG_coeff %d: %g\n", j, SG_coeffs[j]);
    //   }

    // free(SG_Ae);
    // free(SG_out);
    // free(SG_A);
    // free(SG_AA);
    // free(SG_inv_AA);
  }

 public:

 SAVITZKY_GOLAY(int _SG_order=1, int nhalffilter=1 ) : SG_nhalffilter(nhalffilter)
  {
    int SG_length = 2*SG_nhalffilter + 1;
    SG_coeffs.resize(SG_length);
    SG_order=_SG_order;
    ////// SG_coeffs.resize(SG_order, SG_length);
    // create coefficients
    calculate_coeffs();
  }

  //Savitzky-Golay smoothing of distribution
  //  std::vector<double> smooth_distribution(const std::vector<double> &lambda )
 std::vector<double> smooth_distribution()
    {
      size_t csr_nbins = lambda.size();

      std::vector<double> templambda(csr_nbins, 0.0);
      
      int lambda_index;
      for(size_t i=0; i < csr_nbins; i++) {
	for(int j=0; j < (2*SG_nhalffilter+1); j++) {
	  lambda_index = i + j - SG_nhalffilter;
	  if( (lambda_index >= 0) && (lambda_index <= (csr_nbins-1) ) ) {
	    templambda[i] = templambda[i] +  SG_coeffs[j]*lambda[lambda_index] / SG_norm;  
	  }
	}
      }
      lambda_s = templambda;
      return lambda_s;
    }
  
  // Savitzky-Golay differentiation of distribution
  std::vector<double> differentiate_distribution()
    {
      size_t csr_nbins = lambda_s.size();

      std::vector<double> dlambda(csr_nbins, 0.0);

      // 1st order filter (auto generated)   [commmented code kep in case HO filters will be implemented]
      //     for(int j=0; j < (2*SG_nhalffilter+1); j++) {
      //       SG_coeff[j] = -SG_nhalffilter + j;
      //     }
      //SG_nhalffilter=1;

      int SGd_norm = 2*SG_nhalffilter*(SG_nhalffilter+1)*(2*SG_nhalffilter+1) / 6;
      // int SG_coeff[(2*SG_nhalffilter+1)] = {-573,2166,-1249,-3774,-3084,0,3084,3774,1249,-2166,573};   // example of higher-order filter
      // int SG_norm = 17160;
      for(size_t i=0; i < csr_nbins; i++) {
	for(int j=0; j < (2*SG_nhalffilter+1); j++) {
	  int lambda_index = i + j - SG_nhalffilter;
	  if( (lambda_index >= 0) && (lambda_index <= (csr_nbins-1) ) ) {
	    dlambda[i] +=  (-SG_nhalffilter + j)*lambda_s[lambda_index] / SGd_norm;
	    //dlambda[i] = dlambda[i] +  SG_coeff[j]*lambda[lambda_index] / SG_norm;   // [generalization for HO filters]
	  }
	}
	dlambda[i] /= binlength;
      }

      // smoothing of derivative distribution
      //   for(int i=0; i < csr_nbins; i++) {
      //     for(int j=0; j < (2*csr_nhalffilter+1); j++) {
      //       lambda_index = i + j - csr_nhalffilter;
      //       if( (lambda_index >= 0) && (lambda_index <= (csr_nbins-1) ) ) {
      // 	templambda[i] = templambda[i] +  SG_coeffs[j]*dlambda[lambda_index] / SG_norm;  
      //       }
      //     }
      //   }
      //   for(int i=0; i < csr_nbins; i++) {
      //     dlambda[i] = templambda[i];
      //     templambda[i] = 0;
      //   }

      return dlambda;
    }

  inline void set_lambda(std::vector<double> lambda_0){
    lambda=lambda_0;
  }
  inline void set_lambda_s(std::vector<double> lambda_0){
    lambda_s=lambda_0;
  }
  inline void set_binlength(double binlength_0){
    binlength=binlength_0;
  }
  inline void set_z_min(double z_min_0){
    z_min=z_min_0;
  }

  ///// suggested operator for interpolation
  double interpolate_lambda(double x){
    //    int interp_order=3;
 
    size_t csr_nbins = lambda_s.size();
    double out=0;
       
    int lambda_index;
    
    if(x>(csr_nbins+0.5)*binlength || x<-0.5*binlength) return 0.0;

    int i = round(x/binlength);
    //    if(interp_order>SG_order) placet_printf(ERROR,"ERROR - can not interpolate with requested polynomial order\n");

    for(int j=0; j < (2*SG_nhalffilter+1); j++) {
      lambda_index = i + j - SG_nhalffilter;
      if( (lambda_index >= 0) && (lambda_index <= (csr_nbins-1) ) ) {
	//for(int k=0; k<interp_order;k++){
	  for(int k=0; k<SG_order;k++){
	  out += SG_full_coeffs[k][j]*lambda_s[lambda_index]*pow(x/binlength-(double)i,k);  
	}
      }
    }
    return out/SG_norm;
  }

  double interpolate_dlambda(double x){
    //    int interp_order=3;
    
    size_t csr_nbins = lambda_s.size();
    double out=0;
    
    int lambda_index;
    
    if(x>(csr_nbins+0.5)*binlength || x<-0.5*binlength) return 0.0;
    int i = round(x/binlength);
    
    //    if(interp_order>SG_order) placet_printf(ERROR,"ERROR - can not interpolate with requested polynomial order\n");
    
    for(int j=0; j < (2*SG_nhalffilter+1); j++) {
      lambda_index = i + j - SG_nhalffilter;
      if( (lambda_index >= 0) && (lambda_index <= (csr_nbins-1) ) ) {
	//	for(int k=1; k<interp_order;k++){
	for(int k=1; k<SG_order;k++){
	  out += k*SG_full_coeffs[k][j]*lambda_s[lambda_index]*pow(x/binlength-(double)i,k-1);  
	}
      }
    }
    return out/SG_norm;
  }
};
    
#endif /* savitzky_golay_h */
