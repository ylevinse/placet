#ifndef element_h
#define element_h

#include <cstdio>
#include <typeinfo>
#include <sstream>
#include <string>

#include "option_info.hh"
#include "option_set.hh"
#include "placet.h"

#include "matrix.hh"
#include "latex_picture.h"
#include "particle.h"
#include "kick.h"

class BEAM;
class IStream;
class OStream;
struct PARTICLE;

class APERTURE;
class BPM;
class CAVBPM;
class CAVITY;
class CAVITY_PETS;
class DIPOLE;
class DRIFT;
class MULTIPOLE;
class RFMULTIPOLE;
class QUADBPM;
class QUADRUPOLE;
class RMATRIX;
class SBEND;
class SHORT_RANGE;
class SOLENOID;
class LINK;

class CRAB;

struct INTER_DATA_STRUCT;
struct R_MATRIX;

extern Tcl_Interp* beamline_survey_hook_interp;
extern INTER_DATA_STRUCT inter_data;

struct CALLBACK {
  int (*funct)(BEAM*,int,Tcl_Interp*,char*);
  char *script;
  Tcl_Interp *interp;
};

#ifdef HTGEN
class MATERIAL {
public:
  double a,z,p,t,theta,k;
  double X0;
  MATERIAL() : a(0.), z(0.), p(0.), t(300.), theta(0.0), k(0.01), X0(0.) {}
};
#endif

class ELEMENT {

  /// initialise default values
  void init_defaults();
  /// declare attributes
  void init_attributes();

protected:

  option_set attributes;
 
  struct {
    std::string name;
    int number; /// number of this element type in beamline. Only set for Quadrupoles, BPMs and Cavities. Note that this is not the element number in the beamline!
    std::string comment; // An arbitrary string, e.g. a reference to the magnet catalog on EDMS
  } id;
  
  struct {
    double s; // position along the beamline at end of element, set in beamline_set [m]
    double z; // position along the girder, set when adding elements [m]
    double length; // length of the element [m]
  } geometry;
    
  /// short range wakefield (geometric)
  struct {
    SHORT_RANGE* wake;
    std::string name;
  } short_range;
  virtual void apply_short_range_wakefield(BEAM* beam);

  struct {
    bool synrad;
    int thin_lens;
    bool longitudinal;
  } flags;

  /// Corrector struct, can link to any ELEMENT attributes
  /// and manipulate those, directly with vary method or with vary_h/vcorr methods
  /// step_size is the minimum step_size of the corrector
  struct CORRECTOR {
    option_info leverage;
    double step_size;
    bool enabled;
    double vary(double dx );
    CORRECTOR() : step_size(0.0), enabled(false) {}
    CORRECTOR(option_info _leverage, double _step_size=0.0 ) : leverage(_leverage), step_size(_step_size), enabled(true) {}
  } hcorr, vcorr;   

  double tilt;  /// [\f$ rad \f$], a positive angle represents a clockwise rotation
 
#ifdef HTGEN 
  MATERIAL material;
#endif 
 
  /// Reference energy [GeV]
  double ref_energy;
  
  inline double ref_gamma() const	{ return ref_energy * INV_EMASS; }
  inline double ref_gamma2() const	{ double _t = ref_energy * INV_EMASS; return _t*_t; }

  inline double ref_beta() const	{ double _t = ref_energy * INV_EMASS; _t *= _t; return (_t - 0.5) / _t; } // first order approximation
  inline double ref_beta2() const	{ double _t = ref_energy * INV_EMASS; _t *= _t; return (_t - 1) / _t; }

  friend class LINK;
  friend class IRTracking;
#ifdef HTGEN
  friend class HTGen;
#endif

  /// private function used by the drift_step functions
  void _drift_step(BEAM *beam, double factor, bool is_6d, bool is_sliced, bool do_wake);

protected:

  virtual void init_kick() {}
  virtual void finalize_kick() {}
  virtual KICK get_kick(const PARTICLE &particle ) const
  {
    placet_cout << ERROR << "Cannot track using thin lenses through element-type " << typeid(*this).name() << endmsg;
    exit(1);
    return KICK(0., 0.);
  }
  
  void drift_step_4d(BEAM*, double = 1.0);
  void drift_step_4d_0(BEAM*, double = 1.0);
  /**
   * @brief Move the particles through the element as if a drift
   * 
   * If the optional argument factor is given, then you track through
   * a fraction of the element. factor equals 1 to track through the full
   * element. This function is for a sliced beam.
   * 
   * @param BEAM* The beam to be tracked
   * @param double [optional] Track the beam through a fraction of the element
   */
  void drift_step_6d(BEAM*, double = 1.0);
  /**
   * @brief Move the particles through the element as if a drift
   * 
   * If the optional argument factor is given, then you track through
   * a fraction of the element. factor equals 1 to track through the full
   * element.
   * 
   * @param BEAM* The beam to be tracked
   * @param double [optional] Track the beam through a fraction of the element
   */
  void drift_step_6d_0(BEAM*, double = 1.0);
  
  void drift_step_twiss(BEAM *beam,FILE *file,double step,int j, double s,int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int));
  void drift_step_twiss_0(BEAM *beam,FILE *file,double step,int j, double s,int n1,int n2,void (*callback)(FILE*,BEAM*,int,double,int,int));


  /**
   * @brief Changes beam coordinates to be relative to the element
   *
   * This is used for tracking inside the element. Once at the end, 
   * use ELEMENT::step_out() to get back to original coordinate frame again.
   * @param beam The beam to be modified
   **/
  virtual void step_in(BEAM *beam );
  virtual void step_out(BEAM *beam );
  
  /// function used by all partial step functions..
  virtual void step_partial(BEAM *beam, double l, void (ELEMENT::*step_function)(BEAM*) );

  // partial step functions:

  void step_4d(BEAM *beam, double l );
  void step_4d_0(BEAM *beam, double l );
  void step_4d_sr(BEAM *beam, double l );
  void step_4d_sr_0(BEAM *beam, double l );
  
  void step_6d(BEAM *beam, double l );
  void step_6d_0(BEAM *beam, double l );
  void step_6d_sr(BEAM *beam, double l );
  void step_6d_sr_0(BEAM *beam, double l );
  
  virtual void step_4d(BEAM * );
  virtual void step_4d_0(BEAM * );
  virtual void step_4d_sr(BEAM * );
  virtual void step_4d_sr_0(BEAM * );

  virtual void step_6d(BEAM *b );
  virtual void step_6d_0(BEAM *b );
  virtual void step_6d_sr(BEAM *b ); 
  virtual void step_6d_sr_0(BEAM *b );

  // virtual void step_4d_tl(BEAM * ) { placet_cout << ERROR << "Cannot track a sliced beam through element-type '" << typeid(*this).name() << "' using thin-lens approximation" << endmsg; exit(1); }
  // virtual void step_4d_tl_sr(BEAM * ) { placet_cout << ERROR << "Cannot track a sliced beam through element-type '" << typeid(*this).name() << "' with synrad emission using thin-lens approximation" << endmsg; exit(1); }
  // virtual void step_6d_tl(BEAM * ) { placet_cout << ERROR << "Cannot track a sliced beam through element-type '" << typeid(*this).name() << "' with longitudinal motion using thin-lens approximation" << endmsg; exit(1); }
  // virtual void step_6d_tl_sr(BEAM * ) { placet_cout << ERROR << "Cannot track a sliced beam through element-type '" << typeid(*this).name() << "' with synrad emission and longitudinal motion using thin-lens approximation" << endmsg; exit(1); }

  virtual void step_4d_tl_0(BEAM*);
  virtual void step_4d_tl_sr_0(BEAM*);
  virtual void step_6d_tl_0(BEAM*);
  virtual void step_6d_tl_sr_0(BEAM*);

  virtual void step_4d_tl(BEAM*);
  virtual void step_4d_tl_sr(BEAM*);
  virtual void step_6d_tl(BEAM*);
  virtual void step_6d_tl_sr(BEAM*);

  void drift_sigma_matrix(R_MATRIX &a, double l) const;

  /**
   * checks if beam is lost and writes a warning
   * if it was not already lost before
   */
  void check_beam_is_lost(BEAM *beam);

  struct {
    std::string entrance;
    std::string exit;
  } tcl_call;

public:

  struct APERTURE {
    #define DEFAULT_APERTURE_X  1.0; // [m]
    #define DEFAULT_APERTURE_Y  1.0; // [m]
    double inverse_x; // [m^-1]
    double inverse_y; // [m^-1]
    double weight; // weight of the lost particles
    enum TYPE {
      NONE,
      RECTANGULAR,
      ELLIPTIC,
      CIRCULAR
    } type;
  } aperture;
  
  struct OFFSET {
    double x;     /// [\f$ \mu m \f$]
    double y;     /// [\f$ \mu m \f$]
    double xp;    /// [\f$ \mu rad \f$]
    double yp;    /// [\f$ \mu rad \f$]
    double roll;  /// [\f$ rad \f$], a positive angle represents a clockwise rotation
  } offset;
  
  CALLBACK *callback;

  ELEMENT *next;

  explicit ELEMENT(double l=0);
  ELEMENT(int &argc, char **argv );
  virtual ~ELEMENT() {}

  /// check for beam if particles hit the aperture in this element
  /// returns sum of weight of particles lost
  double check_aperture(BEAM *beam);

  /// check if particle hits the aperture in this element
  bool is_lost(const PARTICLE & );

  /// parse attributes
  void set_attributes(int &argc, char **argv ) { attributes.parse_args(argc, argv); }
  template <typename T> void set_attribute(const char *attribute, T value ) { attributes.set_value(attribute, value); }

  virtual Matrix<6,6> get_transfer_matrix_6d(double _energy = -1.0 ) const;
  
  virtual double get_bpm_x_reading_exact() const { placet_cout << ERROR << "error : this is not a BPM!" << endmsg; exit(1); }
  virtual double get_bpm_y_reading_exact() const { placet_cout << ERROR << "error : this is not a BPM!" << endmsg; exit(1); }
  virtual double get_bpm_x_reading() const { placet_cout << ERROR << "error : this is not a BPM!" << endmsg; exit(1); }
  virtual double get_bpm_y_reading() const { placet_cout << ERROR << "error : this is not a BPM!" << endmsg; exit(1); }
  virtual double get_bpm_offset_x() const { placet_cout << ERROR << "error : this is not a BPM!" << endmsg; exit(1); }
  virtual double get_bpm_offset_y() const { placet_cout << ERROR << "error : this is not a BPM!" << endmsg; exit(1); }

  //template <typename T> T 
  //T get_attribute(const char *attribute ) const { return attributes.get_value<T>(attribute); }
  
  int         get_attribute_int(const char *attribute ) const { return attributes.get_value_int(attribute); }
  bool        get_attribute_bool(const char *attribute ) const { return attributes.get_value_bool(attribute); }
  double      get_attribute_double(const char *attribute ) const { return attributes.get_value_double(attribute); }
  std::string get_attribute_string(const char *attribute ) const { return attributes.get_value_string(attribute); }

  std::string get_attribute_as_string(const char *attribute ) const { return attributes.get_value_as_string(attribute); }
 
  const option_set &get_attributes_table() const { return attributes; }
  option_set &get_attributes_table() { return attributes; }
 
  void set_name(const char *_name ) { id.name = _name; }
  void set_name(const std::string &_name ) { id.name = _name; }
  void set_synrad() { flags.synrad=true; }
  void set_length(double l ) {  geometry.length = l; }
  void set_s(double s ) {  geometry.s = s; }
  void set_z(double z ) {  geometry.z = z; }
  void set_number(int n) { id.number = n; }
  
  double get_z() const { return geometry.z; }
  /// returns position along the beamline at end of the element [m]
  double get_s() const { return geometry.s; }
  double get_length() const { return geometry.length; }
  const char *get_name() const { return id.name.c_str(); }
  std::string get_name_as_string() const { return id.name; }
  int get_number() const { return id.number; }
  double get_ref_energy() const { return ref_energy; }
  double get_tilt() const { return tilt; }

  int set_aperture(const char *shape, double x, double y );
  int set_aperture(APERTURE::TYPE type, double x, double y );
  int set_aperture_type_str(const char *shape );
  int set_aperture_type(APERTURE::TYPE type );
  void set_aperture_dim(double x, double y );
  void set_aperture_dim(double x );

  void set_offset(double x, double xp, double y, double yp, double roll ) { offset.x = x; offset.xp = xp; offset.y = y; offset.yp = yp; offset.roll=roll; }
  void set_offset(double x, double xp, double y, double yp ) { offset.x = x; offset.xp = xp; offset.y = y; offset.yp = yp; }
  void set_ref_energy(double new_energy) { ref_energy = new_energy; }
  
  int get_aperture_type() const { return aperture.type; }
  
  double get_aperture_x() const { return 1./aperture.inverse_x; }
  double get_aperture_y() const { return 1./aperture.inverse_y; }
  
  std::string get_aperture_type_str() const // NOTE: this method *must* return std::string instead of const char * because the set/get option feature needs it to be like this
  {
    if (aperture.type == APERTURE::RECTANGULAR) return "rectangular";
    if (aperture.type == APERTURE::ELLIPTIC) return "elliptic";
    if (aperture.type == APERTURE::CIRCULAR) return "circular";
    return "none";
  }

///// htgen extension
#ifdef HTGEN
  void set_gas(double a0,double z0,double p0, double t0, double theta0, double k0)
  {
    material.a=a0;
    material.z=z0;
    material.p=p0;
    material.t=t0;
    material.theta=theta0;
    material.k=k0;
  }

  void set_radiation_length(double X0 )
  {
    material.X0 = X0;
  }

  const MATERIAL &get_material() const 
  {
    return material;
  }
#endif

///// tracking functions


  virtual void step_twiss(BEAM *beam,FILE *file,double step,int j, double s, int n1,int n2, void (*callback0)(FILE*,BEAM*,int,double,int,int)) { drift_step_twiss(beam,file,step,j,s,n1,n2,callback0); }
  virtual void step_twiss_0(BEAM *beam,FILE *file,double step,int j, double s, int n1,int n2, void (*callback0)(FILE*,BEAM*,int,double,int,int)) { drift_step_twiss_0(beam,file,step,j,s,n1,n2,callback0); }

  void track(BEAM *beam);
  void track_0(BEAM *beam);

  virtual bool track_with_error(BEAM * /*beam*/) { return false; }
///////// CONVERSION FUNCTIONS ////////

  template <typename T> inline T *ptr_to() { return dynamic_cast<T*>(this); }
  template <typename T> inline bool is() { return dynamic_cast<T*>(this)!=NULL; }
  
  virtual bool is_aperture() const { return false; }
  virtual bool is_bpm() const { return false; }
  virtual bool is_cavbpm() const { return false; }
  virtual bool is_cavity() const { return false; }
  virtual bool is_cavity_pets() const { return false; }
  virtual bool is_dipole() const { return false; }
  virtual bool is_drift() const { return false; }
  virtual bool is_multipole() const { return false; }
  virtual bool is_rf_multipole() const { return false; }
  virtual bool is_quadbpm() const { return false; }
  virtual bool is_quad() const { return false; }
  virtual bool is_sbend() const { return false; }
  virtual bool is_solenoid() const { return false; }
  virtual bool is_rmatrix() const { return false; }
  virtual bool is_tclcall() const { return false; }
  /// True if element is a crab cavity
  virtual bool is_crab() const { return false; }

  bool is_special() const
  {
    return  !(is_aperture() || 
	      is_bpm() ||
	      is_cavity() ||
	      is_cavity_pets() ||
	      is_dipole() ||
	      is_drift() || 
	      is_multipole() ||
	      is_rf_multipole() ||
	      is_quadbpm() ||
	      is_quad() ||
	      is_sbend() ||
	      is_solenoid() ||
	      is_rmatrix() ||
	      is_crab());
  }

  virtual ::APERTURE  *aperture_ptr()     { return NULL; }
  virtual BPM         *bpm_ptr()          { return NULL; }
  virtual CAVBPM      *cavbpm_ptr()       { return NULL; }
  virtual CAVITY      *cavity_ptr()       { return NULL; }
  virtual CAVITY_PETS *cavity_pets_ptr()  { return NULL; }
  virtual DIPOLE      *dipole_ptr()       { return NULL; }
  virtual DRIFT       *drift_ptr()        { return NULL; }
  virtual MULTIPOLE   *multipole_ptr()    { return NULL; }
  virtual RFMULTIPOLE *rf_multipole_ptr() { return NULL; }
  virtual QUADBPM     *quadbpm_ptr()      { return NULL; }
  virtual QUADRUPOLE  *quad_ptr()         { return NULL; }
  virtual SBEND       *sbend_ptr()        { return NULL; }
  virtual SOLENOID    *solenoid_ptr()     { return NULL; }
  virtual RMATRIX     *rmatrix_ptr()      { return NULL; }
  virtual CRAB        *crab_ptr()         { return NULL; }

  virtual const ::APERTURE  *aperture_ptr()     const { return NULL; }
  virtual const BPM         *bpm_ptr()          const { return NULL; }
  virtual const CAVBPM      *cavbpm_ptr()       const { return NULL; }
  virtual const CAVITY      *cavity_ptr()       const { return NULL; }
  virtual const CAVITY_PETS *cavity_pets_ptr()  const { return NULL; }
  virtual const DIPOLE      *dipole_ptr()       const { return NULL; }
  virtual const DRIFT       *drift_ptr()        const { return NULL; }
  virtual const MULTIPOLE   *multipole_ptr()    const { return NULL; }
  virtual const RFMULTIPOLE *rf_multipole_ptr() const { return NULL; }
  virtual const QUADBPM     *quadbpm_ptr()      const { return NULL; }
  virtual const QUADRUPOLE  *quad_ptr()         const { return NULL; }
  virtual const SBEND       *sbend_ptr()        const { return NULL; }
  virtual const SOLENOID    *solenoid_ptr()     const { return NULL; }
  virtual const RMATRIX     *rmatrix_ptr()      const { return NULL; }
  virtual const CRAB        *crab_ptr()         const { return NULL; }
  
  virtual void set_bookshelf(double /*x*/,double /*y*/) {return;}
    
  virtual std::string aml_string(std::string new_name = "" ) const;
  virtual std::string list() const;

  virtual void list(FILE*file) const { fputs(list().c_str(),file); }
  virtual int list(Tcl_Interp *interp ) const { Tcl_AppendResult(interp,list().c_str(),NULL); return TCL_OK; }
  
  virtual void survey_print(FILE *f,double *lsum)const;
  
  /// prints ELEMENT name and length to FILE
  void print(FILE *file)const;

  bool is_hcorr() const  { return hcorr.enabled; }
  bool is_vcorr() const  { return vcorr.enabled; }
  
  void enable_hcorr(const char *leverage_name, double step_size=0.0 );
  void enable_vcorr(const char *leverage_name, double step_size=0.0 );
  
  void disable_hcorr();
  void disable_vcorr();
  
  double vary_hcorr(double dx );
  double vary_vcorr(double dx );
  
  double get_hcorr_step_size() const { return hcorr.step_size; }
  double get_vcorr_step_size() const { return vcorr.step_size; }

  // TO BE REPLACED BY VARY_H/VCORR() (defined above)
  void correct_x(double dx ) { vary_hcorr(dx); }
  void correct_y(double dx ) { vary_vcorr(dx); } 

  virtual void GetMagField(PARTICLE *particle,double *bfield){ for(int k=0;k<3;k++) bfield[k]=0.0; }
  virtual void GetMagField(PARTICLE*,double sector_length, double* bfield) { for(int k=0;k<3;k++) bfield[k]=0.0; }
  
  friend IStream &operator>>(IStream &stream, ELEMENT &element );
  friend OStream &operator<<(OStream &stream, const ELEMENT &element );

  friend LaTeX_Picture &operator << (LaTeX_Picture &stream, const ELEMENT &element )
  {
    stream.multiput(element.geometry.s, -element.get_aperture_y(), element.geometry.length, 0, 2);
    stream.line(0,1,2*element.get_aperture_y());
    stream.multiput(element.geometry.s, -element.get_aperture_y(), 0, 2*element.get_aperture_y(), 2);
    stream.line(1,0,element.geometry.length);
    return stream;
  }
};

namespace {
  template <typename ELEMENT_TYPE> bool element_help_message(int argc, char **argv, const char *message=NULL, Tcl_Interp *interp=NULL )
  {
    if (argc>=2) {
      if (strstr(argv[1], "help")) { // will succeed for every argument that contains help, better to only allow 'help' or -(-)help ?
        if (interp) {
          Tcl_ResetResult(interp);
        }
        if (message) {
          if (interp)
            Tcl_AppendResult(interp,message,"\n",NULL);
          else
            placet_cout << ALWAYS << message << endmsg;
        }
        int _argc = 0;
        char **_argv = NULL;
        if (ELEMENT_TYPE *elem=new ELEMENT_TYPE(_argc, _argv)) {
          std::ostringstream str;
	  str << elem->get_attributes_table();
          if (interp)
            Tcl_AppendResult(interp,str.str().c_str(),NULL);
          else
            placet_cout << ALWAYS << str.str() << endmsg;
	  delete elem;
        }
        return true;
      }
    }
    return false;
  }
}

#endif /* element_h */
