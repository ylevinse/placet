#ifndef structures_def_h
#define structures_def_h

#include <map>
#include <string>

#include "beamline.h"
#include "beam.h"

class GIRDER;
//class SPLINE;

//const int DimHalo=400000;
const int DimHalo=1000000; // increased in order to run the full bds

class BIN {
 public:
  int nq,nbpm,qlast;
  int *quad,*bpm,*indx;
  int start,stop;
  double *a0,*a1,*a2,*a;
  double *b0,*b1,*b2;
  double *t0,*t1,*t2;
  double *correct,s_pos,*cor0;
  double gain,dtau;
  double *w0,*w,*w2,*pw;
#ifdef TWODIM
  int *indx_x;
  double *a0_x,*a1_x,*a2_x,*a_x;
  double *b0_x,*b1_x,*b2_x;
  double *t0_x,*t1_x,*t2_x;
  double *correct_x,*cor0_x;
  int *bpm_x,*quad_x,nq_x,nbpm_x,qlast_x;
  double *w0_x,*w_x,*w2_x,*pw_x;
#endif
};

struct WAKE_DATA{
  double *Q,*a0,*lambda,*k;
  int n,*which;
  std::string name;
};

struct WAKE_MODE{
  double Q,a0,lambda,k;
  int which,number;
};

struct INTER_DATA_STRUCT{
  /** 
      struct that handles options and globals for the beams and beamlines
   */
  /// beamline points to current Beamline.
  BEAMLINE *beamline;
  /// map of beamlines, key is the beamline name and should be unique
  std::map <std::string,BEAMLINE*> blist;
  /// pointer to the current BEAM
  BEAM *bunch;
  /// pointer to the current GIRDER
  GIRDER *girder;
  /// flag if the current beamline is set with BeamlineSet
  int beamline_set;
  /// option to track photons from background
  int track_photon;
  /// global options for new created ELEMENTS:
  bool synrad;
  int  thin_lens;
  bool longitudinal;
};

struct WAKEFIELD_DATA_STRUCT{
  /** 
   *  struct that globally controls the use of transverse and longitudinal wakefields
   */
  int transv,longit;
};

/* r_over_q is given in circuit ohms per meter :-) */

class PetsWake {
 public:
  double *beta_group_l,*beta_group_t,*r_over_q,*a0,l_cav,*lambda_transverse,
    speed,*lambda_longitudinal,*q_value,*q_value_long, *k_transverse,*k_longitudinal,*rf_size,
    rf_a0,rf_a0_i;
  int rf_kick,rf_long,rf_order,steps,n_rf,n_mode, n_mode_long;
};

struct BUMP{
  double *a11,*a12,*a21,*a22;
  double alpha,beta,gamma;
  int n,start_bin;
  int start,stop,cav[2][100],ncav[2];
  BIN *feedback;
  int type;
};

class INJECTOR_DATA {
 public:
  double a0,l_cav,lambda_transverse,
    lambda[1],gradient,q_value,shift,band;
  int steps;
  WAKE_DATA *wake;
  // short range wake
  // SPLINE *wsr;
  char *name;
};

struct BUMP_DATA{
  int iterations,quad_dist,emitt_dist,ncav,nw,ns;
  double maxshift;
};

struct CORR{
  double pwgt,w,w2,w0;
};

struct ERRORS{
  double bpm_resolution,quad_move_res,jitter_y,field_res,field_zero,
    field_zero_rms,position_error,jitter_x,offset_y,offset_x,
    quad_step_size;
  int do_jitter,do_field,do_position;
};

struct FIELD{
  double *de;
  double *kick;
};

struct RF_DATA{
  double gradient,cavlength,cavdrift,bpmlength,girderdrift,q_bpmlength;
  double phase_shift,lambda;
};
struct BALLISTIC_DATA{
  int ntn,ntc,nta,ntn0,nquad,measure_online,do_rf,do_atl;
  double offset_max,time,gain,last_quad;
  char* size_file;
};
struct SWITCHES_STRUCT{
  double envelope_cut;
  int emitt_axis,e_start,e_range;
};
struct PLACET_SWITCH_STRUCT{
  int first_order;
};

struct SURVEY_ERRORS_STRUCT{
  double cav_error,bpm_error,drift_error,quad_error,quad_roll,piece_error;
  double cav_error_angle,bpm_error_angle,drift_error_angle,quad_error_angle,
    piece_error_angle;
  double cav_error_x,bpm_error_x,drift_error_x,quad_error_x,piece_error_x;
  double cav_error_angle_x,bpm_error_angle_x,drift_error_angle_x,
    quad_error_angle_x,piece_error_angle_x,bpm_roll;
  double cav_error_realign,cav_error_realign_x;
  double cav_error_dipole_x,cav_error_dipole_y;
  struct {
  	double x;
	double y;
	double xp;
	double yp;
	double roll;
  } sbend;
  double dipole_error;
  double time;
};

#ifdef EARTH_FIELD
struct EARTH_FIELD_STRUCT{
  double x,y;
};
extern EARTH_FIELD_STRUCT earth_field;
#endif

struct QUAD_WAKE_STRUCT{
  int on;
};
extern QUAD_WAKE_STRUCT quad_wake;

extern PLACET_SWITCH_STRUCT placet_switch;

extern INTER_DATA_STRUCT inter_data;

#endif /* structures_defs_h */
