#ifndef crab_h
#define crab_h

#include <tcl.h>
#include "element.h"
#include "conversion.h"

/**
 * @file crab.h
 * 
 * This header defines the functionality of the crab cavity
 * implementation.
 * 
 * The Hamiltonian for a crab cavity kick is
 * \f$ H_{crab} = \frac{qV}{E_s} \sin(\phi_s + \frac{\omega z}{c})~x \f$
 * 
 * @author %Andrea Latina <Andrea.Latina@cern.ch>
 * @date Jan. 2012
 */


struct WAKE_DATA;

/**
 * @brief Crab Cavity
 * @author %Andrea Latina <Andrea.Latina@cern.ch>
 */
class CRAB : public ELEMENT {
  
  std::complex<double> voltage;  /// [MV]
  double lambda; /// [m]
  double phase; /// [rad]
  // frequency    // GHz
  
  WAKE_DATA *wake_data;
  /**
   * @brief Apply a wakefield kick inside the crab cavity
   * 
   * Copied out from step_4d functions..
   */
  inline void step_apply_wakekick(BEAM* beam);
  
  
protected:
  
  void step_4d(BEAM *);
  void step_4d_0(BEAM*);

//  void step_6d_0(BEAM*);
  
  /**
   * @brief Crab cavity including energy kick
   * 
   * As per http://prst-ab.aps.org/onecol/PRSTAB/v13/i3/e031001 the energy kick is added
   * to the step_4d function.
   * 
   * @param beam Pointer to the beam to be transported through the crab cavity
   * 
   * @author Yngve Inntjore Levinsen <Yngve.Inntjore.Levinsen@cern.ch>
   * @date January, 2012
   */
  void step_6d_0(BEAM*);
public:
  //CRAB() {}
/**
 * @param frequency  Operating Frequency [GHz]
 * @param voltage    Deflection Voltage [MV]
 * @param lambda     Wavelength [m]
 * @param phase      Phase of the center of kick [deg]
 * @param wakelong   Name of the longrange wakefield
 * 
 *  @author %Andrea Latina <Andrea.Latina@cern.ch>
 */
  CRAB(int &argc, char **argv );
  
  void set_wake_long(const char *name );
  std::string get_wake_long() const;
  
  bool is_crab() const { return true; }

  CRAB *crab_ptr() { return this; } 
  const CRAB *crab_ptr() const { return this; }

  void step_twiss(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0,int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  //  friend IStream &operator>>(IStream &stream, CRAB &crab );
  //  friend OStream &operator<<(OStream &stream, const CRAB &crab ); 
  
};

extern int tk_CrabCavity(ClientData clientdata,Tcl_Interp *interp,int argc, char *argv[]);

#endif /* crab_h */
