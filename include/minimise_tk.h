#ifndef minimise_tk_h
#define minimise_tk_h

#include <tcl.h>
#include <tk.h>

int
tk_MinimiseFunction(ClientData clientdata,Tcl_Interp *interp,int argc,
		    char *argv[]);

int Minimise_Init(Tcl_Interp *interp);

#endif
