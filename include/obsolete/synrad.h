#ifndef synrad_h
#define synrad_h

#include "photon_spectrum.h"
#include "random.hh"

//extern double Ran1(void);
extern double SynRadC(double);
extern double SynGen(double);
extern double SynRadEloss(double,double,double);
extern double synrad_probability(double,double);
extern double synrad_free_path();
extern double synrad_eloss(double,double,double);

inline
double synrad_probability(double e,double angle)
{
    static double const c1=2.5/SQRT3*ALPHA_EM/EMASS;
    return c1*fabs(angle)*e;
}

#endif
