#ifndef DIPOLE_H
#define DIPOLE_H

#include "element.h"

class DIPOLE : public ELEMENT {

  double a_x;
  double a_y;

 protected:
  void step_partial(BEAM *beam, double l, void (ELEMENT::*step_function)(BEAM*) );

  void step_4d(BEAM *beam);
  void step_4d_0(BEAM *beam);
  void step_4d_sr(BEAM *beam) { step_4d(beam); }
  void step_4d_sr_0(BEAM *beam) { step_4d_0(beam); }

  void step_6d(BEAM *beam);
  void step_6d_0(BEAM *beam);
  void step_6d_sr(BEAM *beam) { step_6d(beam); }
  void step_6d_sr_0(BEAM *beam) { step_6d_0(beam); }

  DIPOLE() {}
 
  KICK get_kick(const PARTICLE &particle ) const
  {
    KICK kick;
    kick.xp = a_x / particle.energy / flags.thin_lens;
    kick.yp = a_y / particle.energy / flags.thin_lens;	
    return kick;
  }
  
public:

  DIPOLE(double length, double a_x, double a_y );
  DIPOLE(int &argc, char **argv ) : ELEMENT(), a_x(0.0), a_y(0.0)
  {
    attributes.add("strength_x", "Horizontal kick [urad*GeV=keV]", OPT_DOUBLE, &a_x);
    attributes.add("strength_y", "Vertical kick [urad*GeV=keV]", OPT_DOUBLE, &a_y);
    enable_hcorr("strength_x", 0.0);
    enable_vcorr("strength_y", 0.0);
    set_attributes(argc, argv);
  }
  
  double get_strength_x() { return a_x; }
  double get_strength_y() { return a_y; }

  void set_strength_x(double val) { a_x=val; }
  void set_strength_y(double val) { a_y=val; } 
  void add_strength_x(double val) { a_x+=val; }
  void add_strength_y(double val) { a_y+=val; }
  
  bool is_dipole() const { return true; }

  DIPOLE *dipole_ptr() { return this; }
  const DIPOLE *dipole_ptr() const { return this; }

  void step_twiss(BEAM *beam,FILE *file,double step, int j,double s0, int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));
  void step_twiss_0(BEAM *beam,FILE *file,double step, int j,double s0, int n1,int n2, void (*callback)(FILE*,BEAM*,int,double,int,int));

  void GetMagField(PARTICLE *particle, double *bfield);

  friend OStream &operator<<(OStream &stream, const DIPOLE &d ) { return stream << static_cast<const ELEMENT &>(d) << d.a_x << d.a_y; }
};


#endif /* DIPOLE_H */
