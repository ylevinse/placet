#ifndef PLACET_COUT_H
#define PLACET_COUT_H

#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "placet_print.h"

extern struct PRINT_DATA print_data;

/**
 * @class PLACET_COUT
 * Definition of the PLACET_COUT class used to transmit messages.
 * based on http://alxr.usatlas.bnl.gov/lxr/source/Gaudi/GaudiKernel/GaudiKernel/MsgStream.h
 * 
 * placet_cout behaves exactly like std::cout, EXCEPT FOR:
 * - required is to add the verbosity level at the beginning
 * - flush with 'endmsg' at the end (not with std::endl!)
 * - verbosity level WARNING and ERROR go to std::cerr
 * 
 * example usage: 
 * placet_cout << INFO << "square root of 5 is " << std::setw(12) << sqrt(5) << endmsg;
 *
 * @author Jochem Snuverink
 * @see VERBOSITY
 */

class PLACET_COUT {
private:
  /// internal state
  bool active;
  /// flag to print the verbosity level in front of the message
  bool print_level;
  /// stream used to buffer the output
  std::ostringstream stream;
  /// PLACET_COUT is a singleton
  static PLACET_COUT* instance;
  /// current verbosity level
  VERBOSITY current_level;

protected: 
  /// constructor is protected, because class is singleton
  PLACET_COUT();

public:
  /// access to instance
  static PLACET_COUT* getInstance(){
    if(!instance){instance = new PLACET_COUT();}
    return instance;
  }
  /// accept verbosity level
  PLACET_COUT& operator<<(VERBOSITY verbosity);
  /// main accept method
  template <typename Type> PLACET_COUT& operator<< (const Type& arg) {
    if (active) {
      stream << arg;
    }
    return *this;
  }
  /// accept PLACET_COUT modifiers
  PLACET_COUT& operator<<(PLACET_COUT& (*_f)(PLACET_COUT&))    {
    if (active) _f(*this);
    return *this;
  }
  /// accept ios modifiers
  PLACET_COUT& operator<<(std::ios& (*_f)(std::ios&)) {
    if (active) _f(stream);
    return *this;
  }
  /// accept ostream modifiers
  PLACET_COUT& operator<<(std::ostream& (*_f)(std::ostream&)) {
    if (active) _f(stream);
    return *this;
  }
  /// accept ios base class modifiers
  PLACET_COUT& operator<<(std::ios_base& (*_f)(std::ios_base&)) {
    if (active) _f(stream);
    return *this;
  }
  /// print to screen
  PLACET_COUT& print() {
    if (active) {
      ((current_level == ERROR || current_level == WARNING) ? 
       std::cerr : std::cout)
	<< stream.str() << std::endl;

      // subsequent buffers will need to be activated with a VERBOSITY level
      active = false;
    }
    // clean stream
    stream.str("");
    // exit when error encountered 
    if (print_data.num_errors>0 && print_data.exit_on_error) exit(1);
    return *this;
  }

  /**
   * @brief end message at exit of placet
   */
  static void end_message();
};

/// global printing object placet_cout
extern PLACET_COUT& placet_cout;

/// end command
inline PLACET_COUT& endmsg(PLACET_COUT& p) {
  return p.print();
}

#endif // placet_cout.h
