#ifndef lumi_h
#define lumi_h

double lumi_calc_0(double z[],double w1[],double w2[],int n,
		   double y1[],double yp1[],
		   double y2[],double yp2[],double beta_y);
double Comp_Lumi_0(char *b_1,char *b_2,int center);
double Comp_Emitt_0(char *b_1,int center,double*,double*);
// in ip_feed_tools.cc:
std::pair<double,double> IP_corrections_0(char *b_1);
double IP_collision_0(char *b_1,char *b_2,double ym1,double ypm1,double ym2,
       double ypm2,double gain_pos,double gain_ang,double bpm_pos_res);

#endif // lumi_h
