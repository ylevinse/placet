#ifndef link_h
#define link_h

#include "element.h"

class LINK : public ELEMENT {

  LINK():element(0) {}

  ELEMENT *element;

protected:

  void step_in(BEAM *beam )	    { if (element)  element->step_in(beam); }
  void step_out(BEAM *beam )	    { if (element)  element->step_out(beam); }
  
  void step_4d(BEAM *beam )	    { if (element)  element->step_4d(beam); }
  void step_4d_0(BEAM *beam )	    { if (element)  element->step_4d_0(beam); }
  void step_4d_sr(BEAM *beam )	    { if (element)  element->step_4d_sr(beam); }
  void step_4d_sr_0(BEAM *beam )    { if (element)  element->step_4d_sr_0(beam); }
  
  void step_6d(BEAM *beam )	    { if (element)  element->step_6d(beam); }
  void step_6d_0(BEAM *beam )	    { if (element)  element->step_6d_0(beam); }
  void step_6d_sr(BEAM *beam )	    { if (element)  element->step_6d_sr(beam); }
  void step_6d_sr_0(BEAM *beam )    { if (element)  element->step_6d_sr_0(beam); }
  
  void step_4d_tl(BEAM *beam )	    { if (element)  element->step_4d_tl(beam); }
  void step_4d_tl_0(BEAM *beam )    { if (element)  element->step_4d_tl_0(beam); }
  void step_4d_tl_sr(BEAM *beam )   { if (element)  element->step_4d_tl_sr(beam); }
  void step_4d_tl_sr_0(BEAM *beam ) { if (element)  element->step_4d_tl_sr_0(beam); }
  
  void step_6d_tl(BEAM *beam )	    { if (element)  element->step_6d_tl(beam); }
  void step_6d_tl_0(BEAM *beam )    { if (element)  element->step_6d_tl_0(beam); }
  void step_6d_tl_sr(BEAM *beam )   { if (element)  element->step_6d_tl_sr(beam); }
  void step_6d_tl_sr_0(BEAM *beam ) { if (element)  element->step_6d_tl_sr_0(beam); }

public:

  LINK(ELEMENT *_element );
  
};

#endif /* link_h */
