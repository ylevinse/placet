#ifndef malloc_h
#define malloc_h

void* xmalloc(size_t size);
size_t xmalloc_size();
void xmalloc_init();
void xfree(void* p);

#endif
