#ifndef twostream_h
#define twostream_h

#include "element.h"

class TWOSTREAM : public ELEMENT {
public:

  explicit TWOSTREAM(double length ) : ELEMENT(length) {}

  void step_4d_0(BEAM*);
  void step_4d(BEAM*); 

  void list(FILE*)const;
};


int tk_Twostream(ClientData clientdata,Tcl_Interp *interp,int argc,
		 char *argv[]);

#endif
