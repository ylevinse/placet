#ifndef PLACETFPESVC
#define PLACETFPESVC

#include <fenv.h>

#include "placet_cout.hh"

/** 
 * @class PLACET_FPESVC
 * Definition of the PLACET_FPESVC class used to spot floating point exceptions.
 * based on FPEControlSvc from ATHENA by Scott Snyder

 * Service to enabe or disable floating-point exceptions
 * Usage: 
 * PLACET_FPESVC* fpesvc = PLACET::getInstance();
 * fpesvc->enable_fpe();
 * fpesvc->disable_fpe();
 * @author: Jochem Snuverink (Jochem.Snuverink@cern.ch)
 */

namespace {
  // convert an exception mask into a string
  std::string mask_to_string (int mask)
    {
      std::string out;
      if (mask & FE_INEXACT)
	out += "inexact ";
      if (mask & FE_DIVBYZERO)
	out += "divbyzero ";
      if (mask & FE_UNDERFLOW)
	out += "underflow ";
      if (mask & FE_OVERFLOW)
	out += "overflow ";
      if (mask & FE_INVALID)
	out += "invalid ";
      if (out.size() == 0)
	out = "(none) ";
      return out;
    }
} // anonymous namespace

class PLACET_FPESVC {
 
  // the placet_fpesvc is a singleton

  static PLACET_FPESVC* m_instance;

  fenv_t m_env; // placeholder to store current environment

 protected:
  
  PLACET_FPESVC(){fegetenv(&m_env);} // store current environment;
  
  ~PLACET_FPESVC(){fesetenv (&m_env);} // restore current environment

 public:

  static PLACET_FPESVC* getInstance(){
    if(!m_instance){m_instance = new PLACET_FPESVC();}
    return m_instance;
  }

  void enable_fpe(){

    placet_cout << DEBUG << "enable_fpe() " << endmsg;
    
    fexcept_t pflag;
    //  int enable = FE_ALL_EXCEPT; // there is an error in tcl_init routine, so only enable 
    int enable = FE_INVALID|FE_DIVBYZERO|FE_OVERFLOW;
    
#ifdef __GLIBC__
    placet_cout << DEBUG << "GLIBC" << endmsg;
    feenableexcept (enable);
    //fedisableexcept (disable);
#else
    // The functions above are gnu-specific.
    // Without GNU, we do it the harder way.
    fenv_t newval;
    fegetenv(&newval);
    
    // Ignore MacOSX for now - this needs to be revisited!
  #ifndef __APPLE__
    placet_cout << DEBUG << "NO MACOS" << endmsg;
    newval.__control_word &= ~(FE_INVALID|FE_DIVBYZERO|FE_OVERFLOW);
  #else
    placet_cout << INFO << "FPESVC IS NOT WORKING FOR MACOS!" << endmsg;
    fegetexceptflag(&pflag,enable);
    fesetexceptflag(&pflag,enable);
    //	newval.__control_word &= ~(FE_INVALID|FE_DIVBYZERO|FE_OVERFLOW);
  #endif
    
  #ifdef __x86_64
    placet_cout << DEBUG << "FPESVC 64 bit" << endmsg;
    // SSE floating point uses separate exception masks.
    newval.__mxcsr &= (~(FE_INVALID|FE_DIVBYZERO|FE_OVERFLOW)) << 7;
  #endif
    fesetenv(&newval);
#endif
 
    fegetexceptflag(&pflag,enable);
    placet_cout << VERBOSE << "FPESVC::Enabled: " << mask_to_string (enable) << endmsg;
  }

  void disable_fpe(){
    fesetenv (&m_env);
    placet_cout << VERBOSE << "FPESVC::Disabled" << endmsg;
  }
};

#endif // PLACETFPESVC
