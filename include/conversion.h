#ifndef conversion_h
#define conversion_h

extern double deg2rad(double x);
extern double rad2deg(double x);
extern double freq2lambda(double x ); // GHz to meters
extern double lambda2freq(double x ); // meters to GHz
extern double x2ux(double x );  // X to microX (rad to microrad, m to microm)
extern double ux2x(double x );  // microX to X (microrad to rad, microm to m)

#endif /* conversion_h */
