#ifndef emitt_data_h
#define emitt_data_h

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

#include "element.h"

class BEAM;

// Accepted 'format' tokens
//
// %ex   emittance x [10^-7 m]
// %ey   emittance y [10^-7 m]
// %sex  sigma emittance x [10^-7 m]
// %sey  sigma emittance y [10^-7 m]
// %x    average x [um]
// %y    average y [um]
// %xp   average xp [urad]
// %yp   average yp [urad]
// %x2   average x*x [um^2]
// %y2   average y*y [um^2]
// %xp2  average xp*xp [urad^2]
// %yp2  average yp*yp [urad^2]
// %xxp  average x*xp [um*urad]
// %yyp  average y*yp [um*urad]
// %Env  envelope [um]
// %sx   sigma x [um]
// %sy   sigma y [um]
// %sz   sigma z [um]
// %sxp  sigma xp [urad]
// %syp  sigma yp [urad]
// %E    average energy [GeV]
// %dE   energy spread [GeV]
// %s    element s position [m]
// %n    number of machines [#]
// %name element name

struct EMITT_DATA_LINE {
  double ex, ey, ex2, ey2, x, y, xp, yp, Env,  sx, sy, sz, sxp, syp; 
  double x2, y2, xp2, yp2, xxp, yyp, energy, espread;
  int n; 
  double s;
  ELEMENT *element; // element these data are taken at
EMITT_DATA_LINE() : ex(0.0), ey(0.0), ex2(0.0), ey2(0.0), x(0.0), y(0.0), xp(0.0), yp(0.0), Env(0.0), sx(0.0), sy(0.0), sz(0.0), sxp(0.0), syp(0.0), x2(0.0), y2(0.0), xp2(0.0), yp2(0.0), xxp(0.0), yyp(0.0), energy(0.0), espread(0.0), n(0), s(0.0), element(NULL) {}
  EMITT_DATA_LINE(const BEAM *bunch, ELEMENT *_element ) : n(1), s(_element ? _element->get_s() : 0.0), element(_element) { init(bunch); }
  EMITT_DATA_LINE(const BEAM *bunch, double _s ) : n(1), s(_s), element(NULL) { init(bunch); }
  EMITT_DATA_LINE &operator += (const EMITT_DATA_LINE &b );
  void init(const BEAM *bunch );
  void print(std::ostream &stream, const char *format ) const;
  std::vector<double> get_values_as_vector(const char *format ) const;
  friend OStream &operator<<(OStream &stream, const EMITT_DATA_LINE &e )
  {
    return stream << e.ex << e.ey << e.ex2 << e.ey2 << e.x << e.y << e.xp << e.yp << e.Env << e.sx << e.sy << e.sz << e.sxp << e.syp << e.x2 << e.y2 << e.xp2 << e.yp2 << e.xxp << e.yyp << e.energy << e.espread << e.n << e.s;
  }
  friend IStream &operator>>(IStream &stream, EMITT_DATA_LINE &e )
  {
    return stream >> e.ex >> e.ey >> e.ex2 >> e.ey2 >> e.x >> e.y >> e.xp >> e.yp >> e.Env >> e.sx >> e.sy >> e.sz >> e.sxp >> e.syp >> e.x2 >> e.y2 >> e.xp2 >> e.yp2 >> e.xxp >> e.yyp >> e.energy >> e.espread >> e.n >> e.s;
  }
};

struct EMITT_VALUE {
  double value_x,value_y;
  friend OStream &operator<<(OStream &stream, const EMITT_VALUE &e ) { return stream << e.value_x << e.value_y; }
  friend IStream &operator>>(IStream &stream, EMITT_VALUE &e ) { return stream >> e.value_x >> e.value_y; }
};

class EMITT_DATA {
 private:
  std::vector<EMITT_VALUE> values;

 public:
  EMITT_DATA();

  std::vector<EMITT_DATA_LINE> data;
  double y_off,x_off;
  friend OStream &operator<<(OStream &stream, const EMITT_DATA &e ) { return stream << e.data << e.values << e.x_off << e.y_off; }
  friend IStream &operator>>(IStream &stream, EMITT_DATA &e ) { return stream >> e.data >> e.values >> e.x_off >> e.y_off; }

  void append(const BEAM *beam);
  void store_init(int n );
  void store(int i,const BEAM *bunch, double s );
  void store_el(int i,const BEAM *bunch,ELEMENT *el);
  void store_delete();
  void write_header(std::ostream &stream, const char *format );
  void print(const char *name, const char *format );
};

extern void switches_init();

extern EMITT_DATA emitt_data;

#endif /* emitt_data_h */
