#ifndef focus_h
#define focus_h

class FOCUS : public ELEMENT {

  double strength;

public:
  
  explicit FOCUS(double s ) : strength(s) {}

  void set_strength(double x) {strength=x;};

  void step_4d_0(BEAM*);
  void step_4d(BEAM*); 

  void list(FILE*)const;

};

int tk_Focus(ClientData clientdata,Tcl_Interp *interp,int argc,
	     char *argv[]);

#endif /* focus_h */
